<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Design extends CI_Controller {
	public function __construct(){
		@session_start();
       	parent::__construct();
       	if($this->session->userdata('logueado')){
	       	$this->load->model(array("user_model","paciente_model"));
					$this->datoSess["rol"] = $this->session->userdata('rol');
					$this->datoSess["id_default"]=1;
				switch ($this->session->userdata('rol')) {
	        	case 1:$campos="nombre,apellido";break;
	        	case 2:$campos="nombre,apellido";
						$this->datoSess["rol"] = $this->session->userdata('rol');
						$doctor=$this->user_model->getDoctorByUser($this->session->userdata('user'));
						$dato=$this->paciente_model->selectPrimerPaciente($doctor->id_doctor);
						$this->datoSess["id_default"] = (count($dato) > 0) ? $dato[0]->id_paciente : 1;
				break;
	        	case 3:$campos="nombre,apellido";break;
	        	case 4:$campos="nombre_clinica";break;
	        }
			$this->datoSess["user"] = $this->user_model->getDato($this->session->userdata('user'),$this->session->userdata('rol'),$campos);
			$this->datoSess["imagen"] = $this->user_model->getDato($this->session->userdata('user'),$this->session->userdata('rol'),"imagen");
		}else{
			header('Location: '.base_url());
		}
    }

	public function index( $mode = 0 ){
		$this->load->library('user_agent');
		$this->session->unset_userdata('tablet');

        if( $mode == 1 ){
            $this->session->set_userdata('tablet', 1);
        }
        else{
            $this->session->set_userdata('tablet', 0);
        }

		redirect($this->agent->referrer());
	}
}
