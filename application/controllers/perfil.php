<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Perfil extends CI_Controller {

	public function __construct(){
		@session_start();
       	parent::__construct();
       	if($this->session->userdata('logueado')){
	       	$this->load->model(array("user_model","paciente_model"));
	        $this->datoSess["rol"] = $this->session->userdata('rol');
			switch ($this->session->userdata('rol')) {
	        	case 1:
	        		$campos="nombre,apellido";
        		break;
	        	case 2:
	        		$campos="nombre,apellido";
					$this->datoSess["rol"] = $this->session->userdata('rol');
					$doctor=$this->user_model->getDoctorByUser($this->session->userdata('user'));
					$dato=$this->paciente_model->selectPrimerPaciente($doctor->id_doctor);
					$this->datoSess["id_default"] = (count($dato) > 0) ? $dato[0]->id_paciente : 1;
				break;
	        	case 3:$campos="nombre,apellido";break;
	        	case 4:$campos="nombre_clinica";break;
	        }
			$this->datoSess["user"] = $this->user_model->getDato($this->session->userdata('user'),$this->session->userdata('rol'),$campos);
			$this->datoSess["imagen"] = $this->user_model->getDato($this->session->userdata('user'),$this->session->userdata('rol'),"imagen");
		}else{
			header('Location: '.base_url());
		}
    }
    public function index(){
    	$this->load->view('admin/templateAdmin/header',$this->datoSess);
    	switch ($this->session->userdata('rol')){
    		case 1:
    			$data['usuario'] = $this->user_model->selectBy($this->session->userdata('user'));
					$this->load->view('admin/perfilAdmin',$data);
    		break;
	        case 2:
	        	$this->load->model("doctor_model");
						$this->load->model("profesion_model");
						$this->load->model("clinica_model");
						$this->load->model("paciente_model");
						$data['profesiones'] = $this->profesion_model->selectAll();
						$data['clinicas'] = $this->clinica_model->clinicas();
						$data['paises'] = $this->user_model->getPaises();
						$data['doctor'] = $this->doctor_model->selectByUser($this->session->userdata('user'));
						$data['provincia'] = $this->paciente_model->getUbigeo($data['doctor']->id_provincia,2);
						$data['localidad'] = $this->paciente_model->getUbigeo($data['doctor']->id_localidad,3);
						$this->load->view('admin/perfilDoctor',$data);
	        break;
	        case 3:
	        	$this->load->model("clinica_model");
						$this->load->model("paciente_model");
						$this->load->model("profesion_model");
						$this->load->model("enfermedades_model");
						$data['paises'] = $this->user_model->getPaises();
						$data['paciente'] = $this->paciente_model->selectByUser($this->session->userdata('user'));
						$data['clinicas'] = $this->clinica_model->clinicas();
						$data['profesiones'] = $this->profesion_model->selectAll();
						$data['enf_personales'] = $this->enfermedades_model->get(1);
						$data['enf_familiares'] = $this->enfermedades_model->get(2);
						$data['provincia'] = $this->paciente_model->getUbigeo($data['paciente']->id_provincia,2);
						$data['localidad'] = $this->paciente_model->getUbigeo($data['paciente']->id_localidad,3);
	        	$this->load->view('admin/perfilPaciente',$data);
	        break;
	        case 4:$campos="nombre_clinica";break;
    	}
			$this->load->view('admin/templateAdmin/footer');
    }

	public function addPerfil(){
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/addPerfil');
		$this->load->view('admin/templateAdmin/footer');
	}

	public function add(){
		$nombre = trim($_POST['nombre']);
		$this->load->model("perfil_model");
		$row = $this->perfil_model->insert($nombre);
		if($row){
			echo json_encode(array("status"=>true));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se pudo registrar"));
		}
	}

	public function editPerfil($id){
		$this->load->model("perfil_model");
		$data['perfil'] = $this->perfil_model->selectByID($id);
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/editPerfil',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function edit(){
		$id = trim($_POST['id']);
		$nombre = trim($_POST['nombre']);
		$this->load->model("perfil_model");
		$row = $this->perfil_model->edit($id,$nombre);
		if($row){
			echo json_encode(array("status"=>true));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se pudo actualizar"));
		}
	}

	public function delete(){
		$id = trim($_POST['id']);
		$this->load->model("perfil_model");
		$row = $this->perfil_model->delete($id);
		if($row){
			echo json_encode(array("status"=>true));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se pudo eliminar"));
		}
	}

	public function trae_ejercicio(){

		switch ($_POST["id_ejer"]) {
			case 1:
				$this->load->model("perfil_model");
				$data["escenarios"]=$this->perfil_model->escenarios_ejercicios(1);
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/respiracion",$data,true)));
			break;
			case 3:
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/bote","",true)));
			break;
			case 7:

				$this->load->model("tipo_movimiento_model");
				$data["tipomovimiento"]= $this->tipo_movimiento_model->selectAll();
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/puente",$data,true)));
			break;
			case 8:
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/cuello","",true)));
			break;
			case 9:
				$this->load->model("perfil_model");
				$data["escenarios"]=$this->perfil_model->escenarios_ejercicios(9);
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/estres_ansiedad",$data,true)));
			break;
			case 10:
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/tamo","",true)));
			break;

		}
	}

	public function trae_ejercicio_editar(){
		$this->load->model("paciente_model");
		$data=$this->paciente_model->get_ejercicio($_POST["tipo"],$_POST["id_ejercicio"]);
		switch ($_POST["tipo"]) {
			case 1:
				$this->load->model("perfil_model");
				$data->escenarios_ini=$this->perfil_model->escenarios_ejercicios(1);
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/respiracion_editar",$data,true)));
			break;
			case 3:
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/bote_editar",$data,true)));
			break;
			case 7:

				$this->load->model("tipo_movimiento_model");
				$data->tipomovimientos = $this->tipo_movimiento_model->selectAll();
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/puente_editar",$data,true)));
			break;
			case 8:
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/cuello_editar",$data,true)));
			break;
			case 9:
				$this->load->model("perfil_model");
				$data->escenarios_ini=$this->perfil_model->escenarios_ejercicios(9);
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/estres_ansiedad_editar",$data,true)));
			break;
			case 10:
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/tamo_editar",$data,true)));
			break;
		}
	}

	public function act_edt_ejercicio(){

		$this->load->model("perfil_model");
		$postFecha=str_replace("+"," ",$_POST["fecha"]);
		list($fecha,$cita,$tiempo)=explode(" ",$postFecha);
		list($dia,$mes,$anio)=explode("-",$fecha);
		list($hora,$minuto)=explode(":",$cita);
		if ($tiempo=="PM" && $hora<12){$hora=$hora+12;}
		$fechaFinal=$anio."-".$mes."-".$dia." ".$hora.":".$minuto.":00";
		$datos = array(
			'fecha' => $fechaFinal,
			'id_paciente' => $_POST["id_paciente"]
		);
		switch ($_POST["id_ejercicio"]) {
			case '1':
				$datos['repeticiones'] = $_POST["repeticiones"];
				$datos['inhalacion'] = $_POST["inhalacion"];
				$datos['mantener'] =$_POST["mantener"];
				$datos['exhalacion'] = $_POST["exhalacion"];
				$datos['descanso'] = $_POST["descanso"];
				if (isset($_POST["tutorial"])) {
					$datos["tutorial"]=$_POST["tutorial"];
				}else{
					$datos["tutorial"]="";
				}
				if (isset($_POST["perrito"])) {
					$datos["perrito"]=$_POST["perrito"];
				}else{
					$datos["perrito"]="";
				}
				if (isset($_POST["echado"])) {
					$datos["echado"]=1;

					if( isset( $_POST["tip"] ) && $_POST["tip"] == 'video' ){
						$datos["tipo"] = $_POST["tip"];
						$datos['id_escenario'] = $_POST["escenario"];
					}
					else{
						$datos["tipo"] = $_POST["tip"];
						$datos['id_escenario'] = NULL;
					}

                    if( isset( $_POST["respiracion"] ) ){
                        $datos["respiracion"] = 1;
                    }
                    else{
                        $datos["respiracion"] = 0;
                    }

					$datos["ciclo"]=0;

				}else{
					$datos["echado"]=0;
					$datos["tipo"] = NULL;
					$datos['id_escenario'] = $_POST["escenario"];
                    $datos["respiracion"] = 0;
					if (isset($_POST["ciclo"])) {
						$datos["ciclo"]=1;
					}else{
						$datos["ciclo"]=0;
					}
				}

			break;
			case '2':
				$datos['id_escenario'] = $_POST["escenario"];
				$datos['momento'] = $_POST["momento"];
				$datos['tamanio'] = $_POST["tamanio"];
				$datos['cantidad'] = $_POST["cantidad"];
				$datos['duracion'] = $_POST["duracion"];
			break;
			case '3':
				$datos['dificultad'] = $_POST["dificultad"];
				$datos['circuito'] = $_POST["circuito"];
				$datos['direccion'] = $_POST["direccion"];
				$datos['monedas'] = $_POST["monedas"];
				$datos['tutorial'] = ( isset( $_POST["tutorial"] ) && $_POST["tutorial"] == 'on' ) ? 1 : 0;
			break;
			case '4':
				$datos['id_escenario'] = $_POST["escenario"];
				$datos['momento'] = $_POST["momento"];
				$datos['grado'] = $_POST["grado"];
				$datos['nivel'] = $_POST["nivel"];
				$datos['duracion'] = $_POST["duracion"];
			break;
			case '5':
				$datos['altura'] = $_POST["altura"];
				$datos['velocidad'] = $_POST["velocidad"];
				$datos['espera'] = $_POST["espera"];
				$datos['tipo_guia'] = $_POST["tipo_guia"];
				$datos['pez_rojo'] = $_POST["pez_rojo"];
				$datos['distancia'] = $_POST["distancia"];
				$datos['duracion'] = $_POST["duracion"];
				if (isset($_POST["guiado"])) {
					$datos["guiado"]=$_POST["guiado"];
				}
			break;
			case '6':
				$datos['velocidad'] = $_POST["velocidad"];
				$datos['posicion'] = $_POST["posicion"];
				$datos['tipo_ejercicio'] = $_POST["tipo_ejercicio"];
				$datos['tiempo_cantidad'] = $_POST["tiempo_cantidad"];
			break;
			case '7':

				$datos['espera'] = (int) $_POST["espera"];
				$datos['tipomovimiento'] = (int) $_POST["tipomovimiento"];
				$datos['altura'] = trim( $_POST["altura"] );
				$datos['repeticiones'] = (int) $_POST["repeticiones"];

				$datos['tutorial'] = ( isset( $_POST["tutorial"] ) && $_POST["tutorial"] == 'on' ) ? 1 : 0;

			break;

			case '8':
				$datos['tipo'] = $_POST["tipo_ejercicio"];
				$datos['grados'] = $_POST["grados"];
				$datos['tiempo_ejercicio'] = $_POST["tiempo_ejercicio"];
				if (isset($_POST["tutorial"])) {
					$datos["tutorial"]=$_POST["tutorial"];
				}else{
					$datos["tutorial"]="";
				}
				if (isset($_POST["perrito"])) {
					$datos["perrito"]=$_POST["perrito"];
				}else{
					$datos["perrito"]="";
				}
				if (isset($_POST["editar"])) {//se edita
					if (isset($_POST["array_movimientos_edit"])) {
						$datos['repeticiones'] = "[{".str_replace(";;","},{",substr($_POST["array_movimientos_edit"],0,-2))."}]";
					}else{
						$datos['repeticiones'] = $_POST["repeticiones"];
					}
				}else{
					if (isset($_POST["array_movimientos"])) {
						$datos['repeticiones'] = "[{".str_replace(";;","},{",substr($_POST["array_movimientos"],0,-2))."}]";
					}else{
						$datos['repeticiones'] = $_POST["repeticiones"];
					}
				}
			break;
			case '9':

				//echo json_encode( $_POST );
				$datos['respiracion'] = ( isset( $_POST["respiracion"] ) && $_POST["respiracion"] == 'on' ) ? 1 : 0;
				$datos['tipo'] = $_POST["tipo"];

				if( $datos['respiracion'] == 1 ){

					$datos['repeticiones'] = $_POST["repeticiones"];
					$datos['inhalacion'] = $_POST["inhalacion"];
					$datos['sostenimiento'] = $_POST["sostenimiento"];
					$datos['exhalacion'] = $_POST["exhalacion"];
					$datos['descanso'] = $_POST["descanso"];
				}
				else{

					$datos['repeticiones'] = 0;
					$datos['inhalacion'] = 0;
					$datos['sostenimiento'] = 0;
					$datos['exhalacion'] = 0;
					$datos['descanso'] = 0;
				}

				if (isset($_POST["editar"])) {//se edita
					if (isset($_POST["array_escenarios_edit"])) {
						$datos['escenarios'] = "[{".str_replace(";;","},{",substr($_POST["array_escenarios_edit"],0,-2))."}]";
					}else{
						$datos['escenarios'] = $_POST["escenarios"];
					}


				}else{

					if (isset($_POST["array_escenarios"])) {
						$datos['escenarios'] = "[{".str_replace(";;","},{",substr($_POST["array_escenarios"],0,-2))."}]";
					}else{
						$datos['escenarios'] = '[]';
					}
				}

				$datos["voz_activo"]= ( isset($_POST["voz_vivo"]) && $_POST["voz_vivo"] == 'on' ) ? 1 : 0;
				/**/
			break;
			case '10':

				$datos['repeticiones'] = $_POST["repeticiones"];
				$datos['inhalacion'] = $_POST["inhalacion"];
				$datos['mantener'] =$_POST["mantener"];
				$datos['exhalacion'] = $_POST["exhalacion"];
				$datos['descanso'] = $_POST["descanso"];

				if (isset($_POST["perrito"])) {
					$datos["perrito"]=$_POST["perrito"];
				}else{
					$datos["perrito"]="";
				}

			break;
		}

		if (isset($_POST["editar"])) {//se edita

			$this->perfil_model->editaEjercicio($_POST["id_ejercicio"],$_POST["id_sesion"],$datos);
			echo json_encode(array("msg"=> "Se editaron los datos"));

		}else{//se agrega
			$this->perfil_model->insertEjercicio($_POST["id_ejercicio"],$datos);
			echo json_encode(array("msg"=> "Se registraron los datos"));
		}
		/**/
	}

	public function delEjercicio(){
		$this->load->model("perfil_model");
		$this->perfil_model->delEjercicio($_POST["id_sesion"],$_POST["tipo"]);
		echo json_encode(array("msg"=> "Se elimino la sesion"));
	}

	public function trae_estadisticas(){
		$this->load->model("perfil_model");
		list($dia1,$mes1,$anio1)=explode("-",$_POST["desde"]);
		list($dia2,$mes2,$anio2)=explode("-",$_POST["hasta"]);
		$datos = array(
			'id_paciente' => $_POST["id_paciente"],
			'id_ejer' => $_POST["id_ejer"],
			'desde' => $anio1."-".$mes1."-".$dia1,
			'hasta' => $anio2."-".$mes2."-".$dia2
		);
		$res=$this->perfil_model->trae_ejercicios_terminados($datos);
		$res2 = $this->perfil_model->trae_ejercicios_all_dosificados($datos);
		$res3 = $this->perfil_model->trae_data_del_anio($datos);

		$data['data'] = $res;
		$data['data_all'] = $res2;
		$data['data_anio_meses'] = $res3;


		switch ($_POST["id_ejer"]) {
			case 1:
				$data['pagina'] = $this->load->view("ejercicios/respiracion_estadisticas","",true);
				echo json_encode( $data );
			break;
			case 3:
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/bote_estadisticas",$res,true)));
			break;
			case 7:
				$data['pagina'] = $this->load->view("ejercicios/puente_estadisticas","",true);
				echo json_encode( $data );
			break;
			case 8:
				$data['pagina'] = $this->load->view("ejercicios/cuello_estadisticas","",true);
				echo json_encode( $data );
			break;
			case 9:
				echo json_encode(array("contenido"=>$this->load->view("ejercicios/estres_ansiedad_estadisticas",$res,true)));
			break;
			case 10:
				$data['pagina'] = $this->load->view("ejercicios/tamo_estadisticas","",true);
				echo json_encode( $data );
			break;

		}
	}

	public function trae_detalle_ejercicio(){

		$this->load->model("perfil_model");
		$res = $this->perfil_model->trae_detalle_ejercicio($_POST["id_ejercicio"],$_POST["tipo"]);

		echo json_encode($res);
	}
}
