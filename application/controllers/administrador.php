<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Administrador extends CI_Controller {
	public function __construct(){
		@session_start();
       	parent::__construct();
       	if($this->session->userdata('logueado')){
	       	$this->load->model(array("user_model","paciente_model"));
			$this->datoSess["rol"] = $this->session->userdata('rol');
			$this->datoSess["id_default"]=1;
			$this->load->model("paciente_model");

	        switch ($this->session->userdata('rol')) {
	        	case 1:
	        		$campos="nombre,apellido";
	        		$this->datoSess["id_default"]=1;
	        	break;
				case 2:
					$campos="nombre,apellido";
					$this->datoSess["rol"] = $this->session->userdata('rol');
					$doctor=$this->user_model->getDoctorByUser($this->session->userdata('user'));
					$dato=$this->paciente_model->selectPrimerPaciente($doctor->id_doctor);
					$this->datoSess["id_default"] = (count($dato) > 0) ? $dato[0]->id_paciente : 1;
				break;
	        	case 3:$campos="nombre,apellido";break;
	        	case 4:$campos="nombre_clinica";break;
	        }
			$this->datoSess["user"] = $this->user_model->getDato($this->session->userdata('user'),$this->session->userdata('rol'),$campos);
			$this->datoSess["imagen"] = $this->user_model->getDato($this->session->userdata('user'),$this->session->userdata('rol'),"imagen");
		}else{
			header('Location: '.base_url());
		}
  }

	public function index(){
		$data['usuarios'] = $this->user_model->selectByType(1);
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/homeAdmin',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function addAdmin(){
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/usuariosAdd');
		$this->load->view('admin/templateAdmin/footer');
	}

	public function editUsuario($id){
		$data['usuario'] = $this->user_model->selectBy($id);
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/usuariosEdit',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function delete(){
		$id = trim($_POST['id']);
		$row = $this->user_model->delete($id);
		if($row){
			echo json_encode(array("status"=>true, "msg"=>"Se elimino el registro"));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se pudo eliminar el registro"));
		}
	}

	public function update(){
		$id_user = trim($_POST['id_user']);
		$usuario = trim($_POST['usuario']);
		$pass = trim($_POST['pass']);
		$row = $this->user_model->updateUser($id_user,$usuario,$pass);
		$datos= array(
        	"nombre" => trim($_POST['nombre']),
            "apellido" => trim($_POST['apellido'])
        );
		$id_administrador = trim($_POST['id_administrador']);
		$row2 = $this->user_model->updateAdmin($id_administrador,$datos);
		if($row || $row2){
			echo json_encode(array("status"=>true,"msg"=>"Se actualizo con exito"));
		}else{
			echo json_encode(array("status"=>false,"msg"=>"No se realizo ningun cambio"));
		}
	}

	public function add(){
		$usuario = trim($_POST['usuario']);
		$pass = trim($_POST['pass']);
		$row = $this->user_model->insertUser($usuario,$pass,1);
		$datos= array(
			"fecha" => date("Y-m-d H:i:s"),
        	"nombre" => trim($_POST['nombre']),
            "apellido" => trim($_POST['apellido']),
            "id_user" => $row
        );
		$row2 = $this->user_model->insertAdmin($datos);
		if($row2){
			echo json_encode(array("status"=>true, "msg"=>"Se registro al administrador"));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se pudo registrar"));
		}
	}

	public function perfiles(){
		$this->load->model("perfil_model");
		$data['perfiles'] = $this->perfil_model->selectAll();
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/perfiles',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function clinicas(){
		$this->load->model("clinica_model");
		$data['clinicas'] = $this->clinica_model->selectAll();
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/clinicas',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function addClinica(){
		$data['paises'] = $this->user_model->getPaises();
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/clinicasAdd',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function editClinica($id){
		$this->load->model("clinica_model");
		$this->load->model("paciente_model");
		$data['clinica'] = $this->clinica_model->selectId($id);
		$data['paises'] = $this->user_model->getPaises();
		$data['provincia'] = $this->paciente_model->getUbigeo($data['clinica']->id_provincia,2);
		$data['localidad'] = $this->paciente_model->getUbigeo($data['clinica']->id_localidad,3);
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/clinicasEdit',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function deleteClinica(){
		$id = trim($_POST['id']);
		$this->load->model("clinica_model");
		$row = $this->clinica_model->delete($id);
		if($row){
			echo json_encode(array("status"=>true, "msg"=>"Se elimino el registro"));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se pudo eliminar el registro"));
		}
	}

	public function addClinicaAction(){
		$usuario = trim($_POST['usuario']);
		$pass = trim($_POST['pass']);
		$row = $this->user_model->insertUser($usuario,$pass,4);
		$datos = array(
			'fecha' => date("Y-m-d H:i:s"),
			'ruc' => trim($_POST['ruc']),
			'nombre_clinica' => trim($_POST['nombre']),
			'id_pais' =>trim($_POST['paisSolicitud']),
			'id_provincia' =>trim($_POST['provinciaSolicitud']),
			'id_localidad' =>trim($_POST['localidadSolicitud']),
			"id_user" => $row
		);
		$this->user_model->insertClinica($datos);
		if($row){
			echo json_encode(array("status"=>true, "msg"=>"Se registro la clinica"));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se pudo registrar"));
		}
	}

	public function editClinicaAction(){
		$id = trim($_POST['id']);
		$usuario = trim($_POST['usuario']);
		$pass = trim($_POST['pass']);
		$row = $this->user_model->updateUser($id,$usuario,$pass);
		$datos = array(
			'ruc' => trim($_POST['ruc']),
			'nombre_clinica' => trim($_POST['nombre']),
			'id_pais' =>trim($_POST['paisSolicitud']),
			'id_provincia' =>trim($_POST['provinciaSolicitud']),
			'id_localidad' =>trim($_POST['localidadSolicitud'])
		);
		$row2 = $this->user_model->editClinica($datos,trim($_POST['clinica']));
		if($row || $row2){
			echo json_encode(array("status"=>true, "msg"=>"Se edito la clinica"));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se realizo ningun cambio"));
		}
	}

	public function doctores(){
		$this->load->model("doctor_model");
		$data['doctores'] = $this->doctor_model->selectAll();
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/doctores',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function addDoctor(){
		$this->load->model(array("profesion_model", "clinica_model", "ejercicio_model"));
		$data['profesiones'] = $this->profesion_model->getByModulo(1);
		$data['clinicas'] = $this->clinica_model->clinicas();
		$data['paises'] = $this->user_model->getPaises();
		$data['ejercicios'] = $this->ejercicio_model->get();
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/doctoresAdd',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function editDoctor($id){
		$this->load->model(array("doctor_model","paciente_model","profesion_model", "clinica_model", "ejercicio_model"));
		$data['profesiones'] = $this->profesion_model->getByModulo(1);
		$data['clinicas'] = $this->clinica_model->clinicas();
		$data['paises'] = $this->user_model->getPaises();
		$data['ejercicios'] = $this->ejercicio_model->get();

		$data['doctor'] = $this->doctor_model->selectId($id);
		$data['provincia'] = $this->paciente_model->getUbigeo($data['doctor']->id_provincia,2);
		$data['localidad'] = $this->paciente_model->getUbigeo($data['doctor']->id_localidad,3);
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/doctoresEdit',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function deleteDoctor(){
		$id = trim($_POST['id']);
		$this->load->model("doctor_model");
		$row = $this->doctor_model->delete($id);
		if($row){
			echo json_encode(array("status"=>true, "msg"=>"Se elimino el registro"));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se pudo eliminar"));
		}
	}

	public function addDoctorAction(){
		$this->load->model("solicitud_model");
		$this->load->model("clinica_model");
		if (trim($_POST['nacimientoSolicitud'])) {
			list($dia,$mes,$anio)=explode("-",trim($_POST['nacimientoSolicitud']));	
		}else{
			list($dia,$mes,$anio)=explode("-",trim("00-00-0000"));
		}
		$cli = explode(",",trim($_POST['clinicas']));
		$regisCli = array();
		$est = true;
		$n = "";
		$clinicaslistado = "";
		foreach ($cli as $key) {
			if(!is_numeric($key)){
				$key = str_replace("nuevo-", "", $key);
				array_push($regisCli,$key);
				if($this->solicitud_model->verificarClinica(strtolower($key))){
					$est = false;
					$n = $key;
				}
			}else{
				$clinicaslistado = $clinicaslistado.$key.",";
			}
		}
		if($est){
			foreach ($regisCli as $key) {
				$row = $this->user_model->insertUser("clinica@manual.com","123456",4);
				$newID=$this->clinica_model->insert($key,date("Y-m-d H:i:s"),$row);
				$clinicaslistado = $clinicaslistado.$newID.",";
			}
			$t = ($clinicaslistado=="") ? $clinicaslistado : substr($clinicaslistado, 0, -1);
			$usuario = trim($_POST['usuario']);
			$pass = trim($_POST['pass']);
			$row = $this->user_model->insertUser($usuario,$pass,2);
			$datos = array(
				'fecha' => date("Y-m-d H:i:s"),
				'imagen' =>trim($_POST['renombrado']),
				'dni' => trim($_POST['dni']),
				'nombre' => trim($_POST['nombre']),
				'apellido' => trim($_POST['apellido']),
				'sexo' =>trim($_POST['sexoSolicitud']),
				'edad' =>trim($_POST['edadSolicitud']),
				'nacimiento' => $anio."-".$mes."-".$dia,
				'estado_civil' =>trim($_POST['estadoCivilSolicitud']),
				'profesion' =>trim($_POST['profesionSolicitud']),
				'clinicas' =>$t,
				'ejercicios' => trim($_POST['ejercicios']),
				'id_user' => $row,
				'id_pais' =>trim($_POST['paisSolicitud']),
				'id_provincia' =>trim($_POST['provinciaSolicitud']),
				'id_localidad' =>trim($_POST['localidadSolicitud']),
				'cmp' =>trim($_POST['cmp']),
				'rne' =>trim($_POST['rne'])
			);
			$this->user_model->insertDoctor($datos);
			if($row){
				echo json_encode(array("status"=>true, "msg"=>"Se registro al doctor"));
			}else{
				echo json_encode(array("status"=>false, "msg"=>"No se pudo registrar"));
			}
		}else{
			echo json_encode(array('status'=>false,'msg'=>"La clinica ".$n." ya ha sido registrada."));
		}
	}

	public function editDoctorAction(){
		$this->load->model("solicitud_model");
		$this->load->model("clinica_model");
		$cli = explode(",",trim($_POST['clinicas']));
		$regisCli = array();
		$est = true;
		$n = "";
		$clinicaslistado = "";
		foreach ($cli as $key) {
			if(!is_numeric($key)){
				$key = str_replace("nuevo-", "", $key);
				array_push($regisCli,$key);
				if($this->solicitud_model->verificarClinica(strtolower($key))){
					$est = false;
					$n = $key;
				}
			}else{
				$clinicaslistado = $clinicaslistado.$key.",";
			}
		}
		if($est){
			foreach ($regisCli as $key) {
				$row = $this->user_model->insertUser("clinica@manual.com","123456",4);
				$newID=$this->clinica_model->insert($key,date("Y-m-d H:i:s"),$row);
				$clinicaslistado = $clinicaslistado.$newID.",";
			}
			$t = ($clinicaslistado=="") ? $clinicaslistado : substr($clinicaslistado, 0, -1);
			if (trim($_POST['nacimientoSolicitud'])) {
				list($dia,$mes,$anio)=explode("-",trim($_POST['nacimientoSolicitud']));	
			}else{
				list($dia,$mes,$anio)=explode("-",trim("00-00-0000"));
			}
			$datos = array(
				'dni' => trim($_POST['dni']),
				'nombre' => trim($_POST['nombre']),
				'apellido' => trim($_POST['apellido']),
				'sexo' =>trim($_POST['sexoSolicitud']),
				'edad' =>trim($_POST['edadSolicitud']),
				'nacimiento' => $anio."-".$mes."-".$dia,
				'estado_civil' =>trim($_POST['estadoCivilSolicitud']),
				'profesion' =>trim($_POST['profesionSolicitud']),
				'clinicas' =>$t,
				'ejercicios' => trim($_POST['ejercicios']),
				'id_user' => trim($_POST['id']),
				'id_pais' =>trim($_POST['paisSolicitud']),
				'id_provincia' =>trim($_POST['provinciaSolicitud']),
				'id_localidad' =>trim($_POST['localidadSolicitud']),
				'cmp' =>trim($_POST['cmp']),
				'rne' =>trim($_POST['rne'])
			);
			if (trim($_POST['renombrado'])!="") {
				$uploaddir = 'public/images/profile/';
				$dr = $uploaddir.$_POST["oldImage"];

				if( trim($_POST["oldImage"]) != "" ) {
                    if (file_exists($dr)) {
                        unlink($dr);
                    } else {
                    }
                } else{}
				$datos['imagen']=trim($_POST['renombrado']);
			}
			$usuario = trim($_POST['usuario']);
			$pass = trim($_POST['pass']);
			$row = $this->user_model->updateUser(trim($_POST['id']),$usuario,$pass);
			$row2 = $this->user_model->editDoctor($datos,trim($_POST['doctor']));
			if($row || $row2){
				echo json_encode(array("status"=>true, "msg"=>"Se edito el registro"));
			}else{
				echo json_encode(array("status"=>false, "msg"=>"No se realizo ningun cambio"));
			}
		}else{
			echo json_encode(array('status'=>false,'msg'=>"La clinica ".$n." ya ha sido registrada."));
		}
	}

	public function pacientes(){
		$this->load->model("paciente_model");
		if($this->session->userdata('rol')=="1"){
			$data['pacientes'] = $this->paciente_model->selectAll();
		}else{
			$doctor=$this->user_model->getDoctorByUser($this->session->userdata('user'));
			$data['pacientes'] = $this->paciente_model->selectByDoctor($doctor->id_doctor);
		}

		if( $this->session->userdata('rol')=="1" ){

			$this->load->view('admin/templateAdmin/header',$this->datoSess);
			$this->load->view('admin/pacientes',$data);
			$this->load->view('admin/templateAdmin/footer');
		}
		else if( $this->session->userdata('rol')=="2" ){
			$this->load->view('admin/templateAdmin/header',$this->datoSess);
			$this->load->view('doctor/pacientes',$data);
			$this->load->view('admin/templateAdmin/footer');
		}
		else{
			show_404();
		}
	}

	public function addPaciente(){
		$this->load->model("clinica_model");
		$this->load->model("profesion_model");
		$this->load->model("enfermedades_model");
		$data['paises'] = $this->user_model->getPaises();
		$data['clinicas'] = $this->clinica_model->clinicas();
		$data['profesiones'] = $this->profesion_model->selectAll();
		$data['enf_personales'] = $this->enfermedades_model->get(1);
		$data['enf_familiares'] = $this->enfermedades_model->get(2);

		if( $this->session->userdata('rol') == 2 ){

			$this->load->model('doctor_model');
			$dat = $this->doctor_model->selectByUser( $this->session->userdata('user') );
			$clin = explode(",",$dat->clinicas);
			$clinicas = array();
			$a = 0;
			foreach ($clin as $row ) {
				
				$bus = $this->clinica_model->selectById( $row );

				if( $bus ){
					$clinicas[$a]["id_clinica"] = $bus->clinica_id;
					$clinicas[$a]["nombre_clinica"] = $bus->nombre_clinica;
					$a++;
				}
			}
			$data['clinicas_doctor'] = $clinicas;

			$this->load->view('admin/templateAdmin/header',$this->datoSess);
			$this->load->view('doctor/pacientesAdd',$data);
			$this->load->view('admin/templateAdmin/footer');
		}
		else{
			$this->load->view('admin/templateAdmin/header',$this->datoSess);
			$this->load->view('admin/pacientesAdd',$data);
			$this->load->view('admin/templateAdmin/footer');
		}
		
	}

	public function editPaciente($id){
		$this->load->model("clinica_model");
		$this->load->model("paciente_model");
		$this->load->model("profesion_model");
		$this->load->model("enfermedades_model");
		$data['paises'] = $this->user_model->getPaises();
		$data['paciente'] = $this->paciente_model->selectId($id);
		$data['clinicas'] = $this->clinica_model->clinicas();
		$data['profesiones'] = $this->profesion_model->selectAll();
		$data['enf_personales'] = $this->enfermedades_model->get(1);
		$data['enf_familiares'] = $this->enfermedades_model->get(2);
		$data['provincia'] = $this->paciente_model->getUbigeo($data['paciente']->id_provincia,2);
		$data['localidad'] = $this->paciente_model->getUbigeo($data['paciente']->id_localidad,3);
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/pacientesEdit',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function deletePaciente(){
		$id = trim($_POST['id']);
		$this->load->model("doctor_model");
		$row = $this->doctor_model->delete($id);
		if($row){
			echo json_encode(array("status"=>true, "msg"=>"Se eliminó el registro"));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se pudo eliminar"));
		}
	}

	public function deletePacienteFast(){
		$id = trim($_POST['id']);
		$this->load->model("paciente_model");
		$row = $this->paciente_model->delete($id);
		if($row > 0){
			echo json_encode(array("status"=>true, "msg"=>"Se eliminó el registro"));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se pudo eliminar"));
		}
	}

	public function addPacienteAction(){
		$personales="";
		if(!empty($_POST['personales'])){
			foreach($_POST['personales'] as $key){
				$personales.=$key.",";
		    }
		}
		$familiares="";
		if(!empty($_POST['familiares'])){
			foreach($_POST['familiares'] as $key){
				$familiares.=$key.",";
		    }
		}
		if (trim($_POST['nacimientoSolicitud'])) {
			list($dia,$mes,$anio)=explode("-",trim($_POST['nacimientoSolicitud']));	
		}else{
			list($dia,$mes,$anio)=explode("-",trim("00-00-0000"));
		}
		$usuario = trim($_POST['userSolicitud']);
		$pass = trim($_POST['passSolicitud']);
		$row = $this->user_model->insertUser($usuario,$pass,3);
		$datos = array(
			'fecha' =>trim(date("Y-m-d H:i:s")),
			'imagen' =>trim($_POST['renombrado']),
			'dni' =>trim($_POST['dniSolicitud']),
			'nombre' => trim($_POST['nombreSolicitud']),
			'apellido' => trim($_POST['apellidosSolicitud']),
			'sexo' =>trim($_POST['sexoSolicitud']),
			'edad' =>trim($_POST['edadSolicitud']),
			'fono' =>trim($_POST['fono']),
			//'control' =>trim($_POST['controlSolicitud']),
			'nacimiento' => $anio."-".$mes."-".$dia,
			'estado_civil' =>trim($_POST['estadoCivilSolicitud']),
			'grupo_sanguineo' =>trim($_POST['grupoSolicitud']),
			'dni_acom' =>trim($_POST['dniAcomSolicitud']),
			'nombres_acom' =>trim($_POST['nombreAcomSolicitud']),
			'apellidos_acom' =>trim($_POST['apellidosAcomSolicitud']),
			'edad_acom' =>trim($_POST['edadAcomSolicitud']),
			'fono_acom' =>trim($_POST['fonoAcom']),
			//'profesion_acom' =>trim($_POST['profesionAcomSolicitud']),
			'id_user' => $row,
			'profesion' =>trim($_POST['profesionSolicitud']),
			'id_clinica' =>trim($_POST['clinica']),
			'id_pais' =>trim($_POST['paisSolicitud']),
			'id_provincia' =>trim($_POST['provinciaSolicitud']),
			'id_localidad' =>trim($_POST['localidadSolicitud']),
			'enf_personales' =>substr($personales,0,-1),
			'enf_familiares' =>substr($familiares,0,-1),
			'otros_personales' =>trim($_POST['otros_personales']),
			'otros_familiares' =>trim($_POST['otros_familiares']),
			'observaciones' =>trim($_POST['observSolicitud'])
		);
		if ($this->session->userdata('rol')==2){
			$this->load->model("doctor_model");
			$doctor=$this->doctor_model->selectByUser($this->session->userdata('user'));
			$datos["id_doctor"]=$doctor->id_doctor;
		}
		$this->user_model->insertPaciente($datos);
		if($row){
			echo json_encode(array("status"=>true, "msg"=>"Se registro al paciente"));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se pudo registrar"));
		}
	}

	public function addPacienteFastAction(){
		
		$datos = array(
			'fecha' =>trim(date("Y-m-d H:i:s")),
			'dni' =>trim($_POST['dni']),
			'nombre' => trim($_POST['nombres']),
			'apellido' => trim($_POST['apellidos']),
			'sexo' =>trim($_POST['sexo']),
			'edad' =>trim($_POST['edad']),
			'fono' => trim( $_POST['telefono'] ),
			'id_clinica' =>trim($_POST['clinica'])
		);
		if ($this->session->userdata('rol')==2){
			$this->load->model("doctor_model");
			$doctor=$this->doctor_model->selectByUser($this->session->userdata('user'));
			$datos["id_doctor"]=$doctor->id_doctor;
		}
		$row = $this->user_model->insertPaciente($datos);
		if($row){
			echo json_encode(array("status"=>true, "msg"=>"Se registro al paciente"));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se pudo registrar"));
		}
	}

	public function modificarPacienteAction(){
		$personales="";
		if(!empty($_POST['personales'])){
			foreach($_POST['personales'] as $key){
				$personales.=$key.",";
		    }
		}
		$familiares="";
		if(!empty($_POST['familiares'])){
			foreach($_POST['familiares'] as $key){
				$familiares.=$key.",";
		    }
		}
		$id = trim($_POST['paciente']);
		list($dia,$mes,$anio)=explode("-",trim($_POST['nacimientoSolicitud']));
		$iduser = trim($_POST['id']);
		$usuario = trim($_POST['userSolicitud']);		
		$pass = trim($_POST['passSolicitud']);
		
		if( $iduser == "" ){
			$row1 = $this->user_model->insertUser($usuario,$pass,3);
		}
		else{
			$row1 = $this->user_model->updateUser($iduser,$usuario,$pass);
		}
		
		$datos = array(
			'dni' =>trim($_POST['dniSolicitud']),
			'nombre' => trim($_POST['nombreSolicitud']),
			'apellido' => trim($_POST['apellidosSolicitud']),
			'sexo' =>trim($_POST['sexoSolicitud']),
			'edad' =>trim($_POST['edadSolicitud']),
			'fono' =>trim($_POST['fonoSolicitud']),
			//'control' =>trim($_POST['controlSolicitud']),
			'nacimiento' => $anio."-".$mes."-".$dia,
			'estado_civil' =>trim($_POST['estadoCivilSolicitud']),
			'grupo_sanguineo' =>trim($_POST['grupoSolicitud']),
			'dni_acom' =>trim($_POST['dniAcomSolicitud']),
			'nombres_acom' =>trim($_POST['nombreAcomSolicitud']),
			'apellidos_acom' =>trim($_POST['apellidosAcomSolicitud']),
			'edad_acom' =>trim($_POST['edadAcomSolicitud']),
			'fono_acom' => (int) trim($_POST['fonoAcomSolicitud']),
			//'profesion_acom' =>trim($_POST['profesionAcomSolicitud']),
			'profesion' =>trim($_POST['profesionSolicitud']),
			'id_clinica' =>trim($_POST['clinica']),
			'id_user' => ( $iduser == "" ) ? $row1 : $iduser,
			'id_pais' =>trim($_POST['paisSolicitud']),
			'id_provincia' =>trim($_POST['provinciaSolicitud']),
			'id_localidad' =>trim($_POST['localidadSolicitud']),
			'enf_personales' =>substr($personales,0,-1),
			'enf_familiares' =>substr($familiares,0,-1),
			'otros_personales' =>trim($_POST['otros_personales']),
			'otros_familiares' =>trim($_POST['otros_familiares']),
			//'reporte' =>trim($_POST['reporteSolicitud']),
			'observaciones' =>trim($_POST['observSolicitud'])
		);
		if (trim($_POST['renombrado'])!="") {
			$uploaddir = 'public/images/profile/';
			@unlink($uploaddir.$_POST["oldImage"]);
			$datos['imagen']=trim($_POST['renombrado']);
		}
		$row2 = $this->user_model->modificarPaciente($datos,$id);
		if($row1 || $row2){

			if( $iduser == "" ){
				echo json_encode(array("status"=>true, "msg"=>"Se modifico de forma correcta", "id_user" => $row1));
			}
			else{
				echo json_encode(array("status"=>true, "msg"=>"Se modifico de forma correcta"));
			}
		}else{
			echo json_encode(array("status"=>false, "msg"=>"No se realizo ningun cambio"));
		}
	}

	public function perfil($id){
		if($id!="0"){
			$this->load->model(array("paciente_model","solicitud_model","enfermedades_model","perfil_model","user_model"));
			$this->datoSess["id_default"]=$id;
			if($this->session->userdata('rol')=="1"){
				$data['pacientes'] = $this->paciente_model->selectAll();
			}else{
				$doctor=$this->user_model->getDoctorByUser($this->session->userdata('user'));
				$data['doctor'] = $doctor;
				$data['pacientes'] = $this->paciente_model->getFromDoctor($doctor->id_doctor);
			}

			$data['id'] = $id;
			$data['paciente'] = $this->paciente_model->selectId($id);
			$data['pais'] = ( isset($data['paciente']->id_pais) ) ? $this->paciente_model->getUbigeo($data['paciente']->id_pais,1) : false;
			$data['prov'] = ( isset($data['paciente']->id_provincia) ) ? $this->paciente_model->getUbigeo($data['paciente']->id_provincia,2) : false;
			$data['locali'] = ( isset($data['paciente']->id_localidad) ) ? $this->paciente_model->getUbigeo($data['paciente']->id_localidad,3) : false;
			$data['prof'] = ( isset($data['paciente']->profesion) ) ? $this->paciente_model->getProfesion($data['paciente']->profesion) : false;
			$data['prof_acom'] = ( isset($data['paciente']->profesion_acom) ) ? $this->paciente_model->getProfesion($data['paciente']->profesion_acom) : false;
			$data['enf_personales'] = $this->enfermedades_model->get(1);
			$data['enf_familiares'] = $this->enfermedades_model->get(2);
			$data['clinica'] = ( isset($data['paciente']->id_clinica) ) ? $this->paciente_model->getClinica($data['paciente']->id_clinica) : false;
			$data['ejercicios'] = $this->perfil_model->getEjercicios();
			$data['posicion'] = $this->paciente_model->getUbicacion($id);
		}else{
			$this->datoSess["id_default"]=0;
			$data["id"]=0;
		}
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/perfil',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function selectperfil(){
		$id = $_POST['id'];
		$this->load->model("paciente_model");
		$row = $this->paciente_model->getUbicacionRow($id);
		$online = $this->paciente_model->selectOnline($id);
		if($row){
			$row->online=$online->online;
			$row->ultima=date("d-m-Y H:i A",strtotime($row->fecha));
			echo json_encode(array("status"=>true, "data"=>$row));
		}else{
			echo json_encode(array("status"=>false, "msg"=>"El usuario aun no ha usado el app."));
		}
	}
}