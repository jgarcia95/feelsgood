<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Paciente extends CI_Controller {
	public function __construct(){
		@session_start();
       	parent::__construct();
       	if($this->session->userdata('logueado')){

	       	$this->load->model( array("user_model","paciente_model") );
			$this->datoSess["rol"] = $this->session->userdata('rol');
			$this->datoSess["id_default"]=1;
			
			switch ($this->session->userdata('rol')) {
	        	case 1:
	        		$campos="nombre,apellido";
        		break;
	        	case 2:
	        		$campos="nombre,apellido";
					$this->datoSess["rol"] = $this->session->userdata('rol');
					$doctor=$this->user_model->getDoctorByUser($this->session->userdata('user'));
					$dato=$this->paciente_model->selectPrimerPaciente($doctor->id_doctor);
					$this->datoSess["id_default"] = (count($dato) > 0) ? $dato[0]->id_paciente : 1;
				break;
	        	case 3:
	        		$campos="nombre,apellido";
        		break;
	        	case 4:
	        		$campos="nombre_clinica";
	        	break;
	        }
			$this->datoSess["user"] = $this->user_model->getDato($this->session->userdata('user'),$this->session->userdata('rol'),$campos);
			$this->datoSess["imagen"] = $this->user_model->getDato($this->session->userdata('user'),$this->session->userdata('rol'),"imagen");
		}else{
			header('Location: '.base_url());
		}
    }

    public function index(){
    	$data = array();
    	$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$paciente = $this->paciente_model->selectByUser($this->session->userdata('user'));
		
		$data["id"]= ( isset( $paciente->id_paciente ) ) ? $paciente->id_paciente : 0;
		$this->load->view('admin/sesiones_paciente',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function asignar(){
		$this->load->model("paciente_model");
		$this->load->model("doctor_model");
		$data['pacientes'] = $this->paciente_model->selectIndependientes();
		$data['doctores'] = $this->doctor_model->selectAll();
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/asignar_pacientes',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function asignarAction(){
		$this->load->model("paciente_model");
		$doctor = trim($_POST['doctor']);
		$paciente = trim($_POST['paciente']);
		$row = $this->paciente_model->asignarDoctor($paciente,$doctor);
		if($row){
			echo json_encode(array("status"=>true));
		}else{
			echo json_encode(array("status"=>false,"msg"=>"No se pudo asignar"));
		}
	}

	public function sesiones($id){
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$data["id"]=$id;
		$this->load->view('admin/sesiones_paciente',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function trae_sesiones(){
		$this->load->model("paciente_model");
		$resultados=array();$i=0;
		$ejercicios=array("Respiración","Hongos","Bote","Conejito","Peces",
		"Franja","Puente","Rotación con flexión","Estres y Ansiedad","Tamo");
		for ($j=1; $j <=10 ; $j++) { 
			$datos=$this->paciente_model->get_json($j,$_POST["id"]);
			if($datos){
				foreach ($datos as $dato) {
					$resultados[$i]['id']=$dato->id_ejercicio;
					$resultados[$i]['title']=$ejercicios[$j-1];
					$resultados[$i]['editable']= false;
					$resultados[$i]['allDay']= false;
					
					if(strtotime($dato->fecha) > time()) {
					    if( $dato->status && $dato->status == 1 ){
							$resultados[$i]['eventColor']= '#787878  !important';
							$resultados[$i]['backgroundColor']= '#787878  !important';
							$resultados[$i]['textColor']= '#fff !important';
						}
						else{
							$resultados[$i]['eventColor']= '#E0E0E0 !important';
							$resultados[$i]['backgroundColor']= '#E0E0E0 !important';
							$resultados[$i]['textColor']= '#181818 !important';
						}
					}
					else if(strtotime($dato->fecha) < time()) {
					    if( $dato->status && $dato->status == 1 ){
							$resultados[$i]['eventColor']= '#787878  !important';
							$resultados[$i]['backgroundColor']= '#787878  !important';
							$resultados[$i]['textColor']= '#fff !important';
						}
						else{
							$resultados[$i]['eventColor']= '#E0E0E0 !important';
							$resultados[$i]['backgroundColor']= '#E0E0E0 !important';
							$resultados[$i]['textColor']= '#181818 !important';
						}
					}
					else if(strtotime($dato->fecha) == time()) {
					    $resultados[$i]['eventColor']= '#E0E0E0 !important';
							$resultados[$i]['backgroundColor']= '#E0E0E0 !important';
							$resultados[$i]['textColor']= '#181818 !important';
					}
					else{}

					list($fecha,$hora)=explode(" ",$dato->fecha);
					$resultados[$i]['start']=$fecha."T".$hora;
					$fechaFin=strtotime('+1 hour',strtotime($dato->fecha));
					list($fechaFin,$horaFin)=explode(" ",date("Y-m-d H:i:s",$fechaFin));
					$resultados[$i]['end']=$fechaFin."T".$horaFin;
					$resultados[$i]['tipo'] = $j;

					$i++;
				}
			}
		}
		
		/*$datos=$this->paciente_model->get_json(1,$_POST["id"]);
		foreach ($datos as $dato) {
			$resultados[$i]['id']=$dato->id_ejercicio;
			$resultados[$i]['title']="Respiracion";
			list($fecha,$hora)=explode(" ",$dato->fecha);
			$resultados[$i]['start']=$fecha."T".$hora;
			$fechaFin=strtotime('+1 hour',strtotime($dato->fecha));
			list($fechaFin,$horaFin)=explode(" ",date("Y-m-d H:i:s",$fechaFin));
			$resultados[$i]['end']=$fechaFin."T".$horaFin;
			$resultados[$i]['tipo']=1;
			$resultados[$i]['id_escenario']=$dato->id_escenario;
		    $resultados[$i]['momento']=$dato->momento;
		    $resultados[$i]['repeticiones']=$dato->repeticiones;
		    $resultados[$i]['inhalacion']=$dato->inhalacion;
		    $resultados[$i]['mantener']=$dato->mantener;
		    $resultados[$i]['exhalacion']=$dato->exhalacion;
			$resultados[$i]['descanso']=$dato->descanso;
			$resultados[$i]['tutorial']=$dato->tutorial;
			$resultados[$i]['perrito']=$dato->perrito;
			$i++;
		}
		$datos=$this->paciente_model->get_json(2,$_POST["id"]);
		foreach ($datos as $dato) {
			$resultados[$i]['id']=$dato->id_ejercicio;
			$resultados[$i]['title']="Hongos";
			list($fecha,$hora)=explode(" ",$dato->fecha);
			$resultados[$i]['start']=$fecha."T".$hora;
			$fechaFin=strtotime('+1 hour',strtotime($dato->fecha));
			list($fechaFin,$horaFin)=explode(" ",date("Y-m-d H:i:s",$fechaFin));
			$resultados[$i]['end']=$fechaFin."T".$horaFin;
			$resultados[$i]['tipo']=2;
			$resultados[$i]['id_escenario']=$dato->id_escenario;
      $resultados[$i]['momento']=$dato->momento;
      $resultados[$i]['tamanio']=$dato->tamanio;
      $resultados[$i]['cantidad']=$dato->cantidad;
			$resultados[$i]['duracion']=$dato->duracion;
			$resultados[$i]['tutorial']=$dato->tutorial;
			$resultados[$i]['perrito']=$dato->perrito;
			$i++;
		}
		$datos=$this->paciente_model->get_json(3,$_POST["id"]);
		foreach ($datos as $dato) {
			$resultados[$i]['id']=$dato->id_ejercicio;
			$resultados[$i]['title']="Remos";
			list($fecha,$hora)=explode(" ",$dato->fecha);
			$resultados[$i]['start']=$fecha."T".$hora;
			$fechaFin=strtotime('+1 hour',strtotime($dato->fecha));
			list($fechaFin,$horaFin)=explode(" ",date("Y-m-d H:i:s",$fechaFin));
			$resultados[$i]['end']=$fechaFin."T".$horaFin;
			$resultados[$i]['tipo']=3;
			$resultados[$i]['id_escenario']=$dato->id_escenario;
      $resultados[$i]['momento']=$dato->momento;
      $resultados[$i]['forma']=$dato->forma;
      $resultados[$i]['radio']=$dato->radio;
			$resultados[$i]['duracion']=$dato->duracion;
			$resultados[$i]['tutorial']=$dato->tutorial;
			$resultados[$i]['perrito']=$dato->perrito;
			$i++;
		}
		$datos=$this->paciente_model->get_json(4,$_POST["id"]);
		foreach ($datos as $dato) {
			$resultados[$i]['id']=$dato->id_ejercicio;
			$resultados[$i]['title']="Conejito";
			list($fecha,$hora)=explode(" ",$dato->fecha);
			$resultados[$i]['start']=$fecha."T".$hora;
			$fechaFin=strtotime('+1 hour',strtotime($dato->fecha));
			list($fechaFin,$horaFin)=explode(" ",date("Y-m-d H:i:s",$fechaFin));
			$resultados[$i]['end']=$fechaFin."T".$horaFin;
			$resultados[$i]['tipo']=4;
			$resultados[$i]['id_escenario']=$dato->id_escenario;
      $resultados[$i]['momento']=$dato->momento;
      $resultados[$i]['grado']=$dato->grado;
      $resultados[$i]['nivel']=$dato->nivel;
			$resultados[$i]['duracion']=$dato->duracion;
			$resultados[$i]['tutorial']=$dato->tutorial;
			$resultados[$i]['perrito']=$dato->perrito;
			$i++;
		}
		$datos=$this->paciente_model->get_json(5,$_POST["id"]);
		foreach ($datos as $dato) {
			$resultados[$i]['id']=$dato->id_ejercicio;
			$resultados[$i]['title']="Peces";
			list($fecha,$hora)=explode(" ",$dato->fecha);
			$resultados[$i]['start']=$fecha."T".$hora;
			$fechaFin=strtotime('+1 hour',strtotime($dato->fecha));
			list($fechaFin,$horaFin)=explode(" ",date("Y-m-d H:i:s",$fechaFin));
			$resultados[$i]['end']=$fechaFin."T".$horaFin;
			$resultados[$i]['tipo']=5;
			$resultados[$i]['duracion']=$dato->duracion;
      $resultados[$i]['altura']=$dato->altura;
      $resultados[$i]['velocidad']=$dato->velocidad;
      $resultados[$i]['espera']=$dato->espera;
			$resultados[$i]['tipo_guia']=$dato->tipo_guia;
			$resultados[$i]['guiado']=$dato->guiado;
			$resultados[$i]['pez_rojo']=$dato->pez_rojo;
			$resultados[$i]['distancia']=$dato->distancia;
			$resultados[$i]['tutorial']=$dato->tutorial;
			$resultados[$i]['perrito']=$dato->perrito;
			$i++;
		}
		$datos=$this->paciente_model->get_json(6,$_POST["id"]);
		foreach ($datos as $dato) {
			$resultados[$i]['id']=$dato->id_ejercicio;
			$resultados[$i]['title']="Franja";
			list($fecha,$hora)=explode(" ",$dato->fecha);
			$resultados[$i]['start']=$fecha."T".$hora;
			$fechaFin=strtotime('+1 hour',strtotime($dato->fecha));
			list($fechaFin,$horaFin)=explode(" ",date("Y-m-d H:i:s",$fechaFin));
			$resultados[$i]['end']=$fechaFin."T".$horaFin;
			$resultados[$i]['tipo']=6;
			$resultados[$i]['posicion']=$dato->posicion;
      $resultados[$i]['velocidad']=$dato->velocidad;
      $resultados[$i]['tipo_ejercicio']=$dato->tipo_ejercicio;
			$resultados[$i]['tiempo_cantidad']=$dato->tiempo_cantidad;
			$resultados[$i]['tutorial']=$dato->tutorial;
			$resultados[$i]['perrito']=$dato->perrito;
			$i++;
		}
		$datos=$this->paciente_model->get_json(7,$_POST["id"]);
		foreach ($datos as $dato) {
			$resultados[$i]['id']=$dato->id_ejercicio;
			$resultados[$i]['title']="Puente";
			list($fecha,$hora)=explode(" ",$dato->fecha);
			$resultados[$i]['start']=$fecha."T".$hora;
			$fechaFin=strtotime('+1 hour',strtotime($dato->fecha));
			list($fechaFin,$horaFin)=explode(" ",date("Y-m-d H:i:s",$fechaFin));
			$resultados[$i]['end']=$fechaFin."T".$horaFin;
			$resultados[$i]['tipo']=7;
			$resultados[$i]['posicion']=$dato->posicion;
			$resultados[$i]['tiempo']=$dato->tiempo;
			$resultados[$i]['tutorial']=$dato->tutorial;
			$resultados[$i]['perrito']=$dato->perrito;
			$i++;
		}
		$datos=$this->paciente_model->get_json(8,$_POST["id"]);
		foreach ($datos as $dato) {
			$resultados[$i]['id']=$dato->id_ejercicio;
			$resultados[$i]['title']="Rotación con flexión";
			list($fecha,$hora)=explode(" ",$dato->fecha);
			$resultados[$i]['start']=$fecha."T".$hora;
			$fechaFin=strtotime('+1 hour',strtotime($dato->fecha));
			list($fechaFin,$horaFin)=explode(" ",date("Y-m-d H:i:s",$fechaFin));
			$resultados[$i]['end']=$fechaFin."T".$horaFin;
			$resultados[$i]['tipo']=8;
			$resultados[$i]['tipo_ejercicio']=$dato->tipo;
			$resultados[$i]['tiempo_ejercicio']=$dato->tiempo_ejercicio;
			$resultados[$i]['repeticiones']=$dato->repeticiones;
			$resultados[$i]['tutorial']=$dato->tutorial;
			$resultados[$i]['perrito']=$dato->perrito;
			$i++;
		}*/
		echo json_encode($resultados);
	}

	public function descargar($id){
		$this->load->model("paciente_model");
		$this->load->model("solicitud_model");
		$this->load->model("enfermedades_model");
		$this->load->model("perfil_model");
		$paciente = $this->paciente_model->selectId($id);
		if($paciente){
			$enf_personales = $this->enfermedades_model->get(1);
			$enf_familiares = $this->enfermedades_model->get(2);
			$prof_acom = $this->paciente_model->getProfesion($paciente->profesion_acom);
			require APPPATH.'libraries/PHPExcel.php';
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
			->setCreator("FeelsGood")
			->setTitle("Documento Excel");
			$objWorkSheet = $objPHPExcel->getActiveSheet();
			$objWorkSheet->mergeCells('A1:C1');
			$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
			$objWorkSheet->getStyle('A3')->getFont()->setBold(true);
			$objWorkSheet->getStyle('C3')->getFont()->setBold(true);
			$objWorkSheet
			->setCellValue('A1', 'Antecedentes del paciente')
			->setCellValue('A3', 'Antecedentes Personales')
			->setCellValue('C3', 'Antecedentes Familiares');
			$array_enf_personales=array();
			$i=1;
	    foreach (explode(",",$paciente->enf_personales) as $enf) {
	      $array_enf_personales[$i]=$enf;
	      $i++;
	    }
	    $e=4;
	    foreach ($enf_personales as $key) {
	    	if(array_search($key->id_enfermedad,$array_enf_personales)){ 
	        $objWorkSheet->setCellValue('A'.$e, $key->nombre)->getColumnDimension("A")->setAutoSize(true);
	        $e ++;
	      }
	    }
	    $array_enf_familiares=array();
	    $j=1;
	    foreach (explode(",",$paciente->enf_familiares) as $enf) {
	      $array_enf_familiares[$j]=$enf;
	      $j++;
	  	}
	    $e2=4;
	    foreach ($enf_familiares as $key) { 
	      if(array_search($key->id_enfermedad,$array_enf_familiares)){
	        $objWorkSheet->setCellValue('C'.$e2, $key->nombre)->getColumnDimension("C")->setAutoSize(true);
	        $e2 ++;
	      }
	    }
	    $r = ($e>=$e2)?($e+2):($e2+2);
	    $objWorkSheet->mergeCells('A'.$r.':C'.$r);
	    $objWorkSheet->getStyle('A'.$r)->getFont()->setBold(true);
	    $r=$r+2;
	    $objWorkSheet->getStyle('A'.$r)->getFont()->setBold(true);
	    $objWorkSheet->getStyle('A'.($r-2))->getFont()->setBold(true);
			$objWorkSheet->getStyle('A'.($r+1))->getFont()->setBold(true);
			$objWorkSheet->getStyle('A'.($r+2))->getFont()->setBold(true);
			$objWorkSheet->getStyle('A'.($r+3))->getFont()->setBold(true);
			$objWorkSheet->getStyle('A'.($r+4))->getFont()->setBold(true);
			$objWorkSheet->getStyle('A'.($r+5))->getFont()->setBold(true);
			$objWorkSheet->setCellValue('A'.($r-2), 'Datos del acompañante');
	    $objWorkSheet->setCellValue('A'.$r, 'DNI')->getColumnDimension("A")->setAutoSize(true);
	    $objWorkSheet->setCellValue('A'.($r+1), 'Nombres')->getColumnDimension("A")->setAutoSize(true);
	    $objWorkSheet->setCellValue('A'.($r+2), 'Apellidos')->getColumnDimension("A")->setAutoSize(true);
	    $objWorkSheet->setCellValue('A'.($r+3), 'Edad')->getColumnDimension("A")->setAutoSize(true);
	    $objWorkSheet->setCellValue('A'.($r+4), 'Teléfono')->getColumnDimension("A")->setAutoSize(true);
	    $objWorkSheet->setCellValue('A'.($r+5), 'Profesión')->getColumnDimension("A")->setAutoSize(true);
	    $objWorkSheet->setCellValue('C'.$r, $paciente->dni_acom)->getColumnDimension("C")->setAutoSize(true);
	    $objWorkSheet->setCellValue('C'.($r+1), $paciente->nombres_acom)->getColumnDimension("C")->setAutoSize(true);
	    $objWorkSheet->setCellValue('C'.($r+2), $paciente->apellidos_acom)->getColumnDimension("C")->setAutoSize(true);
	    $objWorkSheet->setCellValue('C'.($r+3), $paciente->edad_acom)->getColumnDimension("C")->setAutoSize(true);
	    $objWorkSheet->setCellValue('C'.($r+4), $paciente->fono_acom)->getColumnDimension("C")->setAutoSize(true);
	    if($prof_acom){ 
	      $objWorkSheet->setCellValue('C'.($r+5), $prof_acom->nombre_profesion)->getColumnDimension("C")->setAutoSize(true);
	    }
	    $objWorkSheet->mergeCells('F1:H1');
			$objWorkSheet->getStyle('F1')->getFont()->setBold(true);
			$objWorkSheet->setCellValue('F1', 'Proximas sesiones');
			$datos1=$this->paciente_model->get_json2(1,$id);
			$datos2=$this->paciente_model->get_json2(2,$id);
			$datos3=$this->paciente_model->get_json2(3,$id);
			$datos4=$this->paciente_model->get_json2(4,$id);
			$datos5=$this->paciente_model->get_json2(5,$id);
			$datos6=$this->paciente_model->get_json2(6,$id);
			$y= 3;
			$i=0;
			$arrayses =array();
			foreach ($datos1 as $dato) {
				$arrayses[$i]['nombre'] = 'Respiración';
				$arrayses[$i]['fecha'] = $dato->fecha;
				$i++;
			}
			foreach ($datos2 as $dato) {
				$arrayses[$i]['nombre'] = 'Hongos';
				$arrayses[$i]['fecha'] = $dato->fecha;
				$i++;
			}
			foreach ($datos3 as $dato) {
				$arrayses[$i]['nombre'] = 'Remos';
				$arrayses[$i]['fecha'] = $dato->fecha;
				$i++;
			}
			foreach ($datos4 as $dato) {
				$arrayses[$i]['nombre'] = 'Sapito';
				$arrayses[$i]['fecha'] = $dato->fecha;
				$i++;
			}
			foreach ($datos5 as $dato) {
				$arrayses[$i]['nombre'] = 'Peces';
				$arrayses[$i]['fecha'] = $dato->fecha;
				$i++;
			}
			foreach ($datos6 as $dato) {
				$arrayses[$i]['nombre'] = 'Franja';
				$arrayses[$i]['fecha'] = $dato->fecha;
				$i++;
			}
	    $objWorkSheet->setTitle("Datos del paciente");
	    usort($arrayses, array($this,'ordenar'));
	    foreach ($arrayses as $dat) {
	      $objWorkSheet->setCellValue('F'.$y, $dat['fecha'])->getColumnDimension("F")->setAutoSize(true);
	      $objWorkSheet->setCellValue('G'.$y, $dat['nombre'])->getColumnDimension("G")->setAutoSize(true);
	      $y++;
			}
			$letras = array("A","B","C","D","E","F","G","H","I","J","K","L");
			$objWorkSheet = $objPHPExcel->createSheet(1);
			$objWorkSheet->mergeCells('A1:C1');
			$ejercicios=$this->perfil_model->getEjercicios();
			$z=1;
			foreach ($ejercicios as $value) {
				$objWorkSheet->setCellValue('A'.$z,$value->nombre);
				$z++;
				switch ($value->id_ejercicio) {
					case 4:
						$objWorkSheet->setCellValue('A'.$z,"Fecha")->getColumnDimension("A")->setAutoSize(true);
						$objWorkSheet->setCellValue('B'.$z,"Manzanas")->getColumnDimension("B")->setAutoSize(true);
						$objWorkSheet->setCellValue('C'.$z,"% acierto")->getColumnDimension("C")->setAutoSize(true);
						$objWorkSheet->setCellValue('D'.$z,"Nivel alcanzado")->getColumnDimension("D")->setAutoSize(true);
						$objWorkSheet->setCellValue('E'.$z,"Resultado")->getColumnDimension("E")->setAutoSize(true);
					break;
				}
				$z++;
				$juego=$this->perfil_model->trae_ejercicios_terminados_excel($value->id_ejercicio,$id,$paciente->reporte);
				foreach ($juego as $key) {
					switch ($value->id_ejercicio) {
						case 4:
							$objWorkSheet->setCellValue('A'.$z,$key->fecha)->getColumnDimension("A")->setAutoSize(true);
							$objWorkSheet->setCellValue('B'.$z,$key->cantidad)->getColumnDimension("B")->setAutoSize(true);
							$objWorkSheet->setCellValue('C'.$z,$key->promedio)->getColumnDimension("C")->setAutoSize(true);
							$objWorkSheet->setCellValue('D'.$z,$key->nivel_final)->getColumnDimension("D")->setAutoSize(true);
							$objWorkSheet->setCellValue('E'.$z,($key->gano==1)? "Ganó":"Perdió")->getColumnDimension("E")->setAutoSize(true);
						break;
					}
					$z++;
				}
				$z++;
			}
			$objWorkSheet->setTitle("Estadisticas");
			// indicar que se envia un archivo de Excel.
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$paciente->nombre." ".$paciente->apellido.'.xlsx"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
		}else{
			header('Location: '.base_url());
		}
	}

	function ordenar( $a, $b ) {
	    return strtotime($a['fecha']) - strtotime($b['fecha']);
	}
}