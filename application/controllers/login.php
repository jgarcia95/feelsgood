<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
@session_start();
class Login extends CI_Controller {
	public function index(){
		if($this->session->userdata('logueado')){
			if($this->session->userdata('rol')==1){
				header('Location: administrador');
			}else if($this->session->userdata('rol')==2){
				header('Location: doctor');
			}else if($this->session->userdata('rol')==3){
				header('Location: paciente');
			}else{
				header('Location: clinica');
			}
		}else{
			$this->load->model("profesion_model");
			$data['profesiones'] = $this->profesion_model->selectAll();
			$this->load->view('login',$data);
		}
	}

	public function logear(){
		$user = trim($_POST['email']);
		$pass = trim($_POST['pass']);
		$this->load->model("user_model");
		$row = $this->user_model->login($user, $pass);
		if ($row) {
			$this->session->set_userdata('logueado',true);
			$this->session->set_userdata('rol',$row->type);
			$this->session->set_userdata('user',$row->id_user);
			echo json_encode(array('status'=>true,'rol'=>$row->type));
		}else{
			echo json_encode(array('status'=>false,'msg'=>"Datos incorrectos"));
		}
	}

	public function solicitarRegistroDoctor(){
		$this->load->model("profesion_model");
		$this->load->model("clinica_model");
		$this->load->model("user_model");
		$data['profesiones'] = $this->profesion_model->selectAll();
		$data['clinicas'] = $this->clinica_model->clinicas();
		$data['paises'] = $this->user_model->getPaises();
		$this->load->view('solicitarRegistroDoctor',$data);
	}

	public function solicitarRegistroClinica(){
		$this->load->model("user_model");
		$data['paises'] = $this->user_model->getPaises();
		$this->load->view('solicitarRegistroClinica',$data);
	}

	public function solicitarRegistroPaciente(){
		$this->load->model("clinica_model");
		$this->load->model("user_model");
		$this->load->model("profesion_model");
		$this->load->model("enfermedades_model");
		$data['paises'] = $this->user_model->getPaises();
		$data['clinicas'] = $this->clinica_model->clinicas();
		$data['profesiones'] = $this->profesion_model->selectAll();
		$data['enf_personales'] = $this->enfermedades_model->get(1);
		$data['enf_familiares'] = $this->enfermedades_model->get(2);
		$this->load->view('solicitarRegistroPaciente',$data);
	}

	public function addSolicitudDoctor(){

		$this->load->model("solicitud_model");
		$this->load->model("clinica_model");
		$this->load->model("user_model");
		
		if (trim($_POST['nacimientoSolicitud'])) {
			list($dia,$mes,$anio)=explode("-",trim($_POST['nacimientoSolicitud']));	
		}else{
			list($dia,$mes,$anio)=explode("-",trim("00-00-0000"));
		}

		$cli = explode(",",trim($_POST['clinicas']));
		$regisCli = array();
		$est = true;
		$n = "";
		$clinicaslistado = "";
		foreach ($cli as $key) {
			if(!is_numeric($key)){
				$key = str_replace("nuevo-", "", $key);
				array_push($regisCli,$key);
				if($this->solicitud_model->verificarClinica(strtolower($key))){
					$est = false;
					$n = $key;
				}
			}else{
				$clinicaslistado = $clinicaslistado.$key.",";
			}
		}

		if($est){
			foreach ($regisCli as $key) {
				$row = $this->user_model->insertUser("clinica@manual.com","123456",4);
				$newID=$this->clinica_model->insert($key,date("Y-m-d H:i:s"),$row);
				$clinicaslistado = $clinicaslistado.$newID.",";
			}
			$t = ($clinicaslistado=="") ? $clinicaslistado : substr($clinicaslistado, 0, -1);
			$datos = array(
				'fecha' =>trim(date("Y-m-d H:i:s")),
				'imagen' =>trim($_POST['renombrado']),
				'dni' =>trim($_POST['dniSolicitud']),
				'nombres' =>trim($_POST['nombreSolicitud']),
				'apellidos' =>trim($_POST['apellidosSolicitud']),
				'sexo' =>trim($_POST['sexoSolicitud']),
				'edad' =>trim($_POST['edadSolicitud']),
				'nacimiento' => $anio."-".$mes."-".$dia,
				'estado_civil' =>trim($_POST['estadoCivilSolicitud']),
				'email' =>trim($_POST['userSolicitud']),
				'pass' =>trim($_POST['passSolicitud']),
				'profesion' =>trim($_POST['profesionSolicitud']),
				'clinicas' =>$t,
				'id_pais' =>trim($_POST['paisSolicitud']),
				'id_provincia' =>trim($_POST['provinciaSolicitud']),
				'id_localidad' =>trim($_POST['localidadSolicitud']),
				'cmp' =>trim($_POST['cmp']),
				'rne' =>trim($_POST['rne']),
				'status' =>1
			);

			$row = $this->solicitud_model->add($datos,2);
			if ($row["status"]) {
				echo json_encode(array('status'=>true,'msg'=>"Se proceso su solicitud con exito."));
			}else{
				echo json_encode(array('status'=>false,'msg'=>"No se pudo procesar su solicitud."));
			}
		}else{
			echo json_encode(array('status'=>false,'msg'=>"La clinica ".$n." ya ha sido registrada."));
		}
	}

	public function addSolicitudClinica(){
		$datos = array(
			'fecha' =>trim(date("Y-m-d H:i:s")),
			'ruc' =>trim($_POST['rucSolicitud']),
			'nombre' =>trim($_POST['nombreSolicitud']),
			'id_pais' =>trim($_POST['paisSolicitud']),
			'id_provincia' =>trim($_POST['provinciaSolicitud']),
			'id_localidad' =>trim($_POST['localidadSolicitud']),
			'email' =>trim($_POST['userSolicitud']),
			'pass' =>trim($_POST['passSolicitud']),
			'status' =>1
		);
		$this->load->model("solicitud_model");
		$row = $this->solicitud_model->add($datos,4);
		if ($row["status"]) {
			echo json_encode(array('status'=>true,'msg'=>"Se proceso su solicitud con exito."));
		}else{
			echo json_encode(array('status'=>false,'msg'=>"No se pudo procesar su solicitud."));
		}
	}

	public function addSolicitudPaciente(){
		$personales="";
		if(!empty($_POST['personales'])){
			foreach($_POST['personales'] as $key){
				$personales.=$key.",";
		    }
		}
		$familiares="";
		if(!empty($_POST['familiares'])){
			foreach($_POST['familiares'] as $key){
				$familiares.=$key.",";
		    }
		}
		if (trim($_POST['nacimientoSolicitud'])) {
			list($dia,$mes,$anio)=explode("-",trim($_POST['nacimientoSolicitud']));	
		}else{
			list($dia,$mes,$anio)=explode("-",trim("00-00-0000"));
		}
		$datos = array(
			'fecha' =>trim(date("Y-m-d H:i:s")),
			'imagen' =>trim($_POST['renombrado']),
			'dni' =>trim($_POST['dniSolicitud']),
			'nombres' =>trim($_POST['nombreSolicitud']),
			'apellidos' =>trim($_POST['apellidosSolicitud']),
			'sexo' =>trim($_POST['sexoSolicitud']),
			'edad' =>trim($_POST['edadSolicitud']),
			'fono' =>trim($_POST['fono']),
			'nacimiento' => $anio."-".$mes."-".$dia,
			'estado_civil' =>trim($_POST['estadoCivilSolicitud']),
			'grupo_sanguineo' =>trim($_POST['grupoSolicitud']),
			'dni_acom' =>trim($_POST['dniAcomSolicitud']),
			'nombres_acom' =>trim($_POST['nombreAcomSolicitud']),
			'apellidos_acom' =>trim($_POST['apellidosAcomSolicitud']),
			'edad_acom' =>trim($_POST['edadAcomSolicitud']),
			'fono_acom' =>trim($_POST['fonoAcom']),
			'profesion_acom' =>trim($_POST['profesionAcomSolicitud']),
			'email' =>trim($_POST['userSolicitud']),
			'pass' =>trim($_POST['passSolicitud']),
			'profesion' =>trim($_POST['profesionSolicitud']),
			'id_clinica' =>trim($_POST['clinica']),
			'id_pais' =>trim($_POST['paisSolicitud']),
			'id_provincia' =>trim($_POST['provinciaSolicitud']),
			'id_localidad' =>trim($_POST['localidadSolicitud']),
			'enf_personales' =>substr($personales,0,-1),
			'enf_familiares' =>substr($familiares,0,-1),
			'otros_personales' =>trim($_POST['otros_personales']),
			'otros_familiares' =>trim($_POST['otros_familiares']),
			'observaciones' =>trim($_POST['observSolicitud']),
			'status' =>1
		);
		$this->load->model("solicitud_model");
		$row = $this->solicitud_model->add($datos,3);
		if ($row["status"]) {
			echo json_encode(array('status'=>true,'msg'=>"Se proceso su solicitud con exito."));
		}else{
			echo json_encode(array('status'=>false,'msg'=>"No se pudo procesar su solicitud."));
		}
	}

	public function logout(){
		//$this->session->set_userdata('logueado',false);
		$this->session->unset_userdata('logueado');
		header('Location: '.base_url());
		exit();
	}

	public function recuperarClave(){
		$this->load->view('recuperarClave');
	}

	public function comprobar_digito(){
		$correo = trim($_GET['correo']);
		$codigo = trim($_GET['codigo']);
		$this->load->model("user_model");
		$row = $this->user_model->comprobarDigito($correo,$codigo);
		if ($row) {
			echo json_encode(array('status'=>false));
		}else{
			echo json_encode(array('status'=>true));
		}
	}

	public function modificarPass(){
		$correoPass = trim($_POST['correoPass']);
		$pass = trim($_POST['pass']);
		$this->load->model("user_model");
		$row = $this->user_model->modificarPass($correoPass,$pass);
		echo json_encode(array('status'=>true));
	}

	public function comprobar_correo(){
		$correo = trim($_GET['correo']);
		$estado = trim($_GET['estado']);
		$this->load->model("user_model");
		$row = $this->user_model->comprobarCorreo($correo);
		$random = $this->RandomString(6,TRUE,TRUE,FALSE);
		if ($row) {
			if($estado == 1){
				$subject="FeelsGood";
				$random_hash=md5(date('r',time()));
				$headers='MIME-Version: 1.0' . "\r\n";
			    $headers.='Content-type: text/html; charset=utf-8' . "\r\n";
				$headers.="From: FeelsGood <consultas@FeelsGood.com.pe>\r\n";
				$headers.="Reply-To: FeelsGood <consultas@FeelsGood.com.pe>\r\n";
				$message = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width:100.0%;background:#e9e9e9;\">";
					$message .= "<tbody><tr><td style=\"padding:0in 0in 0in 0in;\"><div align=\"center\">";
						$message .= "<table width=\"850\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width:600.5pt;\">";
							$message .= "<tbody><tr><td style=\"padding:0in 0in 0in 0in;\"><div align=\"center\">";
								$message .= "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\"><tbody><tr><td style=\"padding:0in 0in 0in 0in;\">";
									$message .= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"1\" style=\"width:100.0%;background:white;border-top:solid #4ce50d 4.5pt;border-left:none;border-bottom:solid #d1d1d1 1.0pt;border-right:none;border-radius:5px;\">";
										$message .= "<tbody><tr><td style=\"border:none;padding:26.25pt 0in 7.5pt 0in;\">";
											$message .= "<div>";
											
											$message .= "<p align=\"center\">";
											$message .= "Clave: ".$random;
											$message .= "</p>";

											$message .= "</div>";
											$message .= "<p><span>&nbsp;</span></p>";
										$message .= "</td></tr></tbody>";
									$message .= "</table></td></tr></tbody>";
								$message .= "</table></div></td></tr>";
							$message .= "</tbody>";
						$message .= "</table></div><br><br>\r\n\r\n</td></tr>";
					$message .= "</tbody>";
				$message .= "</table>";
				$mail=mail($correo,$subject,$message,$headers);
				$estadoUpdate = $this->user_model->modificardigitos($correo,$random);
			}
			echo json_encode(array('status'=>false));
		}else{
			echo json_encode(array('status'=>true));
		}
	}

	private function randomString($length=6,$uc=TRUE,$n=TRUE,$sc=FALSE){
		$source = 'abcdefghijklmnopqrstuvwxyz';
		if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		if($n==1) $source .= '1234567890';
		if($sc==1) $source .= '|@#~$%()=^*+&#91;&#93;{}-_';
		if($length>0){
			$rstr = "";
			$source = str_split($source,1);
			for($i=1; $i<=$length; $i++){
				mt_srand((double)microtime() * 1000000);
				$num = mt_rand(1,count($source));
				$rstr .= $source[$num-1];
			}

		}
		return $rstr;
	}

	public function getProvincias(){
		$id_pais=$_POST["id"];
		$this->load->model("user_model");
		$provincias = $this->user_model->getProvincias($id_pais);
		echo json_encode($provincias);
	}

	public function getLocalidades(){
		$id_provincia=$_POST["id"];
		$this->load->model("user_model");
		$localidades = $this->user_model->getLocalidades($id_provincia);
		echo json_encode($localidades);
	}

	public function subirFoto(){
		$uploaddir = 'public/images/profile/';
		$archivo=basename($_FILES['file']['name']);
		$ext = ".".pathinfo($archivo, PATHINFO_EXTENSION);
		$nuevoArchivo=md5($archivo).uniqid().$ext;
		$uploadfile = $uploaddir.$nuevoArchivo;
		if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
		    echo json_encode(array('status'=>true,'renombrado'=>$nuevoArchivo));
		} else {
		    echo json_encode(array('status'=>false));
		}
	}

	public function eliminarFoto(){
		$uploaddir = 'public/images/profile/';
		unlink($uploaddir.$_POST["file_name"]);
	}

	public function addClinica(){
		$clinica=$_POST["clinica"];
		$this->load->model("clinica_model");
		$this->load->model("user_model");
		$row = $this->user_model->insertUser("clinica@manual.com","123456",4);
		$newID=$this->clinica_model->insert($clinica,date("Y-m-d H:i:s"),$row);
		echo json_encode(array('msg'=>"Se registro la clinica",'id'=>$newID,'valor'=>$clinica));
	}
	public function editClinica(){
		$clinica=$_POST["clinica"];
		$id=$_POST["id"];
		$this->load->model("clinica_model");
		$this->load->model("user_model");
		$newID=$this->clinica_model->edit($clinica,$id);
		echo json_encode(array('msg'=>"Se modifico la clinica"));
	}

	public function finRegistro(){
		$this->load->view('gracias');
	}

}