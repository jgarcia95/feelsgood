<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Doctor extends CI_Controller {
	public function __construct(){
		@session_start();
       	parent::__construct();
       	if($this->session->userdata('logueado')){
					$this->load->model("user_model");
					$this->load->model("paciente_model");
					$this->datoSess["rol"] = $this->session->userdata('rol');
					switch ($this->session->userdata('rol')) {
	        	case 1:$campos="nombre,apellido";break;
	        	case 2:$campos="nombre,apellido";break;
	        	case 3:$campos="nombre,apellido";break;
	        	case 4:$campos="nombre_clinica";break;
	        }
			$this->datoSess["user"] = $this->user_model->getDato($this->session->userdata('user'),$this->session->userdata('rol'),$campos);
			$this->datoSess["imagen"] = $this->user_model->getDato($this->session->userdata('user'),$this->session->userdata('rol'),"imagen");
		}else{
			header('Location: '.base_url());
		}
    }
	
	public function index(){

        $this->load->model("paciente_model");
        if($this->session->userdata('rol')=="1"){
            $data['pacientes'] = $this->paciente_model->selectAll();
        }else{
            $doctor=$this->user_model->getDoctorByUser($this->session->userdata('user'));
            $data['pacientes'] = $this->paciente_model->selectByDoctor($doctor->id_doctor);
        }

        if( $this->session->userdata('rol')=="1" ){

            $this->load->view('admin/templateAdmin/header',$this->datoSess);
            $this->load->view('admin/pacientes',$data);
            $this->load->view('admin/templateAdmin/footer');
        }
        else if( $this->session->userdata('rol')=="2" ){
            $this->load->view('admin/templateAdmin/header',$this->datoSess);
            $this->load->view('doctor/pacientes',$data);
            $this->load->view('admin/templateAdmin/footer');
        }
        else{
            show_404();
        }
        /*$this->load->model(array("paciente_model","solicitud_model","enfermedades_model","perfil_model","user_model"));

		$doctor=$this->user_model->getDoctorByUser($this->session->userdata('user'));
        $data['doctor'] = $doctor;
		$datos=$this->paciente_model->selectPrimerPaciente($doctor->id_doctor);

		if( isset($datos) && count($datos) > 0 && $datos[0]->id_paciente > 0){

			$this->datoSess["id_default"]=$datos[0]->id_paciente;
			$data['id'] = $datos[0]->id_paciente;
			$data['paciente'] = $this->paciente_model->selectId($datos[0]->id_paciente);
			$data['pacientes'] = $this->paciente_model->getFromDoctor($doctor->id_doctor); 
			$data['pais'] = $this->paciente_model->getUbigeo($data['paciente']->id_pais,1);
			$data['prov'] = $this->paciente_model->getUbigeo($data['paciente']->id_provincia,2);
			$data['locali'] = $this->paciente_model->getUbigeo($data['paciente']->id_localidad,3);
			$data['prof'] = $this->paciente_model->getProfesion($data['paciente']->profesion);
			$data['prof_acom'] = $this->paciente_model->getProfesion($data['paciente']->profesion_acom);
			$data['enf_personales'] = $this->enfermedades_model->get(1);
			$data['enf_familiares'] = $this->enfermedades_model->get(2);
			$data['clinica'] = $this->paciente_model->getClinica($data['paciente']->id_clinica);
			$data['ejercicios'] = $this->perfil_model->getEjercicios();
			$data['posicion'] = $this->paciente_model->getUbicacion($datos[0]->id_paciente);
		}else{
			$this->datoSess["id_default"]=0;
			$data["id"]=0;
		}

		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/perfil',$data);
		$this->load->view('admin/templateAdmin/footer');
        /**/
	}
}