<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Solicitudes extends CI_Controller {
	public function __construct(){
		@session_start();
       	parent::__construct();
       	if($this->session->userdata('logueado')){
	       	$this->load->model(array("user_model","paciente_model"));
					$this->datoSess["rol"] = $this->session->userdata('rol');
					$this->datoSess["id_default"]=1;
				switch ($this->session->userdata('rol')) {
	        	case 1:$campos="nombre,apellido";break;
	        	case 2:$campos="nombre,apellido";
						$this->datoSess["rol"] = $this->session->userdata('rol');
						$doctor=$this->user_model->getDoctorByUser($this->session->userdata('user'));
						$dato=$this->paciente_model->selectPrimerPaciente($doctor->id_doctor);
						$this->datoSess["id_default"] = (count($dato) > 0) ? $dato[0]->id_paciente : 1;
				break;
	        	case 3:$campos="nombre,apellido";break;
	        	case 4:$campos="nombre_clinica";break;
	        }
			$this->datoSess["user"] = $this->user_model->getDato($this->session->userdata('user'),$this->session->userdata('rol'),$campos);
			$this->datoSess["imagen"] = $this->user_model->getDato($this->session->userdata('user'),$this->session->userdata('rol'),"imagen");
		}else{
			header('Location: '.base_url());
		}
    }
	
	public function index(){
		$this->load->model("solicitud_model");
		$data['solicitudesDoctores'] = $this->solicitud_model->get_solicitudes(2);
		$data['solicitudesClinicas'] = $this->solicitud_model->get_solicitudes(4);
		$data['solicitudesPacientes'] = $this->solicitud_model->get_solicitudes(3);
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		$this->load->view('admin/solicitudes',$data);
		$this->load->view('admin/templateAdmin/footer');
	}

	public function procesar(){
		$id_solicitud = trim($_POST['id_solicitud']);
		$tipo_solicitud = trim($_POST['tipo_solicitud']);
		$perfil_solicitud = trim($_POST['perfil_solicitud']);
		$this->load->model("solicitud_model");
		switch ($perfil_solicitud) {
			case 2://doctor
				if($tipo_solicitud==1){
					$res=$this->solicitud_model->get_solicitud_byID($id_solicitud,2);
					$row = $this->user_model->insertUser($res[0]->email,$res[0]->pass,2);
					$datos = array(
						'fecha' => date("Y-m-d H:i:s"),
						'imagen' => $res[0]->imagen,
						'dni' => $res[0]->dni,
						'nombre' => $res[0]->nombres,
						'apellido' => $res[0]->apellidos,
						'sexo' => $res[0]->sexo,
						'edad' => $res[0]->edad,
						'nacimiento' => $res[0]->nacimiento,
						'estado_civil' => $res[0]->estado_civil,
						'id_pais' => $res[0]->id_pais,
						'id_provincia' => $res[0]->id_provincia,
						'id_localidad' => $res[0]->id_localidad,
						'id_user' => $row, 
						'profesion' => $res[0]->profesion, 
						'clinicas' => $res[0]->clinicas
					);
					$this->user_model->insertDoctor($datos);
					$this->solicitud_model->updateStatus($id_solicitud,0,2);
					if($row){
						echo json_encode(array("status"=>true,"msg"=>"Se creo el perfil del doctor"));
					}else{
						echo json_encode(array("status"=>false,"msg"=>"No se pudo crear el perfil del doctor"));
					}
				}else{
					$this->solicitud_model->updateStatus($id_solicitud,2,2);
					echo json_encode(array("status"=>true,"msg"=>"Se rechazo el perfil del doctor"));
				}
			break;
			case 3://paciente
				if($tipo_solicitud==1){
					$res=$this->solicitud_model->get_solicitud_byID($id_solicitud,3);
					if($res[0]->email!="") {
						$row = $this->user_model->insertUser($res[0]->email,$res[0]->pass,3);
					}else{
						$row = $this->user_model->insertUser($res[0]->fono,$res[0]->pass,3);
					}
					$datos = array(
						'fecha' =>trim(date("Y-m-d H:i:s")),
						'imagen' =>$res[0]->imagen,
						'dni' =>$res[0]->dni,
						'nombre' =>$res[0]->nombres,
						'apellido' =>$res[0]->apellidos,
						'sexo' =>$res[0]->sexo,
						'edad' =>$res[0]->edad,
						'fono' =>$res[0]->fono,
						'nacimiento' => $res[0]->nacimiento,
						'estado_civil' =>$res[0]->estado_civil,
						'grupo_sanguineo' =>$res[0]->grupo_sanguineo,
						'dni_acom' =>$res[0]->dni_acom,
						'nombres_acom' =>$res[0]->nombres_acom,
						'apellidos_acom' =>$res[0]->apellidos_acom,
						'edad_acom' =>$res[0]->edad_acom,
						'fono_acom' =>$res[0]->fono_acom,
						'profesion_acom' =>$res[0]->profesion_acom,
						'id_user' => $row,
						'profesion' =>$res[0]->profesion,
						'id_clinica' =>$res[0]->id_clinica,
						'id_pais' =>$res[0]->id_pais,
						'id_provincia' =>$res[0]->id_provincia,
						'id_localidad' =>$res[0]->id_localidad,
						'enf_personales' =>$res[0]->enf_personales,
						'enf_familiares' =>$res[0]->enf_familiares,
						'otros_personales' =>$res[0]->otros_personales,
						'otros_familiares' =>$res[0]->otros_familiares,
						'observaciones' =>$res[0]->observaciones
					);
					$this->user_model->insertPaciente($datos);
					$this->solicitud_model->updateStatus($id_solicitud,0,3);
					if($row){
						echo json_encode(array("status"=>true,"msg"=>"Se creo el perfil del paciente"));
					}else{
						echo json_encode(array("status"=>false,"msg"=>"No se pudo crear el perfil del paciente"));
					}
				}else{
					$this->solicitud_model->updateStatus($id_solicitud,2,3);
					echo json_encode(array("status"=>true,"msg"=>"Se rechazo el perfil del paciente"));
				}
			break;
			case 4://clinica
				if($tipo_solicitud==1){
					$res=$this->solicitud_model->get_solicitud_byID($id_solicitud,4);
					$row = $this->user_model->insertUser($res[0]->email,$res[0]->pass,4);
					$datos = array(
						"fecha" => date("Y-m-d H:i:s"),
						'ruc' => $res[0]->ruc,
						'imagen' => $res[0]->imagen,
						'nombre_clinica' => $res[0]->nombre,
						"id_user" => $row,
						'id_pais' =>$res[0]->id_pais,
						'id_provincia' =>$res[0]->id_provincia,
						'id_localidad' =>$res[0]->id_localidad
					);
					$this->user_model->insertClinica($datos);
					$this->solicitud_model->updateStatus($id_solicitud,0,4);
					if($row){
						echo json_encode(array("status"=>true,"msg"=>"Se creo el perfil de la clinica"));
					}else{
						echo json_encode(array("status"=>false,"msg"=>"No se pudo crear el perfil de la clinica"));
					}
				}else{
					$this->solicitud_model->updateStatus($id_solicitud,2,4);
					echo json_encode(array("status"=>true,"msg"=>"Se rechazo el perfil de la clinica"));
				}
			break;
		}
	}

	public function detalle($id,$tipo){
		$this->load->model("solicitud_model");
		$this->load->view('admin/templateAdmin/header',$this->datoSess);
		switch ($tipo){
			case '2'://doctor
				$this->load->model("paciente_model");
				$data['doctor'] = $this->solicitud_model->get_solicitud_byID($id,2);
				$clinicas=explode(",",$data['doctor'][0]->clinicas);
				$clinic='';
				foreach ($clinicas as $key) {
					$c=$this->paciente_model->getClinica($key);
					if($c){
						$clinic.=$c->nombre_clinica.",";
					}
				}
				$data['pais'] = $this->paciente_model->getUbigeo($data['doctor'][0]->id_pais,1);
				$data['prov'] = $this->paciente_model->getUbigeo($data['doctor'][0]->id_provincia,2);
				$data['locali'] = $this->paciente_model->getUbigeo($data['doctor'][0]->id_localidad,3);
				$data['prof'] = $this->paciente_model->getProfesion($data['doctor'][0]->profesion);
				$data['clinicas'] = substr($clinic,0,-1);
				$this->load->view('admin/detalle_doctor',$data);
			break;
			case '3'://paciente
				$this->load->model("enfermedades_model");
				$this->load->model("paciente_model");
				$data['paciente'] = $this->solicitud_model->get_solicitud_byID($id,3);
				$data['pais'] = $this->paciente_model->getUbigeo($data['paciente'][0]->id_pais,1);
				$data['prov'] = $this->paciente_model->getUbigeo($data['paciente'][0]->id_provincia,2);
				$data['locali'] = $this->paciente_model->getUbigeo($data['paciente'][0]->id_localidad,3);
				$data['prof'] = $this->paciente_model->getProfesion($data['paciente'][0]->profesion);
				$data['prof_acom'] = $this->paciente_model->getProfesion($data['paciente'][0]->profesion_acom);
				$data['enf_personales'] = $this->enfermedades_model->get(1);
				$data['enf_familiares'] = $this->enfermedades_model->get(2);
				$data['clinica'] = $this->paciente_model->getClinica($data['paciente'][0]->id_clinica);
				$this->load->view('admin/detalle_paciente',$data);
			break;
			case '4'://clinica
				$this->load->model("paciente_model");
				$data['clinica'] = $this->solicitud_model->get_solicitud_byID($id,4);
				$this->load->view('admin/detalle_clinica',$data);
			break;
		}
		$this->load->view('admin/templateAdmin/footer');
	}
}