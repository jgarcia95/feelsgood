<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class api extends CI_Controller {

	public function login(){
		$user = trim($_POST['email']);
		$pass = trim($_POST['pass']);
		$this->load->model("user_model");
		$row = $this->user_model->login($user,$pass);
		if($row){
			$this->load->model("paciente_model");
			$res = $this->paciente_model->selectByUser($row->id_user);
			$res->status=true;
			$res->rol=$row->type;
			echo json_encode($res);
		}else{
			echo json_encode(array('status'=>false,'msg'=>"Datos incorrectos"));
		}
	}

	public function loginDni(){

		$dni = trim($_POST['dni']);
		$this->load->model("paciente_model");
		$row = $this->paciente_model->getByDni($dni);

		if($row){
			echo json_encode($row);
		}else{
			echo json_encode(array('status'=>false,'msg'=>"Datos incorrectos"));
		}
	}

	public function onlineDevice(){
		$this->load->model("user_model");
		$this->load->model("paciente_model");
		$datos=array(
			"online"=>$_POST["status"]
		);
		$this->user_model->modificarPaciente($datos,$_POST["id_paciente"]);
		$data=array(
			"id_paciente"=>$_POST["id_paciente"],
			"fecha"=>date("Y-m-d H:i:00"),
			"latitud"=>$_POST["latitud"],
			"longitud"=>$_POST["longitud"]
		);
		$ubi=$this->paciente_model->getUbicacion($_POST["id_paciente"]);
		if(!$ubi){
			$this->paciente_model->nuevaConexion($data);
		}else{
			$this->paciente_model->actualizaConexion($data);
		}
	}

	public function getPacientes(){
		$this->load->model("paciente_model");
		$res=$this->paciente_model->selectAll($_POST["id_doctor"]);
		echo json_encode($res);
	}

	public function trae_ejercicios(){
		$this->load->model("perfil_model");
		list($dia1,$mes1,$anio1)=explode("-",$_POST["desde"]);
		list($dia2,$mes2,$anio2)=explode("-",$_POST["hasta"]);
		$datos = array(
			'id_paciente' => $_POST["id_paciente"],
			'id_ejer' => $_POST["tipo"],
			'desde' => $anio1."-".$mes1."-".$dia1,
			'hasta' => $anio2."-".$mes2."-".$dia2
		);
		$res=$this->perfil_model->trae_ejercicios_terminados($datos);
		echo json_encode($res);
	}
	public function trae_detalle_ejercicio(){
		$this->load->model("perfil_model");
		$res=$this->perfil_model->trae_detalle_ejercicio($_POST["id_ejercicio"],$_POST["tipo"]);
		echo json_encode($res);
	}
	public function getEjercicio(){
		$this->load->model("perfil_model");
		$res=$this->perfil_model->trae_datos($_POST["id_paciente"],$_POST["id_ejercicio"]);
		if($res){
			$res->status=true;
		}else{
			$res["status"]=false;
		}
		echo json_encode($res);
	}

	public function completaEjercicio(){
		$this->load->model("perfil_model");
		$datos=array(
			"status"=>1
		);
		$res=$this->perfil_model->editaEjercicio($_POST["tipo"],$_POST["id_ejercicio"],$datos);
	}

	public function guardaEvolucion(){

		$this->load->model("perfil_model");
		switch ($_POST["tipo"]) {
			case 1:
				$datos=array(
					"promedio"=> $_POST["promedio"],
					"status"=> ( $_POST["status"] == 'Complete' ) ? 1 : 0,
					"cantidad"=> $_POST["cantidad"],
					"tiros"=> '{"tiros":'.$_POST["movimientos"].'}'
				);
			break;
			case 2:break;
			case 3:break;
			case 4:
				$datos=array(
					"promedio"=> $_POST["promedio"],
					"gano"=> $_POST["status"],
					"cantidad"=> $_POST["cantidad"],
					"nivel_final"=> $_POST["nivel"],
					"tiros"=> '{"tiros":'.$_POST["tiros"].'}'
				);
			break;
			case 5:
				$datos=array(
					"promedio"=> $_POST["promedio"],
					"gano"=> $_POST["status"],
					"cantidad"=> $_POST["cantidad"],
					"tiros"=> '{"tiros":'.$_POST["movimientos"].'}'
				);
			break;
			case 6:
				$datos=array(
					"promedio"=> $_POST["promedio"],
					"gano"=> $_POST["status"],
					"cantidad"=> $_POST["cantidad"],
					"tiros"=> '{"tiros":'.$_POST["movimientos"].'}'
				);
			break;
			case 7:
				$datos=array(
					"promedio"=> $_POST["promedio"],
					"status"=> ( $_POST["status"] == 'Complete' ) ? 1 : 0,
					"cantidad"=> $_POST["cantidad"],
					"tiros"=> '{"tiros":'.$_POST["movimientos"].'}'
				);
			break;
			case 8:
				$datos=array(
					"promedio"=> $_POST["promedio"],
					"status"=> ( $_POST["status"] == 'Complete' ) ? 1 : 0,
					"cantidad"=> $_POST["cantidad"],
					"tiros"=> '{"tiros":'.$_POST["movimientos"].'}'
				);
			break;
			case 10:
				$datos=array(
					"promedio"=> $_POST["promedio"],
					"status"=> ( $_POST["status"] == 'Complete' ) ? 1 : 0,
					"cantidad"=> $_POST["cantidad"],
					"tiros"=> '{"tiros":'.$_POST["movimientos"].'}'
				);
			break;
		}
		$res=$this->perfil_model->editaEjercicio($_POST["tipo"],$_POST["id_ejercicio"],$datos);
	}

	function ordenar( $a, $b ) {
	  return strtotime($a['fecha']) - strtotime($b['fecha']);
	}

	public function enviar_mail(){
		$this->load->model("paciente_model");
		$id_ejercicio = trim($_POST['id_ejercicio']);
		$id_paciente = trim($_POST['id_paciente']);
		$tipo = trim($_POST['tipo']);
		$paciente = $this->paciente_model->selectId($id_paciente);
		$this->descargar($id_paciente,$id_ejercicio,$tipo);
		$this->mail("yor.azanero@hotmail.com",$paciente->nombre." ".$paciente->apellido.".xlsx");
		/*
		$this->load->model("perfil_model");
		/*$pacientes = $this->paciente_model->selectAll();
		foreach ($pacientes as $key) {
			$ultimo = $this->perfil_model->trae_ultimo_ejercicio($ejercicio,$key->id_paciente);
			if($ultimo){
				$nom1 = $key->nombre.' '.$key->apellido;
				$r = uniqid();
				$this->descargarultimoejercicio($key->id_paciente,$nom1,$r,$ejercicio,$ultimo);
				$this->mailapiultimoejercicio($key->email_user,$r.".xlsx",$nom1);
				$doctor = $this->perfil_model->selectDoctorById($key->id_doctor);
				if($doctor){
					$nom1 = $doctor->nombre.' '.$doctor->apellido;
					$this->mailapiultimoejercicio($doctor->email_user,$r.".xlsx",$nom1);
				}
				unlink("public/".$r.".xlsx");
			}
		}*/
	}

	public function mail($correo,$nombre){
      $subject="Correo api";
      $random_hash=md5(date('r',time()));
      $uid = md5(uniqid(time()));
      $file = "public/".$nombre;
  		$file_size = filesize($file);
    	$handle = fopen($file, "r");
    	$content = fread($handle, $file_size);
    	fclose($handle);
    	$content = chunk_split(base64_encode($content));
      $headers='MIME-Version: 1.0' . "\r\n";
    	$headers.="Content-type: multipart/mixed; boundary=\"" . $uid . "\"\r\n";
      $headers.="From: FeelsGood <FeelsGood@FeelsGood.com.pe>\r\n";
      $headers.="Reply-To: FeelsGood <FeelsGood@FeelsGood.com.pe>\r\n";
      $headers .= "Content-Type: application/octet-stream; name=\"".'Reporte de ultimo ejercicio de '.$nombre."\"\r\n";
      $headers .= "Content-Transfer-Encoding: base64\r\n";
      $headers .= "Content-Disposition: attachment; filename=\"".'Reporte de ultimo ejercicio de '.$nombre."\"\r\n";
      $headers .= $content."\r\n\r\n";
      $mail=mail($correo,$subject,'',$headers);
  	}

  function descargar($id_paciente,$id_ejercicio,$tipo_ejer){
		$this->load->model("paciente_model");
		$this->load->model("solicitud_model");
		$this->load->model("enfermedades_model");
		$this->load->model("perfil_model");
		$paciente = $this->paciente_model->selectId($id_paciente);
		if($paciente){
			$enf_personales = $this->enfermedades_model->get(1);
			$enf_familiares = $this->enfermedades_model->get(2);
			$prof_acom = $this->paciente_model->getProfesion($paciente->profesion_acom);
			require APPPATH.'libraries/PHPExcel.php';
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
			->setCreator("FeelsGood")
			->setTitle("Documento Excel");
			$objWorkSheet = $objPHPExcel->getActiveSheet();
			$objWorkSheet->mergeCells('A1:C1');
			$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
			$objWorkSheet->getStyle('A3')->getFont()->setBold(true);
			$objWorkSheet->getStyle('C3')->getFont()->setBold(true);
			$objWorkSheet
			->setCellValue('A1', 'Antecedentes del paciente')
			->setCellValue('A3', 'Antecedentes Personales')
			->setCellValue('C3', 'Antecedentes Familiares');
			$array_enf_personales=array();
			$i=1;
	    foreach (explode(",",$paciente->enf_personales) as $enf) {
	      $array_enf_personales[$i]=$enf;
	      $i++;
	    }
	    $e=4;
	    foreach ($enf_personales as $key) {
	    	if(array_search($key->id_enfermedad,$array_enf_personales)){
	        $objWorkSheet->setCellValue('A'.$e, $key->nombre)->getColumnDimension("A")->setAutoSize(true);
	        $e ++;
	      }
	    }
	    $array_enf_familiares=array();
	    $j=1;
	    foreach (explode(",",$paciente->enf_familiares) as $enf) {
	      $array_enf_familiares[$j]=$enf;
	      $j++;
	  	}
	    $e2=4;
	    foreach ($enf_familiares as $key) {
	      if(array_search($key->id_enfermedad,$array_enf_familiares)){
	        $objWorkSheet->setCellValue('C'.$e2, $key->nombre)->getColumnDimension("C")->setAutoSize(true);
	        $e2 ++;
	      }
	    }
	    $r = ($e>=$e2)?($e+2):($e2+2);
	    $objWorkSheet->mergeCells('A'.$r.':C'.$r);
	    $objWorkSheet->getStyle('A'.$r)->getFont()->setBold(true);
	    $r=$r+2;
	    $objWorkSheet->getStyle('A'.$r)->getFont()->setBold(true);
	    $objWorkSheet->getStyle('A'.($r-2))->getFont()->setBold(true);
			$objWorkSheet->getStyle('A'.($r+1))->getFont()->setBold(true);
			$objWorkSheet->getStyle('A'.($r+2))->getFont()->setBold(true);
			$objWorkSheet->getStyle('A'.($r+3))->getFont()->setBold(true);
			$objWorkSheet->getStyle('A'.($r+4))->getFont()->setBold(true);
			$objWorkSheet->getStyle('A'.($r+5))->getFont()->setBold(true);
			$objWorkSheet->setCellValue('A'.($r-2), 'Datos del acompañante');
	    $objWorkSheet->setCellValue('A'.$r, 'DNI')->getColumnDimension("A")->setAutoSize(true);
	    $objWorkSheet->setCellValue('A'.($r+1), 'Nombres')->getColumnDimension("A")->setAutoSize(true);
	    $objWorkSheet->setCellValue('A'.($r+2), 'Apellidos')->getColumnDimension("A")->setAutoSize(true);
	    $objWorkSheet->setCellValue('A'.($r+3), 'Edad')->getColumnDimension("A")->setAutoSize(true);
	    $objWorkSheet->setCellValue('A'.($r+4), 'Teléfono')->getColumnDimension("A")->setAutoSize(true);
	    $objWorkSheet->setCellValue('A'.($r+5), 'Profesión')->getColumnDimension("A")->setAutoSize(true);
	    $objWorkSheet->setCellValue('C'.$r, $paciente->dni_acom)->getColumnDimension("C")->setAutoSize(true);
	    $objWorkSheet->setCellValue('C'.($r+1), $paciente->nombres_acom)->getColumnDimension("C")->setAutoSize(true);
	    $objWorkSheet->setCellValue('C'.($r+2), $paciente->apellidos_acom)->getColumnDimension("C")->setAutoSize(true);
	    $objWorkSheet->setCellValue('C'.($r+3), $paciente->edad_acom)->getColumnDimension("C")->setAutoSize(true);
	    $objWorkSheet->setCellValue('C'.($r+4), $paciente->fono_acom)->getColumnDimension("C")->setAutoSize(true);
	    if($prof_acom){
	      $objWorkSheet->setCellValue('C'.($r+5), $prof_acom->nombre_profesion)->getColumnDimension("C")->setAutoSize(true);
	    }
	    $objWorkSheet->mergeCells('F1:H1');
			$objWorkSheet->getStyle('F1')->getFont()->setBold(true);
			$objWorkSheet->setCellValue('F1', 'Proximas sesiones');
			$datos1=$this->paciente_model->get_json2(1,$id_paciente);
			$datos2=$this->paciente_model->get_json2(2,$id_paciente);
			$datos3=$this->paciente_model->get_json2(3,$id_paciente);
			$datos4=$this->paciente_model->get_json2(4,$id_paciente);
			$y= 3;
			$i=0;
			$arrayses =array();
			foreach ($datos1 as $dato) {
				$arrayses[$i]['nombre'] = 'Respiración';
				$arrayses[$i]['fecha'] = $dato->fecha;
				$i++;
			}
			foreach ($datos2 as $dato) {
				$arrayses[$i]['nombre'] = 'Hongos';
				$arrayses[$i]['fecha'] = $dato->fecha;
				$i++;
			}
			foreach ($datos3 as $dato) {
				$arrayses[$i]['nombre'] = 'Remos';
				$arrayses[$i]['fecha'] = $dato->fecha;
				$i++;
			}
			foreach ($datos4 as $dato) {
				$arrayses[$i]['nombre'] = 'Sapito';
				$arrayses[$i]['fecha'] = $dato->fecha;
				$i++;
			}
	    $objWorkSheet->setTitle("Datos del paciente");
	    usort($arrayses, array($this,'ordenar'));
	    foreach ($arrayses as $dat) {
	      $objWorkSheet->setCellValue('F'.$y, $dat['fecha'])->getColumnDimension("F")->setAutoSize(true);
	      $objWorkSheet->setCellValue('G'.$y, $dat['nombre'])->getColumnDimension("G")->setAutoSize(true);
	      $y++;
			}
			$letras = array("A","B","C","D","E","F","G","H","I","J","K","L");
			$objWorkSheet = $objPHPExcel->createSheet(1);
			$objWorkSheet->mergeCells('A1:C1');
			$ejercicios=$this->perfil_model->getEjercicios();
			$z=1;
			foreach ($ejercicios as $value) {
				$objWorkSheet->setCellValue('A'.$z,$value->nombre);
				$z++;
				switch ($value->id_ejercicio) {
					case 4:
						$objWorkSheet->setCellValue('A'.$z,"Fecha")->getColumnDimension("A")->setAutoSize(true);
						$objWorkSheet->setCellValue('B'.$z,"Manzanas")->getColumnDimension("B")->setAutoSize(true);
						$objWorkSheet->setCellValue('C'.$z,"% acierto")->getColumnDimension("C")->setAutoSize(true);
						$objWorkSheet->setCellValue('D'.$z,"Nivel alcanzado")->getColumnDimension("D")->setAutoSize(true);
						$objWorkSheet->setCellValue('E'.$z,"Resultado")->getColumnDimension("E")->setAutoSize(true);
					break;
				}
				$z++;
				$juego=$this->perfil_model->trae_ejercicio_by_id($id_ejercicio,$tipo_ejer);
				foreach ($juego as $key) {
					switch ($value->id_ejercicio) {
						case 4:
							$objWorkSheet->setCellValue('A'.$z,$key->fecha)->getColumnDimension("A")->setAutoSize(true);
							$objWorkSheet->setCellValue('B'.$z,$key->cantidad)->getColumnDimension("B")->setAutoSize(true);
							$objWorkSheet->setCellValue('C'.$z,$key->promedio)->getColumnDimension("C")->setAutoSize(true);
							$objWorkSheet->setCellValue('D'.$z,$key->nivel_final)->getColumnDimension("D")->setAutoSize(true);
							$objWorkSheet->setCellValue('E'.$z,($key->gano==1)? "Ganó":"Perdió")->getColumnDimension("E")->setAutoSize(true);
						break;
					}
					$z++;
				}
				$z++;
			}
			$objWorkSheet->setTitle("Estadisticas");
			// indicar que se envia un archivo de Excel.
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$paciente->nombre." ".$paciente->apellido.'.xlsx"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('public/'.$paciente->nombre." ".$paciente->apellido.'.xlsx');
		}
		/*/ indicar que se envia un archivo de Excel.
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$nom1.'.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('public/'.$na.'.xlsx');*/
	}
}
