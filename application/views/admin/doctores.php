<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Gestion</li>
        <li>Doctores</li>
    </ul>
    <div class="table-responsive">
        <table class="table table-striped table-hover" id="lista-doctores">
            <thead>
                <tr>
                    <th></th>
                    <th>DNI</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Edad</th>
                    <th>Email</th>
                    <th>Activo</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($doctores as $key) { ?>
                    <tr>
                        <td style="min-width: 50px;"><?php if($key->imagen!=""){?>
                            <img src="../public/images/profile/<?=$key->imagen?>"
                            style="width:50px;height:50px;">
                        <?php } ?></td>
                        <td><?= $key->dni ?></td>
                        <td><?= $key->nombre ?></td>
                        <td><?= $key->apellido ?></td>
                        <td><?= $key->edad ?></td>
                        <td><?= $key->email_user ?></td>
                        <td>
                            <?php if($key->status=="1"){
                                echo '<span class="label label-success">Habilitado</span>';
                            }else{
                                echo '<span class="label label-danger">Inhabilitado</span>';
                            } ?>
                        </td>
                        <td>
                            <a title="Actualizar" href="<?= base_url() ?>administrador/editDoctor/<?= $key->id_doctor ?>"><button class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o fa-lg fa-fw" aria-hidden="true"></button></i></a>
                            <a title="Eliminar" data-id="<?=$key->id_user?>" data-toggle="modal" href="#" data-target="#delModalDoctor" class="deldata"><button class="btn btn-default btn-xs"><i class="fa fa-times fa-lg fa-fw" aria-hidden="true"></button></i></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div><!-- ./padding-md -->
<div class="modal fade" id="delModalDoctor">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Confirmar</h4>
            </div>
            <div class="modal-body">
                Eliminar registro?
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal" id="close_doctor_del">Cerrar</a>
                <a class="btn btn-primary" id="eliminar_doctor">Eliminar</a>
            </div>
            <input type="hidden" id="id_doctor_eliminar">
        </div>
    </div>
</div>
<link href="<?= base_url() ?>public/css/dataTables.bootstrap.css" rel="stylesheet">
<script src='<?= base_url() ?>public/js/jquery.dataTables.min.js'></script>
<script src='<?= base_url() ?>public/js/uncompressed/dataTables.bootstrap.js'></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>
<script src="<?= base_url() ?>public/js/doctor.js"></script>
