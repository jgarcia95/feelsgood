<!-- Date Time Picker -->
<link href="<?= base_url() ?>public/css/datetimepicker.css" rel="stylesheet">
<!-- Dropzone -->
<link href="<?= base_url() ?>public/css/dropzone/css/dropzone.css" rel="stylesheet">
<link href="<?= base_url() ?>public/css/solicitudes.css" rel="stylesheet">
<link href="<?= base_url() ?>public/css/select2/select2.css" rel="stylesheet"/>
<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Creación de usuarios</li>
        <li>Doctor</li> 
    </ul>
    <div class="row">
        <form id="form-add-doctor" class="form-horizontal no-margin" style="padding:5%;">
            <div class="form-group">
                <label class="control-label col-lg-2">Foto</label>
                <div class="col-lg-10">
                    <div class="dropzone" id="fileImagen">
                        <div class="fallback">
                            <input name="file" type="file" />
                        </div>
                    </div>
                    <input type="hidden" id="renombrado" name="renombrado">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">DNI</label>
                <div class="col-lg-10">
                    <input type="text" id="dni" name="dni" class="form-control input-sm" placeholder="Ingresa el DNI" data-parsley-required="true" data-parsley-type="number">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Nombre</label>
                <div class="col-lg-10">
                    <input type="text" id="nombre" name="nombre" class="form-control input-sm" placeholder="Ingresa el nombre" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Apellido</label>
                <div class="col-lg-10">
                    <input type="text" id="apellido" name="apellido" class="form-control input-sm" placeholder="Ingresa el apellido" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Genero</label>
                <div class="col-lg-10">
                    <select class="form-control" name="sexoSolicitud">
                        <option value="1">Hombre</option>
                        <option value="0">Mujer</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Edad</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="edadSolicitud" placeholder="Edad" data-parsley-required="true" data-parsley-type="number">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Fecha de nacimiento</label>
                <div class="col-lg-10">
                    <input type="text" name="nacimientoSolicitud" placeholder="Fecha de nacimiento" 
                            class="datepicker-input form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Estado civil</label>
                <div class="col-lg-10">
                    <select class="form-control" name="estadoCivilSolicitud">
                        <option value="1">Soltero</option>
                        <option value="2">Casado</option>
                        <option value="3">Viudo</option>
                        <option value="4">Divorciado</option>
                        <option value="5">Prefiero no indicar</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Pais</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="paisSolicitud" id="paisSolicitud" data-parsley-required="true">
                        <option value="">-- Seleccionar pais --</option>
                        <?php foreach ($paises as $key) { ?>
                            <option value="<?= $key->PaisCod ?>"><?=$key->PaisNom?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Provincia</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="provinciaSolicitud" id="provinciaSolicitud" data-parsley-required="true">
                        <option value="">-- Seleccionar provincia --</option>
                    </select>
                    <input type="hidden" id="provinciaHidden" value="0">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Localidad</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="localidadSolicitud" id="localidadSolicitud" data-parsley-required="true">
                        <option value="">-- Seleccionar localidad --</option>
                    </select>
                    <input type="hidden" id="localidadHidden" value="0">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Profesion u oficio</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="profesionSolicitud" data-parsley-required="true">
                        <option value="">-- Seleccionar profesión u oficio --</option>
                        <?php foreach ($profesiones as $key) { ?>
                            <option value="<?= $key->profesion_id ?>"><?= $key->nombre_profesion ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Número de Colegio Médico del Perú (CMP)</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="cmp" placeholder="Número de Colegio Médico del Perú (CMP)">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Registro Nacional de Especialista (RNE)</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="rne" placeholder="Registro Nacional de Especialista (RNE)">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Clinica(s)</label>
                <div class="col-lg-10">
                    <select id="clinicas" multiple class="select2 width-100" placeholder="clinicas">
                        <optgroup label="Clinicas">
                        <?php foreach ($clinicas as $key) { ?>
                            <option value="<?= $key->clinica_id ?>"><?= $key->nombre_clinica ?></option>
                        <?php } ?>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div id="c-add-clinica" class="col-md-12">
                    <div style="padding:0px;" class="col-md-3"></div>
                    <div style="padding:0px;" class="col-md-4">
                        <input id="nueva_clinica" data-parsley-excluded="" class="form-control" data-parsley-id="5973" type="text"><ul class="parsley-errors-list" id="parsley-id-5973"></ul>
                    </div>
                    <div style="padding:0px;" class="col-md-2">
                        <button data-parsley-excluded="" class="btn btn-success" id="agregar_clinica2">Agregar Clinica</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Ejercicio(s) a dosificar</label>
                <div class="col-lg-10">
                    <select id="ejercicios" multiple class="select2 width-100" placeholder="Selecciona los ejercicios">
                        <optgroup label="Ejercicios">
                        <?php foreach ($ejercicios as $r) { ?>
                            <option value="<?= $r->id_ejercicio ?>"><?= $r->nombre ?></option>
                        <?php } ?>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Email</label>
                <div class="col-lg-10">
                    <input type="text" id="usuario" name="usuario" class="form-control input-sm" placeholder="Ingresa el email" data-parsley-required="true" data-parsley-type="email">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Contraseña</label>
                <div class="col-lg-10">
                    <input type="password" id="pass" name="pass" class="form-control input-sm" placeholder="Ingresa la contraseña" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Confirmar contraseña</label>
                <div class="col-lg-10">
                    <input type="password" id="pass2" name="pass2" class="form-control input-sm" placeholder="Confirma la contraseña" data-parsley-required="true" data-parsley-equalto="#pass">
                </div>
            </div>
            <div class="text-right m-top-md">
                <input type="submit" class="btn btn-info" value="Registrar">
            </div>
        </form>
    </div>
</div><!-- ./padding-md -->
<!-- Moment -->
<script src='<?= base_url() ?>public/js/uncompressed/moment.js'></script>
<!-- Date Time picker -->
<script src='<?= base_url() ?>public/js/uncompressed/bootstrap-datetimepicker.js'></script>
<!-- Dropzone -->
<script src='<?= base_url() ?>public/js/dropzone.min.js'></script>
<!-- Moment -->
<script src='<?= base_url() ?>public/js/uncompressed/moment.js'></script>
<script src="<?= base_url() ?>public/js/select2.min.js"></script>
<script src="<?= base_url() ?>public/js/parsley.min.js"></script>
<script src="<?= base_url() ?>public/js/doctor.js"></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>
