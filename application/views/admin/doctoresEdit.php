<?php
$clinicaarray = array();
$ejerciciosarray = array();
$c = explode(",",$doctor->clinicas);
$e = explode(",",$doctor->ejercicios);
foreach ($c as $key) {
    $clinicaarray[$key] = $key;
}
foreach ($e as $k) {
    $ejerciciosarray[$k] = $k;
}
?>
<!-- Date Time Picker -->
<link href="<?= base_url() ?>public/css/datetimepicker.css" rel="stylesheet">
<!-- Dropzone -->
<link href="<?= base_url() ?>public/css/dropzone/css/dropzone.css" rel="stylesheet">
<link href="<?= base_url() ?>public/css/solicitudes.css" rel="stylesheet">
<link href="<?= base_url() ?>public/css/select2/select2.css" rel="stylesheet"/>
<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Gestion</li>
        <li><a href="<?= base_url() ?>administrador/doctores">Doctores</a></li>
        <li>Editar Doctor</li> 
    </ul>
    <div class="row">
        <form id="form-edit-doctor" class="form-horizontal no-margin" style="padding:5%;">
            <div class="form-group">
                <label class="control-label col-lg-2">Foto</label>
                <div class="col-lg-10">
                    <div class="col-lg-1">
                        <img src="../../public/images/profile/<?=$doctor->imagen?>" 
                        style="width:54px;height:54px;">
                    </div>
                    <div class="col-lg-5">
                        <div class="dropzone" id="fileImagen">
                            <div class="fallback">
                                <input name="file" type="file" />
                            </div>
                        </div>
                        <input type="hidden" id="renombrado" name="renombrado">
                        <input type="hidden" id="oldImage" name="oldImage" value="<?=$doctor->imagen?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">DNI</label>
                <div class="col-lg-10">
                    <input type="text" id="dni" name="dni" value="<?=$doctor->dni ?>" class="form-control input-sm" placeholder="Ingresa el DNI" data-parsley-required="true" data-parsley-type="number">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Nombre</label>
                <div class="col-lg-10">
                    <input type="text" id="nombre" name="nombre" value="<?= $doctor->nombre ?>" class="form-control input-sm" placeholder="Ingresa el nombre" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Apellido</label>
                <div class="col-lg-10">
                    <input type="text" id="apellido" name="apellido" value="<?= $doctor->apellido ?>" class="form-control input-sm" placeholder="Ingresa el apellido" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Genero</label>
                <div class="col-lg-10">
                    <select class="form-control" name="sexoSolicitud">
                        <option value="1" <?php if($doctor->sexo==1){ ?> selected <?php } ?>>Hombre</option>
                        <option value="0" <?php if($doctor->sexo==0){ ?> selected <?php } ?>>Mujer</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Edad</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="edadSolicitud" placeholder="Edad" data-parsley-required="true" data-parsley-type="number" value="<?=$doctor->edad?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Fecha de nacimiento</label>
                <?php list($anio,$mes,$dia)=explode("-",trim($doctor->nacimiento)); ?>
                <div class="col-lg-10">
                    <input type="text" name="nacimientoSolicitud" placeholder="Fecha de nacimiento" 
                            class="datepicker-input form-control" value="<?=$dia.'-'.$mes.'-'.$anio?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Estado civil</label>
                <div class="col-lg-10">
                    <select class="form-control" name="estadoCivilSolicitud">
                        <option value="1" <?php if($doctor->estado_civil==1){ ?> selected <?php } ?>>Soltero</option>
                        <option value="2" <?php if($doctor->estado_civil==2){ ?> selected <?php } ?>>Casado</option>
                        <option value="3" <?php if($doctor->estado_civil==3){ ?> selected <?php } ?>>Viudo</option>
                        <option value="4" <?php if($doctor->estado_civil==4){ ?> selected <?php } ?>>Divorciado</option>
                        <option value="5" <?php if($doctor->estado_civil==5){ ?> selected <?php } ?>>Prefiero no indicar</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Pais</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="paisSolicitud" id="paisSolicitud" data-parsley-required="true">
                        <option value="0">-- Seleccionar pais --</option>
                        <?php foreach ($paises as $key) { ?>
                            <option value="<?= $key->PaisCod ?>" 
                            <?php if($doctor->id_pais==$key->PaisCod){ ?> selected <?php } ?>>
                            <?=$key->PaisNom?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Provincia</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="provinciaSolicitud" id="provinciaSolicitud" data-parsley-required="true">
                        <option value="<?=$doctor->id_provincia?>"><?=$provincia->ProvinNom?></option>
                    </select>
                    <input type="hidden" id="provinciaHidden" value="<?=$doctor->id_provincia?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Localidad</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="localidadSolicitud" id="localidadSolicitud" data-parsley-required="true">
                        <option value="<?=$doctor->id_localidad?>"><?=$localidad->LocalidNom?></option>
                    </select>
                    <input type="hidden" id="localidadHidden" value="<?=$doctor->id_localidad?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Profesion</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="profesionSolicitud" data-parsley-required="true">
                        <option value="">-- Seleccionar profesión u oficio --</option>
                        <?php foreach ($profesiones as $key) { ?>
                            <option value="<?= $key->profesion_id ?>"
                            <?php if($doctor->profesion==$key->profesion_id){ ?> selected <?php } ?>>
                            <?= $key->nombre_profesion ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Número de Colegio Médico del Perú (CMP)</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="cmp" placeholder="Número de Colegio Médico del Perú (CMP)" value="<?= $doctor->cmp ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Registro Nacional de Especialista (RNE)</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="rne" placeholder="Registro Nacional de Especialista (RNE)" value="<?= $doctor->rne ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Clinica(s)</label>
                <div class="col-lg-10">
                    <select id="clinicas" multiple class="select2 width-100" placeholder="clinicas">
                        <optgroup label="Clinicas">
                        <?php foreach ($clinicas as $key) { ?>
                            <option value="<?= $key->clinica_id ?>" <?= isset($clinicaarray[$key->clinica_id])?'selected="selected"':"" ?> ><?= $key->nombre_clinica ?></option>
                        <?php } ?>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div id="c-add-clinica" class="col-md-12">
                    <div style="padding:0px;" class="col-md-3"></div>
                    <div style="padding:0px;" class="col-md-4">
                        <input id="nueva_clinica" data-parsley-excluded="" class="form-control" data-parsley-id="5973" type="text"><ul class="parsley-errors-list" id="parsley-id-5973"></ul>
                    </div>
                    <div style="padding:0px;" class="col-md-2">
                        <button data-parsley-excluded="" class="btn btn-success" id="agregar_clinica2">Agregar Clinica</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Ejercicio(s) a dosificar</label>
                <div class="col-lg-10">
                    <select id="ejercicios" multiple class="select2 width-100" placeholder="Selecciona los ejercicios">
                        <optgroup label="Ejercicios">
                        <?php foreach ($ejercicios as $r) { ?>
                            <option value="<?= $r->id_ejercicio ?>" <?= isset($ejerciciosarray[$r->id_ejercicio])?'selected="selected"':"" ?> ><?= $r->nombre ?></option>
                        <?php } ?>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Email</label>
                <div class="col-lg-10">
                    <input type="text" id="usuario" name="usuario" value="<?= $doctor->email_user ?>" class="form-control input-sm" placeholder="Ingresa el email" data-parsley-required="true" data-parsley-type="email">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Contraseña</label>
                <div class="col-lg-10">
                    <input type="password" id="pass" name="pass" value="<?= $doctor->pass_user ?>" class="form-control input-sm" placeholder="Ingresa la contraseña" data-parsley-required="true">
                </div>
            </div>
            <div class="text-right m-top-md">
                <input type="submit" class="btn btn-info" value="Actualizar">
                <a href="<?=base_url()?>administrador/doctores" class="btn btn-default">Atras</a>
            </div>
            <input type="hidden" id="doctor" name="doctor" value="<?= $doctor->id_doctor ?>">
            <input type="hidden" id="id" name="id" value="<?= $doctor->id_user ?>">
        </form>
    </div>
</div><!-- ./padding-md -->
<!-- Moment -->
<script src='<?= base_url() ?>public/js/uncompressed/moment.js'></script>
<!-- Date Time picker -->
<script src='<?= base_url() ?>public/js/uncompressed/bootstrap-datetimepicker.js'></script>
<!-- Dropzone -->
<script src='<?= base_url() ?>public/js/dropzone.min.js'></script>
<!-- Moment -->
<script src='<?= base_url() ?>public/js/uncompressed/moment.js'></script>
<script src="<?= base_url() ?>public/js/select2.min.js"></script>
<script src="<?= base_url() ?>public/js/parsley.min.js"></script>
<script src="<?= base_url() ?>public/js/doctor.js"></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>
