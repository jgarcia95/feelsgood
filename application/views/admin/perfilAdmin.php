<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Perfil</li>
    </ul>
    <div class="row">
        <form id="form-edit-admin" class="form-horizontal no-margin" style="padding:5%;">
            <div class="form-group">
                <label class="control-label col-lg-2">Nombre</label>
                <div class="col-lg-10">
                    <input type="text" id="nombre" name="nombre" value="<?= $usuario->nombre ?>" class="form-control input-sm" placeholder="Ingresa el nombre" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Apellido</label>
                <div class="col-lg-10">
                    <input type="text" id="apellido" name="apellido" value="<?= $usuario->apellido ?>" class="form-control input-sm" placeholder="Ingresa el apellido" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Email</label>
                <div class="col-lg-10">
                    <input type="text" id="usuario" name="usuario" value="<?= $usuario->email_user ?>" class="form-control input-sm" placeholder="Ingresa el email" data-parsley-required="true" data-parsley-type="email">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Contraseña</label>
                <div class="col-lg-10">
                    <input type="password" id="pass" name="pass" value="<?= $usuario->pass_user ?>" class="form-control input-sm" placeholder="Ingresa la contraseña" data-parsley-required="true">
                </div>
            </div>
            <div class="text-right m-top-md">
                <input type="submit" class="btn btn-info" value="Actualizar">
                <a href="<?=base_url()?>administrador" class="btn btn-default">Atras</a>
            </div>
            <input type="hidden" name="id_administrador" value="<?= $usuario->id_administrador ?>">
            <input type="hidden" name="id_user" value="<?= $usuario->id_user ?>">
        </form>
    </div>
</div><!-- ./padding-md -->
<script src="<?= base_url() ?>public/js/parsley.min.js"></script>
<script src="<?= base_url() ?>public/js/administradores.js"></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>