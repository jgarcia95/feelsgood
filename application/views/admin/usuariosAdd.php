<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Creación de usuarios</li>
        <li>Administrador</li> 
    </ul>
    <div class="row">
        <form id="form-add-admin" class="form-horizontal no-margin" style="padding:5%;">
            <div class="form-group">
                <label class="control-label col-lg-2">Nombre</label>
                <div class="col-lg-10">
                    <input type="text" id="nombre" name="nombre" class="form-control input-sm" placeholder="Ingresa el nombre" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Apellido</label>
                <div class="col-lg-10">
                    <input type="text" id="apellido" name="apellido" class="form-control input-sm" placeholder="Ingresa el apellido" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Email</label>
                <div class="col-lg-10">
                    <input type="text" id="usuario" name="usuario" class="form-control input-sm" placeholder="Ingresa el email" data-parsley-required="true" data-parsley-type="email">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Contraseña</label>
                <div class="col-lg-10">
                    <input type="password" id="pass" name="pass" class="form-control input-sm" placeholder="Ingresa la contraseña" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Confirmar contraseña</label>
                <div class="col-lg-10">
                    <input type="password" id="pass2" name="pass2" class="form-control input-sm" placeholder="Confirma la contraseña" data-parsley-required="true" data-parsley-equalto="#pass">
                </div>
            </div>
            <div class="text-right m-top-md">
                <input type="submit" class="btn btn-info" value="Registrar">
            </div>
        </form>
    </div>
</div><!-- ./padding-md -->
<script src="<?= base_url() ?>public/js/parsley.min.js"></script>
<script src="<?= base_url() ?>public/js/administradores.js"></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>