<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Gestion</li>
        <li><a href="<?= base_url() ?>solicitudes">Solicitudes</a></li>
        <li>Detalle solicitud paciente</li>
    </ul>
		<div class="wrapper no-navigation preload">
				<div class="col-md-6 form-horizontal">
					<div class="form-group">
						<label class="control-label">Datos del paciente</label>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Foto</label>
						<div class="col-lg-9">
							<p class="form-control-static">
								<?php if($paciente[0]->imagen!=""){?>
                                    <img src="../../../public/images/profile/<?=$paciente[0]->imagen?>" 
                                    style="width:50px;height:50px;">
                                <?php } ?>
							</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">DNI</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->dni?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Nombres</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->nombres?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Apellidos</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->apellidos?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Genero</label>
						<div class="col-lg-9">
							<p class="form-control-static">
								<?php if($paciente[0]->sexo==1){ echo "Hombre";}
								else{ echo "Mujer";}?>
							</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Edad</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->edad?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Fecha de nacimiento</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->nacimiento?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Estado civil</label>
						<div class="col-lg-9">
							<p class="form-control-static">
								<?php switch($paciente[0]->estado_civil){
									case '1':echo "Soltero (a)";break;
									case '2':echo "Casado (a)";break;
									case '3':echo "Viudo (a)";break;
									case '4':echo "Divorciado (a)";break;
									case '5':echo "Prefiero no indicar";break;
								}?>
							</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Grupo sanguineo</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->grupo_sanguineo?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Pais</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$pais->PaisNom?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Provincia</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$prov->ProvinNom?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Localidad</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$locali->LocalidNom?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Profesión</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?php if($prof){ echo $prof->nombre_profesion; }?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Clinica</label>
						<div class="col-lg-9">
							<p class="form-control-static">
								<?= ($clinica) ? $clinica->nombre_clinica : ""?>
							</p>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">Datos del acompañante</label>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">DNI</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->dni_acom?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Nombres</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->nombres_acom?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Apellidos</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->apellidos_acom?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Edad</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->edad_acom?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Teléfono</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->fono_acom?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Profesión</label>
						<div class="col-lg-9">
							<p class="form-control-static">
								<?php if($prof_acom){ echo $prof_acom->nombre_profesion;}?>
							</p>
						</div>
					</div>
				</div><!-- ./segmento izquiero -->
				<div class="col-md-6 form-horizontal">
					<div class="form-group">
						<label class="control-label">Antecedentes del paciente</label>
					</div>
					<?php $array_enf_personales=array();$i=1;
					foreach (explode(",",$paciente[0]->enf_personales) as $enf) {
						$array_enf_personales[$i]=$enf;
						$i++;
					}
					foreach ($enf_personales as $key) { ?>
					<div class="form-group col-md-6">
						<div class="custom-checkbox" style="width: auto;">
							<input type="checkbox" 
							<?php if(array_search($key->id_enfermedad,$array_enf_personales)){ ?>
							 checked <?php } ?> >
							<label></label>
							<span style="padding-left: 12px;color: #000;"><?=$key->nombre?></span>
						</div>
					</div>
					<?php } ?>
					<div class="form-group">
						<label class="col-lg-3 control-label">Otros</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->otros_personales?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">Antecedentes familiares</label>
					</div>
					<?php $array_enf_familiares=array();$j=1;
					foreach (explode(",",$paciente[0]->enf_familiares) as $enf) {
						$array_enf_familiares[$j]=$enf;
						$j++;
					}
					foreach ($enf_familiares as $key) { ?>
					<div class="form-group col-md-6">
						<div class="custom-checkbox" style="width: auto;">
							<input type="checkbox" 
							<?php if(array_search($key->id_enfermedad,$array_enf_familiares)){ ?>
							 checked <?php } ?> >
							<label></label>
							<span style="padding-left: 12px;color: #000;"><?=$key->nombre?></span>
						</div>
					</div>
					<?php } ?>
					<div class="form-group">
						<label class="col-lg-3 control-label">Otros</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->otros_familiares?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Observaciones</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->observaciones?></p>
						</div>
					</div>
					<div class="form-group" style="padding-top: 8px;">
						<label class="control-label">Información de usuario</label>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Email</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->email?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Teléfono</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->fono?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Password</label>
						<div class="col-lg-9">
							<p class="form-control-static"><?=$paciente[0]->pass?></p>
						</div>
					</div>
				</div><!--segmento derecho-->
		</div><!-- /wrapper -->
	</div>