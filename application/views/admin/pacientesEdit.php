<!-- Date Time Picker -->
<link href="<?= base_url() ?>public/css/datetimepicker.css" rel="stylesheet">
<!-- Dropzone -->
<link href="<?= base_url() ?>public/css/dropzone/css/dropzone.css" rel="stylesheet">
<link href="<?= base_url() ?>public/css/solicitudes.css" rel="stylesheet">
<link href="<?= base_url() ?>public/css/select2/select2.css" rel="stylesheet"/>
<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Gestion</li>
        <li><a href="<?= base_url() ?>/pacientes">Pacientes</a></li>
        <li>Editar Pacientes</li>
    </ul>
    <div class="row">
        <form id="form-modificar-paciente">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Datos del paciente</label>
                </div>
                <div class="form-group" style="overflow: hidden;">
                    <label class="control-label col-lg-2">Foto</label>
                    <div class="col-lg-10" style="padding:0px;">
                        <div class="col-lg-3">
                            <?php if($paciente->imagen!=""){?>
                            <img src="<?= base_url() ?>public/images/profile/<?=$paciente->imagen?>" 
                            style="width:50px;height:50px;">
                            <?php } ?>
                        </div>
                        <div class="col-lg-9" style="padding:0px;">
                            <div class="dropzone" id="fileImagen">
                                <div class="fallback">
                                    <input name="file" type="file" />
                                </div>
                            </div>
                            <input type="hidden" id="renombrado" name="renombrado">
                            <input type="hidden" id="oldImage" name="oldImage" value="<?=$paciente->imagen?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" value="<?=$paciente->dni?>" name="dniSolicitud" placeholder="DNI" data-parsley-required="true" data-parsley-type="number">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" value="<?=$paciente->nombre?>" name="nombreSolicitud" placeholder="Nombres" data-parsley-required="true">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" value="<?=$paciente->apellido?>" name="apellidosSolicitud" placeholder="Apellidos" data-parsley-required="true">
                </div>
                <div class="form-group">
                    <select class="form-control" name="sexoSolicitud">
                        <option value="1" <?= ($paciente->sexo==1)?"selected":"" ?>>Hombre</option>
                        <option value="0" <?= ($paciente->sexo==0)?"selected":"" ?>>Mujer</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" value="<?=$paciente->edad?>" class="form-control" name="edadSolicitud" placeholder="Edad" data-parsley-required="true" data-parsley-type="number">
                </div>
                <div class="form-group">
                    <input type="text" value="<?=$paciente->fono?>" class="form-control" name="fonoSolicitud" placeholder="Teléfono" data-parsley-type="number">
                </div>
                <div class="form-group">
                    <input type="text" value="<?php if($paciente->nacimiento){ echo date("d-m-Y", strtotime($paciente->nacimiento)); }else{ echo date("d-m-Y"); } ?>" name="nacimientoSolicitud" placeholder="Fecha de nacimiento" 
                    class="datepicker-input form-control">
                </div>
                <div class="form-group">
                    <select class="form-control" name="estadoCivilSolicitud">
                        <option value="1" <?= ($paciente->estado_civil==1)?"selected":"" ?>>Soltero</option>
                        <option value="2" <?= ($paciente->estado_civil==2)?"selected":"" ?>>Casado</option>
                        <option value="3" <?= ($paciente->estado_civil==3)?"selected":"" ?>>Viudo</option>
                        <option value="4" <?= ($paciente->estado_civil==4)?"selected":"" ?>>Divorciado</option>
                        <option value="5" <?= ($paciente->estado_civil==5)?"selected":"" ?>>Prefiero no indicar</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" value="<?= $paciente->grupo_sanguineo ?>" name="grupoSolicitud" placeholder="Grupo sanguineo" 
                    class="form-control">
                </div>
                <div class="form-group">
                    <select class="select2 width-100" name="paisSolicitud" id="paisSolicitud" data-parsley-required="true">
                        <option value="">-- Seleccionar pais --</option>
                        <?php foreach ($paises as $key) { ?>
                            <option value="<?= $key->PaisCod ?>" <?= ($paciente->id_pais==$key->PaisCod)?"selected":"" ?>><?=$key->PaisNom?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <select class="select2 width-100" name="provinciaSolicitud" id="provinciaSolicitud" data-parsley-required="true">
                        <option value="<?=$paciente->id_provincia?>"><?php if(isset($provincia->ProvinNom)){ echo $provincia->ProvinNom; }else{echo"--Seleccione--";}?></option>
                    </select>
                </div>
                <div class="form-group">
                    <select class="select2 width-100" name="localidadSolicitud" id="localidadSolicitud" data-parsley-required="true">
                        <option value="<?=$paciente->id_localidad?>"><?php if(isset($localidad->LocalidNom)){ echo $localidad->LocalidNom;} else{ echo"--Seleccione--";}?></option>
                    </select>
                </div>
                <div class="form-group">
                    <select class="select2 width-100" name="profesionSolicitud">
                        <option value="">-- Seleccionar profesión --</option>
                        <?php foreach ($profesiones as $key) { ?>
                            <option value="<?= $key->profesion_id ?>" <?= ($paciente->profesion==$key->profesion_id)?"selected":"" ?>><?= $key->nombre_profesion ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group" id="content-clinica" style="position:relative;">
                    <select id="clinica" name="clinica" class="select2 width-100">
                        <?php foreach ($clinicas as $key) { ?>
                            <option value="<?= $key->clinica_id ?>" <?= ($key->clinica_id==$paciente->id_clinica)? "selected":"" ?> ><?= $key->nombre_clinica ?></option>
                        <?php } ?>
                    </select>
                    <i id="img-edit" style="position: absolute;right: -35px;top: 6px;font-size: 23px;cursor: pointer;display: none;" class="fa fa-pencil-square-o fa-lg fa-fw" aria-hidden="true"></i>
                </div>
                <div class="form-group" style="position:relative;">
                    <div id="c-add-clinica" class="col-md-12" style="padding:0px;margin-bottom: 20px;">
                        <div class="col-md-8" style="padding:0px;">
                            <input id="nueva_clinicap" data-parsley-excluded type="text" class="form-control">
                        </div>
                        <div class="col-md-4" style="padding:0px;"><button data-parsley-excluded class="btn btn-success" id="agregar_clinica2p">Crear Clínica</button></div>
                    </div>
                    <div id="c-edit-clinica" class="col-md-12" style="padding:0px;margin-bottom:20px;display:none;">
                        <div class="col-md-4" style="padding:0px;"><button class="btn btn-success" id="agregar_clinica3p">Editar Clinica</button></div>
                        <div class="col-md-8" style="padding:0px;">
                            <input id="nueva_clinica2p" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Datos del acompañante</label>
                </div>
                <div class="form-group">
                    <input type="text" value="<?= $paciente->dni_acom ?>" class="form-control" name="dniAcomSolicitud" placeholder="DNI" data-parsley-required="true" data-parsley-type="number">
                </div>
                <div class="form-group">
                    <input type="text" value="<?= $paciente->nombres_acom ?>" class="form-control" name="nombreAcomSolicitud" placeholder="Nombres" data-parsley-required="true">
                </div>
                <div class="form-group">
                    <input type="text" value="<?= $paciente->apellidos_acom ?>" class="form-control" name="apellidosAcomSolicitud" placeholder="Apellidos" data-parsley-required="true">
                </div>
                <div class="form-group">
                    <input type="text" value="<?= $paciente->edad_acom ?>" class="form-control" name="edadAcomSolicitud" placeholder="Edad" data-parsley-required="true" data-parsley-type="number">
                </div>
                <div class="form-group">
                    <input type="text" value="<?=$paciente->fono_acom?>" class="form-control" name="fonoAcomSolicitud" placeholder="Teléfono" data-parsley-type="number">
                </div>
                <!--<div class="form-group">
                    <select class="select2 width-100" name="profesionAcomSolicitud">
                        <option value="">-- Seleccionar profesión --</option>
                        <?php foreach ($profesiones as $key) { ?>
                            <option value="<?= $key->profesion_id ?>" <?= ($paciente->profesion_acom==$key->profesion_id)?"selected":"" ?> ><?= $key->nombre_profesion ?></option>
                        <?php } ?>
                    </select>
                </div>-->
            </div><!-- ./segmento izquiero -->
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Antecedentes del paciente</label>
                </div>
                <?php
                    $eper = $paciente->enf_personales;
                    $arrayeper = explode(",",$eper);
                    $arrayeperc = array();
                    foreach ($arrayeper as $key) {
                        $arrayeperc[$key] = $key;
                    }
                ?>
                <?php foreach ($enf_personales as $key) { ?>
                <div class="form-group col-md-6">
                    <div class="custom-checkbox" style="width: auto;">
                        <input type="checkbox" <?= (isset($arrayeperc[$key->id_enfermedad]))? "checked":"" ?> id="personales<?=$key->id_enfermedad?>" 
                            value="<?=$key->id_enfermedad?>" name="personales[]">
                        <label for="personales<?=$key->id_enfermedad?>"></label>
                        <span style="padding-left: 12px;color: #000;"><?=$key->nombre?></span>
                    </div>
                </div>
                <?php } ?>
                <div class="form-group">
                    <input type="text" value="<?=$paciente->otros_personales ?>" class="form-control" name="otros_personales" placeholder="Otros antecedentes personales">
                </div>
                <div class="form-group">
                    <label class="control-label">Antecedentes familiares</label>
                </div>
                <?php
                    $efam = $paciente->enf_familiares;
                    $arrayefam = explode(",",$efam);
                    $arrayefamc = array();
                    foreach ($arrayefam as $key) {
                        $arrayefamc[$key] = $key;
                    }
                ?>
                <?php foreach ($enf_familiares as $key) { ?>
                <div class="form-group col-md-6">
                    <div class="custom-checkbox" style="width: auto;">
                        <input type="checkbox" <?= (isset($arrayefamc[$key->id_enfermedad]))? "checked":"" ?> id="familiares<?=$key->id_enfermedad?>" 
                        value="<?=$key->id_enfermedad?>" name="familiares[]">
                        <label for="familiares<?=$key->id_enfermedad?>"></label>
                        <span style="padding-left: 12px;color: #000;"><?=$key->nombre?></span>
                    </div>
                </div>
                <?php } ?>
                <div class="form-group">
                    <input type="text" value="<?=$paciente->otros_familiares ?>" class="form-control" name="otros_familiares" placeholder="Otros antecedentes familiares">
                </div>
                <div class="form-group">
                    <input type="text" value="<?= $paciente->observaciones ?>" class="form-control" name="observSolicitud" placeholder="Observaciones">
                </div>
                <div class="form-group" style="padding-top: 8px;">
                    <label class="control-label">Información de usuario</label>
                </div>
                <div class="form-group">
                    <input type="text" value="<?= $paciente->email_user ?>" class="form-control" name="userSolicitud" placeholder="Email Address" data-parsley-required="true">
                </div>
                <div class="form-group">
                    <input type="password" value="<?= $paciente->pass_user ?>" class="form-control" name="passSolicitud" placeholder="Password" id="password" data-parsley-required="true">
                </div>
                <!--<div class="form-group">
                    <select class="form-control" name="controlSolicitud">
                        <option value="">-- Tipo de control --</option>
                        <option value="Visor" <?= ($paciente->control=="Visor")?"selected":"" ?>>Visor</option>
                        <option value="Mando" <?= ($paciente->control=="Mando")?"selected":"" ?>>Mando</option>
                    </select>
                </div>-->
            </div><!--segmento derecho-->
            <div class="text-right m-top-md">
                <input type="submit" class="btn btn-info" value="Actualizar">
                <a href="<?=base_url()?>administrador/pacientes" class="btn btn-default">Atras</a>
            </div>
            <input type="hidden" id="paciente" name="paciente" value="<?= $paciente->id_paciente ?>">
            <input type="hidden" id="id" name="id" value="<?php if( isset($paciente->id_user) ){ echo $paciente->id_user; } else{} ?>">
        </form>
        <input type="hidden" id="provincia_paciente_edit" value="<?= $paciente->id_provincia ?>">
        <input type="hidden" id="localidad_paciente_edit" value="<?= $paciente->id_localidad ?>">
    </div>
</div><!-- ./padding-md -->
<!-- Moment -->
<script src='<?= base_url() ?>public/js/uncompressed/moment.js'></script>
<!-- Date Time picker -->
<script src='<?= base_url() ?>public/js/uncompressed/bootstrap-datetimepicker.js'></script>
<!-- Dropzone -->
<script src='<?= base_url() ?>public/js/dropzone.min.js'></script>
<!-- Moment -->
<script src='<?= base_url() ?>public/js/uncompressed/moment.js'></script>
<script src="<?= base_url() ?>public/js/select2.min.js"></script>
<script src="<?= base_url() ?>public/js/parsley.min.js"></script>
<script src="<?= base_url() ?>public/js/pacientes.js"></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>