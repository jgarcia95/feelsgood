                <div id="loading">
                    <i class="fa fa-spinner fa-spin m-right-xs"></i>Loading
                </div>
            </div><!-- /main-container -->
            <footer class="footer">
                <p class="no-margin">&copy; 2017 <strong>FeelsGood</strong>. ALL Rights Reserved.</p>
            </footer>
        </div><!-- /wrapper -->
        <div id="modalMessage" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <p id="messagemodal"></p>
                        <button type="button" class="btn" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="scroll-to-top hidden-print"><i class="fa fa-chevron-up fa-lg"></i></a>
        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <!-- Bootstrap -->
        <script src="<?= base_url() ?>public/bootstrap/js/bootstrap.min.js"></script>
        <!-- Slimscroll -->
        <script src='<?= base_url() ?>public/js/jquery.slimscroll.min.js'></script>
        <!-- Morris -->
        <script src='<?= base_url() ?>public/js/rapheal.min.js'></script>
        <script src='<?= base_url() ?>public/js/morris.min.js'></script>
        <!-- Sparkline -->
        <script src='<?= base_url() ?>public/js/sparkline.min.js'></script>
        <!-- Skycons -->
        <script src='<?= base_url() ?>public/js/uncompressed/skycons.js'></script>
        <!-- Popup Overlay -->
        <script src='<?= base_url() ?>public/js/jquery.popupoverlay.min.js'></script>
        <!-- Sortable -->
        <script src='<?= base_url() ?>public/js/uncompressed/jquery.sortable.js'></script>
        <!-- Modernizr -->
        <script src='<?= base_url() ?>public/js/modernizr.min.js'></script>
        <!-- Simplify -->
        <script src="<?= base_url() ?>public/js/simplify/simplify.js"></script>
    </body>
</html>