<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Gestion</li>
        <li>Solicitudes</li>   
    </ul>
    <div class="smart-widget">
        <div class="smart-widget-inner">
            <ul class="nav nav-tabs tab-style1">
                <li class="active">
                    <a href="#style1Tab1" data-toggle="tab">Doctores</a>
                </li>
                <li>
                    <a href="#style1Tab2" data-toggle="tab">Clinicas</a>
                </li>
                <li>
                    <a href="#style1Tab3" data-toggle="tab">Pacientes</a>
                </li>
            </ul>
            <div class="smart-widget-body">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="style1Tab1">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="lista-solicitudes-doctores">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>DNI</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Edad</th>
                                        <th>Email</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($solicitudesDoctores as $key) { ?>
                                        <tr>
                                            <td><?php if($key->imagen!=""){?>
                                            <img src="public/images/profile/<?=$key->imagen?>" 
                                            style="width:50px;height:50px;">
                                            <?php } ?></td>
                                            <td><?= $key->dni ?></td>
                                            <td><?= $key->nombres ?></td>
                                            <td><?= $key->apellidos ?></td>
                                            <td><?= $key->edad ?></td>
                                            <td><?= $key->email ?></td>
                                            <td>
                                                <a title="Ver detalle" href="<?= base_url() ?>solicitudes/detalle/<?= $key->solicitud_id ?>/2" target="_blank"><button class="btn btn-default btn-xs"><i class="fa fa-eye fa-lg fa-fw" aria-hidden="true"></i></button></a>
                                                <a title="Aceptar Solicitud" data-toggle="modal" href="#" data-target="#yesModal" class="openModalSolicitud" data-tipo="1" data-id="<?=$key->solicitud_id;?>" data-perfil="2"><button class="btn btn-default btn-xs"><i class="fa fa-check fa-lg fa-fw" aria-hidden="true"></i></button></a>
                                                <a title="Rechazar solicitud" data-toggle="modal" href="#" data-target="#noModal" class="openModalSolicitud" data-tipo="2" data-id="<?=$key->solicitud_id;?>" data-perfil="2"><button class="btn btn-default btn-xs"><i class="fa fa-times fa-lg fa-fw" aria-hidden="true"></i></button></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- ./tab-pane -->
                    <div class="tab-pane fade" id="style1Tab2">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="lista-solicitudes-clinicas">
                                <thead>
                                    <tr>
                                    	<th>RUC</th>
                                        <th>Nombre</th>
                                        <th>Email</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($solicitudesClinicas as $keyC) { ?>
                                        <tr>
                                            <td><?= $keyC->ruc ?></td>
                                            <td><?= $keyC->nombre ?></td>
                                            <td><?= $keyC->email ?></td>
                                            <td>
                                                <a title="Ver detalle" href="<?= base_url() ?>solicitudes/detalle/<?= $keyC->solicitud_id ?>/4" target="_blank"><i class="fa fa-eye fa-lg fa-fw" aria-hidden="true"></i></a>
                                                <a title="Aceptar Solicitud" data-toggle="modal" href="#" data-target="#yesModal" class="openModalSolicitud" data-tipo="1" data-id="<?=$keyC->solicitud_id;?>" data-perfil="4"><i class="fa fa-check fa-lg fa-fw" aria-hidden="true"></i></a>
                                                <a title="Rechazar solicitud" data-toggle="modal" href="#" data-target="#noModal" class="openModalSolicitud" data-tipo="2" data-id="<?=$keyC->solicitud_id;?>" data-perfil="4"><i class="fa fa-times fa-lg fa-fw" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- ./tab-pane -->
                    <div class="tab-pane fade" id="style1Tab3">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="lista-solicitudes-pacientes">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>DNI</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Edad</th>
                                        <th>Email</th>
                                        <th>Teléfono</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($solicitudesPacientes as $key) { ?>
                                        <tr>
                                            <td><?php if($key->imagen!=""){?>
                                            <img src="public/images/profile/<?=$key->imagen?>" 
                                            style="width:50px;height:50px;">
                                            <?php } ?></td>
                                            <td><?= $key->dni ?></td>
                                            <td><?= $key->nombres ?></td>
                                            <td><?= $key->apellidos ?></td>
                                            <td><?= $key->edad ?></td>
                                            <td><?= $key->email ?></td>
                                            <td><?= $key->fono ?></td>
                                            <td>
                                                <a title="Ver detalle" href="<?= base_url() ?>solicitudes/detalle/<?= $key->solicitud_id ?>/3" target="_blank"><i class="fa fa-eye fa-lg fa-fw" aria-hidden="true"></i></a>
                                                <a title="Aceptar Solicitud" data-toggle="modal" href="#" data-target="#yesModal" class="openModalSolicitud" data-tipo="1" data-id="<?=$key->solicitud_id;?>" data-perfil="3"><i class="fa fa-check fa-lg fa-fw" aria-hidden="true"></i></a>
                                                <a title="Rechazar solicitud" data-toggle="modal" href="#" data-target="#noModal" class="openModalSolicitud" data-tipo="2" data-id="<?=$key->solicitud_id;?>" data-perfil="3"><i class="fa fa-times fa-lg fa-fw" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- ./tab-pane -->
                </div><!-- ./tab-content -->
            </div>
        </div>
    </div><!-- ./smart-widget -->
</div><!-- ./padding-md -->
<input type="hidden" id="id_solicitud">
<input type="hidden" id="tipo_solicitud">
<input type="hidden" id="perfil_solicitud">
<div class="modal fade" id="yesModal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Aceptar</h4>
            </div>
            <div class="modal-body">
                Aceptar solicitud?
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal" id="close_yesmodal">Cerrar</a>
                <a class="btn btn-primary procesar_solicitud">Aceptar</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="noModal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Rechazar</h4>
            </div>
            <div class="modal-body">
                Rechazar solicitud?
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal" id="close_nomodal">Cerrar</a>
                <a class="btn btn-primary procesar_solicitud">Rechazar</a>
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>public/css/dataTables.bootstrap.css" rel="stylesheet">
<script src='<?= base_url() ?>public/js/jquery.dataTables.min.js'></script>
<script src='<?= base_url() ?>public/js/uncompressed/dataTables.bootstrap.js'></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>
<script src="<?= base_url() ?>public/js/solicitudes.js"></script>