<div id="title_module">
    <h2><span class="s_icons icon_slider"></span>EDITAR PERFIL</h2>
    <p class="clear"></p>
</div>
<div class="maintenance">
    <form id="form" action="" data-action="" method="post">
        <div class="box_form">
            <p class="label">Nombre(*)</p>
            <p class="ptxts"><input type="text" id="nombre" name="nombre" value="<?= $perfil->nombre ?>" class="txts"></p>
        </div>
        <div class="btnsMaintenance">
            <p id="btns">
                <input type="submit" id="upd_btn" name="upd_btn" class="btns" value="Actualizar">
                <input type="button" id="back_btn" name="back_btn" class="btns" value="Atras">
            </p>
        </div>
        <input type="hidden" name="id" id="id" value="<?= $perfil->id_perfil ?>">
        <input type="hidden" id="modulo" value="administrador/perfiles">
    </form>
</div>
<script>
$(document).ready(function(){
    $("#form").submit(function(e){
        e.preventDefault();
        $.post(window.base_url+'perfil/edit',$("#form").serialize(),function(data){
            if(data.status){
                $("#back_btn").click();
            }else{
                gotravel.showError(data.msg);
            }
        },'json');
    });
});
</script>