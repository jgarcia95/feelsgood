<div class="padding-md">
    <link href="<?= base_url() ?>public/css/select2/select2.css" rel="stylesheet"/>
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Creación de usuarios</li>
        <li>Clinica</li> 
    </ul>
    <div class="row">
        <form id="form-add-clinica" class="form-horizontal no-margin" style="padding:5%;">
            <div class="form-group">
                <label class="control-label col-lg-2">RUC</label>
                <div class="col-lg-10">
                    <input type="text" id="ruc" name="ruc" class="form-control input-sm" placeholder="Ingresa el ruc" data-parsley-required="true" data-parsley-type="number">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Nombre</label>
                <div class="col-lg-10">
                    <input type="text" id="nombre" name="nombre" class="form-control input-sm" placeholder="Ingresa el nombre" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Pais</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="paisSolicitud" id="paisSolicitud" data-parsley-required="true">
                        <option value="">-- Seleccionar pais --</option>
                        <?php foreach ($paises as $key) { ?>
                            <option value="<?= $key->PaisCod ?>"><?=$key->PaisNom?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Provincia</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="provinciaSolicitud" id="provinciaSolicitud" data-parsley-required="true">
                        <option value="">-- Seleccionar provincia --</option>
                    </select>
                    <input type="hidden" id="provinciaHidden" value="0">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Localidad</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="localidadSolicitud" id="localidadSolicitud" data-parsley-required="true">
                        <option value="">-- Seleccionar localidad --</option>
                    </select>
                    <input type="hidden" id="localidadHidden" value="0">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Email</label>
                <div class="col-lg-10">
                    <input type="text" id="usuario" name="usuario" class="form-control input-sm" placeholder="Ingresa el email" data-parsley-required="true" data-parsley-type="email">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Contraseña</label>
                <div class="col-lg-10">
                    <input type="password" id="pass" name="pass" class="form-control input-sm" placeholder="Ingresa el nombre" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Confirmar contraseña</label>
                <div class="col-lg-10">
                    <input type="password" id="pass2" name="pass2" class="form-control input-sm" placeholder="Confirma la contraseña" data-parsley-required="true" data-parsley-equalto="#pass">
                </div>
            </div>
            <div class="text-right m-top-md">
                <input type="submit" class="btn btn-info" value="Registrar">
            </div>
        </form>
    </div>
</div><!-- ./padding-md -->
<script src="<?= base_url() ?>public/js/parsley.min.js"></script>
<script src="<?= base_url() ?>public/js/select2.min.js"></script>
<script src="<?= base_url() ?>public/js/clinicas.js"></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>