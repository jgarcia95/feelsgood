<div class="padding-md">
    <link href="<?= base_url() ?>public/css/select2/select2.css" rel="stylesheet"/>
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Gestion</li>
        <li><a href="<?= base_url() ?>administrador/clinicas">Clinicas</a></li>
        <li>Editar Clinica</li> 
    </ul>
    <div class="row">
        <form id="form-edit-clinica" class="form-horizontal no-margin" style="padding:5%;">
            <div class="form-group">
                <label class="control-label col-lg-2">RUC</label>
                <div class="col-lg-10">
                    <input type="text" id="ruc" name="ruc" value="<?=$clinica->ruc ?>" class="form-control input-sm" placeholder="Ingresa el ruc" data-parsley-required="true" data-parsley-type="number">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Nombre</label>
                <div class="col-lg-10">
                    <input type="text" id="nombre" name="nombre" value="<?=$clinica->nombre_clinica ?>" class="form-control input-sm" placeholder="Ingresa el nombre" data-parsley-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Pais</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="paisSolicitud" id="paisSolicitud" data-parsley-required="true">
                        <option value="0">-- Seleccionar pais --</option>
                        <?php foreach ($paises as $key) { ?>
                            <option value="<?= $key->PaisCod ?>" 
                            <?php if($clinica->id_pais==$key->PaisCod){ ?> selected <?php } ?>>
                            <?=$key->PaisNom?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Provincia</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="provinciaSolicitud" id="provinciaSolicitud" data-parsley-required="true">
                        <?php if ($provincia) { ?>
                        <option value="<?=$clinica->id_provincia?>"><?=$provincia->ProvinNom?></option>
                        <?php }else{?>
                        <option value="0">-- Seleccionar provincia --</option>
                        <?php } ?>
                    </select>
                    <input type="hidden" id="provinciaHidden" value="<?=$clinica->id_provincia?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Localidad</label>
                <div class="col-lg-10">
                    <select class="select2 width-100" name="localidadSolicitud" id="localidadSolicitud" data-parsley-required="true">
                        <?php if ($localidad) { ?>
                        <option value="<?=$clinica->id_localidad?>"><?=$localidad->LocalidNom?></option>
                        <?php }else{?>
                        <option value="0">-- Seleccionar localidad --</option>
                        <?php } ?>
                    </select>
                    <input type="hidden" id="localidadHidden" value="<?=$clinica->id_localidad?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Email</label>
                <div class="col-lg-10">
                    <input type="text" id="usuario" name="usuario" value="<?=$clinica->email_user ?>" class="form-control input-sm" placeholder="Ingresa el email" data-parsley-required="true" data-parsley-type="email">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Contraseña</label>
                <div class="col-lg-10">
                    <input type="password" id="pass" name="pass" value="<?=$clinica->pass_user ?>" class="form-control input-sm" placeholder="Ingresa la contraseña" data-parsley-required="true">
                </div>
            </div>
            <div class="text-right m-top-md">
                <input type="submit" class="btn btn-info" value="Actualizar">
                <a href="<?=base_url()?>administrador/clinicas" class="btn btn-default">Atras</a>
            </div>
            <input type="hidden" id="clinica" name="clinica" value="<?= $clinica->clinica_id ?>">
            <input type="hidden" id="id" name="id" value="<?= $clinica->id_user ?>">
        </form>
    </div>
</div><!-- ./padding-md -->
<script src="<?= base_url() ?>public/js/select2.min.js"></script>
<script src="<?= base_url() ?>public/js/parsley.min.js"></script>
<script src="<?= base_url() ?>public/js/clinicas.js"></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>