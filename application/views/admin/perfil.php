<link href="<?=base_url()?>public/css/fullcalendar.css" rel="stylesheet">
<link href="<?=base_url()?>public/css/fullcalendar.print.css" rel="stylesheet">
<link href="<?=base_url()?>public/css/newperfil.css" rel="stylesheet">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/variable-pie.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<style type="text/css">
	#simuladorContainer canvas{
		width: 100%;
		height: 100%;
	}
	#visorContainer canvas{
		width: 100%;
		height: 100%;
	}
</style>
<?php if($id_default!=0){ ?>
	<div class=" preload">
		<div class="padding-md">
			<div class="row user-profile-wrapper">
			<!--<div class="col-md-3" id="cont-perfil-left">
				<div id="cont-search-clientes">
					<div id="cont-search-clientes-relative">
						<input type="search" id="input-search" onkeyup="filtrarcliente()" placeholder="search">
						<div class="circle-search">
							<i class="fa fa-search" aria-hidden="true"></i>
						</div>
					</div>
				</div>
				<div id="cont-btn-clientes">
					<div class="col-md-6">
						<div id="cont-btn-new-clientes-relative">
							<a href="<?= base_url() ?>administrador/addPaciente"><button><i class="fa fa-plus-circle" aria-hidden="true"></i><span>Add New</span></button></a>
						</div>
					</div>
					<div class="col-md-6">
						<div id="cont-select-clientes-relative">
							<select id="combo-search-paciente" onChange="filtrarcliente();">
								<option value="1">Por nombre</option>
								<option value="2">Por DNI</option>
								<option value="3">Por correo</option>
							</select>
						</div>
					</div>
				</div> -->
				<!--<div id="cont-list-clientes">
					<ul>
						<?php foreach ($pacientes as $key) {
							$c = ($key->id_paciente == $id)?"selec-clie":""?>
							<li class="<?= $c ?>"><a data-dni="<?= $key->dni ?>" data-correo="<?= $key->email_user ?>" href="<?= base_url() ?>administrador/perfil/<?=$key->id_paciente ?>"><?= $key->nombre.' '.$key->apellido ?></a></li>
						<?php } ?>
					</ul>
					</div>
				</div>-->
						<div class="col-md-12">
					<div class="smart-widget">
						<div class="smart-widget-inner">
							<div class="col-md-12 user-profile-sidebar m-bottom-md">
								<div class="row">
									<div class="col-xs-3 col-sm-2 col-md-1" >
										<div class="">
											<?php if ($paciente->imagen=="") {?>
											<img width="100px" src="<?=base_url() ?>public/images/profile/default.jpg">
											<?php }else{ ?>
											<img width="100px" src="<?= base_url() ?>public/images/profile/<?=$paciente->imagen?>">
											<?php } ?>
										</div>
									</div>
									<div class="col-xs-9 col-sm-10 col-md-11">
										<div class="user-name m-top-sm">
											<?= $paciente->nombre." ".$paciente->apellido ?> <?php if ($paciente->online==1){?>
											<i class="fa fa-circle text-success m-left-xs font-14" style="padding-left:3%;"></i>
											<span class="text-success" style="padding-left:5px;font-size:0.6em;">(Conectado)</span>
											<?php }else{ ?>
												<i class="fa fa-circle text-muted m-left-xs font-14" style="padding-left:3%;"></i>
												<span class="text-muted" style="padding-left:5px;font-size:0.6em;">(Desconectado)</span>
											<?php } ?>
										</div>
										<br>
										<div class="m-top-sm">
											<div>
												<a download="<?= $paciente->nombre.' '.$paciente->apellido ?>" href="<?= base_url() ?>paciente/descargar/<?=$key->id_paciente ?>">
													<i class="fa fa-file-excel-o user-profile-icon"></i>
													Descargar Reporte
												</a>
												<a title="Ver conexiones" onClick="modalmapapaciente(<?=$key->id_paciente ?>)">
													<i class="fa fa-map-marker user-profile-icon"></i>
													Ver Conexiones
												</a>
												<a title="Editar" href="<?= base_url() ?>administrador/editPaciente/<?= $key->id_paciente ?>">
													<i class="fa fa-external-link user-profile-icon"></i>
													Editar
												</a>
												<a title="Eliminar" data-id="<?=$key->id_user?>" data-toggle="modal" href="#" data-target="#delModalPaciente" class="deldata">
													<i class="fa fa-times user-profile-icon"></i>
													Eliminar
												</a>
											</div>
										</div>
									</div>
								</div><!-- ./row -->
							</div><!-- ./col -->
							<ul class="nav nav-tabs tab-style2 tab-right bg-grey">
								<li class="active">
									<a href="#profileTab1" data-toggle="tab">
										<span class="icon-wrapper"></span>
										<span class="text-wrapper">General</span>
									</a>
								</li>
								<li>
									<a href="#profileTab2" data-toggle="tab">
										<span class="icon-wrapper"></span>
										<span class="text-wrapper">Antecedentes</span>
									</a>
								</li>
								<li>
									<a href="#profileTab3" data-toggle="tab">
										<span class="icon-wrapper"></span>
										<span class="text-wrapper">Ejercicios</span>
									</a>
								</li>
								<li>
									<a href="#profileTab4" data-toggle="tab" id="btnSesiones">
										<span class="icon-wrapper"><i class="fa fa-calendar"></i></span>
										<span class="text-wrapper">Calendario de ejercicios</span>
									</a>
								</li>
								<li>
									<a id="btnsesiones" href="#profileTab5" data-toggle="tab">
										<span class="icon-wrapper"></span>
										<span class="text-wrapper">Estadisticas</span>
									</a>
								</li>

								<!--<li>
									<a href="#profileTab6" data-toggle="tab">
										<span class="icon-wrapper"><i class="fa fa-tripadvisor"></i></span>
										<span class="text-wrapper">Visor</span>
									</a>
								</li>-->
							</ul>
							<div class="smart-widget-body">
										<div class="tab-content">
											<div class="tab-pane fade in active" id="profileTab1">
												<div class="col-md-6 col-xs-6">
													<div class="smart-widget m-top-lg widget-dark-blue">
														<div class="smart-widget-header">
															<strong>Datos del Paciente</strong>
														</div>
														<div class="smart-widget-inner">
															<div class="smart-widget-body">
																<div class="row">
																	<div class="form-group col-md-6">
																		<label class="control-label"><ins>DNI</ins></label>
																		<p class="form-control-static"><?=$paciente->dni?></p>
																	</div>

																	<div class="form-group col-md-6">
																		<label class="control-label"><ins>Ubicación</ins></label>
																		<p class="form-control-static">
																			<?php if($locali){echo $locali->LocalidNom.", ";}else{ echo".";}
																			if ($prov){echo $prov->ProvinNom.", ";}else{ echo".";}
																			if ($pais){echo $pais->PaisNom;}else{ echo".";} ?>
																		</p>
																	</div>
																	<div class="form-group col-md-6">
																		<label class="control-label"><ins>Edad</ins></label>
																		<p class="form-control-static"><?=$paciente->edad?></p>

																	</div>
																	<div class="form-group col-md-6">
																		<label class="control-label"><ins>Teléfono</ins></label>
																		<p class="form-control-static"><?php if($paciente->fono){echo $paciente->fono;}else{echo"...";}?></p>

																	</div>

																	<div class="form-group col-md-6">
																		<label class="control-label"><ins>Profesión</ins></label>
																		<p class="form-control-static">
																			&nbsp;<?php if($prof){ echo $prof->nombre_profesion;}else{echo"...";}?>
																		</p>
																	</div>
																	<div class="form-group col-md-6">
																		<label class="control-label"><ins>Fecha de nacimiento</ins></label>
																		<p class="form-control-static">
																			<?php if($paciente->nacimiento){ echo $paciente->nacimiento; }else{ echo"..."; } ?>
																		</p>

																	</div>
																	<div class="form-group col-md-6">
																		<label class="control-label"><ins>Estado civil</ins></label>
																		<p class="form-control-static">
																			<?php switch($paciente->estado_civil){
																				case '1':echo "Soltero";break;
																				case '2':echo "Casado";break;
																				case '3':echo "Viudo";break;
																				case '4':echo "Divorciado";break;
																				case '5':echo "Prefiero no indicar";break;
																				default: echo"...";break;
																			}?>
																		</p>
																	</div>
																	<div class="form-group col-md-6">
																		<label class="control-label"><ins>Grupo sanguineo</ins></label>
																		<p class="form-control-static">&nbsp;<?php if($paciente->grupo_sanguineo){ echo $paciente->grupo_sanguineo; } else{echo"...";}?></p>
																	</div>
																	<div class="form-group col-md-6">
																		<label class="control-label"><ins>Clinica</ins></label>
																		<p class="form-control-static"><?= ($clinica)? $clinica->nombre_clinica : ""?></p>
																	</div>
																</div>
															</div>
														</div><!-- ./smart-widget-inner -->
													</div><!-- ./smart-widget -->
												</div><!-- ./row -->

												<div class="col-md-6 col-xs-6">
													<div class="smart-widget m-top-lg widget-dark-blue">
														<div class="smart-widget-header">
															<strong>Datos del Acompañante</strong>
														</div>
														<div class="smart-widget-inner">
															<div class="smart-widget-body">
																<div class="form-group col-md-6">
																	<label class="control-label"><ins>DNI</ins></label>
																	<p class="form-control-static"><?=$paciente->dni_acom?></p>
																</div>
																<div class="form-group col-md-6">
																	<label class="control-label"><ins>Nombre</ins></label>
																	<p class="form-control-static"><?=$paciente->nombres_acom?></p>
																</div>
																<div class="form-group col-md-6">
																	<label class="control-label"><ins>Apellido</ins></label>
																	<p class="form-control-static"><?=$paciente->apellidos_acom?></p>
																</div>
																<div class="form-group col-md-6">
																	<label class="control-label"><ins>Edad</ins></label>
																	<p class="form-control-static"><?=$paciente->edad_acom?></p>
																</div>
																<div class="form-group col-md-6">
																	<label class="control-label"><ins>Teléfono</ins></label>
																	<p class="form-control-static"><?=$paciente->fono_acom?></p>
																</div>
																<div class="form-group col-md-6">
																	<label class="control-label"><ins>Profesión</ins></label>
																	<p class="form-control-static">
																		<?php if($prof_acom){ echo $prof_acom->nombre_profesion;}?>
																	</p>
																</div>
															</div><!-- ./smart-widget-inner -->
														</div><!-- ./smart-widget -->
													</div><!-- ./row -->
												</div>
											</div><!-- ./tab-pane -->



											<div class="tab-pane fade" id="profileTab2">
												<div class="smart-widget m-top-lg widget-dark-blue">
													<div class="smart-widget-header">
														<strong>Antecedentes</strong>
													</div>
													<div class="smart-widget-inner">
														<div class="smart-widget-body">
															<div class="row">
																<?php $array_enf_personales=array();$i=1;
																foreach (explode(",",$paciente->enf_personales) as $enf) {
																	$array_enf_personales[$i]=$enf;
																	$i++;
																}
																foreach ($enf_personales as $key) { ?>
																<div class="form-group col-xs-7 col-sm-5 col-md-3">
																	<div class="custom-checkbox" style="width: auto;">
																		<input type="checkbox"
																		<?php if(array_search($key->id_enfermedad,$array_enf_personales)){ ?>
																		 checked <?php } ?> >
																		<label></label>
																		<span style="padding-left: 12px;color: #000;"><?=$key->nombre?></span>
																	</div>
																</div>
																<?php } ?>
																<h4 class="header-text m-top-md">Familiares</h4>
																<?php $array_enf_familiares=array();$j=1;
																foreach (explode(",",$paciente->enf_familiares) as $enf) {
																	$array_enf_familiares[$j]=$enf;
																	$j++;
																}
																foreach ($enf_familiares as $key) { ?>
																<div class="form-group col-md-3">
																	<div class="custom-checkbox" style="width: auto;">
																		<input type="checkbox"
																		<?php if(array_search($key->id_enfermedad,$array_enf_familiares)){ ?>
																		 checked <?php } ?> >
																		<label></label>
																		<span style="padding-left: 12px;color: #000;"><?=$key->nombre?></span>
																	</div>
																</div>
																<?php } ?>
																<h4 class="header-text m-top-md">Observaciones</h4>
																<p class="form-control-static"><?=$paciente->observaciones?></p>
															</div>
														</div><!-- ./smart-widget-inner -->
													</div><!-- ./smart-widget -->
												</div><!-- ./row -->
											</div><!-- ./tab-pane -->

											<div class="tab-pane fade" id="profileTab3">
												<br>
												<div class="col-lg-2">
													<label class="control-label">Seleccione tipo de ejercicio:</label>
												</div>
												<?php if($this->session->userdata('rol')=="1"){ ?>
												<div class="col-lg-10">
													<select class="form-control" name="tipo" id="tipo">
														<option value="">--Seleccione--</option>
														<?php foreach ($ejercicios as $value) { ?>
														<option value="<?= $value->id_ejercicio?>"><?=$value->nombre?></option>
														<?php } ?>
													</select>
												</div>
												<?php } else{ ?>

													<?php if( isset($doctor) && trim($doctor->ejercicios) != "" ){

														$aux = explode(',', $doctor->ejercicios);
													?>
													<div class="col-lg-10">
														<select class="form-control" name="tipo" id="tipo">
															<option value="">--Seleccione--</option>
															<?php
																foreach ($ejercicios as $value) {
																	for( $a = 0; $a < count( $aux ); $a++ ){

																		if( $aux[$a] == $value->id_ejercicio ){
															?>
																	<option value="<?= $value->id_ejercicio?>"><?=$value->nombre?></option>
															<?php
																		}
																		else{}
																	}
																}
															?>
														</select>
													</div>
													<?php } else{ ?>
														<div class="col-lg-10">
															<select class="form-control" name="" id="">
																<option value="">--No tiene ejercicios a dosificar asignados--</option>
															</select>
														</div>
													<?php } ?>

												<?php } ?>
												<br>
												<!--<div id="cont-abs">
													<div id="contenedorroyal" style="overflow:hidden">

													</div>
												</div><!-- ./slider -->
												<form class="no-margin m-top-md" id="dosificacion">
													<input type="hidden" id="id_paciente" name="id_paciente" value="<?=$paciente->id_paciente?>">
													<input type="hidden" id="id_ejercicio" name="id_ejercicio">
													<div id="body-form-dosificacion"></div>
													<div class="form-group m-top-lg" id="div-button-add" style="display:none;">
														<label class="col-sm-4 control-label"></label>
														<div class="col-sm-4">
															<button type="submit" class="btn btn-info m-left-xs btn-block">
															Registrar</button>
														</div>
														<label class="col-sm-4 control-label"></label>
													</div>
													<br><br>
												</form>
											</div><!-- ./tab-pane -->

											<div class="tab-pane fade" id="profileTab4">
												<div class=" clearfix">
													<br>
													<div class="col-md-12 text-center">
														<span style="min-width: 10px; padding: 5px; height: 10px; background: #787878; border-radius: 3px; color: #FFF">Ejercicio Completado</span>
														&nbsp;&nbsp;
														<span style="min-width: 10px; padding: 5px; height: 10px; background: #E0E0E0; border-radius: 3px; color: #181818;">Ejercicio No Completado</span>
													</div>
													<br><br>
													<div class="full-calendar-body clearfix" >
														<div class="full-calendar"></div>
													</div>
												</div>
											</div><!-- ./tab-pane -->

											<div class="tab-pane fade" id="profileTab5">
												<input type="hidden" id="id_paciente_est" value="<?=$paciente->id_paciente?>">
												<div class="">
													<div class="col-lg-12 " style="margin: 20px 0 30px;">
														<div class="col-lg-1">
															<label class="control-label">Ejercicio:</label>
														</div>
														<?php if($this->session->userdata('rol')=="1"){ ?>
														<div class="col-lg-3">
															<select class="form-control" name="tipo2" id="tipo2">
																<option value="">--Seleccione--</option>
																<?php foreach ($ejercicios as $value) { ?>
																<option value="<?=$value->id_ejercicio?>"><?=$value->nombre?></option>
																<?php } ?>
															</select>
														</div>
														<?php } else{ ?>
														<?php
															if( isset($doctor) && trim($doctor->ejercicios) != "" ){

																$aux = explode(',', $doctor->ejercicios);
														?>
														<div class="col-lg-3">
															<select class="form-control" name="tipo2" id="tipo2">
																<option value="">--Seleccione--</option>
																<?php
																	foreach ($ejercicios as $value) {
																		for( $a = 0; $a < count( $aux ); $a++ ){

																			if( $aux[$a] == $value->id_ejercicio ){
																?>
																<option value="<?=$value->id_ejercicio?>"><?=$value->nombre?></option>
																<?php
																			}
																		}
																	}
																?>
															</select>
														</div>
														<?php
															}
															else{
														?>
															<div class="col-lg-3">
																<select class="form-control" name="tipo2" id="tipo2">
																	<option value="">--No tiene ejercicios a dosificar asignados--</option>
																</select>
															</div>
														<?php
															}
														}
														?>
														<div class="col-lg-1"><label class="control-label">Desde:</label></div>
														<div class="col-lg-2">
															<input class="datepicker-input form-control" type="text" id="desde" value="<?=date('d-m-Y')?>">
														</div>
														<div class="col-lg-1"><label class="control-label">Hasta:</label></div>
														<div class="col-lg-2">
															<input class="datepicker-input form-control" type="text" id="hasta" value="<?=date('d-m-Y')?>">
														</div>
														<div class="col-lg-2">
															<a id="consulta_sesiones" data-tipo="0" class="btn btn-primary">Consultar</a>
														</div>
													</div>
												</div>
												<div id="div_estadisticas" class="col-lg-12" style="display: none; margin: 20px 0;">

												<!--Aqui se carga de manera dinamica la data de estadisticas-->

												</div><!--hasta aqui div_estadisticas-->

												<br><br>
											</div><!-- ./tab-pane -->

											<!--<div class="tab-pane fade" id="profileTab6">
												<div class="visor-content" style="position:relative;">
													<div id="visorContainer" style="width:100%;height:600px"></div>
													<div id="playVisor">
														<i class="fa fa-play-circle" aria-hidden="true"></i>
													</div>
												</div>
												<div id="mapaVisor" class="mapa"></div>
											</div> -->




										</div><!-- ./tab-content -->
									</div><!-- ./smart-widget-body -->
								</div><!-- ./smart-widget-inner -->
							</div><!-- ./smart-widget -->
						</div>

				</div><!-- ./padding-md -->
			</div><!-- /main-container -->
		</div><!-- /wrapper -->

<?php }else{echo "<div style='margin:30px;'>No tiene pacientes asignados</div>";} ?>
<div class="modal fade" id="modalEditaSesion">
	<div class="modal-dialog modal-md" style="width: 70% !important">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
				<h4 class="modal-title">Modificar dosificacion</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal m-top-md" id="dosificacionEdit">
					<input type="hidden" id="id_paciente" name="id_paciente" value="<?=$paciente->id_paciente?>">
					<input type="hidden" id="id_sesion" name="id_sesion">
					<input type="hidden" id="id_ejercicio_edit" name="id_ejercicio">
					<input type="hidden" name="editar" value="1">
					<div id="body-form-dosificacion-edit"></div>
				</form>
			</div>
			<div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal">Cerrar</a>
				<a class="btn btn-primary" id="modificar_dosificacion">Actualizar</a>
				<a class="btn btn-danger" id="eliminar_dosificacion">Eliminar</a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalDelSesion">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Confirmaci&oacute;n</h4>
			</div>
			<div class="modal-body">
				Eliminar Sesi&oacute;n?
			</div>
			<div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal">Cancelar</a>
				<a class="btn btn-danger" id="eliminar_dosificacion_confirm">Eliminar</a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalmapapaciente">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div id="mapaVisor" class="mapa" style="display:block;margin-top:0px;height:480px;"></div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="delModalPaciente">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
				<h4 class="modal-title">Confirmar</h4>
			</div>
			<div class="modal-body">
				Eliminar registro?
			</div>
			<div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal" id="close_paciente_del">Cerrar</a>
				<a class="btn btn-primary" id="eliminar_paciente">Eliminar</a>
			</div>
			<input type="hidden" id="id_paciente_eliminar">
		</div>
	</div>
</div>

<link href="<?=base_url()?>public/css/dataTables.bootstrap.css" rel="stylesheet">
<!-- Moment -->
<script src='<?=base_url()?>public/js/moment.min.js'></script>
<script src='<?=base_url()?>public/js/jquery.dataTables.min.js'></script>
<script src='<?= base_url()?>public/js/uncompressed/dataTables.bootstrap.js'></script>
<script src='<?=base_url()?>public/js/jquery.easypiechart.min.js'></script>
<script src='<?=base_url()?>public/js/fullcalendar.min.js'></script>

<script src='<?=base_url()?>public/js/uncompressed/bootstrap-datetimepicker.js'></script>
<script src="<?=base_url()?>public/js/jquery.royalslider.min.js"></script>
<script src='<?=base_url()?>public/js/jquery.noty.packaged.min.js'></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXPkUWREAI7gvPtLfhTECqZcjrnRpOcm4&v=3.exp"></script>
<script src="<?=base_url()?>public/js/perfil.js"></script>
<!--<script src="https://code.highcharts.com/highcharts.js"></script>-->

<script src="<?=base_url()?>public/js/ejercicios/cuello.js"></script>
<script src="<?=base_url()?>public/js/ejercicios/respiracion.js"></script>
<script src="<?=base_url()?>public/js/ejercicios/estres_ansiedad.js"></script>


<!--Unity-->

<link rel="stylesheet" href="<?=base_url()?>public/simuladorTemplateData/style.css">
<script src="<?=base_url()?>public/simuladorTemplateData/UnityProgress.js"></script>
<script src="<?=base_url()?>public/simuladorBuild/UnityLoader.js"></script>

<link rel="stylesheet" href="<?=base_url()?>public/simuladorBrazoTemplateData/style.css">
<script src="<?=base_url()?>public/simuladorBrazoTemplateData/UnityProgress.js"></script>
<script src="<?=base_url()?>public/simuladorBrazoBuild/UnityLoader.js"></script>

<script type="text/javascript">
var sca = 0;
var simuladorInstance;
var simuladorInstance2;
var simuladorInstance3;
var simuladorBInstance;

function cargaCabeza( div ){

	//1= cuello 2= respiracion 3=tamo
	if( div == 1 ){
		if( simuladorInstance ){}
		else{
			simuladorInstance = UnityLoader.instantiate("simuladorContainer", base_url+"public/simuladorBuild/Replicador_Cuello_v3.0.json", {onProgress:UnityProgress });
		}
	}
	else if( div == 2 ){
		if( simuladorInstance2 ){}
		else{
			simuladorInstance2 = UnityLoader.instantiate("simuladorContainer2", base_url+"public/simuladorBuild/Replicador_Cuello_v3.0.json", {onProgress:UnityProgress });
		}
	}
	else if( div == 3 ){
		if( simuladorInstance3 ){}
		else{
			simuladorInstance3 = UnityLoader.instantiate("simuladorContainer3", base_url+"public/simuladorBuild/Replicador_Cuello_v3.0.json", {onProgress:UnityProgress });
		}
	}
	else{}
}

function sendDatosSimuladorCabeza(div, values) {

	if( div == 1 ){
		if( simuladorInstance ){
			var a = simuladorInstance.SendMessage("ReceiverObject", "SendParameter", values);
			$(".simulador-content").show();
            $(".buttons-zoom").show();
		}
		else{
			noty({text: "Espere unos segundos mientras carga el simulador.",type: 'success',layout: 'center',timeout: '3000'});
			$(".simulador-content").hide();
            $(".buttons-zoom").hide();
		}
	}
	else if( div == 2 ){
		if( simuladorInstance2 ){
			var a = simuladorInstance2.SendMessage("ReceiverObject", "SendParameter", values);
			$(".simulador-content").show();
            $(".buttons-zoom").show();
		}
		else{
			noty({text: "Espere unos segundos mientras carga el simulador.",type: 'success',layout: 'center',timeout: '3000'});
			$(".simulador-content").hide();
            $(".buttons-zoom").hide();
		}
	}
	else if( div == 3 ){
		if( simuladorInstance3 ){
			var a = simuladorInstance3.SendMessage("ReceiverObject", "SendParameter", values);
			$(".simulador-content").show();
            $(".buttons-zoom").show();
		}
		else{
			noty({text: "Espere unos segundos mientras carga el simulador.",type: 'success',layout: 'center',timeout: '3000'});
			$(".simulador-content").hide();
            $(".buttons-zoom").hide();
		}
	}
	else{}
}


function cargaBrazo(){

	if( simuladorBInstance ){

	}
	else{
		simuladorBInstance = UnityLoader.instantiate("simuladorContainerBrazo", base_url+"public/simuladorBrazoBuild/Replicador_Brazo_v2.2.json", {onProgress:UnityProgress });
	}

}

function sendDatosSimuladorBrazo(values) {

	if( simuladorBInstance ){
		var a = simuladorBInstance.SendMessage("ReceiverObject", "SendParameter", values);
		 $(".simulador-content").show();
        $(".buttons-zoom").show();
	}
	else{
		noty({
			text: "Espere unos segundos mientras carga el simulador.",
			type: 'success',
			layout: 'center',
			timeout: '3000'
		});
		$(".simulador-content").hide();
        $(".buttons-zoom").hide();
	}

}


function zoomDiv( div, opc ){

    //div: 1=respiracion, 2=cuello, 3=puente, 4=tamo

    var di = '';

    if( opc == 1 ){

        sca = sca + 10;
    }
    else{
        sca = sca - 10;
    }

    if( div == 1 ){
        di = 'simuladorContainer2'; //respiracion
    }
    else if( div == 2 ){
        di = 'simuladorContainer'; //cuello
    }
    else if( div == 3 ){
        di = 'simuladorContainerBrazo'; //puente
    }
    else if( div == 4 ){
        di = 'simuladorContainer3'; //tamo
    }
    else{}

    //aqui se realiza un calculo que sea mayor a la unidad sobre un valor porcentual
    var scale=1+(sca/100);
    $("#"+di).css('-moz-transform','scale('+scale+')');
    $("#"+di).css('-ms-transform','scale('+scale+')');
    $("#"+di).css('-moz-transform','scale('+scale+')');
    $("#"+di).css('-o-transform','scale('+scale+')');
    $("#"+di).css('-webkit-transform','scale('+scale+')');
    $("#"+di).css('transform','scale('+scale+')');
}
</script>
