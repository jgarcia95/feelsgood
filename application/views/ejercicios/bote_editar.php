<div class="form-group">
	<label class="col-sm-3 control-label">Fecha de sesión</label>
	<div class="col-sm-9">
		<input class="datetimepicker-input form-control" name="fecha" id="fecha" value="<?=  date("d-m-Y h:i A",strtotime($fecha)) ?>" type="text">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Dificultad</label>
	<div class="col-sm-9">
		<select class="form-control" name="dificultad" id="dificultad">
			<option value="">--Seleccione--</option>
			<option <?php echo ( $dificultad == 1 ) ? 'selected' : ''; ?> value="1">1</option>
			<option <?php echo ( $dificultad == 2 ) ? 'selected' : ''; ?> value="2">2</option>
			<option <?php echo ( $dificultad == 3 ) ? 'selected' : ''; ?> value="3">3</option>
		</select>
	</div>
	<br>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Circuito</label>
	<div class="col-sm-9">
		<select class="form-control" name="circuito" id="circuito">
			<option value="">--Seleccione--</option>
			<option <?php echo ( $circuito == 1 ) ? 'selected' : ''; ?> value="1">1</option>
			<option <?php echo ( $circuito == 2 ) ? 'selected' : ''; ?> value="2">2</option>
			<option <?php echo ( $circuito == 3 ) ? 'selected' : ''; ?> value="3">3</option>
			<option <?php echo ( $circuito == 4 ) ? 'selected' : ''; ?> value="4">4</option>
			<option <?php echo ( $circuito == 5 ) ? 'selected' : ''; ?> value="5">5</option>
		</select>
	</div>
	<br>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Dirección</label>
	<div class="col-sm-9">
		<select class="form-control" name="direccion" id="direccion">
			<option value="">--Seleccione--</option>
			<option <?php echo ( $direccion == 1 ) ? 'selected' : ''; ?> value="1">IZQUIERDA</option>
			<option <?php echo ( $direccion == 2 ) ? 'selected' : ''; ?> value="2">DERECHA</option>
		</select>
	</div>
	<br>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Repeticiones</label>
	<div class="col-sm-9">
		<input class="form-control" value="<?= $monedas ?>" name="monedas" id="monedas" type="number" min="10" max="30">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tutorial</label>
	<div class="col-sm-9">
		<label class="toogleswitch">
			<input type="checkbox" <?php echo ( $tutorial == 1 ) ? 'checked="checked"' : ''; ?> name="tutorial" id="tutorial">
			<span class="toogleslider toogleround"></span>
		</label>
	</div>
</div>
<div class="row"><br><br></div>
