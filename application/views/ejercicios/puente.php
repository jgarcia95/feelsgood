	<div class="smart-widget m-top-lg widget-dark-blue">
		<div class="smart-widget-header">
			Ejercicio Puente
		</div>
		<div class="smart-widget-inner">
			<div class="smart-widget-body">
				<div class="row">
					<div class="col-md-6">			
						<div class="form-group">
							<label class="control-label">Fecha de sesión</label>
							<input class="datetimepicker-input form-control" name="fecha" id="fecha" type="text">
						</div>
					</div>					
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tiempo de espera</label>
							<input class="form-control" name="espera" id="espera" type="number" min="0" max="10" step="1">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tipo de Movimiento</label>
							<select class="form-control" name="tipomovimiento" id="tipomovimiento">
								<option value="">--Seleccione--</option>
								<?php foreach ($tipomovimiento as $data) { ?>
									<option value="<?= $data->id ?>"><?= $data->descripcion ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Rango de movimiento</label>
							<select class="form-control" name="altura" id="altura">
								<option value="">--Seleccione--</option>
								<option value="high">160 grados</option>
								<option value="low">90 grados</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Repeticiones</label>.
							<input class="form-control" name="repeticiones" id="repeticiones" type="number" min="1" max="100">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tutorial</label>
							<label class="toogleswitch">
								<input type="checkbox" name="tutorial" id="tutorial">
								<span class="toogleslider toogleround"></span>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div><!-- ./smart-widget-inner -->
	</div><!-- ./smart-widget -->
