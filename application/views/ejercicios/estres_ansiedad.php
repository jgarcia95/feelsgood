	<div class="smart-widget m-top-lg widget-dark-blue">
		<div class="smart-widget-header">
			Ejercicio Cuello y Cabeza
		</div>
		<div class="smart-widget-inner">
			<div class="smart-widget-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Fecha de sesión</label>
							<input class="datetimepicker-input form-control" name="fecha" id="fecha" type="text">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Respiración</label>
							<label class="toogleswitch">
								<input type="checkbox" onchange="displayCamposDependientes()" name="respiracion" id="respiracion">
								<span class="toogleslider toogleround"></span>
							</label>
						</div>
					</div>
						

					<div class="col-md-12">
						<div id="div_campos_dependientes" class="row" style="display: none;">
							<div class="form-group col-md-6">
								<label class="control-label">Repeticiones</label>
								<input class="form-control" name="repeticiones" id="repeticiones" placeholder="Valor mínimo: 2" type="number" min="2">
							</div>
							<div class="col-md-12"></div>
							<div class="form-group col-md-3">
								<label class="control-label">Inhalación</label>
								<input class="form-control" name="inhalacion" id="inhalacion" placeholder="En segundos (Mínimo: 4)" type="number" min="4">
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Sostenimiento</label>
								<input class="form-control" name="sostenimiento" id="sostenimiento" placeholder="En segundos (Mínimo: 4)" type="number" min="4">
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Exhalación</label>
								<input class="form-control" name="exhalacion" id="exhalacion" placeholder="En segundos (Mínimo: 4)" type="number" min="4">
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Descanso</label>
								<input class="form-control" name="descanso" id="descanso" placeholder="En segundos (Mínimo: 4)" type="number" min="4">
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tipo</label>
							<select class="form-control" name="tipo" id="tipo">
								<option value="">--Seleccione--</option>
								<option value="A">Ansiedad</option>
								<option value="E">Estrés</option>
							</select>
						</div>
					</div>	
					<div class="col-md-12">
						<br><br>
						<div class="">
							<div class="form-group">
								<div id="cont-abs_estres" class="col-lg-12" style="min-height: 300px; width: 98%">
									<div class="cont-img-flecha" id="imgflecha1_estres" data-div="2" style="position: absolute; left: 40px !important;">
										<img src="<?= base_url() ?>public/images/flecha1.png">
									</div>
									<div  class="cont-img-flecha"  id="imgflecha2_estres" data-div="2" style="position: absolute; right: 40px !important;">
										<img src="<?= base_url() ?>public/images/flecha2.png">
									</div>
												
									<div id="contenedorroyal" style="overflow:hidden">
										<div id="contSliders" class="col-lg-6 col-sm-12 col-xs-6" style="padding:0px;">
											<?php foreach( $escenarios as $key ) { ?>
												<div class="col-lg-12">
													<div class="statistic-box bg-primary m-bottom-md" >
														<div class="statistic-title">
														</div>
														<div class="statistic-value">
															<?= $key->nombre ?>
														</div>
														<div class="m-top-md">
															<a style="cursor:pointer" data-nombre="<?= $key->nombre ?>" data-id="<?= $key->id ?>" class="btn btn-success m-left-xs" id="button-add-scene">
																Agregar
															</a>
														</div>
														<div class="statistic-icon-background">
															<i class="fa fa-user"></i>
														</div>
													</div>
												</div>
											<?php } ?>
										</div>
									</div><!-- ./slider -->
								</div>
								<br>
								<div class="col-md-12">
									<div class="col-md-2">
										<div class='ui-state-default' style="height: 155px !important; text-align: center; line-height: 140px; font-weight: bold; font-size: 14px;">
											INICIO
										</div>
									</div>
									<div class="col-md-8">
										<div id="slider_escenarios" style="">
											<div id="sortable2"></div>
										</div>
										<input id="array_escenarios" name="array_escenarios" type="hidden">
									</div>
									<div class="col-md-2">
										<div class='ui-state-default' style="height: 155px !important; text-align: center; line-height: 140px; font-weight: bold; font-size: 14px;">
											FINAL
										</div>
									</div>
								</div>
								<br><br>
								<div class="col-md-12">
									<br>
									<div class="form-group col-md-12" style="text-align: center;">
										<label style="text-align: right;" class="col-sm-6 control-label">Voz en vivo</label>
										<div style="text-align: left;" class="col-sm-6">
											<label class="toogleswitch">
												<input type="checkbox" name="voz_vivo" id="voz_vivo">
												<span class="toogleslider toogleround"></span>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>	
						

						<script>
							var slider33 = '';

							slider33 = $("#cont-abs_estres #contSliders").royalSlider({
								autoHeight: true
							}).data('royalSlider');

							$('#cont-abs_estres #imgflecha2_estres').click(function() {
								slider33.next();
							});
							$('#cont-abs_estres #imgflecha1_estres').click(function() {
								slider33.prev();
							});
							slider33.ev.trigger("rsAfterSlideChange");
									
						</script>

				</div>
			</div>
		</div><!-- ./smart-widget-inner -->
	</div><!-- ./smart-widget -->