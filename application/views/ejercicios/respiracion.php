	<div class="smart-widget m-top-lg widget-dark-blue">
		<div class="smart-widget-header">
			Ejercicio Respiración
		</div>
		<div class="smart-widget-inner">
			<div class="smart-widget-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Fecha de sesión</label>.
							<input class="datetimepicker-input form-control" name="fecha" id="fecha" type="text">
						</div>
					</div>
					<div class="col-md-6">
						<label class="control-label">Acostado</label><br>
						<label class="toogleswitch">
							<input type="checkbox" onchange="cambioEscenarios()" name="echado" id="echado">
							<span class="toogleslider toogleround"></span>
						</label>
					</div>				
					<div class="col-md-12"></div>
                    <div class="col-md-12" id="div_respi" style="display: none;">
                        <div class="form-group">
                            <label class="control-label">Respiración</label><br>
                            <label class="toogleswitch">
                                <input type="checkbox" name="respiracion" id="respiracion">
                                <span class="toogleslider toogleround"></span>
                            </label>
                        </div>
                    </div>
					<div class="col-md-12" id="div_tip" style="display: none;">
						<div class="form-group">
							<label class="control-label">Tipo de Escenario</label><br>
							<div class="">
								<div class="radio inline-block">
									<div class="custom-radio m-right-xs">
										<input type="radio" id="tipo_video" value="video" checked name="tip">
										<label for="tipo_video"></label>
									</div>
									<div class="inline-block vertical-top">Video</div>
								</div>
								<div class="radio inline-block">
									<div class="custom-radio m-right-xs">
										<input type="radio" id="tipo_acuario" value="acuario" name="tip">
										<label for="tipo_acuario"></label>
									</div>
									<div class="inline-block vertical-top">Acuario</div>
								</div>
							</div>
						</div>
					</div>
						
					<div class="col-md-6" id="div_escenario">
						<div class="form-group">
							<label class="control-label">Escenario</label>
							<select class="form-control" name="escenario" id="escenario">
								<?php foreach ($escenarios as $value) { ?>
									<option value="<?= $value->id ?>"><?= $value->nombre ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6" id="div_ciclo">
						<label class="control-label">Transición Dia/Noche</label><br>
						<label class="toogleswitch">
							<input type="checkbox" checked name="ciclo" id="ciclo">
							<span class="toogleslider toogleround"></span>
						</label>
					</div>
					<div class="col-md-12"></div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Repeticiones</label>
							<input class="form-control" name="repeticiones" min="2" id="repeticiones" placeholder="" type="number">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tiempo Inhalacion</label>	
							<input class="form-control" name="inhalacion" id="inhalacion" placeholder="En segundos" type="number" min="0">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tiempo Mantener respiracion</label>
							<input class="form-control" name="mantener" id="mantener" placeholder="En segundos" type="number" min="0">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tiempo Exhalacion</label>
							<input class="form-control" name="exhalacion" id="exhalacion" placeholder="En segundos" type="number" min="0">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tiempo Descanso</label>
							<input class="form-control" name="descanso" id="descanso" placeholder="En segundos" type="number" min="0">
						</div>
					</div>
					<div class="col-md-12"></div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tutorial</label><br>
							<label class="toogleswitch">
								<input name="tutorial" id="tutorial" type="checkbox">
								<span class="toogleslider toogleround"></span>
							</label>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Escala visual analogica</label><br>
							<label class="toogleswitch">
								<input name="perrito" id="perrito" type="checkbox">
								<span class="toogleslider toogleround"></span>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div><!-- ./smart-widget-inner -->
	</div><!-- ./smart-widget -->

<script type="text/javascript">

	var scene='';
	var escena = '<option value="0">Video 1</option>'+
    '<option value="1">Video 2</option>'+
    '<option value="2">Video 3</option>'+
    '<option value="3">Video 4</option>';

	<?php foreach ($escenarios as $val) { ?>
		scene += '<option value="<?= $val->id ?>"><?= $val->nombre ?></option>';
	<?php } ?>
	
	function cambioEscenarios(){

		if( $("#echado").prop("checked") == true ){

			$("#escenario").empty().html( escena );
			$("#div_tip").show();
            $("#div_respi").show();
            $("#respiracion").attr('checked', 'checked');
			$("#tip").val("video").change();
			$("#div_ciclo").hide();
			$("#ciclo").removeAttr('checked');
		}
		else{
			$("#escenario").empty().html( scene );
			$("#div_tip").hide();
            $("#div_respi").hide();
            $("#respiracion").attr('checked', 'checked');
			$("#tip").val("video").change();
			$("#div_ciclo").show();
			$("#ciclo").attr('checked');
		}
	}


	$("input[name='tip']").change(function(e){

		if( $(this).val() == 'acuario' ){

			$("#div_escenario").hide();
			$("#escenario").attr('disabled','disabled');
		}
		else{

			$("#div_escenario").show();
			$("#escenario").removeAttr('disabled');
		}
	});
</script>