<link href="<?=base_url()?>public/css/fullcalendar.css" rel="stylesheet">
<link href="<?=base_url()?>public/css/fullcalendar.print.css" rel="stylesheet">
<link href="<?=base_url()?>public/css/newperfil.css" rel="stylesheet">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/variable-pie.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<style type="text/css">
	#simuladorContainer canvas{
		width: 100%;
		height: 100%;
	}
	#visorContainer canvas{
		width: 100%;
		height: 100%;
	}
</style>
<?php if($id_default!=0){ ?>
	<div class=" preload">            
		<div class="padding-md">                    
			<div class="row user-profile-wrapper">
			
						<div class="col-md-12">
					<div class="smart-widget">
						<div class="smart-widget-inner">
							<div class="col-md-12 user-profile-sidebar m-bottom-md">
								<div class="row">
									<div class="col-sm-4 col-md-1" >
										<div class="">
											<?php if (!isset($paciente->imagen) || $paciente->imagen=="") {?>
											<img width="100px" src="<?=base_url() ?>public/images/profile/default.jpg">
											<?php }else{ ?>
											<img width="100px" src="<?= base_url() ?>public/images/profile/<?=$paciente->imagen?>">
											<?php } ?>
										</div>
									</div>
									<div class="col-sm-12 col-md-11">
										<div class="user-name m-top-sm">
											<?= $paciente->nombre." ".$paciente->apellido ?> <?php if ($paciente->online==1){?>
											<i class="fa fa-circle text-success m-left-xs font-14" style="padding-left:3%;"></i>
											<span class="text-success" style="padding-left:5px;font-size:0.6em;">(Conectado)</span>
											<?php }else{ ?>
												<i class="fa fa-circle text-muted m-left-xs font-14" style="padding-left:3%;"></i>
												<span class="text-muted" style="padding-left:5px;font-size:0.6em;">(Desconectado)</span>
											<?php } ?>
										</div>

										<div class="m-top-sm">
											<div>
												<a download="<?= $key->nombre.' '.$key->apellido ?>" href="<?= base_url() ?>paciente/descargar/<?=$key->id_paciente ?>">
													<i class="fa fa-file-excel-o user-profile-icon"></i>
													Descargar Reporte
												</a>
												<a title="Ver conexiones" onClick="modalmapapaciente(<?=$key->id_paciente ?>)">
													<i class="fa fa-map-marker user-profile-icon"></i>
													Ver Conexiones
												</a>
												<a title="Editar" href="<?= base_url() ?>administrador/editPaciente/<?= $key->id_paciente ?>">
													<i class="fa fa-external-link user-profile-icon"></i>
													Editar
												</a>
												<a title="Eliminar" data-id="<?=$key->id_user?>" data-toggle="modal" href="#" data-target="#delModalPaciente" class="deldata">
													<i class="fa fa-times user-profile-icon"></i>
													Eliminar
												</a>
											</div>                                  
										</div>
									</div>
								</div><!-- ./row -->
							</div><!-- ./col -->
							<ul class="nav nav-tabs tab-style2 tab-right bg-grey">
								<li class="active">
									<a href="#profileTab1" data-toggle="tab">
										<span class="icon-wrapper"></span>
										<span class="text-wrapper">General</span>
									</a>
								</li>
								<li>
									<a href="#profileTab2" data-toggle="tab">
										<span class="icon-wrapper"></span>
										<span class="text-wrapper">Antecedentes</span>
									</a>
								</li>
								<li>
									<a href="#profileTab3" data-toggle="tab">
										<span class="icon-wrapper"></span>
										<span class="text-wrapper">Ejercicios</span>
									</a>
								</li>
								<li>
									<a href="#profileTab4" data-toggle="tab" id="btnSesiones">
										<span class="icon-wrapper"><i class="fa fa-calendar"></i></span>
										<span class="text-wrapper">Calendario de ejercicios</span>
									</a>
								</li>
								<li>
									<a id="btnsesiones" href="#profileTab5" data-toggle="tab">
										<span class="icon-wrapper"></span>
										<span class="text-wrapper">Estadisticas</span>
									</a>
								</li>   

								<!--<li>
									<a href="#profileTab6" data-toggle="tab">
										<span class="icon-wrapper"><i class="fa fa-tripadvisor"></i></span>
										<span class="text-wrapper">Visor</span>
									</a>
								</li>-->
							</ul>
							<div class="smart-widget-body">
										<div class="tab-content">
											<div class="tab-pane fade in active" id="profileTab1">
												<h4 class="header-text m-bottom-md">
													General                                         
												</h4>
												<div class="row">
													<div class="col-lg-6">
														<h4 class="m-top-md">Datos del Paciente</h4>

														<div class="form-group">
															<label class="col-lg-4 control-label">DNI</label>
															<div class="col-lg-8">
																<p class="form-control-static"><?=$paciente->dni?></p>
															</div>
														</div>

														<div class="form-group">
															<label class="col-lg-4 control-label">Ubicación</label>
															<div class="col-lg-8">
																<p class="form-control-static">
																	<?php if($locali){echo $locali->LocalidNom.", ";}
																	if ($prov){echo $prov->ProvinNom.", ";}
																	if ($pais){echo $pais->PaisNom;} ?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="col-lg-4 control-label">Edad</label>
															<div class="col-lg-8">
																<p class="form-control-static"><?=$paciente->edad?></p>
															</div>
														</div>
														<div class="form-group">
															<label class="col-lg-4 control-label">Teléfono</label>
															<div class="col-lg-8">
																<p class="form-control-static"><?=$paciente->fono?></p>
															</div>
														</div>

														<div class="form-group">
															<label class="col-lg-4 control-label">Profesión</label>
															<div class="col-lg-8">
																<p class="form-control-static">
																	&nbsp;<?php if($prof){ echo $prof->nombre_profesion;}?>
																</p>

															</div>
														</div>
														<div class="form-group">
															<label class="col-lg-4 control-label">Fecha de nacimiento</label>
															<div class="col-lg-8">
																<p class="form-control-static">
																	<?=$paciente->nacimiento?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="col-lg-4 control-label">Estado civil</label>
															<div class="col-lg-8">
																<p class="form-control-static">
																<?php switch($paciente->estado_civil){
																	case '1':echo "Soltero";break;
																	case '2':echo "Casado";break;
																	case '3':echo "Viudo";break;
																	case '4':echo "Divorciado";break;
																	case '5':echo "Prefiero no indicar";break;
																}?>
															</p>
															</div>
														</div>
														<div class="form-group">
															<label class="col-lg-4 control-label">Grupo sanguineo</label>
															<div class="col-lg-8">
																<p class="form-control-static">&nbsp;<?=$paciente->grupo_sanguineo?></p>
															</div>
														</div>
														<div class="form-group">
															<label class="col-lg-4 control-label">Clinica</label>
															<div class="col-lg-8">
																<p class="form-control-static"><?= ($clinica)? $clinica->nombre_clinica : ""?></p>
															</div>
														</div>
													</div> 

													<div class="col-lg-6">
														<h4 class="m-top-md">Datos del acompañante</h4>

														<div class="form-group">
															<label class="col-lg-4 control-label">DNI</label>
															<div class="col-lg-8">
																<p class="form-control-static"><?=$paciente->dni_acom?></p>
															</div>
														</div>
														<div class="form-group">
															<label class="col-lg-4 control-label">Nombre</label>
															<div class="col-lg-8">
																<p class="form-control-static"><?=$paciente->nombres_acom?></p>
															</div>
														</div>
														<div class="form-group">
															<label class="col-lg-4 control-label">Apellido</label>
															<div class="col-lg-8">
																<p class="form-control-static"><?=$paciente->apellidos_acom?></p>
															</div>
														</div>
														<div class="form-group">
															<label class="col-lg-4 control-label">Edad</label>
															<div class="col-lg-8">
																<p class="form-control-static"><?=$paciente->edad_acom?></p>
															</div>
														</div>
														<div class="form-group">
															<label class="col-lg-4 control-label">Teléfono</label>
															<div class="col-lg-8">
																<p class="form-control-static"><?=$paciente->fono_acom?></p>
															</div>
														</div>
														<div class="form-group">
															<label class="col-lg-4 control-label">Profesión</label>
															<div class="col-lg-8">
																<p class="form-control-static">
																	<?php if($prof_acom){ echo $prof_acom->nombre_profesion;}?>
																</p>
															</div>
														</div>
													</div>
												</div><!-- ./row -->
											</div><!-- ./tab-pane -->

											<div class="tab-pane fade" id="profileTab2">
												<h4 class="header-text m-bottom-md">
													Antecedentes                                            
												</h4>
												<?php $array_enf_personales=array();$i=1;
												foreach (explode(",",$paciente->enf_personales) as $enf) {
													$array_enf_personales[$i]=$enf;
													$i++;
												}
												foreach ($enf_personales as $key) { ?>
												<div class="form-group col-md-6">
													<div class="custom-checkbox" style="width: auto;">
														<input type="checkbox" 
														<?php if(array_search($key->id_enfermedad,$array_enf_personales)){ ?>
														 checked <?php } ?> >
														<label></label>
														<span style="padding-left: 12px;color: #000;"><?=$key->nombre?></span>
													</div>
												</div>
												<?php } ?>
												<h4 class="header-text m-top-md">Familiares</h4>
												<?php $array_enf_familiares=array();$j=1;
												foreach (explode(",",$paciente->enf_familiares) as $enf) {
													$array_enf_familiares[$j]=$enf;
													$j++;
												}
												foreach ($enf_familiares as $key) { ?>
												<div class="form-group col-md-6">
													<div class="custom-checkbox" style="width: auto;">
														<input type="checkbox" 
														<?php if(array_search($key->id_enfermedad,$array_enf_familiares)){ ?>
														 checked <?php } ?> >
														<label></label>
														<span style="padding-left: 12px;color: #000;"><?=$key->nombre?></span>
													</div>
												</div>
												<?php } ?>
												<h4 class="header-text m-top-md">Observaciones</h4>
												<p class="form-control-static"><?=$paciente->observaciones?></p>
											</div><!-- ./tab-pane -->

											<div class="tab-pane fade" id="profileTab3">
												<br>
												<div class="col-lg-2">
													<label class="control-label">Seleccione tipo de ejercicio:</label>
												</div>
												<div class="col-lg-10">
													<select class="form-control" name="tipo" id="tipo">
														<option value="">--Seleccione--</option>
														<?php foreach ($ejercicios as $value) { ?>
														<option value="<?= $value->id_ejercicio?>"><?=$value->nombre?></option>
														<?php } ?>
													</select>
												</div>
												<br>
												<form class="no-margin m-top-md" id="dosificacion">
													<input type="hidden" id="id_paciente" name="id_paciente" value="<?=$paciente->id_paciente?>">
													<input type="hidden" id="id_ejercicio" name="id_ejercicio">
													<div id="body-form-dosificacion"></div>
													<div class="form-group m-top-lg" id="div-button-add" style="display:none;">
														<label class="col-sm-4 control-label"></label>
														<div class="col-sm-4">
															<button type="submit" class="btn btn-info m-left-xs btn-block">
															Registrar</button>
														</div>
														<label class="col-sm-4 control-label"></label>
													</div>
													<br><br>
												</form>
											</div><!-- ./tab-pane -->

											<div class="tab-pane fade" id="profileTab4">
												<div class=" clearfix">
													<div class="full-calendar-body clearfix" >
														<div class="full-calendar"></div>
													</div>
												</div>
											</div><!-- ./tab-pane -->

											<div class="tab-pane fade" id="profileTab5">
												<input type="hidden" id="id_paciente_est" value="<?=$paciente->id_paciente?>">
												<div class="">
													<div class="col-lg-12 " style="margin: 20px 0 30px;">
														<div class="col-lg-1">
															<label class="control-label">Ejercicio:</label>
														</div>
														<div class="col-lg-3">
															<select class="form-control" name="tipo2" id="tipo2">
																<option value="">--Seleccione--</option>
																<?php foreach ($ejercicios as $value) { ?>
																<option value="<?=$value->id_ejercicio?>"><?=$value->nombre?></option>
																<?php } ?>
															</select>
														</div>
														<div class="col-lg-1"><label class="control-label">Desde:</label></div>
														<div class="col-lg-2">
															<input class="datepicker-input form-control" type="text" id="desde" value="<?=date('d-m-Y')?>">
														</div>
														<div class="col-lg-1"><label class="control-label">Hasta:</label></div>
														<div class="col-lg-2">
															<input class="datepicker-input form-control" type="text" id="hasta" value="<?=date('d-m-Y')?>">
														</div>
														<div class="col-lg-2">
															<a id="consulta_sesiones" data-tipo="0" class="btn btn-primary">Consultar</a>
														</div>
													</div>
												</div>
												<div id="div_estadisticas" class="col-lg-12" style="display: none; margin: 20px 0;">
												
												<!--Aqui se carga de manera dinamica la data de estadisticas-->

												</div><!--hasta aqui div_estadisticas-->

												<br><br>
											</div><!-- ./tab-pane -->

											<!--<div class="tab-pane fade" id="profileTab6">
												<div class="visor-content" style="position:relative;">
													<div id="visorContainer" style="width:100%;height:600px"></div>
													<div id="playVisor">
														<i class="fa fa-play-circle" aria-hidden="true"></i>
													</div>
												</div>
												<div id="mapaVisor" class="mapa"></div>
											</div> -->




										</div><!-- ./tab-content -->
									</div><!-- ./smart-widget-body -->
								</div><!-- ./smart-widget-inner -->
							</div><!-- ./smart-widget -->
						</div>
					
				</div><!-- ./padding-md -->
			</div><!-- /main-container -->
		</div><!-- /wrapper -->

<?php }else{echo "<div style='margin:30px;'>No tiene pacientes asignados</div>";} ?>
<div class="modal fade" id="modalEditaSesion">
	<div class="modal-dialog modal-md" style="width: 70% !important">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
				<h4 class="modal-title">Modificar dosificacion</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal m-top-md" id="dosificacionEdit">
					<input type="hidden" id="id_paciente" name="id_paciente" value="<?=$paciente->id_paciente?>">
					<input type="hidden" id="id_sesion" name="id_sesion">
					<input type="hidden" id="id_ejercicio_edit" name="id_ejercicio">
					<input type="hidden" name="editar" value="1">
					<div id="body-form-dosificacion-edit"></div>
				</form>
			</div>
			<div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal">Cerrar</a>
				<a class="btn btn-primary" id="modificar_dosificacion">Actualizar</a>
				<a class="btn btn-danger" id="eliminar_dosificacion">Eliminar</a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalDelSesion">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Confirmaci&oacute;n</h4>
			</div>
			<div class="modal-body">
				Eliminar Sesi&oacute;n?
			</div>
			<div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal">Cancelar</a>
				<a class="btn btn-danger" id="eliminar_dosificacion_confirm">Eliminar</a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalmapapaciente">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div id="mapaVisor" class="mapa" style="display:block;margin-top:0px;height:480px;"></div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="delModalPaciente">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
				<h4 class="modal-title">Confirmar</h4>
			</div>
			<div class="modal-body">
				Eliminar registro?
			</div>
			<div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal" id="close_paciente_del">Cerrar</a>
				<a class="btn btn-primary" id="eliminar_paciente">Eliminar</a>
			</div>
			<input type="hidden" id="id_paciente_eliminar">
		</div>
	</div>
</div>

<link href="<?=base_url()?>public/css/dataTables.bootstrap.css" rel="stylesheet">
<!-- Moment -->
<script src='<?=base_url()?>public/js/moment.min.js'></script>
<script src='<?=base_url()?>public/js/jquery.dataTables.min.js'></script>
<script src='<?= base_url()?>public/js/uncompressed/dataTables.bootstrap.js'></script>
<script src='<?=base_url()?>public/js/jquery.easypiechart.min.js'></script>
<script src='<?=base_url()?>public/js/fullcalendar.min.js'></script>

<script src='<?=base_url()?>public/js/uncompressed/bootstrap-datetimepicker.js'></script>
<script src="<?=base_url()?>public/js/jquery.royalslider.min.js"></script>
<script src='<?=base_url()?>public/js/jquery.noty.packaged.min.js'></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXPkUWREAI7gvPtLfhTECqZcjrnRpOcm4&v=3.exp"></script>
<script src="<?=base_url()?>public/js/perfil.js"></script>
<!--<script src="https://code.highcharts.com/highcharts.js"></script>-->

<script src="<?=base_url()?>public/js/ejercicios/cuello.js"></script>
<script src="<?=base_url()?>public/js/ejercicios/respiracion.js"></script>
<script src="<?=base_url()?>public/js/ejercicios/estres_ansiedad.js"></script>


<!--Unity-->

<link rel="stylesheet" href="<?=base_url()?>public/simuladorTemplateData/style.css">
<script src="<?=base_url()?>public/simuladorTemplateData/UnityProgress.js"></script>  
<script src="<?=base_url()?>public/simuladorBuild/UnityLoader.js"></script>

<link rel="stylesheet" href="<?=base_url()?>public/simuladorBrazoTemplateData/style.css">
<script src="<?=base_url()?>public/simuladorBrazoTemplateData/UnityProgress.js"></script>  
<script src="<?=base_url()?>public/simuladorBrazoBuild/UnityLoader.js"></script>

<script type="text/javascript">
	var simuladorInstance;
    var simuladorBInstance;

    function cargaCabeza(){

        if( simuladorInstance ){

        }
        else{
            simuladorInstance = UnityLoader.instantiate("simuladorContainer", base_url+"public/simuladorBuild/Replicador_Cuello_v2.5.json", {onProgress:UnityProgress });
        }

    }

    function sendDatosSimuladorCabeza(values) {

    	console.log( simuladorInstance );
        if( simuladorInstance ){

            var a = simuladorInstance.SendMessage("ReceiverObject", "SendParameter", values);
             $(".simulador-content").show();
        }
        else{
            noty({
                text: "Espere unos segundos mientras carga el simulador.",
                type: 'success',
                layout: 'center',
                timeout: '3000'
            });
            $(".simulador-content").hide();
        }

    }


    function cargaBrazo(){

        if( simuladorBInstance ){

        }
        else{
            simuladorBInstance = UnityLoader.instantiate("simuladorContainerBrazo", base_url+"public/simuladorBrazoBuild/Replicador_Brazo_v2.2.json", {onProgress:UnityProgress });
        }

    }

    function sendDatosSimuladorBrazo(values) {

        if( simuladorBInstance ){
            var a = simuladorBInstance.SendMessage("ReceiverObject", "SendParameter", values);
             $(".simulador-content").show();
        }
        else{
            noty({
                text: "Espere unos segundos mientras carga el simulador.",
                type: 'success',
                layout: 'center',
                timeout: '3000'
            });
            $(".simulador-content").hide();
        }

    }
	/**/
</script>
