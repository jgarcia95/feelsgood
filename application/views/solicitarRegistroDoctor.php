<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <title>FeelsGood Registro Doctores</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <!-- Bootstrap core CSS -->
	    <link href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="<?= base_url() ?>public/css/font-awesome.min.css" rel="stylesheet">
		<!-- ionicons -->
		<link href="<?= base_url() ?>public/css/ionicons.min.css" rel="stylesheet">
		<!-- Simplify -->
		<link href="<?= base_url() ?>public/css/simplify.min.css" rel="stylesheet">
		<link href="<?= base_url() ?>public/css/select2/select2.css" rel="stylesheet"/>
		<!-- Dropzone -->
		<link href="<?= base_url() ?>public/css/dropzone/css/dropzone.css" rel="stylesheet">
		<!-- Date Time Picker -->
		<link href="<?= base_url() ?>public/css/datetimepicker.css" rel="stylesheet">
		<link href="<?= base_url() ?>public/css/solicitudes.css" rel="stylesheet">
  	</head>
  	<body class="overflow-hidden light-background">
		<div class="wrapper no-navigation preload">
			<div class="sign-in-wrapper">
				<div class="sign-in-inner">
					<div class="login-brand text-center">
						<i class="fa fa-user-md m-right-xs"></i> FeelsGood <strong class="text-skin">Registro</strong>
					</div>
					<form class="form-horizontal no-margin" id="form-registro-doctor">
						<div class="form-group">
							<div class="dropzone" id="fileImagen">
								<div class="fallback">
									<input name="file" type="file" />
								</div>
							</div>
							<input type="hidden" id="renombrado" name="renombrado">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="dniSolicitud" placeholder="DNI" data-parsley-required="true" data-parsley-type="number">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="nombreSolicitud" placeholder="Nombres" data-parsley-required="true">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="apellidosSolicitud" placeholder="Apellidos" data-parsley-required="true">
						</div>
						<div class="form-group">
							<select class="form-control" name="sexoSolicitud">
								<option value="1">Hombre</option>
								<option value="0">Mujer</option>
							</select>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="edadSolicitud" placeholder="Edad" data-parsley-required="true" data-parsley-type="number">
						</div>
						<div class="form-group">
							<input type="text" name="nacimientoSolicitud" placeholder="Fecha de nacimiento" 
							class="datepicker-input form-control">
						</div>
						<div class="form-group">
							<select class="form-control" name="estadoCivilSolicitud">
								<option value="1">Soltero</option>
								<option value="2">Casado</option>
								<option value="3">Viudo</option>
								<option value="4">Divorciado</option>
								<option value="5">Prefiero no indicar</option>
							</select>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="userSolicitud" placeholder="Email Address" data-parsley-required="true" data-parsley-type="email">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="passSolicitud" placeholder="Password" id="password" data-parsley-required="true">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="passSolicitud2"  data-parsley-equalto="#password" placeholder="Confirmar Password" data-parsley-required="true">
						</div>
						<div class="form-group">
							<select class="select2 width-100" name="paisSolicitud" id="paisSolicitud" data-parsley-required="true">
								<option value="">-- Seleccionar pais --</option>
								<?php foreach ($paises as $key) { ?>
									<option value="<?= $key->PaisCod ?>"><?=$key->PaisNom?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<select class="select2 width-100" name="provinciaSolicitud" id="provinciaSolicitud" data-parsley-required="true">
								<option value="">-- Seleccionar provincia --</option>
							</select>
						</div>
						<div class="form-group">
							<select class="select2 width-100" name="localidadSolicitud" id="localidadSolicitud" data-parsley-required="true">
								<option value="">-- Seleccionar localidad --</option>
							</select>
						</div>
						<div class="form-group">
							<select class="select2 width-100" name="profesionSolicitud" data-parsley-required="true">
								<option value="0">-- Seleccionar profesión u oficio --</option>
								<?php foreach ($profesiones as $key) { ?>
									<option value="<?= $key->profesion_id ?>"><?= $key->nombre_profesion ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="cmp" placeholder="Número de Colegio Médico del Perú (CMP)">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="rne" placeholder="Registro Nacional de Especialista (RNE)">
						</div>
						<div class="form-group" style="position:relative;">
							<select id="clinicas" multiple class="select2 width-100" placeholder="clinica(s)">
								<optgroup label="Clinicas">
								<?php foreach ($clinicas as $key) { ?>
									<option value="<?= $key->clinica_id ?>"><?= $key->nombre_clinica ?></option>
								<?php } ?>
								</optgroup>
							</select>
						</div>
						<div class="form-group" style="position:relative;">
							<div id="c-add-clinica" class="col-md-12" style="padding:0px;margin-bottom: 20px;">
								<div class="col-md-4" style="padding:0px;"><button data-parsley-excluded class="btn btn-success" id="agregar_clinica2">Agregar Clinica</button></div>
								<div class="col-md-8" style="padding:0px;">
									<input id="nueva_clinica" data-parsley-excluded type="text" class="form-control">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="custom-checkbox" style="width: auto;">
								<input type="checkbox" id="chkAccept" data-parsley-required="true">
								<label for="chkAccept"></label>
								<span style="padding-left: 12px;color: #000;">Acepto los terminos del servicio</span>
							</div>
						</div>
						<div class="m-top-md p-top-sm">
							<button type="submit" style="width:100%;" class="btn btn-success block">Crear cuenta</button>
						</div>
						<div class="m-top-md p-top-sm">
							<div class="font-12 text-center m-bottom-xs">Ya tienes una cuenta?</div>
							<a href="<?=base_url()?>" class="btn btn-default block">Inicia sesion</a>
						</div>
					</form>
				</div><!-- ./sign-in-inner -->
			</div><!-- ./sign-in-wrapper -->
		</div><!-- /wrapper -->
		<a href="" id="scroll-to-top" class="hidden-print"><i class="icon-chevron-up"></i></a>
		<div id="loading">
		    <i class="fa fa-spinner fa-spin m-right-xs"></i>Loading
		</div>
	    <!-- Le javascript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script type="text/javascript">
	    	base_url = '<?= base_url() ?>';
	    </script>
		<!-- Jquery -->
		<script src="<?= base_url() ?>public/js/jquery-1.11.1.min.js"></script>
		<!-- Bootstrap -->
	    <script src="<?= base_url() ?>public/bootstrap/js/bootstrap.min.js"></script>
		<!-- Slimscroll -->
		<script src='<?= base_url() ?>public/js/jquery.slimscroll.min.js'></script>
		<!-- Popup Overlay -->
		<script src='<?= base_url() ?>public/js/jquery.popupoverlay.min.js'></script>
		<!-- Modernizr -->
		<script src='<?= base_url() ?>public/js/modernizr.min.js'></script>
		<!-- Parsley -->
		<script src="<?= base_url() ?>public/js/parsley.min.js"></script>
		<!-- Simplify -->
		<script src="<?= base_url() ?>public/js/simplify/simplify.js"></script>
		<!-- Dropzone -->
		<script src='<?= base_url() ?>public/js/dropzone.min.js'></script>
		<!-- Moment -->
		<script src='<?= base_url() ?>public/js/uncompressed/moment.js'></script>
		<!-- Date Time picker -->
		<script src='<?= base_url() ?>public/js/uncompressed/bootstrap-datetimepicker.js'></script>
		<script src="<?= base_url() ?>public/js/select2.min.js"></script>
		<script src="<?= base_url() ?>public/js/registro.js"></script>
		<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>
  	</body>
</html>