<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <title>Gracias por su solicitud</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">

	    <!-- Bootstrap core CSS -->
	    <link href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		
		<!-- Font Awesome -->
		<link href="<?= base_url() ?>public/css/font-awesome.min.css" rel="stylesheet">

		<!-- ionicons -->
		<link href="<?= base_url() ?>public/css/ionicons.min.css" rel="stylesheet">
		
		<!-- Simplify -->
		<link href="<?= base_url() ?>public/css/simplify.min.css" rel="stylesheet">
	
  	</head>

  	<body class="overflow-hidden light-background">
		<div class="wrapper no-navigation preload">
			<div class="error-wrapper">
				<div class="error-inner" style="width:700px;">
					<div class="error-type">¡Gracias!</div>
					<h1>Por su solicitud</h1>
					<p>Su solicitud sera procesada por un administrador y se le informará a través de su correo si su perfil es aceptado en la plataforma.</p>
					<div class="m-top-md">
						<a href="<?= base_url() ?>" class="btn btn-default btn-lg text-upper">Ir al inicio</a>
					</div>
				</div><!-- ./error-inner -->
			</div><!-- ./error-wrapper -->
		</div><!-- /wrapper -->

		<a href="" id="scroll-to-top" class="hidden-print"><i class="icon-chevron-up"></i></a>

	    <!-- Le javascript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
		
		<!-- Jquery -->
		<script src="<?= base_url() ?>public/js/jquery-1.11.1.min.js"></script>
		
		<!-- Bootstrap -->
	    <script src="<?= base_url() ?>public/bootstrap/js/bootstrap.min.js"></script>
		
		<!-- Slimscroll -->
		<script src='<?= base_url() ?>public/js/jquery.slimscroll.min.js'></script>
		
		<!-- Popup Overlay -->
		<script src='<?= base_url() ?>public/js/jquery.popupoverlay.min.js'></script>

		<!-- Modernizr -->
		<script src='<?= base_url() ?>public/js/modernizr.min.js'></script>
		
		<!-- Simplify -->
		<script src="<?= base_url() ?>public/js/simplify/simplify.js"></script>

		<script>
			$(function()	{
				setTimeout(function() {
					$('.error-type').addClass('animated');
				},100);
			});
		</script>
		
  	</body>
</html>
