<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <title>Feels Good Recuperar Clave</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <!-- Bootstrap core CSS -->
	    <link href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="<?= base_url() ?>public/css/font-awesome.min.css" rel="stylesheet">
		<!-- ionicons -->
		<link href="<?= base_url() ?>public/css/ionicons.min.css" rel="stylesheet">
		<!-- jQuery steps -->
		<link href="<?= base_url() ?>public/css/jquery.steps.css" rel="stylesheet"/>
		<!-- Simplify -->
		<link href="<?= base_url() ?>public/css/simplify.min.css" rel="stylesheet">
		<style type="text/css">
			.wizard > .content{
				min-height: 11em;
			}
		</style>
  	</head>
  	<body class="overflow-hidden light-background">
		<div class="wrapper no-navigation preload">
			<div class="sign-in-wrapper" style="max-width: 700px;margin-left: auto;margin-right: auto;padding-top: 20px;">
				<div class="login-brand text-center">
					<i class="fa fa-user-md m-right-xs"></i> FeelsGood <strong class="text-skin">Recuperar Clave</strong>
				</div>
				<div class="padding-md">
					<!--<form class="form-horizontal" id="formRecuperarClave" data-validate="parsley" novalidate>-->
						<div id="wizard">
			                <h2>Enviar codigo</h2>
			                <section>
			                	<p>Ingrese su email para enviarle un código de autorización.</p>
			                    <div class="padding-md" style="padding-top: 3px!important;">
			                    	<form class="form-horizontal" id="formCorreo" data-validate="parsley" novalidate>
			                    		
							        <div class="form-group">
										<label>Email</label>
										<input type="email" id="correo" class="form-control" placeholder="Ingrese su email" data-parsley-required="true" data-parsley-group="step1">
									</div>
									</form>
								</div>
			                </section>
			                <h2>Ingresar codigo</h2>
			                <section>
			                	<p>Ingrese el código de autorización que se le envío a su email.</p>
			                    <div class="padding-md" style="padding-top: 3px!important;">
			                    	
							        <div class="form-group">
							        	<form class="form-horizontal" id="formCodigo" data-validate="parsley" novalidate>
										<label>Codigo</label>
										<input type="text" id="codigo" class="form-control" placeholder="Ingrese su código" data-parsley-required="true" data-parsley-group="step1">
										</form>
									</div>
								</div>
			                </section>
			                <h2>Cambiar clave</h2>
			                <section>
			                	<p>Ahora ya puedes modificar tu contraseña</p>
			                    <div class="padding-md" style="padding-top: 3px!important;">
			                    	
							        <div class="form-group">
							        	<form class="form-horizontal" id="formPass" data-validate="parsley" novalidate>
										<label>Contraseña</label>
										<input type="hidden" id="correoPass" name="correoPass" value="">
										<input type="password" id="pass" name="pass" class="form-control" placeholder="Ingrese su nueva contraseña" data-parsley-required="true" data-parsley-group="step1">
										</form>
									</div>
								</div>
			                </section>
			            </div>
		            <!--</form>-->
		        </div><!-- ./padding-md -->
			</div><!-- /main-container -->
		</div><!-- /wrapper -->
		<a href="#" class="scroll-to-top hidden-print"><i class="fa fa-chevron-up fa-lg"></i></a>
		<!-- ./modal -->
		<div class="modal fade" id="wizardModal">
			<div class="modal-dialog">
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        		<h4 class="modal-title">Exitoso</h4>
		      		</div>
		      		<div class="modal-body">
		        		<p>La contraseña ha sido cambiada exitosamente</p>
		      		</div>
		      		<div class="modal-footer">
		        		<a href="<?= base_url() ?>" class="btn btn-info">Login</a>
		      		</div>
		    	</div><!-- /.modal-content -->
		  	</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	    <!-- Le javascript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
		<!-- Jquery -->
		<script src="<?= base_url() ?>public/js/jquery-1.11.1.min.js"></script>
		<!-- Bootstrap -->
	    <script src="<?= base_url() ?>public/bootstrap/js/bootstrap.min.js"></script>
		<!-- Popup Overlay -->
		<script src='<?= base_url() ?>public/js/jquery.popupoverlay.min.js'></script>
		<!-- Parsley -->
		<script src="<?= base_url() ?>public/js/parsley.min.js"></script>
		<!-- jQuery Step -->
		<script src='<?= base_url() ?>public/js/jquery.steps.min.js'></script>
		<!-- Slimscroll -->
		<script src='<?= base_url() ?>public/js/jquery.slimscroll.min.js'></script>
		<!-- Modernizr -->
		<script src='<?= base_url() ?>public/js/modernizr.min.js'></script>
		<!-- Simplify -->
		<script src="<?= base_url() ?>public/js/simplify/simplify.js"></script>
		<script>
		var estado=1;
			$(function (){
	            $("#wizard").steps({
	                headerTag: "h2",
	                bodyTag: "section",
	                transitionEffect: "slideLeft",
	                onStepChanging: function (event, currentIndex, newIndex) { 
	                	console.log(newIndex);
	                	if(newIndex > currentIndex)	{ 

	                		if (newIndex==1) {

	                			if($('#formCorreo').parsley().validate('step' + newIndex))	{
			                		comprobar = false;
			                		var form= new FormData();
			                		form.append("correo",$("#correo").val());
			                		jQuery.ajax({
			                			type: 'POST',
			                			data: form,
			                			contentType: "application/json",
			                			processData: false,
	                        			dataType: 'json',
								        url:    "<?= base_url() ?>login/comprobar_correo?estado="+estado+"&correo="+$("#correo").val(),
								        async:   false,
								        success: function(response) {
								        	
								            if(response.status){
												alert("Ese correo no existe");
												comprobar = false;
											}else{
												$("#correoPass").val($("#correo").val());
												estado = 0;
												comprobar = true;
											}  
								        }
								    }); 
								    return comprobar;
			                		
			                	}else{
			                		return false;
		                		}

	                		}else if (newIndex==2){

	                			if($('#formCodigo').parsley().validate('step' + newIndex))	{
			                		comprobar = false;
			                		var form= new FormData();
			                		form.append("codigo",$("#codigo").val());
			                		jQuery.ajax({
			                			type: 'POST',
			                			data: form,
			                			contentType: "application/json",
			                			processData: false,
	                        			dataType: 'json',
								        url:    "<?= base_url() ?>login/comprobar_digito?codigo="+$("#codigo").val()+"&correo="+$("#correo").val(),
								        async:   false,
								        success: function(response) {
								        	
								            if(response.status){
												alert("Ese codigo es incorrecto");
												comprobar = false;
											}else{
												comprobar = true;
											}  
								        }
								    }); 
								    return comprobar;
			                		
			                	}else{
			                		return false;
		                		}

	                		}

		                }else{
		                	return true;
		                }
	                },
	                onFinished: function (event, currentIndex) { 

	                	$.post("<?= base_url() ?>login/modificarPass",$('#formPass').serialize(),function(response){
							if(response.status){
								$('#wizardModal').modal('show');
							}else{
								alert("No se modifico.");
							}
						},"json");
                		
	                }
	            });
	        });
		</script>
  	</body>
</html>