<!DOCTYPE html>
<html lang="es">
<head>
	<title>Panel</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>public/css/basic.css">
    <script>
        window.base_url = '<?= base_url() ?>';
    </script>
    <script src="<?= base_url() ?>public/js/jquery.min.js"></script>
    <script src="<?= base_url() ?>public/js/panel.js"></script>
    <script src="<?= base_url() ?>public/js/script.js"></script>

</head>
<body>
	<div id="errorMSG" class="panelMSG">Error</div>
	<div id="loadingMSG" class="panelMSG">Cargando</div>
	<div id="successMSG" class="panelMSG">Se ha completado una tarea</div>
	<section id="sidebar">
        <header>
            <div id="logo">
                <a href="<?= base_url() ?>panel"><img src="<?= base_url() ?>public/img/logo.png" width="200"></a>
            </div>
            <div id="panel_adts">
                <a href="<?= base_url() ?>panel/change_pass" title="Cambiar Contraseña"><img src="<?= base_url() ?>public/img/panel/chg_pass.png"></a>
                <a href="<?= base_url() ?>login/logout" title="Cerrar Sesión">czx<img src="<?= base_url() ?>public/img/panel/logout.png"></a>
            </div>
        </header>
        <nav>
            <div id="sub">
                <div class="sub">
                    
                </div>
            </div>  
            <ul>
                <li><a href="<?= base_url() ?>administrador"><span class="s_icons icon_slider"></span>Usuarios</a></li>

                
            </ul>
        </nav>
    </section>
    <section id="main">
