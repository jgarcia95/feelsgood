<div id="title_module">
    <h2><span class="s_icons icon_slider"></span>CREACIÓN DE NUEVO PERFIL</h2>
    <p class="clear"></p>
</div>
<div class="maintenance">
    <form id="form" action="" data-action="" method="post">
        <div class="box_form">
            <p class="label">Nombre(*)</p>
            <p class="ptxts"><input type="text" id="nombre" name="nombre" class="txts"></p>
        </div>
        <div class="btnsMaintenance">
            <p id="btns">
                <input type="submit" id="add_btn" name="add_btn" class="btns" value="Crear">
                <input type="button" id="back_btn" name="back_btn" class="btns" value="Atras">
            </p>
        </div>
        <input type="hidden" id="modulo" value="administrador/perfiles">
    </form>
</div>
<script>
$(document).ready(function(){
    $("#form").submit(function(e){
        e.preventDefault();
        $.post(window.base_url+'perfil/add',$("#form").serialize(),function(data){
            if(data.status){
                $("#back_btn").click();
            }else{
                gotravel.showError(data.msg);
            }
        },'json');
    });
});
</script>