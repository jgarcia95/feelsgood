<div class="padding-md">
	<ul class="breadcrumb">
		<li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
		<li>Gestion</li>
		<li>Pacientes</li>
	</ul>
	<div class="table-responsive">
		<table class="table table-striped table-hover" id="lista-pacientes">
			<thead>
				<tr>
					<th></th>
					<th>DNI</th>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Edad</th>
					<th>Email</th>
					<th>Teléfono</th>
					<th width="10%">Acciones</th>
					<th>Activo</th>
					<th>Online</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($pacientes as $key) { ?>
					<tr>
						<td style="min-width: 50px;"><?php if($key->imagen!=""){?>
							<img src="<?= base_url() ?>public/images/profile/<?=$key->imagen?>"
							style="width:50px;height:50px;">
						<?php } ?></td>
						<td><?= $key->dni ?></td>
						<td><?= $key->nombre ?></td>
						<td><?= $key->apellido ?></td>
						<td><?= $key->edad ?></td>
						<td><?= $key->email_user ?></td>
						<td><?= $key->fono ?></td>
						<td>
							<!--<a download="<?= $key->nombre.' '.$key->apellido ?>" href="<?= base_url() ?>paciente/descargar/<?=$key->id_paciente ?>"><i class="fa fa-file-excel-o fa-lg fa-fw" aria-hidden="true"></i></a>
							<a onClick="modalmapapaciente(<?=$key->id_paciente ?>)"><i class="fa fa-map-marker fa-lg fa-fw" style="cursor:pointer;" aria-hidden="true"></i></a>-->
							<a title="Ver perfil" href="<?= base_url() ?>administrador/perfil/<?=$key->id_paciente ?>"><button class="btn btn-default btn-xs"><i class="fa fa-eye fa-lg fa-fw" aria-hidden="true"></i></button></a>
							<a title="Ver sesiones" href="<?= base_url() ?>paciente/sesiones/<?=$key->id_paciente ?>"><button class="btn btn-default btn-xs"><i class="fa fa-calendar fa-lg fa-fw" aria-hidden="true"></i></button></a>
							<a title="Actualizar" href="<?= base_url() ?>administrador/editPaciente/<?= $key->id_paciente ?>"><button class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o fa-lg fa-fw" aria-hidden="true"></i></button></a>
							<a title="Eliminar" data-id="<?=$key->id_user?>" data-toggle="modal" href="#" data-target="#delModalPaciente" class="deldata"><button class="btn btn-default btn-xs"><i class="fa fa-times fa-lg fa-fw" aria-hidden="true"></i></button></a>

						</td>
						<td class="text-left">
							<?php if($key->status=="1"){
								echo '<span class="label label-success">Habilitado</span>';
							}else{
								echo '<span class="label label-danger">Inhabilitado</span>';
							} ?>
						</td>
						<td>
							<?php if ($key->online==1) { ?>
								<button class="btn btn-success btn-xs" style="width:90px;">Conectado</button>
							<?php }else{ ?>
								<button class="btn btn-default btn-xs" style="width:90px;">Desconectado</button>
							<?php } ?>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div><!-- ./padding-md -->
<div class="modal fade" id="delModalPaciente">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
				<h4 class="modal-title">Confirmar</h4>
			</div>
			<div class="modal-body">
				Eliminar registro?
			</div>
			<div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal" id="close_paciente_del">Cerrar</a>
				<a class="btn btn-primary" id="eliminar_paciente_ad">Eliminar</a>
			</div>
			<input type="hidden" id="id_paciente_eliminar">
		</div>
	</div>
</div>

<div class="modal fade" id="modalmapapaciente">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div id="mapaVisor" class="mapa" style="display:block;margin-top:0px;height:480px;"></div>
			</div>
		</div>
	</div>
</div>

<link href="<?= base_url() ?>public/css/dataTables.bootstrap.css" rel="stylesheet">
<script src='<?= base_url() ?>public/js/jquery.dataTables.min.js'></script>
<script src='<?= base_url() ?>public/js/uncompressed/dataTables.bootstrap.js'></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXPkUWREAI7gvPtLfhTECqZcjrnRpOcm4&v=3.exp"></script>
<script src="<?= base_url() ?>public/js/pacientes.js"></script>
