<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Gestion</li>
        <li><a href="<?= base_url() ?>solicitudes">Solicitudes</a></li>
        <li>Detalle Solicitud Clinica</li> 
    </ul>
    <div class="row">
        <form class="form-horizontal no-margin" style="padding:5%;">
            <div class="form-group">
                <label class="control-label col-lg-2">RUC</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?=$clinica[0]->ruc ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Nombre</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?=$clinica[0]->nombre ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Email</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?=$clinica[0]->email ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Contraseña</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?=$clinica[0]->pass ?></p>
                </div>
            </div>
        </form>
    </div>
</div><!-- ./padding-md -->