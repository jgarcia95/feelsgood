<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Gestion</li>
        <li>Administradores</li>   
    </ul>
    <div class="table-responsive">
        <table class="table table-striped table-hover" id="lista-administradores">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($usuarios as $key) { ?>
                <tr>
                    <td><?= $key->nombre ?></td>
                    <td><?= $key->apellido ?></td>
                    <td><?= $key->email_user ?></td>
                    <td><?php if($key->status=="1"){
                            echo '<span class="label label-success">Habilitado</span>';
                        }else{
                            echo '<span class="label label-danger">Inhabilitado</span>';
                        } ?></td>
                    <td>
                        <a title="Actualizar" href="<?= base_url() ?>administrador/editUsuario/<?= $key->id_administrador ?>"><button class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o fa-lg fa-fw" aria-hidden="true"></i></button></a>
                      <a title="Eliminar" data-id="<?=$key->id_user?>" data-toggle="modal" href="#" data-target="#delModalAdmin" class="deldata"><button class="btn btn-default btn-xs"><i class="fa fa-times fa-lg fa-fw" aria-hidden="true"></i></button></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div><!-- ./padding-md -->
<div class="modal fade" id="delModalAdmin">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Confirmar</h4>
            </div>
            <div class="modal-body">
                Eliminar registro?
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal" id="close_admin_del">Cerrar</a>
                <a class="btn btn-primary" id="eliminar_administrador">Eliminar</a>
            </div>
            <input type="hidden" id="id_administrador_eliminar">
        </div>
    </div>
</div>
<link href="<?= base_url() ?>public/css/dataTables.bootstrap.css" rel="stylesheet">
<script src='<?= base_url() ?>public/js/jquery.dataTables.min.js'></script>
<script src='<?= base_url() ?>public/js/uncompressed/dataTables.bootstrap.js'></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>
<script src="<?= base_url() ?>public/js/administradores.js"></script>