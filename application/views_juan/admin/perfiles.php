<div id="title_module">
    <h2>Perfiles</h2>
    <a id="lnk_module" href="<?= base_url() ?>perfil/addPerfil">Crear nuevo perfil</a>
    <p class="clear"></p>
</div>
<div class="listing">
    <table>
        <tbody><tr>
            <th>Nombre</th>
            <th></th>
        </tr>
        <?php
            foreach ($perfiles as $key) { ?>
               <tr class=""><td><?= $key->nombre ?></td>
               <td class="tdcenter">
                <a title="Actualizar" href="<?= base_url() ?>perfil/editPerfil/<?= $key->id_perfil ?>"><img src="<?= base_url() ?>public/img/panel/edt.png"></a>
                <a title="Eliminar" href="" class="deldata" data-id="<?= $key->id_perfil ?>"><img src="<?= base_url() ?>public/img/panel/del.png"></a></td></tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".deldata").click(function(e){
            e.preventDefault();

            var id= $(this).data("id");
            bootbox.confirm({
                size: "small",
                title: "Eliminar perfil",
                message: "¿Deseas eliminar este perfil?",
                buttons: {
                    confirm: {
                        label: "Si",
                        className: "btn-danger pull-left"
                    },
                    cancel: {
                        label: "No",
                        className: "btn-default pull-right"
                    }
                },
                callback: function(result) {
                    if (result) {
                        $.post(window.base_url+'perfil/delete',{id:id},function(data){
                            if(data.status){
                                location.reload();
                            }else{
                                gotravel.showError(data.msg);
                            }
                        },'json');
                    } 
                }
            });

        });
    });
</script>