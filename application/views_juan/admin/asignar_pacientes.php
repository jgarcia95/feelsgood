<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Gestion</li>
        <li>Asignar Pacientes</li>   
    </ul>
    <div class="form-group">
    	<label class="col-lg-2 control-label">Elija doctor</label>
	    <div class="col-lg-10">
			<select class="select2 width-100" id="doctor_elegido">
				<?php foreach ($doctores as $key) { ?>
				<option value="<?= $key->id_doctor ?>"><?= $key->nombre." ".$key->apellido ?></option>
				<?php } ?>
			</select>
		</div>
    </div>
    <div style="clear: both;"></div>
    <div class="form-group" style="margin-top: 30px;">
	    <table class="table table-striped" id="lista-pacientes-independientes">
	        <thead>
	            <tr>
	                <th>Foto</th>
	                <th>DNI</th>
	                <th>Nombre</th>
	                <th>Apellido</th>
	                <th>Email</th>
	                <th>Activo</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	            <?php foreach ($pacientes as $key) { ?>
	                <tr id="paciente<?= $key->id_paciente ?>">
	                	<td><?php if($key->imagen!=""){?>
	                        <img src="../public/images/profile/<?=$key->imagen?>" 
	                        style="width:50px;height:50px;">
	                        <?php } ?></td>
	                    <td><?= $key->dni ?></td>
	                    <td><?= $key->nombre ?></td>
	                    <td><?= $key->apellido ?></td>
	                    <td><?= $key->email_user ?></td>
	                    <td>
	                        <?php if($key->status=="1"){
	                            echo '<span class="label label-success">Habilitado</span>';
	                        }else{
	                            echo '<span class="label label-danger">Inhabilitado</span>';
	                        } ?>
	                    </td>
	                    <td>
	                        <a title="Ver perfil" href="<?= base_url() ?>administrador/perfil/<?= $key->id_paciente ?>" target="_blank"><button class="btn btn-default btn-xs"><i class="fa fa-eye fa-lg fa-fw" aria-hidden="true"></i></button></a>
	                        <a title="Asignar" style="cursor:pointer;" onClick="asignar(<?= $key->id_paciente ?>,this)"><button class="btn btn-default btn-xs"><i class="fa fa-check-square-o fa-lg fa-fw" aria-hidden="true"></i></button></a>
	                    </td>
	                </tr>
	            <?php } ?>
	        </tbody>
	    </table>
    </div>
</div><!-- ./padding-md -->
<link href="<?= base_url() ?>public/css/dataTables.bootstrap.css" rel="stylesheet">
<script src='<?= base_url() ?>public/js/jquery.dataTables.min.js'></script>
<script src='<?= base_url() ?>public/js/uncompressed/dataTables.bootstrap.js'></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>
<!-- Select2 -->
<script src='<?= base_url() ?>public/js/select2.min.js'></script>
<link href="<?= base_url() ?>public/css/select2/select2.css" rel="stylesheet"/>	
<script src="<?= base_url() ?>public/js/pacientes.js"></script>