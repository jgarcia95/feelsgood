<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Gestion</li>
        <li><a href="<?= base_url() ?>solicitudes">Solicitudes</a></li>
        <li>Detalle Solicitud doctor</li> 
    </ul>
    <div class="row">
        <form class="form-horizontal no-margin" style="padding:5%;">
            <div class="form-group">
                <label class="control-label col-lg-2">Foto</label>
                <div class="col-lg-10">
                    <p class="form-control-static">
                        <img src="../../../public/images/profile/<?=$doctor[0]->imagen?>" 
                        style="width:54px;height:54px;">
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Genero</label>
                <div class="col-lg-10">
                    <p class="form-control-static">
                        <?php if($doctor[0]->sexo==1){ echo "Hombre"; }?>
                        <?php if($doctor[0]->sexo==0){ echo "Mujer"; }?>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Edad</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?=$doctor[0]->edad?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Fecha de nacimiento</label>
                <?php list($anio,$mes,$dia)=explode("-",trim($doctor[0]->nacimiento)); ?>
                <div class="col-lg-10">
                    <p class="form-control-static"><?=$dia.'-'.$mes.'-'.$anio?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Estado civil</label>
                <div class="col-lg-10">
                    <p class="form-control-static">
                        <?php if($doctor[0]->estado_civil==1){ echo "Soltero"; } ?>
                        <?php if($doctor[0]->estado_civil==2){ echo "Casado"; } ?>
                        <?php if($doctor[0]->estado_civil==3){ echo "Viudo"; } ?>
                        <?php if($doctor[0]->estado_civil==4){ echo "Divorciado"; } ?>
                        <?php if($doctor[0]->estado_civil==5){ echo "Prefiero no indicar"; } ?>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Pais</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?=$pais->PaisNom?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Provincia</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?=$prov->ProvinNom?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Localidad</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?=$locali->LocalidNom?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Profesion</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?php if($prof){ echo $prof->nombre_profesion; }?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Clinica(s)</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?=$clinicas?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Nombre</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?= $doctor[0]->nombres ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Apellido</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?= $doctor[0]->apellidos ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Email</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?= $doctor[0]->email ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2">Contraseña</label>
                <div class="col-lg-10">
                    <p class="form-control-static"><?= $doctor[0]->pass ?></p>
                </div>
            </div>
        </form>
    </div>
</div><!-- ./padding-md -->