<link href="<?=base_url()?>public/css/fullcalendar.css" rel="stylesheet">
<link href="<?=base_url()?>public/css/fullcalendar.print.css" rel="stylesheet">
<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li><a href="<?= base_url() ?>administrador/pacientes">Pacientes</a></li>
        <li>Sesiones</li>   
    </ul>
    <!--<div class="full-calendar-wrapper clearfix">
        <div class="full-calendar-body clearfix" style="margin:0 auto;float:none;">
            <div class="full-calendar"></div>
        </div>
    </div>-->
    <div class=" clearfix">
        <br>
        <div class="col-md-12 text-center">
            <span style="min-width: 10px; padding: 5px; height: 10px; background: #787878; border-radius: 3px; color: #FFF">Ejercicio Completado</span>
            &nbsp;&nbsp;
            <span style="min-width: 10px; padding: 5px; height: 10px; background: #E0E0E0; border-radius: 3px; color: #181818;">Ejercicio No Completado</span>
        </div>
        <br><br>
        <div class="full-calendar-body clearfix" >
            <div class="full-calendar"></div>
        </div>
    </div>
</div><!-- ./padding-md -->
<div class="modal fade" id="modalEditaSesion">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Modificar dosificacion</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal m-top-md" id="dosificacion">
                    <input type="hidden" id="id_paciente" name="id_paciente" value="<?=$id?>">
                    <input type="hidden" id="id_sesion" name="id_sesion">
                    <input type="hidden" id="id_ejercicio" name="id_ejercicio">
                    <input type="hidden" name="editar" value="1">
                    <div id="body-form-dosificacion"></div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">Cerrar</a>
				<?php if($this->session->userdata('rol')<=2){?>
					<a class="btn btn-primary" id="modificar_dosificacion">Actualizar</a>
					<a class="btn btn-danger" id="eliminar_dosificacion">Eliminar</a>
				<?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalDelSesion">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirmaci&oacute;n</h4>
            </div>
            <div class="modal-body">
                Eliminar Sesi&oacute;n?
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">Cancelar</a>
                <a class="btn btn-danger" id="eliminar_dosificacion_confirm">Eliminar</a>
            </div>
        </div>
    </div>
</div>
<script src='<?=base_url()?>public/js/moment.min.js'></script>
<script src='<?=base_url()?>public/js/fullcalendar.min.js'></script>
<script src='<?=base_url()?>public/js/jquery.noty.packaged.min.js'></script>
<script src='<?= base_url() ?>public/js/uncompressed/bootstrap-datetimepicker.js'></script>
<script src="<?=base_url()?>public/js/sesiones.js"></script>