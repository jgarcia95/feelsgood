<!-- Date Time Picker -->
<link href="<?= base_url() ?>public/css/datetimepicker.css" rel="stylesheet">
<!-- Dropzone -->
<link href="<?= base_url() ?>public/css/dropzone/css/dropzone.css" rel="stylesheet">
<link href="<?= base_url() ?>public/css/solicitudes.css" rel="stylesheet">
<link href="<?= base_url() ?>public/css/select2/select2.css" rel="stylesheet"/>
<div class="padding-md">
    <ul class="breadcrumb">
        <li><span class="primary-font"><i class="icon-home"></i></span><a href="<?= base_url() ?>"> Home</a></li>
        <li>Creación de usuarios</li>
        <li>Paciente</li> 
    </ul>
    <div class="row">
        <form id="form-add-paciente">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Datos del paciente</label>
                </div>
                <div class="form-group">
                    <div class="dropzone" id="fileImagen">
                        <div class="fallback">
                            <input name="file" type="file" />
                        </div>
                    </div>
                    <input type="hidden" id="renombrado" name="renombrado">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="dniSolicitud" placeholder="DNI" data-parsley-required="true" data-parsley-type="number">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="nombreSolicitud" placeholder="Nombres" data-parsley-required="true">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="apellidosSolicitud" placeholder="Apellidos" data-parsley-required="true">
                </div>
                <div class="form-group">
                    <select class="form-control" name="sexoSolicitud">
                        <option value="1">Hombre</option>
                        <option value="0">Mujer</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="edadSolicitud" placeholder="Edad" data-parsley-required="true" data-parsley-type="number">
                </div>
                <div class="form-group">
                    <input type="text" name="nacimientoSolicitud" placeholder="Fecha de nacimiento" 
                    class="datepicker-input form-control">
                </div>
                <div class="form-group">
                    <select class="form-control" name="estadoCivilSolicitud">
                        <option value="1">Soltero</option>
                        <option value="2">Casado</option>
                        <option value="3">Viudo</option>
                        <option value="4">Divorciado</option>
                        <option value="5">Prefiero no indicar</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" name="grupoSolicitud" placeholder="Grupo sanguineo" 
                    class="form-control">
                </div>
                <div class="form-group">
                    <select class="select2 width-100" name="paisSolicitud" id="paisSolicitud" data-parsley-required="true">
                        <option value="">-- Seleccionar pais --</option>
                        <?php foreach ($paises as $key) { ?>
                            <option value="<?= $key->PaisCod ?>"><?=$key->PaisNom?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <select class="select2 width-100" name="provinciaSolicitud" id="provinciaSolicitud" data-parsley-required="true">
                        <option value="">-- Seleccionar provincia --</option>
                    </select>
                </div>
                <div class="form-group">
                    <select class="select2 width-100" name="localidadSolicitud" id="localidadSolicitud" data-parsley-required="true">
                        <option value="">-- Seleccionar localidad --</option>
                    </select>
                </div>
                <div class="form-group">
                    <select class="select2 width-100" name="profesionSolicitud">
                        <option value="">-- Seleccionar profesión u oficio --</option>
                        <?php foreach ($profesiones as $key) { ?>
                            <option value="<?= $key->profesion_id ?>"><?= $key->nombre_profesion ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group" id="content-clinica" style="position:relative;">
                    <select id="clinica" name="clinica" class="select2 width-100">
                        <?php foreach ($clinicas as $key) { ?>
                            <option value="<?= $key->clinica_id ?>"><?= $key->nombre_clinica ?></option>
                        <?php } ?>
                    </select>
                    <i id="img-edit" style="position: absolute;right: -35px;top: 6px;font-size: 23px;cursor: pointer;display: none;" class="fa fa-pencil-square-o fa-lg fa-fw" aria-hidden="true"></i>
                </div>
                <div class="form-group" style="position:relative;">
                    <div id="c-add-clinica" class="col-md-12" style="padding:0px;margin-bottom: 20px;">
                        <div class="col-md-8" style="padding:0px;">
                            <input id="nueva_clinicap" data-parsley-excluded type="text" class="form-control">
                        </div>
                        <div class="col-md-4" style="padding:0px;"><button data-parsley-excluded class="btn btn-success" id="agregar_clinica2p">Crear Clínica</button></div>
                    </div>
                    <div id="c-edit-clinica" class="col-md-12" style="padding:0px;margin-bottom:20px;display:none;">
                        <div class="col-md-4" style="padding:0px;"><button class="btn btn-success" id="agregar_clinica3p">Editar Clinica</button></div>
                        <div class="col-md-8" style="padding:0px;">
                            <input id="nueva_clinica2p" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <!--<div class="form-group">
                    <select class="form-control" name="controlSolicitud">
                        <option value="">-- Tipo de control --</option>
                        <option value="Visor">Visor</option>
                        <option value="Mando">Mando</option>
                    </select>
                </div>-->
                <div class="form-group">
                    <label class="control-label">Datos del acompañante</label>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="dniAcomSolicitud" placeholder="DNI" data-parsley-required="true" data-parsley-type="number">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="nombreAcomSolicitud" placeholder="Nombres" data-parsley-required="true">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="apellidosAcomSolicitud" placeholder="Apellidos" data-parsley-required="true">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="edadAcomSolicitud" placeholder="Edad" data-parsley-required="true" data-parsley-type="number">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="fonoAcom" placeholder="Teléfono fijo o movil" data-parsley-type="number">
                </div>
                <!--<div class="form-group">
                    <select class="select2 width-100" name="profesionAcomSolicitud">
                        <option value="">-- Seleccionar profesión u oficio --</option>
                        <?php foreach ($profesiones as $key) { ?>
                            <option value="<?= $key->profesion_id ?>"><?= $key->nombre_profesion ?></option>
                        <?php } ?>
                    </select>
                </div>-->
            </div><!-- ./segmento izquiero -->
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Antecedentes del paciente</label>
                </div>
                <?php foreach ($enf_personales as $key) { ?>
                <div class="form-group col-md-6">
                    <div class="custom-checkbox" style="width: auto;">
                        <input type="checkbox" id="personales<?=$key->id_enfermedad?>" 
                            value="<?=$key->id_enfermedad?>" name="personales[]">
                        <label for="personales<?=$key->id_enfermedad?>"></label>
                        <span style="padding-left: 12px;color: #000;"><?=$key->nombre?></span>
                    </div>
                </div>
                <?php } ?>
                <div class="form-group">
                    <input type="text" class="form-control" name="otros_personales" placeholder="Otros antecedentes personales">
                </div>
                <div class="form-group">
                    <label class="control-label">Antecedentes familiares</label>
                </div>
                <?php foreach ($enf_familiares as $key) { ?>
                <div class="form-group col-md-6">
                    <div class="custom-checkbox" style="width: auto;">
                        <input type="checkbox" id="familiares<?=$key->id_enfermedad?>" 
                        value="<?=$key->id_enfermedad?>" name="familiares[]">
                        <label for="familiares<?=$key->id_enfermedad?>"></label>
                        <span style="padding-left: 12px;color: #000;"><?=$key->nombre?></span>
                    </div>
                </div>
                <?php } ?>
                <div class="form-group">
                    <input type="text" class="form-control" name="otros_familiares" placeholder="Otros antecedentes familiares">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="observSolicitud" placeholder="Observaciones">
                </div>
                <div class="form-group" style="padding-top: 8px;">
                    <label class="control-label">Información de usuario</label>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="userSolicitud" placeholder="Email Address"  data-parsley-type="email">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="fono" id="fono" placeholder="Teléfono fijo o movil" data-parsley-type="number">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="passSolicitud" placeholder="Password" id="password" data-parsley-required="true">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="passSolicitud2"  data-parsley-equalto="#password" placeholder="Confirmar Password" data-parsley-required="true">
                </div>
            </div><!--segmento derecho-->
            <div class="text-right m-top-md">
                <input class="btn btn-info" value="Registrar" type="submit">
            </div>
        </form>
    </div>
</div><!-- ./padding-md -->
<!-- Moment -->
<script src='<?= base_url() ?>public/js/uncompressed/moment.js'></script>
<!-- Date Time picker -->
<script src='<?= base_url() ?>public/js/uncompressed/bootstrap-datetimepicker.js'></script>
<!-- Dropzone -->
<script src='<?= base_url() ?>public/js/dropzone.min.js'></script>
<!-- Moment -->
<script src='<?= base_url() ?>public/js/uncompressed/moment.js'></script>
<script src="<?= base_url() ?>public/js/select2.min.js"></script>
<script src="<?= base_url() ?>public/js/parsley.min.js"></script>
<script src="<?= base_url() ?>public/js/pacientes.js"></script>
<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>