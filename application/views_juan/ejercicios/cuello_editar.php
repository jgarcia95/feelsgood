<div class="form-group">
	<label class="col-sm-3 control-label">Fecha de sesión</label>
	<div class="col-sm-9">
		<input class="datetimepicker-input form-control" type="text" name="fecha" id="fecha" 
		value="<?=date("d-m-Y h:i A",strtotime($fecha))?>">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Dosificación</label>
	<div class="col-sm-9">
		<select class="form-control" name="tipo_ejercicio" id="tipo_ejercicio_edit">
			<option value="1"<?php if ($tipo==1){ ?> selected="true" <?php } ?>>Manual</option>
			<option value="2"<?php if ($tipo==2){ ?> selected="true" <?php } ?>>Al azar</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Velocidad del movimiento</label>
	<div class="col-sm-9">
		<input class="form-control" type="text" name="tiempo_ejercicio" id="tiempo_ejercicio" placeholder="En segundos" 
		value="<?=$tiempo_ejercicio?>">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tipo de ejercicio</label>
	<div class="col-sm-9">
		<select class="form-control" name="tipo_movimiento_ajax" id="tipo_movimiento_ajax_edit">
			<option <?php echo ( $tipo == 1 ) ? 'selected' : ''; ?> value="1">--Seleccione--</option>
			<option <?php echo ( $tipo == 2 ) ? 'selected' : ''; ?> value="2">Rotación con estiramiento</option>
			<option <?php echo ( $tipo == 3 ) ? 'selected' : ''; ?> value="3">Rotación cabeza y cuello</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Rango de movimiento</label>
	<div class="col-sm-9">
		<select class="form-control" name="grados" id="grados_edit">
			<option <?php echo ( $grados == 1 ) ? 'selected' : '' ?> value="1">80</option>
			<option <?php echo ( $grados == 2 ) ? 'selected' : '' ?> value="2">100</option>
			<option <?php echo ( $grados == 3 ) ? 'selected' : '' ?> value="3">120</option>
			<option <?php echo ( $grados == 4 ) ? 'selected' : '' ?> value="4">140</option>
			<option <?php echo ( $grados == 5 ) ? 'selected' : '' ?> value="5">160</option>
			<option <?php echo ( $grados == 6 ) ? 'selected' : '' ?> value="6">180</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tutorial</label>
	<div class="col-sm-9">
		<label class="toogleswitch">
			<input type="checkbox" name="tutorial" id="tutorial" <?php if ($tutorial=="on"){ ?> checked="true" <?php } ?>>
			<span class="toogleslider toogleround"></span>
		</label>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Escala visual analógica</label>
	<div class="col-sm-9">
		<label class="toogleswitch">
			<input type="checkbox" name="perrito" id="perrito" <?php if ($perrito=="on"){ ?> checked="true" <?php } ?>>
			<span class="toogleslider toogleround"></span>
		</label>
	</div>
</div>
<div class="form-group m-top-lg" id="div-button-add-move-edit">
	<label class="col-sm-3 control-label"></label>
	<div class="col-sm-9">
		<a style="cursor:pointer" class="btn btn-success m-left-xs" id="button-add-move-edit">Agregar movimiento</a>
	</div>
</div>
<div id="slider_movimientos_edit" style="position:relative">
	<?php 
	if( $repeticiones == '[{}]' ){}
	else{
	?>
	<div id="sortable_edit" style="width: <?=sizeof(json_decode($repeticiones))*210?>px">
		<?php foreach (json_decode($repeticiones) as $value) {
			$datos="\"TipoRepeticion\":\"".$value->TipoRepeticion."\",";
			$datos.="\"grados\":\"".$value->grados."\"";
			switch ($value->TipoRepeticion) {
				case '2':$texto="Rotación con estiramiento";break;
				case '3':$texto="Rotación cabeza y cuello";break;
			}
			$texto.=" con ".$value->grados." grados de rotación";
			?>
			<div class="ui-state-default" data-datos='<?=$datos?>' draggable="true">
				<button type="button" class="close"><span aria-hidden="true">×</span></button>
				<?=$texto?>
			</div>
		<?php } ?>
	</div>
	<?php } ?>
</div>
<input id="array_movimientos_edit" name="array_movimientos_edit" type="hidden">