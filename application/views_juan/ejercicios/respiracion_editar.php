<div class="form-group">
	<label class="col-sm-3 control-label">Fecha de Sesión</label>
	<div class="col-sm-9">
		<input class="datetimepicker-input form-control" name="fecha" id="fecha" type="text" 
		value="<?=date("d-m-Y h:i A",strtotime($fecha))?>">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Acostado</label>
	<div class="col-sm-9">
		<label class="toogleswitch">
			<input type="checkbox" onchange="cambioEscenarios()" <?php echo ( $echado == 1 ) ? 'checked' : ''; ?> name="echado" id="echado">
			<span class="toogleslider toogleround"></span>
		</label>
	</div>
</div>
<div class="col-md-12"></div>
<div class="form-group" id="div_tip" <?php if( isset($echado) && $echado != 1 ){ echo 'style="display: none;"'; }else{} ?>>
	<label class="col-md-3 control-label">Tipo de Escenario</label><br>
	<div class="col-md-9">
		<div class="radio inline-block">
			<div class="custom-radio m-right-xs">
				<input type="radio" id="tipo_video" value="video" <?php echo ( isset($tipo) && $tipo == 'video' ) ? 'checked' : ''; ?> name="tip">
				<label for="tipo_video"></label>
			</div>
			<div class="inline-block vertical-top">Video</div>
		</div>
		<div class="radio inline-block">
			<div class="custom-radio m-right-xs">
				<input type="radio" id="tipo_acuario" value="acuario" <?php echo ( isset($tipo) && $tipo == 'acuario' ) ? 'checked' : ''; ?> name="tip">
				<label for="tipo_acuario"></label>
			</div>
			<div class="inline-block vertical-top">Acuario</div>
		</div>
	</div>
</div>			

<div class="form-group" id="div_escenario" <?php if( isset( $tipo ) && $tipo == 'acuario' ){ echo 'style="display: none;"'; } else{} ?>>
	<label class="col-sm-3 control-label">Escenario</label>
	<div class="col-sm-9">
		<select class="form-control" name="escenario" id="escenario">

			<?php 
				if( $echado == 1 ){

					if( isset($tipo) && $tipo == 'video' ){
			?>
						<option <?php echo ( $id_escenario == 0 ) ? 'selected' : ''; ?> value="0">Video 1</option>
					    <option <?php echo ( $id_escenario == 1 ) ? 'selected' : ''; ?> value="1">Video 2</option>
					    <option <?php echo ( $id_escenario == 2 ) ? 'selected' : ''; ?> value="2">Video 3</option>
					    <option <?php echo ( $id_escenario == 3 ) ? 'selected' : ''; ?> value="3">Video 4</option>
			<?php
					}

				} 
				else{
			?>
				<?php foreach ($escenarios_ini as $value) { ?>
					<option value="<?=$value->id?>" 
						<?php if ($id_escenario==$value->id){ ?> selected="true" <?php } ?>>
						<?=$value->nombre?></option>
				<?php } ?>
			<?php } ?>
		</select>
	</div>
</div>
<div class="form-group" id="div_ciclo" <?php if( isset($echado) && $echado == 1 ){ echo 'style="display: none;"'; } else{} ?>>
	<label class="col-sm-3 control-label">Transición Dia/Noche</label>
	<div class="col-sm-9">
		<label class="toogleswitch">
			<input type="checkbox" <?php echo ( $ciclo == 1 ) ? 'checked' : ''; ?> name="ciclo" id="ciclo">
			<span class="toogleslider toogleround"></span>
		</label>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Repeticiones</label>
	<div class="col-sm-9">
		<input class="form-control" name="repeticiones" id="repeticiones" placeholder="" min="2" type="number" value="<?=$repeticiones?>">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tiempo Inhalación</label>
	<div class="col-sm-9">
		<input class="form-control" name="inhalacion" id="inhalacion" placeholder="En segundos" min="0" type="number" value="<?=$inhalacion?>">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tiempo Mantener Respiración</label>
	<div class="col-sm-9">
		<input class="form-control" name="mantener" id="mantener" placeholder="En segundos" min="0" type="number" value="<?=$mantener?>">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tiempo Exhalación</label>
	<div class="col-sm-9">
		<input class="form-control" name="exhalacion" id="exhalacion" placeholder="En segundos" min="0" type="number" value="<?=$exhalacion?>">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tiempo Descanso</label>
	<div class="col-sm-9">
		<input class="form-control" name="descanso" id="descanso" placeholder="En segundos" min="0" type="number" value="<?=$descanso?>">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tutorial</label>
	<div class="col-sm-9">
		<label class="toogleswitch">
			<input name="tutorial" id="tutorial" type="checkbox" <?php if ($tutorial=="on"){ ?> checked="true" <?php } ?>>
			<span class="toogleslider toogleround"></span>
		</label>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Escala Visual Analógica</label>
	<div class="col-sm-9">
		<label class="toogleswitch">
			<input name="perrito" id="perrito" type="checkbox" <?php if ($perrito=="on"){ ?> checked="true" <?php } ?>>
			<span class="toogleslider toogleround"></span>
		</label>
	</div>
</div>
<!---->
<script type="text/javascript">

	var sce = '';
	var scenes = '<option value="0">Video 1</option>'+
    '<option value="1">Video 2</option>'+
    '<option value="2">Video 3</option>'+
    '<option value="3">Video 4</option>';
	<?php foreach ($escenarios_ini as $value) { ?>
		sce += '<option value="<?=$value->id?>"><?=$value->nombre?></option>';
	<?php } ?>

	function cambioEscenarios(){

		if( $("#echado").prop("checked") == true ){

			$("#escenario").empty().html( scenes );
			$("#div_tip").show();
			$("#tip").val("video");
			$("#div_ciclo").hide();
			$("#ciclo").removeAttr('checked');
		}
		else{

			$("#escenario").empty().html( sce );
			$("#div_tip").hide();
			$("#tip").val("video");
			$("#div_escenario").show();
			$("#div_ciclo").show();
			$("#ciclo").attr('checked');
		}
	}

	$("input[name='tip']").change(function(e){

		if( $(this).val() == 'acuario' ){

			$("#div_escenario").hide();
			$("#escenario").attr('disabled','disabled');
		}
		else{

			$("#div_escenario").show();
			$("#escenario").removeAttr('disabled');
			$("#escenario").empty().html( scenes );
		}
	});

</script>