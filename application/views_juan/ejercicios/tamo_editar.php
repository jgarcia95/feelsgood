<div class="form-group">
	<label class="col-sm-3 control-label">Fecha de sesión</label>
	<div class="col-sm-9">
		<input class="datetimepicker-input form-control" name="fecha" id="fecha" type="text" 
		value="<?=date("d-m-Y h:i A",strtotime($fecha))?>">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Repeticiones</label>
	<div class="col-sm-9">
		<input class="form-control" name="repeticiones" min="2" value="<?php echo (isset($repeticiones)) ? $repeticiones : '' ?>" id="repeticiones" placeholder="" type="number">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tiempo Inhalación</label>
	<div class="col-sm-9">
		<input class="form-control" value="<?php echo (isset($inhalacion)) ? $inhalacion : '' ?>" name="inhalacion" id="inhalacion" placeholder="En segundos" type="number" min="1">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tiempo Mantener Respiración</label>
	<div class="col-sm-9">
		<input class="form-control" value="<?php echo (isset($mantener)) ? $mantener : '' ?>" name="mantener" id="mantener" placeholder="En segundos" type="number" min="1">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tiempo Exhalación</label>
	<div class="col-sm-9">
		<input class="form-control" value="<?php echo (isset($exhalacion)) ? $exhalacion : '' ?>" name="exhalacion" id="exhalacion" placeholder="En segundos" type="number" min="1">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tiempo Descanso</label>
	<div class="col-sm-9">
		<input class="form-control" value="<?php echo (isset($descanso)) ? $descanso : '' ?>" name="descanso" id="descanso" placeholder="En segundos" type="number" min="1">
	</div>
</div>
<div class="col-md-12"></div>
<div class="form-group">
	<label class="col-sm-3 control-label">Escala Visual Analógica</label>
	<div class="col-sm-9">
		<label class="toogleswitch">
			<input name="perrito" <?php if(isset($perrito) && $perrito == 'on' ){ echo 'checked'; }else{  } ?> id="perrito" type="checkbox">
			<span class="toogleslider toogleround"></span>
		</label>
	</div>
</div>
