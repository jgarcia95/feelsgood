	<div class="smart-widget m-top-lg widget-dark-blue">
		<div class="smart-widget-header">
			Ejercicio Bote
		</div>
		<div class="smart-widget-inner">
			<div class="smart-widget-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Fecha de Sesión</label>
							<input class="datetimepicker-input form-control" name="fecha" id="fecha" type="text">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Dificultad</label>
							<select class="form-control" name="dificultad" id="dificultad">
								<option value="">--Seleccione--</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Circuito</label>
							<select class="form-control" name="circuito" id="circuito">
								<option value="">--Seleccione--</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Dirección</label>
							<select class="form-control" name="direccion" id="direccion">
								<option value="">--Seleccione--</option>
								<option value="1">IZQUIERDA</option>
								<option value="2">DERECHA</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Repeticiones</label>
							<input class="form-control" name="monedas" id="monedas" type="number" min="10" max="30">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tutorial</label>
							<label class="toogleswitch">
								<input type="checkbox" name="tutorial" id="tutorial">
								<span class="toogleslider toogleround"></span>
							</label>
						</div>
					</div>
				</div>
				<div class="row"><br><br></div>

			</div>
		</div><!-- ./smart-widget-inner -->
	</div><!-- ./smart-widget -->
