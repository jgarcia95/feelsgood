<?php
	$nombres = array();
	$scen = array();
?>

<?php 

$aux = json_decode( $escenarios );

	for( $i = 0; $i < count( $aux ); $i++ ) {

		if( isset( $aux[$i]->id_escenario ) ){
			$scen[ $aux[$i]->id_escenario ] = 1;
		}
		else{}
	}

	foreach ($escenarios_ini as $val) {
							
		$nombres[$val->id] = $val->nombre;
	}

	
?>

<div class="form-group">
	<label class="col-sm-3 control-label">Fecha de sesión</label>
	<div class="col-sm-8">
		<input class="datetimepicker-input form-control" name="fecha" id="fecha" type="text" value="<?=date("d-m-Y h:i A",strtotime($fecha))?>">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Respiración</label>
	<div class="col-sm-8">
		<label class="toogleswitch">
			<input type="checkbox" <?php if ($respiracion=="1"){ ?> checked="true" <?php } ?> onchange="displayCamposDependientes()" name="respiracion" id="respiracion">
			<span class="toogleslider toogleround"></span>
		</label>
	</div>
</div>
<div id="div_campos_dependientes" class="row" <?php if ($respiracion=="0"){ ?> style="display: none;" <?php } else{} ?>>
	<div class="form-group">
		<label class="col-sm-3 control-label">Repeticiones</label>
		<div class="col-sm-8">
			<input class="form-control" name="repeticiones" id="repeticiones" placeholder="En segundos (Minimo 2)" type="number" min="2" value="<?php echo ( $repeticiones >= 2 ) ? $repeticiones : 2;?>">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Inhalación</label>
		<div class="col-sm-8">
			<input class="form-control" name="inhalacion" id="inhalacion" placeholder="En segundos (Minimo 4)" type="number" min="4" value="<?php echo ( $inhalacion >= 4 ) ? $inhalacion : 4;?>">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Sostenimiento</label>
		<div class="col-sm-8">
			<input class="form-control" name="sostenimiento" id="sostenimiento" placeholder="En segundos (Minimo 4)" type="number" min="4" value="<?php echo ( $sostenimiento >= 4 ) ? $sostenimiento : 4;?>">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Exhalación</label>
		<div class="col-sm-8">
			<input class="form-control" name="exhalacion" id="exhalacion" placeholder="En segundos (Minimo 4)" type="number" min="4" value="<?php echo ( $exhalacion >= 4 ) ? $exhalacion : 4;?>">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Descanso</label>
		<div class="col-sm-8">
			<input class="form-control" name="descanso" id="descanso" placeholder="En segundos (Minimo 4)" type="number" min="4" value="<?php echo ( $descanso >= 4 ) ? $descanso : 4;?>">
		</div>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 control-label">Tipo</label>
	<div class="col-sm-8">
		<select class="form-control" name="tipo" id="tipo_emo">
			<option value="">--Seleccione--</option>
			<option <?php echo ( $tipo == 'A' ) ? "selected" : "";  ?> value="A">Ansiedad</option>
			<option <?php echo ( $tipo == 'E' ) ? "selected" : "";  ?> value="E">Estrés</option>
		</select>
	</div>
</div>
<div class="form-group">
	<div class="col-md-12">
		<div id="cont-abs2" style="min-height: 300px;">
	        <div class="cont-img-flecha" id="imgflecha1">
	        	<img src="<?= base_url() ?>public/images/flecha1.png">
	        </div>
	        <div  class="cont-img-flecha"  id="imgflecha2">
	        	<img src="<?= base_url() ?>public/images/flecha2.png">
	        </div>
	                    
	        <div id="contenedorroyal" style="overflow:hidden">
	            <div id="contSliders" class="col-lg-6 col-sm-12 col-xs-6" style="padding:0px;">
	                <?php 
	                	foreach( $escenarios_ini as $key ) { 

	                	?>
	                    <div class="col-lg-12">
	                        <div class="statistic-box bg-primary m-bottom-md" >
	                            <div class="statistic-title">
	                            </div>
	                            <div class="statistic-value">
									<?= $key->nombre ?>
	                            </div>
	                            <div class="m-top-md">
									<a style="cursor:pointer" data-nombre="<?= $key->nombre ?>" <?php if( isset( $scen[$key->id] ) && $scen[$key->id] == 1 ){ echo "disabled"; }else{ } ?>  data-id="<?= $key->id ?>" class="btn btn-success m-left-xs" id="button-add-scene_edit">
										Agregar
									</a>
	                            </div>
	                            <div class="statistic-icon-background">
	                                <i class="fa fa-user"></i>
	                            </div>
	                        </div>
	                    </div>
	                <?php } ?>
	            </div>
	    	</div><!-- ./slider -->
		</div>
	</div>
	<br>
	<div class="col-md-12">
		<div class="col-md-2">
			<div class='ui-state-default' style="height: 155px !important; text-align: center; line-height: 140px; font-weight: bold; font-size: 14px;">
				INICIO
			</div>
		</div>
		<div class="col-md-8">
			<div id="slider_escenarios_edit" style="">
				<div id="sortable2_edit" style="width: <?= count( $aux )*210?>px">
				<?php				
				if( isset($aux) && count( $aux ) > 0 ){
					foreach( $aux as $row ){
						if( isset($row->id_escenario) ){
						$dat="\"id_escenario\":\"".$row->id_escenario."\",";
						$dat.="\"nombre\":\"".$nombres[ $row->id_escenario ]."\"";

						$texto = $nombres[ $row->id_escenario ];
				?>
							<div class="ui-state-default" data-datos='<?= $dat ?>' draggable="true">
								<button onclick="unlockSceneEdit( parseInt('<?= $row->id_escenario ?>') )" type="button" class="close"><span aria-hidden="true">×</span></button>
								<?= $texto ?>
							</div>
				<?php
						}
						else{}
					}
				}
			 	?>
				</div>
			</div>
			<input id="array_escenarios_edit" name="array_escenarios_edit" type="hidden">
		</div>
		<div class="col-md-2">
			<div class='ui-state-default' style="height: 155px !important; text-align: center; line-height: 140px; font-weight: bold; font-size: 14px;">
				FINAL
			</div>
		</div>
	</div>
	<br><br>
		<div class="col-md-12">
			<br>
			<div class="form-group col-md-12" style="text-align: center;">
				<label style="text-align: right;" class="col-sm-6 control-label">Voz en vivo</label>
				<div style="text-align: left;" class="col-sm-6">
					<label class="toogleswitch">
						<input type="checkbox" <?php echo ( $voz_activo == 1 ) ? 'checked="checked"' : ''; ?> name="voz_vivo" id="voz_vivo">
						<span class="toogleslider toogleround"></span>
					</label>
				</div>
			</div>
		</div>

</div>

<script>
//$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        
    var slider4 = '';

    setTimeout( function(){
		
    	slider4 = $("#cont-abs2 #contSliders").royalSlider({
		    autoHeight: true
		}).data('royalSlider');

		$('#cont-abs2 #imgflecha2').click(function() {
		    slider4.next();
		});

		$('#cont-abs2 #imgflecha1').click(function() {
		    slider4.prev();
		});

		slider4.ev.trigger("rsAfterSlideChange");

	}, 1000);


	

</script>