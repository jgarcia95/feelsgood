<div class="smart-widget m-top-lg widget-dark-blue">
	<div class="smart-widget-header">
		Ejercicio Tamo
	</div>
	<div class="smart-widget-inner">
		<div class="smart-widget-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Fecha de sesión</label>.
						<input class="datetimepicker-input form-control" name="fecha" id="fecha" type="text">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Repeticiones</label>
						<input class="form-control" name="repeticiones" min="2" id="repeticiones" placeholder="" type="number">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Tiempo Inhalación</label>
						<input class="form-control" name="inhalacion" id="inhalacion" placeholder="En segundos" type="number" min="1">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Tiempo Mantener Respiración</label>
						<input class="form-control" name="mantener" id="mantener" placeholder="En segundos" type="number" min="1">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Tiempo Exhalación</label>
						<input class="form-control" name="exhalacion" id="exhalacion" placeholder="En segundos" type="number" min="1">
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Tiempo Descanso</label>
						<input class="form-control" name="descanso" id="descanso" placeholder="En segundos" type="number" min="1">
					</div>
				</div>
				<div class="col-md-12"></div>
				
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Escala visual analogica</label>
						<label class="toogleswitch">
							<input name="perrito" id="perrito" type="checkbox">
							<span class="toogleslider toogleround"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
	</div><!-- ./smart-widget-inner -->
</div><!-- ./smart-widget -->
