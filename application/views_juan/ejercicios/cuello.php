	<div class="smart-widget m-top-lg widget-dark-blue">
		<div class="smart-widget-header">
			Ejercicio Cuello y Cabeza
		</div>
		<div class="smart-widget-inner">
			<div class="smart-widget-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Fecha de Sesión</label>
							<input class="datetimepicker-input form-control" type="text" name="fecha" id="fecha">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Dosificación</label>
							<select class="form-control" name="tipo_ejercicio" id="tipo_ejercicio">
								<option value="1">Manual</option>
								<option value="2">Al azar</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Velocidad del Movimiento</label>
							<input class="form-control" type="number" min="0" name="tiempo_ejercicio" id="tiempo_ejercicio" placeholder="En segundos">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tipo de Ejercicio</label>
							<select class="form-control" name="tipo_movimiento_ajax" id="tipo_movimiento_ajax">
								<option value="1">--Seleccione--</option>
								<option value="2">Rotación con Estiramiento</option>
								<option value="3">Rotación Cabeza y Cuello</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Rango de Movimiento</label>
							<select class="form-control" name="grados" id="grados">
								<option value="1">80</option>
								<option value="2">100</option>
								<option value="3">120</option>
								<option value="4">140</option>
								<option value="5">160</option>
								<option value="6">180</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tutorial</label>
							<label class="toogleswitch">
								<input type="checkbox" name="tutorial" id="tutorial">
								<span class="toogleslider toogleround"></span>
							</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Escala Visual Analógica</label>
							<label class="toogleswitch">
								<input type="checkbox" name="perrito" id="perrito">
								<span class="toogleslider toogleround"></span>
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group m-top-lg" id="div-button-add-move">
							<div class="col-sm-12 text-center">
								<a style="cursor:pointer" class="btn btn-success m-left-xs" id="button-add-move">Agregar Movimiento</a>
							</div>
						</div>
						<br><br>
						<div id="slider_movimientos" style="position:relative">
							<div id="sortable"></div>
						</div>
						<input id="array_movimientos" name="array_movimientos" type="hidden">
					</div>
				</div>
			</div>
		</div><!-- ./smart-widget-inner -->
	</div><!-- ./smart-widget -->