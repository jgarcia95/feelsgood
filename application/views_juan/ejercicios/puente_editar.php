<div class="form-group">
	<label class="col-sm-3 control-label">Fecha de sesión</label>
	<div class="col-sm-9">
		<input class="datetimepicker-input form-control" name="fecha" id="fecha" value="<?=date("d-m-Y h:i A",strtotime($fecha))?>" type="text">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tiempo de espera</label>
	<div class="col-sm-9">
		<input value="<?= $espera ?>" class="form-control" name="espera" id="espera"  type="number" min="0" max="10">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tipo de Movimiento</label>
	<div class="col-sm-9">
		<select class="form-control" name="tipomovimiento" id="tipomovimiento">
			<option value="">--Seleccione--</option>
			<?php foreach ($tipomovimientos as $data) { ?>
				<option <?php echo ( $tipomovimiento == $data->id ) ? 'selected' : ''; ?> value="<?= $data->id ?>"><?= $data->descripcion ?></option>
			<?php } ?>
		</select>
	</div>
	<br>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Rango de movimiento</label>
	<div class="col-sm-9">
		<select class="form-control" name="altura" id="altura">
			<option value="">--Seleccione--</option>
			<option <?php echo ( strtolower(trim($altura)) == 'high' ) ? 'selected' : ''  ?> value="high">160 grados</option>
			<option <?php echo ( strtolower(trim($altura)) == 'low' ) ? 'selected' : ''  ?> value="low">90 grados</option>
		</select>
	</div>
	<br>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Repeticiones</label>
	<div class="col-sm-9">
		<input value="<?= $repeticiones ?>" class="form-control" name="repeticiones" id="repeticiones" type="number" min="1" max="100">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Tutorial</label>
	<div class="col-sm-9">
		<label class="toogleswitch">
			<input <?php echo ( $tutorial == '1' ) ? 'checked="checked"' : ''; ?> type="checkbox" name="tutorial" id="tutorial">
			<span class="toogleslider toogleround"></span>
		</label>
	</div>
</div>
<div class="row"><br><br></div>