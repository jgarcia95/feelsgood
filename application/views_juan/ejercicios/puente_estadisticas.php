<script type="text/javascript">
    cargaBrazo();
</script>
<div class="col-lg-11 table-responsive" style="margin: 0 auto;float: none;">
    <table class="table table-striped no-margin table-hover" id="resultados_puente">
        <thead>
            <tr>
                <th class="text-right" style="width:250px">Fecha</th>
                <th class="text-right" style="width:400px">Logro</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
                   
        </tbody>
    </table>
</div>
<br>
<div style="clear:both;"></div>
<div id="div_tabla_detalle" class="col-lg-12" style="display: none; margin:20px 0;">
    <div class="col-lg-11 table-responsive">
        <table class="table table-striped no-margin table-hover" id="tabla_detalle_puente">
            <thead>
                <tr></tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div><!--./tabla detalle-->
<input type="hidden" id="puntos" value="">
<div class="simulador-content col-lg-12" id="simulador_brazo" style="display:none;margin:20px 0;">
    <div class="col-lg-11" style="margin:0px auto;float:none;">
        <div id="simuladorContainerBrazo" style="height:600px;width:100%;"></div>
        <div id="playSimuladorBrazo">
            <i class="fa fa-play-circle" onclick="ver_movimiento_puente()" id="btn-play-brazo" aria-hidden="true"></i>
        </div>
    </div>
</div>
<div id="" class="col-lg-12" style="margin:20px 0;">                         
    <div class="widget-stat2 col-lg-3">
        <div class="stat-icon bg-danger">
            <i class="fa fa-play-circle"></i>
        </div>
        <div class="stat-value">
            <div class="text-danger"><strong>MOVIMIENTOS </strong></div>
            <div class="text-muted h5" id="div_canti_movimientos"></div>
        </div>
    </div>    
    <div class="widget-stat2 col-lg-3">
        <div class="stat-icon bg-info">
            <i class="fa fa-check"></i>
        </div>
        <div class="stat-value">
            <div class="text-info"><strong>EJERCICIOS COMPLETADOS </strong></div>
            <div class="text-muted h5" id="div_ejercicios_completados"></div>
        </div>
    </div>    
    <div class="widget-stat2 col-lg-3">
        <div class="stat-icon bg-purple">
            <i class="fa fa-clock-o"></i>
        </div>
        <div class="stat-value">
            <div class="text-purple"><strong>TIEMPO EN LA PLATAFORMA </strong></div>
            <div class="text-muted h5" id="div_tiempo_plataforma"></div>
        </div>
    </div>    
    <div class="widget-stat2 col-lg-3">
        <div class="stat-icon bg-success">
            <i class="fa fa-line-chart"></i>
        </div>
        <div class="stat-value">
            <div class="text-success"><strong>PUNTOS LOGRADOS </strong></div>
            <div class="text-muted h5">19,087</div>
        </div>
    </div>

<br><br><br><br>

<div class="col-lg-11" style="">
    <div class="col-lg-12">
        <div id="containerPIE"></div>
        <br><br><br><br>
    </div>
</div>

<div class="col-lg-6" style="display: none;">
    <div class="col-lg-12">
        <div id="container33" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
    <div class="col-lg-12">
        <div id="container34" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
</div>

<br><br><br><br>
<div style="clear:both;"></div>

<div class="col-lg-12">
    <div id="containerLine" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>
</div>

<script type="text/javascript">
    /*Highcharts.chart('container33', {
    chart: {
        type: 'area'
    },
    title: {
        text: ''
    },
    
    xAxis: {
        allowDecimals: false,
        labels: {
            formatter: function () {
                return this.value; // clean, unformatted number for year
            }
        }
    },
    yAxis: {
        title: {
            text: ''
        },
        labels: {
            formatter: function () {
                return this.value / 1000 + 'k';
            }
        }
    },
    tooltip: {
        pointFormat: '{series.name} had stockpiled <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
    },
    plotOptions: {
        area: {
            pointStart: 1940,
            marker: {
                enabled: false,
                symbol: 'circle',
                radius: 2,
                states: {
                    hover: {
                        enabled: true
                    }
                }
            }
        }
    },
    series: [{
        name: '',
        data: [
            null, null, null, null, null, 6, 11, 32, 110, 235,
            369, 640, 1005, 1436, 2063, 3057, 4618, 6444, 9822, 15468,
            20434, 24126, 27387, 29459, 31056, 31982, 32040, 31233, 29224, 27342,
            26662, 26956, 27912, 28999, 28965, 27826, 25579, 25722, 24826, 24605,
            24304, 23464, 23708, 24099, 24357, 24237
        ]
    }, {
        name: '',
        data: [null, null, null, null, null, null, null, null, null, null,
            5, 25, 50, 120, 150, 200, 426, 660, 869, 1060,
            1605, 2471, 3322, 4238, 5221, 6129, 7089, 8339, 9399, 10538,
            11643, 13092, 14478, 15915, 17385, 19055, 21205, 23044, 25393, 27935,
            30062, 32049, 33952, 35804, 37431, 39197
        ]
    }]
});/**/
</script>
<script type="text/javascript">
    /*Highcharts.chart('container34', {
    chart: {
        type: 'area'
    },
    title: {
        text: ''
    },
    
    xAxis: {
        allowDecimals: false,
        labels: {
            formatter: function () {
                return this.value; // clean, unformatted number for year
            }
        }
    },
    yAxis: {
        title: {
            text: ''
        },
        labels: {
            formatter: function () {
                return this.value / 1000 + 'k';
            }
        }
    },
    tooltip: {
        pointFormat: '{series.name} had stockpiled <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
    },
    plotOptions: {
        area: {
            pointStart: 1940,
            marker: {
                enabled: false,
                symbol: 'circle',
                radius: 2,
                states: {
                    hover: {
                        enabled: true
                    }
                }
            }
        }
    },
    series: [{
        name: '',
        data: [
            null, null, null, null, null, 6, 11, 32, 110, 235,
            369, 640, 1005, 1436, 2063, 3057, 4618, 6444, 9822, 15468,
            20434, 24126, 27387, 29459, 31056, 31982, 32040, 31233, 29224, 27342,
            26662, 26956, 27912, 28999, 28965, 27826, 25579, 25722, 24826, 24605,
            24304, 23464, 23708, 24099, 24357, 24237, 24401, 24344, 23586, 22380,
            21004, 17287, 14747, 13076, 12555, 12144, 11009, 10950, 10871, 10824,
            10577, 10527, 10475, 10421, 10358, 10295, 10104, 9914, 9620, 9326,
            5113, 5113, 4954, 4804, 4761, 4717, 4368, 4018
        ]
    }, {
        name: '',
        data: [null, null, null, null, null, null, null, null, null, null,
            5, 25, 50, 120, 150, 200, 426, 660, 869, 1060,
            1605, 2471, 3322, 4238, 5221, 6129, 7089, 8339, 9399, 10538,
            11643, 13092, 14478, 15915, 17385, 19055, 21205, 23044, 25393, 27935,
            30062, 32049, 33952, 35804, 37431, 39197, 45000, 43000, 41000, 39000,
            37000, 35000, 33000, 31000, 29000, 27000, 25000, 24000, 23000, 22000,
            21000, 20000, 19000, 18000, 18000, 17000, 16000, 15537, 14162, 12787,
            12600, 11400, 5500, 4512, 4502, 4502, 4500, 4500
        ]
    }]
});*/
</script>
<script type="text/javascript">

function cargarGraficoPie( dosificados, completados, realizados ){ 
    
    Highcharts.chart('containerPIE', {
        chart: {
            type: 'variablepie'
        },
        title: {
            text: 'Ejercicios'
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
                'Cantidad: <b>{point.y}</b><br/>'
        },
        series: [{
            minPointSize: 10,
            innerSize: '50%',
            zMin: 0,
            name: 'Registered',
            data: [{
                name: 'Dosificados',
                y: dosificados,
                z: 50
            }, {
                name: 'Realizados',
                y: realizados,
                z: 50
            }, {
                name: 'Completados correctamente',
                y: completados,
                z: 80
            }]
        }]
    });

}
</script>
<script type="text/javascript">

function cargarGraficoLine( dat ){
    Highcharts.chart('containerLine', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Ejercicios Realizados'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Ejercicios Realizados',
            data: dat
        }]
    });
}
</script>