<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <title>FeelsGood Login</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <!-- Bootstrap core CSS -->
	    <link href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="<?= base_url() ?>public/css/font-awesome.min.css" rel="stylesheet">
		<!-- ionicons -->
		<link href="<?= base_url() ?>public/css/ionicons.min.css" rel="stylesheet">
		<!-- Simplify -->
		<link href="<?= base_url() ?>public/css/simplify.min.css" rel="stylesheet">
		<style type="text/css">
			#messagemodal{
			    font-size: 17px;
			    margin-top: 9px;
			    margin-bottom: 10px;
			    color: #000;
			}
			#loading {
			    box-shadow: 0 0 6px rgba(0, 0, 0, .4);
				display: none;
				position: fixed;
				top: 50%;
				left: 50%;
				margin-left: -75px;
				margin-top: -75px;
				z-index: 9999;
				padding: 10px 20px;
				border-radius: 10px;
				background: rgba(255,255,255,0.5);
			}
		</style>
  	</head>
  	<body class="overflow-hidden light-background">
		<div class="wrapper no-navigation preload">
			<div class="sign-in-wrapper">
				<div class="sign-in-inner">
					<div class="login-brand text-center">
						<i class="fa fa-user-md m-right-xs"></i> FeelsGood <strong class="text-skin">Login</strong>
					</div>
					<form onkeypress="validar(event)">
						<div class="form-group m-bottom-md">
							<input type="text" class="form-control" placeholder="Email o teléfono" 
							name="email" id="email">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" placeholder="Password" 
							name="pass" id="pass">
						</div>
						<div class="m-top-md p-top-sm">
							<a id="btn-login" class="btn btn-success block">Ingresar</a>
						</div>
						<div class="m-top-md p-top-sm">
							<div class="font-12 text-center m-bottom-xs">
								<a class="font-12" href="recuperar-clave">Olvidaste la clave ?</a>
							</div>
							<div class="font-12 text-center m-bottom-xs">No tienes una cuenta?</div>
							<div class="input-group">
								<div class="input-group-btn">
									<button type="button" class="btn btn-default" tabindex="-1">Registrarse como</button>
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1"><span class="caret"></span></button>
									<ul class="dropdown-menu col-md-12 col-xs-12 col-lg-12 col-sm-12" role="menu">
										<li><a href="solicitar-registro-doctor">Profesional de salud</a></li>
										<li><a href="solicitar-registro-clinica">Institución de salud</a></li>
										<li><a href="solicitar-registro-paciente">Paciente</a></li>
									</ul>
								</div>
							</div><!-- /input-group -->
						</div>
					</form>
				</div><!-- ./sign-in-inner -->
			</div><!-- ./sign-in-wrapper -->
		</div><!-- /wrapper -->
		<a href="" id="scroll-to-top" class="hidden-print"><i class="icon-chevron-up"></i></a>
		<!-- Modal -->
		<div id="modalMessage" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content">
		      <div class="modal-body text-center">
		        <p id="messagemodal"></p>
		        <button type="button" class="btn btn-danger" data-dismiss="modal">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<div id="loading">
		    <i class="fa fa-spinner fa-spin m-right-xs"></i>Loading
		</div>
	    <!-- Le javascript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
		<!-- Jquery -->
		<script src="<?= base_url() ?>public/js/jquery-1.11.1.min.js"></script>
		<!-- Bootstrap -->
	    <script src="<?= base_url() ?>public/bootstrap/js/bootstrap.min.js"></script>
		<!-- Slimscroll -->
		<script src='<?= base_url() ?>public/js/jquery.slimscroll.min.js'></script>
		<!-- Popup Overlay -->
		<script src='<?= base_url() ?>public/js/jquery.popupoverlay.min.js'></script>
		<!-- Modernizr -->
		<script src='<?= base_url() ?>public/js/modernizr.min.js'></script>
		<!-- Simplify -->
		<script src="<?= base_url() ?>public/js/simplify/simplify.js"></script>
		<!-- Login -->
		<script src="<?= base_url() ?>public/js/login.js"></script>
		<script src='<?= base_url() ?>public/js/jquery.noty.packaged.min.js'></script>
  	</body>
</html>