<?php
    class perfil_model extends CI_Model {

        public function selectAll()
        {
            $this->db->select('*');
            $this->db->from('perfiles');
            $query = $this->db->get();
            return $query->result();
        }

        public function selectByID($id){
            $this->db->select('*');
            $this->db->from('perfiles');
            $this->db->where('id_perfil', $id);
            $query = $this->db->get();
            return $query->row();
        }

        public function getEjercicios(){
            $this->db->select('id_ejercicio,nombre,descripcion,icono,fondo,form');
            $this->db->from('ejercicios');
            $this->db->where('status', 1);
            $query = $this->db->get();
            return $query->result();
        }

        public function delete($id)
        {
            $this->db->where('id_perfil', $id);
            $this->db->delete('perfiles');
            return $this->db->affected_rows();
        }
        public function insert($nombre){
            $data= array(
                "nombre" => $nombre
            );
            $this->db->insert('perfiles', $data);
            return $this->db->insert_id();
        }
        public function edit($id,$nombre){
            $data= array(
                "nombre" => $nombre
            );
            $this->db->where('id_perfil', $id);
            $this->db->update('perfiles', $data);
            return $this->db->affected_rows();
        }

        /*public function trae_form($id)
        {
            $this->db->select('form');
            $this->db->from('ejercicios');
            $this->db->where('id_ejercicio', $id);
            $query = $this->db->get();
            return $query->row();
        }*/

        public function escenarios_ejercicios($id)
        {
            $this->db->select('id,nombre');
            $this->db->from('escenarios');
            $this->db->where('id_ejercicio', $id);
            $query = $this->db->get();
            return $query->result();
        }

        public function trae_datos($paciente,$ejercicio)
        {
            $this->db->select('*');
            switch ($ejercicio) {
                case 1:$this->db->from('ejercicio_respiracion');break;
                case 2:$this->db->from('ejercicio_hongos');break;
                case 3:$this->db->from('ejercicio_bote');break;
                case 4:$this->db->from('ejercicio_sapito');break;
                case 5:$this->db->from('ejercicio_peces');break;
                case 6:$this->db->from('ejercicio_franja');break;
                case 7:$this->db->from('ejercicio_puente');break;
                case 8:$this->db->from('ejercicio_cuello');break;
                case 9:$this->db->from('ejercicio_estres_ansiedad');break;
                case 10:$this->db->from('ejercicio_tamo');break;
            }
            $this->db->where('id_paciente', $paciente);
            $this->db->where('status', 0);
            $this->db->where('DATE(fecha)', date("Y-m-d"));
            $this->db->order_by('fecha', 'ASC');
            $query = $this->db->get();
            return $query->row();
        }

        public function editaEjercicio($id_ejercicio,$id_sesion,$data)
        {
            $this->db->where('id_ejercicio', $id_sesion);
            switch ($id_ejercicio) {
                case 1:$this->db->update('ejercicio_respiracion',$data);break;
                case 2:$this->db->update('ejercicio_hongos',$data);break;
                case 3:$this->db->update('ejercicio_bote',$data);break;
                case 4:$this->db->update('ejercicio_sapito',$data);break;
                case 5:$this->db->update('ejercicio_peces',$data);break;
                case 6:$this->db->update('ejercicio_franja',$data);break;
                case 7:$this->db->update('ejercicio_puente',$data);break;
                case 8:$this->db->update('ejercicio_cuello',$data);break;
                case 9:$this->db->update('ejercicio_estres_ansiedad',$data);break;
                case 10:$this->db->update('ejercicio_tamo',$data);break;
            }
            return $this->db->affected_rows();
        }

        public function insertEjercicio($tipo_ejercicio,$data){
            switch ($tipo_ejercicio){
                case 1:$this->db->insert('ejercicio_respiracion',$data);break;
                case 2:$this->db->insert('ejercicio_hongos',$data);break;
                case 3:$this->db->insert('ejercicio_bote',$data);break;
                case 4:$this->db->insert('ejercicio_sapito',$data);break;
                case 5:$this->db->insert('ejercicio_peces',$data);break;
                case 6:$this->db->insert('ejercicio_franja',$data);break;
                case 6:$this->db->insert('ejercicio_franja',$data);break;
                case 7:$this->db->insert('ejercicio_puente',$data);break;
                case 8:$this->db->insert('ejercicio_cuello',$data);break;
                case 9:$this->db->insert('ejercicio_estres_ansiedad',$data);break;
                case 10:$this->db->insert('ejercicio_tamo',$data);break;
            }
            return $this->db->insert_id();
        }

        public function delEjercicio($id_sesion,$tipo_ejercicio)
        {
            $this->db->where('id_ejercicio', $id_sesion);
            switch ($tipo_ejercicio){
                case 1:$this->db->delete('ejercicio_respiracion');break;
                case 2:$this->db->delete('ejercicio_hongos');break;
                case 3:$this->db->delete('ejercicio_bote');break;
                case 4:$this->db->delete('ejercicio_sapito');break;
                case 5:$this->db->delete('ejercicio_peces');break;
                case 6:$this->db->delete('ejercicio_franja');break;
                case 7:$this->db->delete('ejercicio_puente');break;
                case 8:$this->db->delete('ejercicio_cuello');break;
                case 9:$this->db->delete('ejercicio_estres_ansiedad');break;
                case 10:$this->db->delete('ejercicio_tamo');break;
            }
            return $this->db->affected_rows();
        }

        //Esto es para mostrar las estadisticas en el perfil del paciente.
        public function trae_ejercicios_terminados($data)
        {
            switch ($data["id_ejer"]) {
                case 1:
                    $query=$this->db->query('SELECT id_ejercicio,DATE_FORMAT(fecha,"%Y-%m-%d %h:%i %p") AS fecha,promedio,cantidad, tiros, status,s FROM ejercicio_respiracion WHERE id_paciente='.$data["id_paciente"].' AND status=1 AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
                case 2:$this->db->from('ejercicio_hongos');break;
                case 3:
                $query=$this->db->query('SELECT id_ejercicio FROM ejercicio_bote WHERE id_paciente='.$data["id_paciente"].' AND status=1 AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
                case 4:
                    $query=$this->db->query('SELECT id_ejercicio,DATE_FORMAT(fecha,"%Y-%m-%d %h:%i %p") AS fecha,promedio,gano,cantidad,nivel_final FROM ejercicio_sapito WHERE id_paciente='.$data["id_paciente"].' AND status=1 AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
                case 5:
                    $query=$this->db->query('SELECT id_ejercicio,DATE_FORMAT(fecha,"%Y-%m-%d %h:%i %p") AS fecha,promedio,gano,cantidad FROM ejercicio_peces WHERE id_paciente='.$data["id_paciente"].' AND status=1 AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
                case 6:
                    $query=$this->db->query('SELECT id_ejercicio,DATE_FORMAT(fecha,"%Y-%m-%d %h:%i %p") AS fecha,promedio,cantidad FROM ejercicio_franja WHERE id_paciente='.$data["id_paciente"].' AND status=1 AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
                case 7:
                    $query=$this->db->query('SELECT id_ejercicio, DATE_FORMAT(fecha,"%Y-%m-%d %h:%i %p") AS fecha,promedio,cantidad, tiros, status,s,tipomovimiento FROM ejercicio_puente WHERE id_paciente='.$data["id_paciente"].' AND status=1 AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
                case 8:
                    $query=$this->db->query('SELECT id_ejercicio,DATE_FORMAT(fecha,"%Y-%m-%d %h:%i %p") AS fecha,promedio,cantidad, tiros, status,s FROM ejercicio_cuello WHERE id_paciente='.$data["id_paciente"].' AND status=1 AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
                case 9:
                    $query=$this->db->query('SELECT id_ejercicio FROM ejercicio_estres_ansiedad WHERE id_paciente='.$data["id_paciente"].' AND status=1 AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
                case 10:
                    $query=$this->db->query('SELECT id_ejercicio,DATE_FORMAT(fecha,"%Y-%m-%d %h:%i %p") AS fecha,promedio,cantidad, tiros, status,s FROM ejercicio_tamo WHERE id_paciente='.$data["id_paciente"].' AND status=1 AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
            }
            return $query->result();
        }


        public function trae_ejercicios_all_dosificados($data)
        {
            switch ($data["id_ejer"]) {
                case 1:
                $query=$this->db->query('SELECT id_ejercicio,DATE_FORMAT(fecha,"%Y-%m-%d %h:%i %p") AS fecha,promedio,cantidad, status, s FROM ejercicio_respiracion WHERE id_paciente='.$data["id_paciente"].' AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
                case 2: break;
                case 3: break;
                case 4: break;
                case 5: break;
                case 6: break;
                case 7:
                    $query=$this->db->query('SELECT id_ejercicio,DATE_FORMAT(fecha,"%Y-%m-%d %h:%i %p") AS fecha,promedio,cantidad, status, s, tipomovimiento FROM ejercicio_puente WHERE id_paciente='.$data["id_paciente"].' AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
                case 8:
                    $query=$this->db->query('SELECT id_ejercicio,DATE_FORMAT(fecha,"%Y-%m-%d %h:%i %p") AS fecha,promedio,cantidad, status, s FROM ejercicio_cuello WHERE id_paciente='.$data["id_paciente"].' AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
                case 9:
                    $query=$this->db->query('SELECT id_ejercicio FROM ejercicio_estres_ansiedad WHERE id_paciente='.$data["id_paciente"].' AND status=1 AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;
                case 10:
                $query=$this->db->query('SELECT id_ejercicio,DATE_FORMAT(fecha,"%Y-%m-%d %h:%i %p") AS fecha,promedio,cantidad, status, s FROM ejercicio_tamo WHERE id_paciente='.$data["id_paciente"].' AND DATE(fecha) BETWEEN "'.$data["desde"].'" AND "'.$data["hasta"].'" ORDER BY fecha DESC');
                break;

            }
            return $query->result();
        }

        //Esto es para mostrar el detalle de los ejercicios
        public function trae_detalle_ejercicio($id_ejercicio,$tipo){
            $this->db->select('s,tiros');
          switch ($tipo) {
            case 1:$this->db->from('ejercicio_respiracion');break;
            case 2:$this->db->from('ejercicio_hongos');break;
            case 3:$this->db->from('ejercicio_bote');break;
            case 4:$this->db->from('ejercicio_sapito');break;
            case 5:$this->db->from('ejercicio_peces');break;
            case 6:$this->db->from('ejercicio_franja');break;
            case 7:$this->db->from('ejercicio_puente');break;
            case 8:$this->db->from('ejercicio_cuello');break;
            case 9:$this->db->from('ejercicio_estres_ansiedad');break;
            case 10:$this->db->from('ejercicio_tamo');break;
          }
          $this->db->where('id_ejercicio', $id_ejercicio);
          $query = $this->db->get();
          return $query->row();
        }

        public function trae_ejercicios_terminados_excel($ejer,$paciente,$tiempo)
        {
            switch ($tiempo) {
                case 'Diario':
                  $desde = new DateTime(date("Y-m-d"));
                  $desde->sub(new DateInterval('P1D'));
                break;
                case 'Semanal':
                  $desde = new DateTime(date("Y-m-d"));
                  $desde->sub(new DateInterval('P1W'));
                break;
                case 'Mensual':
                  $desde = new DateTime(date("Y-m-d"));
                  $desde->sub(new DateInterval('P1M'));
                                break;
            }
            $sql='SELECT id_ejercicio,DATE_FORMAT(fecha,"%Y-%m-%d %h:%i %p") AS fecha,';
            switch ($ejer) {
                case 1:$query=$this->db->query($sql.'1 FROM ejercicio_respiracion WHERE id_paciente='.
                $paciente.' AND status=1 AND DATE(fecha) BETWEEN "'.$desde->format('Y-m-d').'" AND "'.date("Y-m-d").'" ORDER BY fecha DESC');break;
                case 2:$query=$this->db->query($sql.'1 FROM ejercicio_hongos WHERE id_paciente='.
                $paciente.' AND status=1 AND DATE(fecha) BETWEEN "'.$desde->format('Y-m-d').'" AND "'.date("Y-m-d").'" ORDER BY fecha DESC');break;
                case 3:$query=$this->db->query($sql.'1 FROM ejercicio_remos WHERE id_paciente='.
                $paciente.' AND status=1 AND DATE(fecha) BETWEEN "'.$desde->format('Y-m-d').'" AND "'.date("Y-m-d").'" ORDER BY fecha DESC');break;
                case 4: $query=$this->db->query($sql.'promedio,gano,cantidad,nivel_final FROM ejercicio_sapito WHERE id_paciente='.
                $paciente.' AND status=1 AND DATE(fecha) BETWEEN "'.$desde->format('Y-m-d').'" AND "'.date("Y-m-d").'" ORDER BY fecha DESC');
                                break;
                                case 5:$query=$this->db->query($sql.'1 FROM ejercicio_peces WHERE id_paciente='.
                                $paciente.' AND status=1 AND DATE(fecha) BETWEEN "'.$desde->format('Y-m-d').'" AND "'.date("Y-m-d").'" ORDER BY fecha DESC');break;
                                case 6:$query=$this->db->query($sql.'1 FROM ejercicio_franja WHERE id_paciente='.
                                $paciente.' AND status=1 AND DATE(fecha) BETWEEN "'.$desde->format('Y-m-d').'" AND "'.date("Y-m-d").'" ORDER BY fecha DESC');break;
                                case 7:$query=$this->db->query($sql.'1 FROM ejercicio_puente WHERE id_paciente='.
                                $paciente.' AND status=1 AND DATE(fecha) BETWEEN "'.$desde->format('Y-m-d').'" AND "'.date("Y-m-d").'" ORDER BY fecha DESC');break;
                                case 8:$query=$this->db->query($sql.'1 FROM ejercicio_cuello WHERE id_paciente='.
                $paciente.' AND status=1 AND DATE(fecha) BETWEEN "'.$desde->format('Y-m-d').'" AND "'.date("Y-m-d").'" ORDER BY fecha DESC');break;
            }
            return $query->result();
        }

        public function mailEjercicio($id_ejercicio,$id_sesion)
        {
            switch ($id_ejercicio) {
                case 1:$this->db->from('ejercicio_respiracion as x');break;
                case 2:$this->db->from('ejercicio_hongos as x');break;
                case 3:$this->db->from('ejercicio_bote as x');break;
                case 4:$this->db->from('ejercicio_sapito as x');break;
                case 5:$this->db->from('ejercicio_peces as x');break;
                case 6:$this->db->from('ejercicio_franja as x');break;
                case 7:$this->db->from('ejercicio_puente as x');break;
                case 8:$this->db->from('ejercicio_cuello as x');break;
                case 9:$this->db->from('ejercicio_estres_ansiedad as x');break;
                case 10:$this->db->from('ejercicio_tamo as x');break;
            }
            $this->db->join('paciente', 'x.id_paciente = paciente.id_paciente');
            $this->db->join('user', 'user.id_user = paciente.id_user');
            $this->db->where('id_ejercicio', $id_sesion);
            $query = $this->db->get();
            return $query->result();
        }
        public function selectDoctorById($id){
            $this->db->from('doctor');
            $this->db->join('user', 'user.id_user = doctor.id_user');
            $this->db->where('id_doctor', $id);
            $query = $this->db->get();
            return $query->row();
        }

        public function getEjercicioByID($id){
                    $this->db->select('id_ejercicio,nombre,descripcion,icono,fondo,form');
                    $this->db->from('ejercicios');
                    $this->db->where('id_ejercicio', $id);
                    $query = $this->db->get();
                    return $query->row();
        }

        public function trae_data_del_anio($data)
        {
            switch ($data["id_ejer"]) {
                case 1:
                $query=$this->db->query('SELECT MONTH(fecha) AS mes FROM ejercicio_respiracion WHERE id_paciente = '.$data["id_paciente"].' AND status = 1 AND YEAR( fecha ) = YEAR( NOW() )');
                break;
                case 2: break;
                case 3: break;
                case 4: break;
                case 5: break;
                case 6: break;
                case 7:
                    $query=$this->db->query('SELECT MONTH(fecha) AS mes FROM ejercicio_puente WHERE id_paciente = '.$data["id_paciente"].' AND status = 1 AND YEAR( fecha ) = YEAR( NOW() )');
                break;
                case 8:
                    $query=$this->db->query('SELECT MONTH(fecha) AS mes FROM ejercicio_cuello WHERE id_paciente = '.$data["id_paciente"].' AND status = 1 AND YEAR( fecha ) = YEAR( NOW() )');
                break;
                case 9: break;
                case 10:
                $query=$this->db->query('SELECT MONTH(fecha) AS mes FROM ejercicio_tamo WHERE id_paciente = '.$data["id_paciente"].' AND status = 1 AND YEAR( fecha ) = YEAR( NOW() )');
                break;

            }

            return $query->result();
        }
    }
?>
