<?php
    class paciente_model extends CI_Model {

        public function selectAll($id_doctor=0)
        {
            $this->db->select('*');
            $this->db->from('paciente');
            $this->db->join('user','paciente.id_user=user.id_user');
            $this->db->where('status', 1);
            if ($id_doctor!==0) {
                $this->db->where('id_doctor', $id_doctor);
            }
            $this->db->order_by("fecha","DESC");
            $query = $this->db->get();
            return $query->result();
        }

        public function getFromDoctor($id_doctor=0)
        {
            $this->db->select('*');
            $this->db->from('paciente');
            $this->db->where('status_p', 1);
            if ($id_doctor!==0) {
                $this->db->where('id_doctor', $id_doctor);
            }
            $this->db->order_by("fecha","DESC");
            $query = $this->db->get();
            return $query->result();
        }

        public function selectByDoctor( $id_doctor ){

            $this->db->select('*');
            $this->db->from('paciente');
            $this->db->where('id_doctor', $id_doctor);
            $this->db->where('status_p', 1);
            $this->db->order_by("fecha","DESC");
            $query = $this->db->get();
            return $query->result();
        }

        public function selectAllSinFiltro()
        {
            $this->db->select('*');
            $this->db->from('paciente');
            $this->db->join('user','paciente.id_user=user.id_user');
            $this->db->where('status', 1);
            $query = $this->db->get();
            return $query->result();
                }
                
        public function validaDni($dni){
            $this->db->select('*');
            $this->db->from('paciente p');
            $this->db->join('user u','u.id_user=p.id_user');
            $this->db->where('p.dni', $dni);
            $query = $this->db->get();
            return $query->row();
        }

        public function getByDni( $dni ){
            $this->db->select('*');
            $this->db->from('paciente p');
            $this->db->where('p.dni', $dni);
            $query = $this->db->get();
            return $query->row();
        }

        public function delete($id)
        {
            $data= array(
                "status_p" => 0
            );
            $this->db->where('id_paciente', $id);
            $this->db->update('paciente', $data);
            return $this->db->affected_rows();
        }

        public function selectId($id){
            $this->db->select('*');
            $this->db->from('paciente');
            $this->db->join('user','paciente.id_user=user.id_user','left');
            $this->db->like('id_paciente', $id);
            $query = $this->db->get();
            return $query->row();
        }

        public function getUbigeo($id,$tipo)
        {
            switch ($tipo) {
                case 1://Pais
                    $this->db->select('PaisNom');
                    $this->db->from('paises');
                    $this->db->where('PaisCod',$id);
                break;
                case 2://Provincia
                    $this->db->select('ProvinNom');
                    $this->db->from('provincia');
                    $this->db->where('ProvinCod',$id);
                break;
                case 3://Localidad
                    $this->db->select('LocalidNom');
                    $this->db->from('localidades');
                    $this->db->where('LocalidCod',$id);
                break;
            }
            $query = $this->db->get();
            return $query->row();
        }

        public function getProfesion($id){
            $this->db->select('nombre_profesion');
            $this->db->from('profesion');
            $this->db->where('profesion_id', $id);
            $query = $this->db->get();
            return $query->row();
        }

        public function getClinica($id){
            $this->db->select('nombre_clinica');
            $this->db->from('clinica');
            $this->db->where('clinica_id', $id);
            $query = $this->db->get();
            return $query->row();
        }

        public function get_ejercicio($tipo,$id){
            $this->db->select('*');
            switch ($tipo) {
                case 1: $this->db->from('ejercicio_respiracion'); break;
                case 2: $this->db->from('ejercicio_hongos'); break;
                case 3: $this->db->from('ejercicio_bote'); break;
                case 4: $this->db->from('ejercicio_sapito'); break;
                case 5: $this->db->from('ejercicio_peces'); break;
                case 6: $this->db->from('ejercicio_franja'); break;
                case 7: $this->db->from('ejercicio_puente'); break;
                case 8: $this->db->from('ejercicio_cuello'); break;
                case 9: $this->db->from('ejercicio_estres_ansiedad'); break;
                case 10: $this->db->from('ejercicio_tamo'); break;
            }
            $this->db->where('id_ejercicio',$id);
            $query = $this->db->get();
            return $query->row();
        }

        public function get_json($tipo,$id)
        {
            $this->db->select('*');
            switch ($tipo) {
                case 1: $this->db->from('ejercicio_respiracion'); break;
                case 2: $this->db->from('ejercicio_hongos'); break;
                case 3: $this->db->from('ejercicio_bote'); break;
				case 4: $this->db->from('ejercicio_sapito'); break;
				case 5: $this->db->from('ejercicio_peces'); break;
				case 6: $this->db->from('ejercicio_franja'); break;
				case 7: $this->db->from('ejercicio_puente'); break;
				case 8: $this->db->from('ejercicio_cuello'); break;
                case 9: $this->db->from('ejercicio_estres_ansiedad'); break;
                case 10: $this->db->from('ejercicio_tamo'); break;
            }
            $this->db->where("id_paciente",$id);
            $query = $this->db->get();
            return $query->result();
        }

        public function get_json2($tipo,$id)
        {
            $this->db->select('*');
            switch ($tipo) {
                case 1: $this->db->from('ejercicio_respiracion'); break;
                case 2: $this->db->from('ejercicio_hongos'); break;
                case 3: $this->db->from('ejercicio_bote'); break;
                case 4: $this->db->from('ejercicio_sapito'); break;
                case 5: $this->db->from('ejercicio_peces'); break;
                case 6: $this->db->from('ejercicio_franja'); break;
                case 7: $this->db->from('ejercicio_puente'); break;
                case 8: $this->db->from('ejercicio_cuello'); break;
                case 9: $this->db->from('ejercicio_estres_ansiedad'); break;
                case 10: $this->db->from('ejercicio_tamo'); break;
            }
            $this->db->where('fecha >=', 'NOW()', FALSE);
            $this->db->where('id_paciente',$id);
            $query = $this->db->get();
            return $query->result();
        }

        public function selectIndependientes()
        {
            $this->db->select('*');
            $this->db->from('paciente');
            $this->db->join('user','paciente.id_user=user.id_user');
            $this->db->where('status', 1);
            $this->db->where('id_doctor', 0);
            $query = $this->db->get();
            return $query->result();
        }

        public function asignarDoctor($paciente,$doctor){
            $data= array(
                "id_doctor" => $doctor
            );
            $this->db->where('id_paciente', $paciente);
            $this->db->update('paciente', $data);
            return $this->db->affected_rows();
        }

        public function selectByUser($id){
            $this->db->select('*');
            $this->db->from('paciente p');
            $this->db->join('user u','p.id_user=u.id_user');
            $this->db->like('u.id_user', $id);
            $query = $this->db->get();
            return $query->row();
        }

        public function getUbicacion($id){
            $this->db->select('latitud,longitud');
            $this->db->from('conexiones');
            $this->db->where('id_paciente',$id);
            $query = $this->db->get();
            return $query->result();
        }

        public function getUbicacionRow($id){
            $this->db->select('latitud,longitud,fecha');
            $this->db->from('conexiones');
            $this->db->where('id_paciente',$id);
            $query = $this->db->get();
            return $query->row();
        }

        public function nuevaConexion($data){
            $this->db->insert('conexiones', $data);
            return $this->db->insert_id();
        }

        public function actualizaConexion($data){
            $this->db->where('id_paciente', $data["id_paciente"]);
            $this->db->update('conexiones', $data);
            return $this->db->affected_rows();
        }

        public function selectOnline($id){
            $this->db->select('online');
            $this->db->from('paciente');
            $this->db->like('id_paciente', $id);
            $query = $this->db->get();
            return $query->row();
        }
                
        public function selectPrimerPaciente($id_doctor){
            $this->db->select('id_paciente');
            $this->db->from('paciente');
            $this->db->where('id_doctor', $id_doctor);
            $this->db->limit(1);
            $query = $this->db->get();
            return $query->result();
        }
    }
?>