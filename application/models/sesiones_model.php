<?php
    class sesiones_model extends CI_Model {
        public function getAll($id,$tipo){
            $this->db->trans_start();
            switch ($tipo) {
                 case 1: $this->db->from('ejercicio_respiracion'); break;
                 case 2: $this->db->from('ejercicio_hongos'); break;
                 case 3: $this->db->from('ejercicio_bote'); break;
                 case 10: $this->db->from('ejercicio_tamo'); break;
            }
            $id = $this->db->insert_id();
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE)
            {
                return array("status"=>false,"id"=>0);
            }else{
                return array("status"=>true,"id"=>$id);
            }
        }
    }
?>
