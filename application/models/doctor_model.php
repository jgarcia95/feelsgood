<?php
    class doctor_model extends CI_Model {

        public function selectAll()
        {
            $this->db->select('*');
            $this->db->from('doctor');
            $this->db->join('user','doctor.id_user=user.id_user');
            $this->db->where('status', 1);
            $query = $this->db->get();
            return $query->result();
        }

        public function selectAllSinFiltro($buscar)
        {
            $this->db->select('*');
            $this->db->from('doctor');
            $this->db->join('user','doctor.id_user=user.id_user');
            $this->db->where('status', 1);
            $query = $this->db->get();
            return $query->result();
        }

        public function delete($id)
        {
            $data= array(
                "status" => 0
                );
            $this->db->where('id_user', $id);
            $this->db->update('user', $data);
            return $this->db->affected_rows();
        }

        public function selectId($id){
            $this->db->select('*');
            $this->db->from('doctor');
            $this->db->join('user','doctor.id_user=user.id_user');
            $this->db->where('status', 1);
            $this->db->like('id_doctor', $id);
            $query = $this->db->get();
            return $query->row();
        }

        public function selectByUser($id){
            $this->db->select('*');
            $this->db->from('doctor d');
            $this->db->join('user u','d.id_user=u.id_user');
            $this->db->like('u.id_user', $id);
            $query = $this->db->get();
            return $query->row();
        }
    }
?>