<?php
    class solicitud_model extends CI_Model {

        public function add($data,$tipo)
        {   
            $this->db->trans_start();
            switch ($tipo) {
                 case 2: $this->db->insert('solicitud_registro_doctores', $data); break;
                 case 3: $this->db->insert('solicitud_registro_pacientes', $data); break;
                 case 4: $this->db->insert('solicitud_registro_clinicas', $data); break;
            }
            $id = $this->db->insert_id();
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE)
            {
                return array("status"=>false,"id"=>0);
            }else{
                return array("status"=>true,"id"=>$id);
            }
        }

        public function get_solicitudes($tipo)
        {
            $this->db->select('*');
            switch ($tipo) {
                 case 2: $this->db->from('solicitud_registro_doctores'); break;
                 case 3: $this->db->from('solicitud_registro_pacientes'); break;
                 case 4: $this->db->from('solicitud_registro_clinicas'); break;
            }
            $this->db->where('status',1);
            $this->db->order_by('fecha',"DESC");
            $query = $this->db->get();
            return $query->result();
        }

        public function get_solicitud_byID($id,$tipo)
        {
            $this->db->select('*');
            switch ($tipo) {
                 case 2: $this->db->from('solicitud_registro_doctores'); break;
                 case 3: $this->db->from('solicitud_registro_pacientes'); break;
                 case 4: $this->db->from('solicitud_registro_clinicas'); break;
            }
            $this->db->where('solicitud_id',$id);
            $query = $this->db->get();
            return $query->result();
        }

        public function updateStatus($id,$status,$tipo){
            $data= array(
                "status" => $status
                );
            $this->db->where('solicitud_id', $id);
            switch ($tipo) {
                 case 2: $this->db->update('solicitud_registro_doctores', $data); break;
                 case 3: $this->db->update('solicitud_registro_pacientes', $data); break;
                 case 4: $this->db->update('solicitud_registro_clinicas', $data); break;
            }
            return $this->db->affected_rows();
        }

        public function verificarClinica($name){
            $this->db->select('*');
            $this->db->from('clinica');
            $this->db->where('LOWER(nombre_clinica)', $name);
            $query = $this->db->get();
            return $query->result();
        }
    }
?>