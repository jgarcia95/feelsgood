<?php
    class tipo_movimiento_model extends CI_Model {

       
        public function selectAll()
        {
            $this->db->select('*');
            $this->db->from('tipo_movimiento');
            $this->db->where('estatus', 1);
            $this->db->order_by('descripcion', "DESC");
            $query = $this->db->get();
            return $query->result();
        }

    }


     
?>