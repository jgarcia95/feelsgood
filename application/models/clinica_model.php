<?php
    class clinica_model extends CI_Model {

        public function search($q)
        {
            $this->db->select('*');
            $this->db->from('clinica');
            $this->db->like('nombre_clinica', $q);
            $query = $this->db->get();
            return $query->result();
        }

        public function selectById( $id )
        {
            $this->db->select('*');
            $this->db->from('clinica');
            $this->db->where('clinica_id', $id);
            $this->db->where('status', 1);
            $query = $this->db->get();

            if( $query->num_rows() > 0 ){
                return $query->row();
            }

            return false;
        }

        public function clinicas()
        {
            $this->db->select('*');
            $this->db->from('clinica');
            $this->db->where('status', 1);
            $query = $this->db->get();
            return $query->result();
        }

        public function selectAll()
        {
            $this->db->select('*');
            $this->db->from('clinica');
            $this->db->join('user','clinica.id_user=user.id_user');
            $this->db->where('clinica.status', 1);
            $this->db->order_by('fecha', "DESC");
            $query = $this->db->get();
            return $query->result();
        }

        public function selectAllSinFiltro($buscar)
        {
            $this->db->select('*');
            $this->db->from('clinica');
            $this->db->join('user','clinica.id_user=user.id_user');
            $this->db->where('status', 1);
             $this->db->where("(nombre_clinica like '%".$buscar."%' or name_user like '%".$buscar."%')");
            $query = $this->db->get();
            return $query->result();
        }

        public function delete($id)
        {
            $data= array(
                "status" => 0
                );
            $this->db->where('id_user', $id);
            $this->db->update('user', $data);
            return $this->db->affected_rows();
        }

        public function selectId($id){
            $this->db->select('*');
            $this->db->from('clinica');
            $this->db->join('user','clinica.id_user=user.id_user');
            $this->db->where('status', 1);
            $this->db->like('clinica_id', $id);
            $query = $this->db->get();
            return $query->row();
        }

        public function insert($nombre,$fecha,$user){
            $data= array(
                "id_user" => $user,
                "fecha" => $fecha,
                "nombre_clinica" => $nombre
            );
            $this->db->insert('clinica', $data);
            return $this->db->insert_id();
        }
        public function edit($nombre,$id){
            $data= array(
                "nombre_clinica" => $nombre
            );
            $this->db->where('clinica_id', $id);
            $this->db->update('clinica', $data);
            return $this->db->affected_rows();
        }
    }
?>