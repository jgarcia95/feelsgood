<?php

class ejercicio_model extends CI_Model {

    public function get($id_ejercicio=0)
    {
        $this->db->select('id_ejercicio, nombre');
        $this->db->from('ejercicios');
        $this->db->where('status', 1);
        if ($id_ejercicio!==0) {
            $this->db->where('id_ejercicio', $id_ejercicio);
        }
        $this->db->order_by("nombre","ASC");
        $query = $this->db->get();
        return $query->result();
    }
}