<?php
    class user_model extends CI_Model {

        public function login($user, $pass)
        {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('user.email_user', $user);
      		$this->db->where('user.pass_user', $pass);
      		$this->db->where('user.status', 1);
            $query = $this->db->get();
            return $query->row();
        }

        public function selectAll($buscar, $pagina)
        {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->like('name_user', $buscar);
            $this->db->limit( 9, 9*($pagina-1) );
            $query = $this->db->get();
            return $query->result();
        }

        public function selectByType($tipo)
        {
            $this->db->select('a.id_administrador,a.nombre,a.apellido,u.id_user,u.email_user,u.status');
            $this->db->from('user u');
            $this->db->join('administradores a','a.id_user=u.id_user');
            $this->db->where('u.type', $tipo);
            $this->db->where('u.status', 1);
            $query = $this->db->get();
            return $query->result();
        }

        public function selectAllSinFiltro($buscar, $pagina)
        {
            $this->db->select('a.id_administrador');
            $this->db->from('user u');
            $this->db->join('administradores a','a.id_user=u.id_user');
            $this->db->where('u.status', 1);
            $this->db->like('u.email_user', $buscar);
            $query = $this->db->get();
            return $query->result();
        }

        public function selectBy($id){
            $this->db->select('a.id_administrador,a.nombre,a.apellido,u.id_user,u.email_user,u.pass_user');
            $this->db->from('user u');
            $this->db->join('administradores a','a.id_user=u.id_user');
            $this->db->where('a.id_administrador', $id);
            $query = $this->db->get();
            return $query->row();
        }

        public function delete($id)
        {
            $data= array(
                "status" => 0
                );
            $this->db->where('id_user', $id);
            $this->db->update('user', $data);
            return $this->db->affected_rows();
        }
        public function insertUser($email,$pass,$type){
            $data= array(
                "email_user" => $email,
                "pass_user" => $pass,
                "type" => $type,
                "status" => 1
                );
            $this->db->insert('user', $data);
            return $this->db->insert_id();
        }
        public function insertAdmin($data){
            $this->db->insert('administradores', $data);
            return $this->db->insert_id();
        }
        public function insertClinica($data){
            $this->db->insert('clinica', $data);
            return $this->db->insert_id();
        }
        public function insertDoctor($data){
            $this->db->insert('doctor', $data);
            return $this->db->insert_id();
        }
        public function insertPaciente($data){
            $this->db->insert('paciente', $data);
            return $this->db->insert_id();
        }
        public function modificarPaciente($data,$id){
            $this->db->where('id_paciente', $id);
            $this->db->update('paciente', $data);
            return $this->db->affected_rows();
        }
        public function updateUser($id,$email,$pass){
            $data= array(
                "email_user" => $email,
                "pass_user" => $pass
                );
            $this->db->where('id_user', $id);
            $this->db->update('user', $data);
            return $this->db->affected_rows();
        }

        public function updateAdmin($id,$data){
            $this->db->where('id_administrador', $id);
            $this->db->update('administradores', $data);
            return $this->db->affected_rows();
        }
        public function editClinica($data, $id){
            $this->db->where('clinica_id', $id);
            $this->db->update('clinica', $data);
            return $this->db->affected_rows();
        }
        public function editDoctor($data,$id){
            $this->db->where('id_doctor', $id);
            $this->db->update('doctor', $data);
            return $this->db->affected_rows();
        }
        public function editPaciente($nombre,$apellido ,$id){
            $data= array(
                "nombre" => $nombre,
                "apellido" => $apellido
                );
            $this->db->where('id_paciente', $id);
            $this->db->update('paciente', $data);
            return $this->db->affected_rows();
        }

        public function comprobarCorreo($correo)
        {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('user.email_user', $correo);
            $query = $this->db->get();
            return $query->row();
        }

        public function comprobarDigito($correo,$digito)
        {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('user.digitos', $digito);
            $this->db->where('email_user', $correo);
            $query = $this->db->get();
            return $query->row();
        }

        public function modificardigitos($correo,$digitos){
            $data= array(
                "digitos" => $digitos
                );
            $this->db->where('email_user', $correo);
            $this->db->update('user', $data);
            return $this->db->affected_rows();
        }

        public function modificarPass($correo,$pass){
            $data= array(
                "pass_user" => $pass
                );
            $this->db->where('email_user', $correo);
            $this->db->update('user', $data);
            return $this->db->affected_rows();
        }

        public function getPaises()
        {
            $this->db->select('*');
            $this->db->from('paises');
            $this->db->order_by('PaisNom','ASC');
            $query = $this->db->get();
            return $query->result();
        }

        public function getProvincias($idPais)
        {
            $this->db->select('*');
            $this->db->from('provincia');
            $this->db->where('PaisCod',$idPais);
            $this->db->order_by('ProvinNom','ASC');
            $query = $this->db->get();
            return $query->result();
        }

        public function getLocalidades($idProvincia)
        {
            $this->db->select('*');
            $this->db->from('localidades');
            $this->db->where('ProvinCod',$idProvincia);
            $this->db->order_by('LocalidNom','ASC');
            $query = $this->db->get();
            return $query->result();
        }

        public function getDoctorByUser($user)
        {
            $this->db->select('id_doctor, ejercicios');
            $this->db->from('doctor');
            $this->db->where('id_user',$user);
            $query = $this->db->get();
            return $query->row();
        }

        public function getDato($id,$tipo,$campos){
            switch ($tipo) {
                case 1:
                    $this->db->select($campos);
                    $this->db->from('administradores a');
                    $this->db->join('user u','u.id_user=a.id_user');
                    $this->db->where('u.id_user', $id);
                break;
                case 2:
                    $this->db->select($campos);
                    $this->db->from('doctor d');
                    $this->db->join('user u','u.id_user=d.id_user');
                    $this->db->where('u.id_user', $id);
                break;
                case 3:
                    $this->db->select($campos);
                    $this->db->from('paciente p');
                    $this->db->join('user u','u.id_user=p.id_user');
                    $this->db->where('u.id_user', $id);
                break;
                case 4:
                    $this->db->select($campos);
                    $this->db->from('clinica c');
                    $this->db->join('user u','u.id_user=c.id_user');
                    $this->db->where('u.id_user', $id);
                break;
            }
            $query = $this->db->get();
            return $query->row();
        }
    }
?>