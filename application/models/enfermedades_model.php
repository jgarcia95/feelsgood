<?php
    class enfermedades_model extends CI_Model {

        public function get($tipo)
        {
            $this->db->select('*');
            $this->db->from('enfermedades');
            $this->db->where("tipo",$tipo);
            $query = $this->db->get();
            return $query->result();
        }
    }
?>