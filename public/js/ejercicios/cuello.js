$(document).ready(function(){
	$("#body-form-dosificacion").on("change","#tipo_ejercicio",function(){
		if($("#tipo_ejercicio").val()==="1"){
			$("#tipo_movimiento_ajax").parent().parent().show();
			$("#div-button-add-move").show();
			$("#slider_movimientos").show();
			$("#repeticiones").parent().parent().remove();
		}else{
			$("#grados").parent().parent().before('<div class="form-group col-md-6" style="display: block;"><label class="col-sm-6 control-label">'+
			'Repeticiones</label><br><div class="col-sm-9"><input type="number" min="1" class="form-control" name="repeticiones" id="repeticiones" /></div></div>')
			$("#tipo_movimiento_ajax").parent().parent().hide();
			$("#div-button-add-move").hide();
			$("#slider_movimientos").hide();
			$("#slider_movimientos #sortable").html("");
			$("#slider_movimientos #sortable").css("width","0px");
		}
	});
	$("#dosificacion").on("click","#button-add-move",function(e){
		e.preventDefault();
		var ancho=parseInt($("#sortable").width())+210;
		$("#sortable").css("width",ancho);
		var datos="\"TipoRepeticion\":\""+$("#tipo_movimiento_ajax").val()+"\",\"grados\":\""+$("#grados option:selected").text()+"\"";
		var texto="<div class='ui-state-default' data-datos='"+datos+"'><button type='button' class='close'>";
		texto+="<span aria-hidden='true'>×</span></button>";
		texto+=$("#tipo_movimiento_ajax option:selected").text()+" con "+$("#grados option:selected").text()+" grados de rotación</div>";
		$("#slider_movimientos #sortable").append(texto);
		$("#sortable").sortable();
	});
	$("#dosificacion").on("click",".close",function(){
		var ancho=parseInt($("#sortable").width())-210;
		$("#sortable").css("width",ancho);
		$(this).parent().remove();
	});
	/////////////Para editar/////////////
	$("#dosificacionEdit").on("change","#tipo_ejercicio_edit",function(){
		/*if($("#tipo_ejercicio_edit").val()=="1"){
			$("#tipo_movimiento_ajax_edit").parent().parent().show();
			$("#div-button-add-move-edit").show();
			$("#slider_movimientos_edit").show();
			$("#repeticiones_edit").parent().parent().remove();
		}else{
			$("#grados_edit").parent().parent().before('<div class="form-group col-md-12" style="display: block;"><label class="col-sm-3 control-label">'+
			'Repeticiones</label><div class="col-sm-9"><input type="number" min="1" class="form-control" name="repeticiones" id="repeticiones" /></div></div>')
			$("#tipo_movimiento_ajax_edit").parent().parent().hide();
			$("#div-button-add-move-edit").hide();
			$("#slider_movimientos_edit").hide();
			$("#slider_movimientos_edit #sortable_edit").html("");
			$("#slider_movimientos_edit #sortable_edit").css("width","0px");
			$("#array_movimientos_edit").val("");
		}/**/
	});
	$("#dosificacionEdit").on("click","#button-add-move-edit",function(e){
		e.preventDefault();
		var ancho=parseInt($("#sortable_edit").width())+210;
		$("#sortable_edit").css("width",ancho);
		var datos="\"TipoRepeticion\":\""+$("#tipo_movimiento_ajax_edit").val()+"\",\"grados\":\""+$("#grados_edit option:selected").text()+"\"";
		var texto="<div class='ui-state-default' data-datos='"+datos+"'><button type='button' class='close'>";
		texto+="<span aria-hidden='true'>×</span></button>";
		texto+=$("#tipo_movimiento_ajax_edit option:selected").text()+" con "+$("#grados_edit option:selected").text()+" grados de rotación</div>";
		$("#slider_movimientos_edit #sortable_edit").append(texto);
		$("#sortable_edit").sortable();
	});
	$("#dosificacionEdit").on("click",".close",function(){
		var ancho=parseInt($("#sortable_edit").width())-210;
		$("#sortable_edit").css("width",ancho);
		$(this).parent().remove();
	});
});