function displayCamposDependientes(){

    if( $("#respiracion").prop("checked") == true ){
        
        $("#div_campos_dependientes").show();

    }
    else{
        $("#div_campos_dependientes").hide();
    }
}	


$("#dosificacion").on("click","#button-add-scene",function(e){
	
	var id_e = $(this).attr('data-id');
	var nombre_e = $(this).attr('data-nombre'); 
	var dat="";
	var tex="";

	e.preventDefault();
	var anch=parseInt($("#sortable2").width())+210;
	$("#sortable2").css("width",anch);
	dat="\"id_escenario\":\""+id_e+"\",\"nombre\":\""+nombre_e+"\"";
	tex="<div class='ui-state-default' data-datos='"+dat+"'><button onclick='unlockScene("+id_e+")' type='button' class='close'>";
	tex+="<span aria-hidden='true'>×</span></button>";
	tex+=$(this).attr('data-nombre').trim()+"</div>";
	$("#slider_escenarios #sortable2").append(tex);
	$("#sortable2").sortable();
	$(this).attr('disabled', 'disabled');
});


	function unlockScene( id ){

		$("#cont-abs_estres a[data-id='"+id+"']").removeAttr('disabled');
	}

	function unlockSceneEdit( id ){

		$("#cont-abs2 a[data-id='"+id+"']").removeAttr('disabled');
	}
	
	

	/*EDITAR*/

	$("#dosificacionEdit").on("click","#button-add-scene_edit",function(e){
		
		var id_e = $(this).attr('data-id');
		var nombre_e = $(this).attr('data-nombre');
		var dat="";
		var tex="";

		//alert( id_e );

		e.preventDefault();
		var anch=parseInt($("#sortable2_edit").width())+210;
		$("#sortable2_edit").css("width",anch);
		dat="\"id_escenario\":\""+id_e+"\",\"nombre\":\""+nombre_e+"\"";


		tex="<div class='ui-state-default' data-datos='"+dat+"'><button type='button' class='close'>";
		tex+="<span aria-hidden='true'>×</span></button>";
		tex+=$(this).attr('data-nombre').trim()+"</div>";
		$("#slider_escenarios_edit #sortable2_edit").append(tex);
		$("#sortable2_edit").sortable();
		$(this).attr('disabled', 'disabled');

	});