if ($('#lista-doctores').length>0) {
	$('#lista-doctores').dataTable();
	$(".deldata").click(function(e){
		e.preventDefault();
		$("#id_doctor_eliminar").val($(this).data("id"));
    });
	$("#eliminar_doctor").click(function(e){
		manageLoading(1);
		$.post(base_url+'administrador/deleteDoctor',{id:$("#id_doctor_eliminar").val()},function(response){
			manageLoading(0);
			$("#close_doctor_del").trigger("click");
			if(response.status){
				noty({
                	text: response.msg,
		            type: 'success',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
		        setTimeout(function(){location.reload();},1000);
			}else{
				noty({
                	text: response.msg,
		            type: 'error',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }
        },'json');
	});
}
if ($('#form-edit-doctor').length>0) {
	$('#form-edit-doctor').parsley( { listeners: {
		onFormSubmit: function ( isFormValid, event ) {
			return false;
		}
	}});
	$("#form-edit-doctor").submit(function(e){
		manageLoading(1);
		var numClinicas = $("#clinicas :selected").select2('data');
		var numEjer = $("#ejercicios :selected").select2('data');
		e.preventDefault();
		var clinicas='';
		var ejercicios='';
		for (var i = 0; i < numClinicas.length; i++) {
			clinicas += numClinicas[i]["value"]+",";
		};
		for (var a = 0; a < numEjer.length; a++) {
			ejercicios += numEjer[a]["value"]+",";
		};
		clinicas = (clinicas=="")? "" : clinicas.substr(0,clinicas.length-1);
		ejercicios = (ejercicios=="")? "" : ejercicios.substr(0,ejercicios.length-1);
		if ( $(this).parsley().isValid() ) {
	        $.post(base_url+'administrador/editDoctorAction',$("#form-edit-doctor").serialize()+"&clinicas="+clinicas+"&ejercicios="+ejercicios,function(response){
	            manageLoading(0);
	            if(response.status){
	                noty({
	                	text: response.msg,
			            type: 'success',
			            layout: 'topCenter',
			            timeout: '3000'
			        });
	            }else{
	                noty({
	                	text: response.msg,
			            type: 'error',
			            layout: 'topCenter',
			            timeout: '3000'
			        });
	            }
	        },'json');
	    }
	    else{
	    	manageLoading(0);
	    }
    });
}
if ($('#form-add-doctor').length>0) {
	$('#form-add-doctor').parsley( { listeners: {
		onFormSubmit: function ( isFormValid, event ) {
			return false;
		}
	}});
	$("#form-add-doctor").submit(function(e){
		manageLoading(1);
		var numClinicas = $("#clinicas :selected").select2('data');
		var numEjer = $("#ejercicios :selected").select2('data');
        e.preventDefault();
        var clinicas='';
        var ejercicios='';
		for (var i = 0; i < numClinicas.length; i++) {
			clinicas += numClinicas[i]["value"]+",";
		};
		for (var a = 0; a < numEjer.length; a++) {
			ejercicios += numEjer[a]["value"]+",";
		};
		clinicas = (clinicas=="")? "" : clinicas.substr(0,clinicas.length-1);
		ejercicios = (ejercicios=="")? "" : ejercicios.substr(0,ejercicios.length-1);
		if ( $(this).parsley().isValid() ) {
			$.post(base_url+'administrador/addDoctorAction',$("#form-add-doctor").serialize()+"&clinicas="+clinicas+"&ejercicios="+ejercicios,function(response){
	            manageLoading(0);
	            if(response.status){
	                noty({
	                	text: response.msg,
			            type: 'success',
			            layout: 'topCenter',
			            timeout: '3000'
			        });
	            }else{
	                noty({
	                	text: response.msg,
			            type: 'error',
			            layout: 'topCenter',
			            timeout: '3000'
			        });
	            }
	        },'json');
	    }
	    else{
	    	manageLoading(0);
	    }
    });
}
$("#agregar_clinica2").click(function (e){
	e.preventDefault();
		$('#clinicas').append($('<option>', {
			value: "nuevo-"+$('#nueva_clinica').val(),
			text: $('#nueva_clinica').val()
		}));
		var act = $('#clinicas').val();
		if(act == null){
			act = "nuevo-"+$('#nueva_clinica').val();
		}else{
			act.push("nuevo-"+$('#nueva_clinica').val());
		}
		$('#clinicas').val(act).trigger("change");
});
if($('.select2').length>0){
	$('.select2').select2();
	Dropzone.autoDiscover = false;
	var myDropzone = new Dropzone("div#fileImagen", { 
		url: base_url+"login/subirFoto",
		dictDefaultMessage: "Arrastra o da click para subir tu foto",
		maxFiles: 1,
		acceptedFiles: 'image/jpeg, image/jpg, image/png',
		addRemoveLinks: true,
		accept: function(file, done) {
			done();
		},
		init: function() {
			this.on("success", function(file, response) {
				var datos=$.parseJSON(response);
		        $("#renombrado").val(datos.renombrado);
		    });
			this.on("addedfile", function() {
				if (this.files[1]!=null){
				    var archivo=$("#renombrado").val();
				    this.removeFile(this.files[0]);
				    $.ajax({
			            type: 'POST',
			            url: base_url+'login/eliminarFoto',
			            data: "file_name="+archivo,
			            dataType: 'html'
			        });
				}
			});
	    }
	});
	$('.datepicker-input').datetimepicker({
		pickTime: false,format:'DD-MM-YYYY'
	});
}
$('div.dz-default.dz-message > span').show(); // Show message span
$('div.dz-default.dz-message').css({'opacity':1, 'background-image': 'none'});
$('#paisSolicitud').change(function(e) {
	manageLoading(1);
	$.post(base_url+"login/getProvincias",{id:$("#paisSolicitud").val()},function(response){
		manageLoading(0);
		var cad='<option value="0">-- Seleccionar provincia --</option>';
        $.each(response,function(index, value){
            cad+='<option value="'+value.ProvinCod+'">'+value.ProvinNom+'</option>';
        });
        $("#provinciaSolicitud").html(cad);
	},"json");
});
$('#provinciaSolicitud').change(function(e) {
	manageLoading(1);
	$.post(base_url+"login/getLocalidades",{id:$("#provinciaSolicitud").val()},function(response){
		manageLoading(0);
		var cad='<option value="0">-- Seleccionar localidad --</option>';
        $.each(response,function(index, value){
            cad+='<option value="'+value.LocalidCod+'">'+value.LocalidNom+'</option>';
        });
        $("#localidadSolicitud").html(cad);
	},"json");
});