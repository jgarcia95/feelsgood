var estadoCombo1 = true;
var estadoCombo2 = true;
var table;
if ($('#lista-pacientes-independientes').length>0) {
	$('#doctor_elegido').select2();
	table = $('#lista-pacientes-independientes').DataTable({"bSort": false});
}
if ($('#lista-pacientes').length>0) {
	$('#lista-pacientes').dataTable({"bSort": false});
	$(".deldata").click(function(e){
		e.preventDefault();
		$("#id_paciente_eliminar").val($(this).data("id"));
    });
	$("#eliminar_paciente").click(function(e){
		manageLoading(1);
		$.post(base_url+'administrador/deletePacienteFast',{id:$("#id_paciente_eliminar").val()},function(response){
			$("#close_paciente_del").trigger("click");
			if(response.status){
				noty({
                	text: response.msg,
		            type: 'success',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
		        setTimeout(function(){location.reload();},1000);
			}else{
				noty({
                	text: response.msg,
		            type: 'error',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }
            manageLoading(0);
        },'json');
	});
	$("#eliminar_paciente_ad").click(function(e){
		manageLoading(1);
		$.post(base_url+'administrador/deletePaciente',{id:$("#id_paciente_eliminar").val()},function(response){
			$("#close_paciente_del").trigger("click");
			if(response.status){
				noty({
                	text: response.msg,
		            type: 'success',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
		        setTimeout(function(){location.reload();},1000);
			}else{
				noty({
                	text: response.msg,
		            type: 'error',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }
            manageLoading(0);
        },'json');
	});
}
if ($('#form-add-paciente').length>0) {
	$('#form-add-paciente').parsley( { listeners: {
		onFormSubmit: function ( isFormValid, event ) {
			return false;
		}
	}});
	$("#form-add-paciente").submit(function(e){
        e.preventDefault();
		if ( $(this).parsley().isValid() ) {
			manageLoading(1);
	        $.post(base_url+'administrador/addPacienteAction',$("#form-add-paciente").serialize(),function(response){
	            if(response.status){
	                noty({
	                	text: response.msg,
			            type: 'success',
			            layout: 'topCenter',
			            timeout: '3000'
			        });
	            }else{
	                noty({
	                	text: response.msg,
			            type: 'error',
			            layout: 'topCenter',
			            timeout: '3000'
			        });
	            }
	            manageLoading(0);
	        },'json');
	    }
    });
}
if ( $('#form-add-paciente-fast').length>0 ) {
	$('#form-add-paciente-fast').parsley( { listeners: {
		onFormSubmit: function ( isFormValid, event ) {
			return false;
		}
	}});
	$("#form-add-paciente-fast").submit(function(ev){
        ev.preventDefault();

        if ( $(this).parsley().isValid() ) {
			manageLoading(1);
	        $.post(base_url+'administrador/addPacienteFastAction',$("#form-add-paciente-fast").serialize(),function(response){
	            if(response.status){
	                noty({
	                	text: response.msg,
			            type: 'success',
			            layout: 'topCenter',
			            timeout: '3000'
			        });
			        $("#form-add-paciente-fast")[0].reset();
	            }else{
	                noty({
	                	text: response.msg,
			            type: 'error',
			            layout: 'topCenter',
			            timeout: '3000'
			        });
	            }
	            manageLoading(0);
	        },'json');
	    }
	    else{}
    });
}
if ($('.select2').length) {
	$('.select2').select2();
	Dropzone.autoDiscover = false;
	var myDropzone = new Dropzone("div#fileImagen", { 
		url: base_url+"login/subirFoto",
		dictDefaultMessage: "Arrastra o da click para subir tu foto",
		maxFiles: 1,
		acceptedFiles: 'image/jpeg, image/jpg, image/png',
		addRemoveLinks: true,
		accept: function(file, done) {
		    done();
		},
		init: function() {
			this.on("success", function(file, response) {
				var datos=$.parseJSON(response);
	            $("#renombrado").val(datos.renombrado);
	        });
		    this.on("addedfile", function() {
			    if (this.files[1]!=null){
			    	var archivo=$("#renombrado").val();
			        this.removeFile(this.files[0]);
			        $.ajax({
		                type: 'POST',
		                url: base_url+'login/eliminarFoto',
		                data: "file_name="+archivo,
		                dataType: 'html'
		            });
			    }
		    });
	       }
	});
	$('.datepicker-input').datetimepicker({
		pickTime: false,format:'DD-MM-YYYY'
	});
}
$('div.dz-default.dz-message > span').show(); // Show message span
$('div.dz-default.dz-message').css({'opacity':1, 'background-image': 'none'});
$('#paisSolicitud').change(function(e) {
	manageLoading(1);
	$.post(base_url+"login/getProvincias",{id:$("#paisSolicitud").val()},function(response){
		var cad='<option value="0">-- Seleccionar provincia --</option>';
        $.each(response,function(index, value){
            cad+='<option value="'+value.ProvinCod+'">'+value.ProvinNom+'</option>';
        });
        $("#provinciaSolicitud").html(cad);
        if ( $( "#provincia_paciente_edit" ).length && estadoCombo1) {
			estadoCombo1 = false;
		    $("#provinciaSolicitud").val($("#provincia_paciente_edit" ).val());
		 	$('#provinciaSolicitud').trigger("change");
		}
		manageLoading(0);
	},"json");
});
$('#provinciaSolicitud').change(function(e) {
	manageLoading(1);
	$.post(base_url+"login/getLocalidades",{id:$("#provinciaSolicitud").val()},function(response){
		var cad='<option value="0">-- Seleccionar localidad --</option>';
        $.each(response,function(index, value){
            cad+='<option value="'+value.LocalidCod+'">'+value.LocalidNom+'</option>';
        });
        $("#localidadSolicitud").html(cad);
        if ( $( "#localidad_paciente_edit" ).length && estadoCombo2) {
			estadoCombo2 = false;
		    $("#localidadSolicitud").val($("#localidad_paciente_edit" ).val());
		}
		manageLoading(0);
	},"json");
});

if ($('#form-modificar-paciente').length>0) {
	$('#form-modificar-paciente').parsley( { listeners: {
		onFormSubmit: function ( isFormValid, event ) {
			return false;
		}
	}});
	$("#form-modificar-paciente").submit(function(e){
        e.preventDefault();
		if ( $(this).parsley().isValid() ) {
			manageLoading(1);
	        $.post(base_url+'administrador/modificarPacienteAction',$("#form-modificar-paciente").serialize(),function(response){
	            if(response.status){
	                noty({
	                	text: response.msg,
			            type: 'success',
			            layout: 'topCenter',
			            timeout: '3000'
			        });

			        if( response.id_user ){
			        	$("#id").val( $.trim(response.id_user) );
			        } else{}
	            }else{
	                noty({
	                	text: response.msg,
			            type: 'error',
			            layout: 'topCenter',
			            timeout: '3000'
			        });
	            }
	            manageLoading(0);
	        },'json');
	    }
    });
}

$("#img-edit").click(function(){
	$("#c-edit-clinica").show();
});

$("#agregar_clinica2p").click(function (e){
	e.preventDefault();
	$.post(base_url+"login/addClinica",{clinica:$('#nueva_clinicap').val()},function(response){
		$('#clinica').append($('<option>', {
			value: response.id,
			text: response.valor
		}));
		$("#clinica").prop("disabled", true);
		$('#clinica').val(response.id).trigger("change");
		$("#nueva_clinica2p").val(response.valor);
		$("#img-edit").show();
		$("#c-add-clinica").hide();
		$("#content-clinica").css("margin-right","30px");
		noty({
		    text: response.msg,
		    type: 'success',
		    layout: 'topCenter',
		    timeout: '3000'
		});
	},"json");
});

$("#agregar_clinica3p").click(function (e){
	e.preventDefault();
	$.post(base_url+"login/editClinica",{clinica:$('#nueva_clinica2p').val(),id:$("#clinica").val()},function(response){
		$('#clinica option[value='+$("#clinica").val()+']').html($('#nueva_clinica2p').val());
		$("#clinica").prop("disabled", true);
		$('#clinica').val($("#clinica").val()).trigger("change");
		$("#img-edit").show();
		$("#c-edit-clinica").hide();
		noty({
		    text: response.msg,
		    type: 'success',
		    layout: 'topCenter',
		    timeout: '3000'
		});
	},"json");
});

function asignar(id,e){
	manageLoading(1);
	$.post(base_url+'paciente/asignarAction',{paciente:id,doctor:$("#doctor_elegido").val()},function(response){
        if(response.status){
        	$("#messagemodal").html("Se asignó de forma correcta.");
        	$("#modalMessage button").removeClass("btn-success");
        	$("#modalMessage button").removeClass("btn-danger");
        	$("#modalMessage button").addClass("btn-success");
        	table.row( $(e).parents('tr') ).remove().draw();
        	$("#modalMessage").modal("show");
        }else{
            $("#messagemodal").html(response.msg);
            $("#modalMessage button").removeClass("btn-danger");
            $("#modalMessage button").removeClass("btn-success");
            $("#modalMessage button").addClass("btn-danger");
	        $("#modalMessage").modal("show");
        }
        manageLoading(0);
    },'json');
}