$('#body-form-dosificacion').on('focus',".datetimepicker-input", function(){
    $(this).datetimepicker({format:'DD-MM-YYYY hh:mm A'});
});
function trae_sesiones() {
	$.post(base_url+'paciente/trae_sesiones',{id:$("#id_paciente").val()},function(response){
		$('.full-calendar').fullCalendar('destroy');
		$('.full-calendar').fullCalendar({
			header: {
				defaultDate: moment(Date()),
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			navLinks: true, // can click day/week names to navigate views
			eventLimit: true, // allow "more" link when too many events
			editable: false,
			events: response,
			eventClick: function(event, jsEvent, view) {
				manageLoading(1);
				$.post(base_url+"perfil/trae_ejercicio_editar",{id_ejercicio:event.id,tipo:event.tipo},function(response){
					
					//console.log( response );
					$("#id_ejercicio").val(event.tipo);
					$("#id_sesion").val(event.id);
					$("#body-form-dosificacion").html(response.contenido);
					$("#dosificacion input, select, button").attr('disabled', 'disabled');
			        $("#modalEditaSesion").modal();
					manageLoading(0);
				},"json");
			}
		});
	},'json');
}

trae_sesiones();

$("#modificar_dosificacion").click(function(){
	$('#dosificacion').trigger('submit');
});

$('#dosificacion').submit(function(e) {
    manageLoading(1);
    e.preventDefault();
    $.post(base_url+"perfil/act_edt_ejercicio",$('#dosificacion').serialize(),function(resultado){
        noty({
            text: resultado.msg,
            type: 'success',
            layout: 'topCenter',
            timeout: '3000'
        });
        trae_sesiones();
        manageLoading(0);
    },"json");
});

$('#eliminar_dosificacion').click(function(e) {
	$("#modalDelSesion").modal();
});

$('#eliminar_dosificacion_confirm').click(function(e) {
	manageLoading(1);
    e.preventDefault();
    var sendObj={id_sesion:$("#id_sesion").val(),tipo:$("#id_ejercicio").val()};
    $.post(base_url+"perfil/delEjercicio",sendObj,function(resultado){
        noty({
            text: resultado.msg,
            type: 'success',
            layout: 'topCenter',
            timeout: '3000'
        });
        trae_sesiones();
        manageLoading(0);
        $("#modalEditaSesion").modal('hide');
        $("#modalDelSesion").modal('hide');
    },"json");
});