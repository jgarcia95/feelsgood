var piex=false;


function convertir( time ){

    var hours = Math.floor( time / 3600 );  
    var minutes = Math.floor( (time % 3600) / 60 );
    var seconds = time % 60;
 
    minutes = minutes < 10 ? '0' + minutes : minutes;
     
    seconds = seconds < 10 ? '0' + seconds : seconds;
     
    var result = hours + "h " + minutes + "m " + seconds+"s";

    return result;
}

$(document).ready(function () {
    $('.datepicker-input').datetimepicker({
        pickTime: false,
        format:'DD-MM-YYYY'
    });
    $('#body-form-dosificacion').on('focus',".datetimepicker-input", function(){
        $(this).datetimepicker({format:'DD-MM-YYYY hh:mm A'});
    });
    $('#body-form-dosificacion-edit').on('focus',".datetimepicker-input", function(){
        $(this).datetimepicker({format:'DD-MM-YYYY hh:mm A'});
    });
    //Slider de ejercicios
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if($(e.target).attr("href")=="#profileTab3"){

        }
    });
    //Slider de estadisticas
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if($(e.target).attr("href")=="#profileTab5"){
            
        }
    });
    $("#btnSesiones").on("click",function(){
        $.post(base_url+'paciente/trae_sesiones',{id:$("#id_paciente").val()},function(response){
           
            setTimeout(function(){
                $('.full-calendar').fullCalendar('destroy');
                $('.full-calendar').fullCalendar({
                    header: {
                        defaultDate: moment(Date()),
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    navLinks: true, // can click day/week names to navigate views
                    eventLimit: true, // allow "more" link when too many events
                    editable: false,
                    events: response,
                    eventClick: function(event, jsEvent, view) {
                        manageLoading(1);
                        $.post(base_url+"perfil/trae_ejercicio_editar",{id_ejercicio:event.id,tipo:event.tipo},function(response){
                            
                            $("#body-form-dosificacion-edit").html( response.contenido );
                            $("#modalEditaSesion").modal();
                            
                            $("#id_ejercicio_edit").val(event.tipo);
                            $("#id_sesion").val(event.id);
                            manageLoading(0);
                        },"json");
                    }
                });
            },500);

        },'json');
    });
});

$("#tipo").change(function() {
    manageLoading(1);
    var id=$(this).val();
    $("#id_ejercicio").val(id);
    $("#div-button-add-move").remove();
    $("#slider_movimientos").remove();
    $("#slider_escenarios").remove();
    $("#slider_escenarios2").remove();
    $("#array_movimientos").remove();
    $("#array_escenarios").remove();
    $("#array_escenarios2").remove();
    $("#estres_tab").empty();
    $("#ansiedad_tab").empty();

    $.post(base_url+"perfil/trae_ejercicio",{id_ejer:id},function(response){
        
        $("#body-form-dosificacion").empty().html(response.contenido);
        $("#div-button-add").show();
        manageLoading(0);
    },"json");
});

$('#dosificacion').submit(function(e) {
    manageLoading(1);
    e.preventDefault();
    if($("#slider_movimientos").length>0){
        var movimientos="";
        $.each($(".ui-state-default"),function(index,value){
            movimientos+=$(this).data("datos")+";;";
        });
        $("#array_movimientos").val(movimientos);
    }

    if($("#slider_escenarios2").length>0){
        var escenarios2="";
        $.each($("#slider_escenarios2 .ui-state-default"),function(index,value){
            escenarios2+=$(this).attr("data-datos")+";;";
        });
        $("#array_escenarios2").val( escenarios2 );
    }

    var canti_esc = 0;
    if($("#slider_escenarios").length>0){
        var escenarios="";

        $.each($("#slider_escenarios .ui-state-default"),function(index,value){
            escenarios+=$(this).attr("data-datos")+";;";

            canti_esc = canti_esc + 1;
        });

        if( canti_esc < 2 ){

            noty({
                text: "Debes seleccionar minimo 2 escenarios",
                type: 'error',
                layout: 'topCenter',
                timeout: '3000'
            });
             manageLoading(0);

            return false;
        }
        else{
            $("#array_escenarios").val( escenarios );
        }
    }

    var id_e = $("#id_ejercicio").val().trim();

    if( id_e == 1 ){
        var msj = '';
        //respiracion
        if( $("#fecha").val().trim() === "" ){
            msj += 'El campo fecha no debe estar vacío <br>';
        }
        else if( $("#repeticiones").val().trim() === "" || parseInt($("#repeticiones").val().trim()) < 2 ){
            msj += 'Ingresa un valor válido en el campo Repeticiones <br>';
        }
        else if( $("#inhalacion").val().trim() === "" || parseInt($("#inhalacion").val().trim()) < 0 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Inhalación <br>';
        }
        else if( $("#mantener").val().trim() === "" || parseInt($("#mantener").val().trim()) < 0 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Mantener Respiración<br>';
        }
        else if( $("#exhalacion").val().trim() === "" || parseInt($("#exhalacion").val().trim()) < 0 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Exhalación<br>';
        }
        else if( $("#exhalacion").val().trim() === "" || parseInt($("#exhalacion").val().trim()) < 0 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Exhalación<br>';
        }
        else if( $("#descanso").val().trim() === "" || parseInt($("#descanso").val().trim()) < 0 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Descanso<br>';
        }
        else{}
        if( $.trim(msj) !== "" ){
            manageLoading(0);
            noty({text:msj,type:'error',layout:'topCenter',timeout:'3000'});
            return false;
        }
        else{}
    }
    else if( id_e == 3 ){
        var msj = '';
        //bote
        if( $("#fecha").val().trim() === "" ){
            msj += 'El campo fecha no debe estar vacío <br>';
        }
        else if( $("#dificultad").val().trim() === "" ){
            msj += 'Debes seleccionar la Dificultad <br>';
        }
        else if( $("#circuito").val().trim() === "" ){
            msj += 'Debes seleccionar el Circuito <br>';
        }
        else if( $("#direccion").val().trim() === "" ){
            msj += 'Debes seleccionar la Dirección <br>';
        }
        else if( $("#monedas").val().trim() === "" || parseInt($("#monedas").val().trim()) < 10 || parseInt($("#monedas").val().trim()) > 30 ){
            msj += 'Ingresa un valor válido en el campo Repeticiones (Entre 10 - 30)<br>';
        }
        else{}
        if( $.trim(msj) !== "" ){
            manageLoading(0);
            noty({text:msj,type:'error',layout:'topCenter',timeout:'3000'});
            return false;
        }
        else{}
    }
    else if( id_e == 7 ){
        var msj = '';
        //puente
        if( $("#fecha").val().trim() === "" ){
            msj += 'El campo fecha no debe estar vacío <br>';
        }
        else if( $("#espera").val().trim() === "" || parseInt($("#espera").val().trim()) < 0 || parseInt($("#espera").val().trim()) > 10 ){
            msj += 'Ingresa un valor válido en el campo Tiempo de Espera (Entre 0 - 10) <br>';
        }
        else if( $("#tipomovimiento").val().trim() === "" ){
            msj += 'Debes seleccionar el Tipo de Movimiento <br>';
        }
        else if( $("#altura").val().trim() === "" ){
            msj += 'Debes seleccionar el Rango de Movimiento <br>';
        }
        else if( $("#repeticiones").val().trim() === "" || parseInt($("#repeticiones").val().trim()) < 1 || parseInt($("#repeticiones").val().trim()) > 100 ){
            msj += 'Ingresa un valor válido en el campo Repeticiones (Entre 1 - 100)<br>';
        }
        else{}
        if( $.trim(msj) !== "" ){
            manageLoading(0);
            noty({text:msj,type:'error',layout:'topCenter',timeout:'3000'});
            return false;
        }
        else{}
    }
    else if( id_e == 8 ){
        var msj = '';
        //cuello y cabeza
        if( $("#fecha").val().trim() === "" ){
            msj += 'El campo fecha no debe estar vacío <br>';
        }
        else if( $("#tiempo_ejercicio").val().trim() === "" || parseInt($("#tiempo_ejercicio").val().trim()) < 0 ){
            msj += 'Ingresa un valor válido en el campo Velocidad del Movimiento<br>';
        }
        else if( $("#tipo_ejercicio option:selected").val().trim() === "2" && $("#repeticiones").length > 0 && ( $("#repeticiones").val().trim() === "" || parseInt($("#repeticiones").val().trim()) < 1 ) ){
            msj += 'Ingresa un valor válido en el campo Repeticiones <br>';
        }
        else if( $("#array_movimientos").val().trim() === "" ){
            msj += 'Debes agregar al menos un movimiento <br>';
        }
        else{}
        if( $.trim(msj) !== "" ){
            manageLoading(0);
            noty({text:msj,type:'error',layout:'topCenter',timeout:'3000'});
            return false;
        }
        else{}
    }
    else if( id_e == 9 ){
        var msj = '';
        //estres ansiedad
        if( $("#fecha").val().trim() === "" ){
            msj += 'El campo fecha no debe estar vacío <br>';
        }
        else if( $("#tipo_emo").val().trim() === "" ){
            msj += 'Selecciona el Tipo<br>';
        }
        else if( $("#respiracion").prop("checked") == true && $("#repeticiones").length > 0 && ( $("#repeticiones").val().trim() === "" || parseInt($("#repeticiones").val().trim()) < 2 ) ){
            msj += 'Ingresa un valor válido en el campo Repeticiones<br>';
        }
        else if( $("#respiracion").prop("checked") == true && $("#inhalacion").length > 0 && ( $("#inhalacion").val().trim() === "" || parseInt($("#inhalacion").val().trim()) < 4 ) ){
            msj += 'Ingresa un valor válido en el campo Inhalación<br>';
        }
        else if( $("#respiracion").prop("checked") == true && $("#sostenimiento").length > 0 && ( $("#sostenimiento").val().trim() === "" || parseInt($("#sostenimiento").val().trim()) < 4 ) ){
            msj += 'Ingresa un valor válido en el campo Sostenimiento<br>';
        }
        else if( $("#respiracion").prop("checked") == true && $("#exhalacion").length > 0 && ( $("#exhalacion").val().trim() === "" || parseInt($("#exhalacion").val().trim()) < 4 ) ){
            msj += 'Ingresa un valor válido en el campo Exhalación<br>';
        }
        else if( $("#respiracion").prop("checked") == true && $("#descanso").length > 0 && ( $("#descanso").val().trim() === "" || parseInt($("#descanso").val().trim()) < 4 ) ){
            msj += 'Ingresa un valor válido en el campo Descanso<br>';
        }
        else if( $("#array_escenarios").val().trim() === "" ){
            msj += 'Debes agregar al menos 2 Escenarios <br>';
        }
        else{}
        if( $.trim(msj) !== "" ){
            manageLoading(0);
            noty({text:msj,type:'error',layout:'topCenter',timeout:'3000'});
            return false;
        }
        else{}
    }
    else if( id_e == 10 ){
        var msj = '';
        //tamo
        if( $("#fecha").val().trim() === "" ){
            msj += 'El campo fecha no debe estar vacío <br>';
        }
        else if( $("#repeticiones").val().trim() === "" || parseInt( $("#repeticiones").val().trim() ) < 2 ){
            msj += 'Ingresa un valor válido en el campo Repeticiones<br>';
        }
        else if( $("#inhalacion").val().trim() === "" || parseInt( $("#inhalacion").val().trim() ) < 1 ){
            msj += 'Ingresa un valor válido en el campo Tiempo de Inhalación<br>';
        }
        else if( $("#mantener").val().trim() === "" || parseInt( $("#mantener").val().trim() ) < 1 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Mantener Respiración<br>';
        }
        else if( $("#exhalacion").val().trim() === "" || parseInt( $("#exhalacion").val().trim() ) < 1 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Exhalación<br>';
        }
        else if( $("#descanso").val().trim() === "" || parseInt( $("#descanso").val().trim() ) < 1 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Descanso<br>';
        }
        else{}
        if( $.trim(msj) !== "" ){
            manageLoading(0);
            noty({text:msj,type:'error',layout:'topCenter',timeout:'3000'});
            return false;
        }
        else{}
    }
    else{}


    $.post(base_url+"perfil/act_edt_ejercicio",$('#dosificacion').serialize(),function(resultado){
       
        console.log( resultado );
       noty({
            text: resultado.msg,
            type: 'success',
            layout: 'topCenter',
            timeout: '3000'
        });
        $(".sombra-interna-ejer").css("opacity",0);
        $("#body-form-dosificacion").html("");
        $("#div-button-add").hide();
        $("#div-button-add-move").remove();
        $("#slider_movimientos").remove();
        $("#slider_escenarios").remove();
        $("#slider_escenarios2").remove();
        $("#array_movimientos").remove();
        $("#array_escenarios").remove();
        $("#array_escenarios2").remove();
        
        manageLoading(0);
    },"json");
});

$("#modificar_dosificacion").click(function(){
    if($("#slider_movimientos").length>0){
        var movimientos="";
        $.each($(".ui-state-default"),function(index,value){
            movimientos+=$(this).data("datos")+";;";
        });
        $("#array_movimientos").val(movimientos);
    }
  $('#dosificacionEdit').trigger('submit');
});

$('#dosificacionEdit').submit(function(e) {
    manageLoading(1);
    e.preventDefault();
    if($("#slider_movimientos_edit").length>0){
        var movimientos="";
        var canti = 0;
        $.each($(".ui-state-default"),function(index,value){
            movimientos+=$(this).data("datos")+";;";
            canti = canti + 1;
        });
        if( canti < 1 ){
            noty({
                text: "Debes agregar mínimo 1 movimiento",
                type: 'error',
                layout: 'topCenter',
                timeout: '3000'
            });
            $("#array_movimientos_edit").val("");
            manageLoading(0);
            return false;
        }
        else {
            $("#array_movimientos_edit").val(movimientos);
        }
    }

    if($("#slider_escenarios_edit").length>0){
        var escenarios="";
        var canti_es = 0;
        $.each($("#slider_escenarios_edit .ui-state-default"),function(index,value){
            escenarios+=$(this).attr("data-datos")+";;";

            canti_es = canti_es + 1;
        });

        if( canti_es < 2 ){

            noty({
                text: "Debes seleccionar minimo 2 escenarios",
                type: 'error',
                layout: 'topCenter',
                timeout: '3000'
            });
             manageLoading(0);

            return false;
        }
        else{
            $("#array_escenarios_edit").val( escenarios );
        }
       
    }

    var id_e = $("#id_ejercicio_edit").val().trim();

    if( id_e == 1 ){
        var msj = '';
        //respiracion
        if( $("#fecha").val().trim() === "" ){
            msj += 'El campo fecha no debe estar vacío <br>';
        }
        else if( $("#repeticiones").val().trim() === "" || parseInt($("#repeticiones").val().trim()) < 2 ){
            msj += 'Ingresa un valor válido en el campo Repeticiones <br>';
        }
        else if( $("#inhalacion").val().trim() === "" || parseInt($("#inhalacion").val().trim()) < 0 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Inhalación <br>';
        }
        else if( $("#mantener").val().trim() === "" || parseInt($("#mantener").val().trim()) < 0 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Mantener Respiración<br>';
        }
        else if( $("#exhalacion").val().trim() === "" || parseInt($("#exhalacion").val().trim()) < 0 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Exhalación<br>';
        }
        else if( $("#exhalacion").val().trim() === "" || parseInt($("#exhalacion").val().trim()) < 0 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Exhalación<br>';
        }
        else if( $("#descanso").val().trim() === "" || parseInt($("#descanso").val().trim()) < 0 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Descanso<br>';
        }
        else{}
        if( $.trim(msj) !== "" ){
            manageLoading(0);
            noty({text:msj,type:'error',layout:'topCenter',timeout:'3000'});
            return false;
        }
        else{}
    }
    else if( id_e == 3 ){
        var msj = '';
        //bote
        if( $("#fecha").val().trim() === "" ){
            msj += 'El campo fecha no debe estar vacío <br>';
        }
        else if( $("#dificultad").val().trim() === "" ){
            msj += 'Debes seleccionar la Dificultad <br>';
        }
        else if( $("#circuito").val().trim() === "" ){
            msj += 'Debes seleccionar el Circuito <br>';
        }
        else if( $("#direccion").val().trim() === "" ){
            msj += 'Debes seleccionar la Dirección <br>';
        }
        else if( $("#monedas").val().trim() === "" || parseInt($("#monedas").val().trim()) < 10 || parseInt($("#monedas").val().trim()) > 30 ){
            msj += 'Ingresa un valor válido en el campo Repeticiones (Entre 10 - 30)<br>';
        }
        else{}
        if( $.trim(msj) !== "" ){
            manageLoading(0);
            noty({text:msj,type:'error',layout:'topCenter',timeout:'3000'});
            return false;
        }
        else{}
    }
    else if( id_e == 7 ){
        var msj = '';
        //puente
        if( $("#fecha").val().trim() === "" ){
            msj += 'El campo fecha no debe estar vacío <br>';
        }
        else if( $("#espera").val().trim() === "" || parseInt($("#espera").val().trim()) < 0 || parseInt($("#espera").val().trim()) > 10 ){
            msj += 'Ingresa un valor válido en el campo Tiempo de Espera (Entre 0 - 10) <br>';
        }
        else if( $("#tipomovimiento").val().trim() === "" ){
            msj += 'Debes seleccionar el Tipo de Movimiento <br>';
        }
        else if( $("#altura").val().trim() === "" ){
            msj += 'Debes seleccionar el Rango de Movimiento <br>';
        }
        else if( $("#repeticiones").val().trim() === "" || parseInt($("#repeticiones").val().trim()) < 1 || parseInt($("#repeticiones").val().trim()) > 100 ){
            msj += 'Ingresa un valor válido en el campo Repeticiones (Entre 1 - 100)<br>';
        }
        else{}
        if( $.trim(msj) !== "" ){
            manageLoading(0);
            noty({text:msj,type:'error',layout:'topCenter',timeout:'3000'});
            return false;
        }
        else{}
    }
    else if( id_e == 8 ){
        var msj = '';
        //cuello y cabeza
        if( $("#fecha").val().trim() === "" ){
            msj += 'El campo fecha no debe estar vacío <br>';
        }
        else if( $("#tiempo_ejercicio").val().trim() === "" || parseInt($("#tiempo_ejercicio").val().trim()) < 0 ){
            msj += 'Ingresa un valor válido en el campo Velocidad del Movimiento<br>';
        }
        else if( $("#tipo_ejercicio_edit option:selected").val().trim() === "2" && $("#repeticiones").length > 0 && ( $("#repeticiones").val().trim() === "" || parseInt($("#repeticiones").val().trim()) < 1 ) ){
            msj += 'Ingresa un valor válido en el campo Repeticiones <br>';
        }
        else if( $("#array_movimientos_edit").val().trim() === "" ){
            msj += 'Debes agregar al menos 1 movimiento <br>';
        }
        else{}
        if( $.trim(msj) !== "" ){
            manageLoading(0);
            noty({text:msj,type:'error',layout:'topCenter',timeout:'3000'});
            return false;
        }
        else{}
    }
    else if( id_e == 9 ){
        var msj = '';
        //estres ansiedad
        if( $("#fecha").val().trim() === "" ){
            msj += 'El campo fecha no debe estar vacío <br>';
        }
        else if( $("#tipo_emo").val().trim() === "" ){
            msj += 'Selecciona el Tipo<br>';
        }
        else if( $("#respiracion").prop("checked") == true && $("#repeticiones").length > 0 && ( $("#repeticiones").val().trim() === "" || parseInt($("#repeticiones").val().trim()) < 2 ) ){
            msj += 'Ingresa un valor válido en el campo Repeticiones<br>';
        }
        else if( $("#respiracion").prop("checked") == true && $("#inhalacion").length > 0 && ( $("#inhalacion").val().trim() === "" || parseInt($("#inhalacion").val().trim()) < 4 ) ){
            msj += 'Ingresa un valor válido en el campo Inhalación<br>';
        }
        else if( $("#respiracion").prop("checked") == true && $("#sostenimiento").length > 0 && ( $("#sostenimiento").val().trim() === "" || parseInt($("#sostenimiento").val().trim()) < 4 ) ){
            msj += 'Ingresa un valor válido en el campo Sostenimiento<br>';
        }
        else if( $("#respiracion").prop("checked") == true && $("#exhalacion").length > 0 && ( $("#exhalacion").val().trim() === "" || parseInt($("#exhalacion").val().trim()) < 4 ) ){
            msj += 'Ingresa un valor válido en el campo Exhalación<br>';
        }
        else if( $("#respiracion").prop("checked") == true && $("#descanso").length > 0 && ( $("#descanso").val().trim() === "" || parseInt($("#descanso").val().trim()) < 4 ) ){
            msj += 'Ingresa un valor válido en el campo Descanso<br>';
        }
        else if( $("#array_escenarios_edit").val().trim() === "" ){
            msj += 'Debes agregar al menos 2 Escenarios <br>';
        }
        else{}
        if( $.trim(msj) !== "" ){
            manageLoading(0);
            noty({text:msj,type:'error',layout:'topCenter',timeout:'3000'});
            return false;
        }
        else{}
    }
    else if( id_e == 10 ){
        var msj = '';
        //tamo
        if( $("#fecha").val().trim() === "" ){
            msj += 'El campo fecha no debe estar vacío <br>';
        }
        else if( $("#repeticiones").val().trim() === "" || parseInt( $("#repeticiones").val().trim() ) < 2 ){
            msj += 'Ingresa un valor válido en el campo Repeticiones<br>';
        }
        else if( $("#inhalacion").val().trim() === "" || parseInt( $("#inhalacion").val().trim() ) < 1 ){
            msj += 'Ingresa un valor válido en el campo Tiempo de Inhalación<br>';
        }
        else if( $("#mantener").val().trim() === "" || parseInt( $("#mantener").val().trim() ) < 1 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Mantener Respiración<br>';
        }
        else if( $("#exhalacion").val().trim() === "" || parseInt( $("#exhalacion").val().trim() ) < 1 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Exhalación<br>';
        }
        else if( $("#descanso").val().trim() === "" || parseInt( $("#descanso").val().trim() ) < 1 ){
            msj += 'Ingresa un valor válido en el campo Tiempo Descanso<br>';
        }
        else{}
        if( $.trim(msj) !== "" ){
            manageLoading(0);
            noty({text:msj,type:'error',layout:'topCenter',timeout:'3000'});
            return false;
        }
        else{}
    }
    else{}

    $.post(base_url+"perfil/act_edt_ejercicio",$('#dosificacionEdit').serialize(),function(resultado){
        
        noty({
            text: resultado.msg,
            type: 'success',
            layout: 'topCenter',
            timeout: '3000'
        });
        $("#btnSesiones").trigger("click");
        manageLoading(0);
    },"json");
});

$('#eliminar_dosificacion').click(function(e) {
    $("#modalDelSesion").modal();
});

$('#eliminar_dosificacion_confirm').click(function(e) {
    manageLoading(1);
    e.preventDefault();
    var sendObj={id_sesion:$("#id_sesion").val(),tipo:$("#id_ejercicio_edit").val()};
    $.post(base_url+"perfil/delEjercicio",sendObj,function(resultado){
        noty({
            text: resultado.msg,
            type: 'success',
            layout: 'topCenter',
            timeout: '3000'
        });
        $("#btnSesiones").trigger("click");
        manageLoading(0);
        $("#modalEditaSesion").modal('hide');
        $("#modalDelSesion").modal('hide');
    },"json");
});

$("#tipo2").change(function () {

    $("#consulta_sesiones").attr("data-tipo",$(this).val());
});

$("#consulta_sesiones").click(function () {
    var tiempo_plataforma = 0;
    var canti_movimientos = 0;
    var ejercicios_completados = 0;
    var ejercicios_no_completados = 0;
    var ejercicios_dosificados = 0;
    var data_anio_meses = [0,0,0,0,0,0,0,0,0,0,0,0];
    $("#tabla-detalle").hide();
    var id=parseInt($(this).attr("data-tipo"));


    if (id<1){
        noty({
            text: "Debe elegir un tipo de ejercicio",
            type: 'error',
            layout: 'topCenter',
            timeout: '3000'
        });
        return false;
    }
    else if( id < 7 || id > 8 ){

        noty({
            text: "Actualmente no disponible",
            type: 'error',
            layout: 'topCenter',
            timeout: '3000'
        });
        return false;
    }
    else{}
    var sendObj={
        id_ejer:id,
        id_paciente:$("#id_paciente_est").val(),
        desde:$("#desde").val(),
        hasta:$("#hasta").val()
    }
    manageLoading(1);
    $.post(base_url+"perfil/trae_estadisticas",sendObj,function(response){

        if( response.data.length > 0 ){

            var colors = ['progress-bar-success', 'progress-bar-danger', 'progress-bar-info', 'progress-bar-warning'];
            
            $("#div_estadisticas").empty().html( $.trim( response.pagina ) ).show();

            var porcentaje=0,cadH="",cadB="";
            
            var cad='';

            switch(id){
                case 1:
                break;
                case 2:
                break;
                case 3:
                break;
                case 4:
                  cadH+='<th class="text-right" style="width:80px">Fecha</th><th class="text-right">Manzanas'+
                  '</th><th class="text-right">% acierto</th><th class="text-r'+
                  'ight">Nivel</th><th class="text-right">Resultado</th>';
                break;
                case 5:
                    cadH+='<th class="text-right" style="width:80px">Fecha</th><th class="text-right">Peces'+
                    '</th><th class="text-right">% acierto</th><th class="text-right">Resultado</th>';
                break;
                case 6:
                    cadH+='<th class="text-right" style="width:80px">Fecha</th><th class="text-right">Agarrados'+
                    '</th><th class="text-right">% acierto</th><th class="text-right">Resultado</th>';
                break;
                case 7:
                    ejercicios_completados = parseInt( response.data.length );
                    ejercicios_dosificados = parseInt( response.data_all.length );
                    ejercicios_no_completados = ejercicios_dosificados - ejercicios_completados;

                    
                    for( var y = 0; y < response.data_anio_meses.length; y++ ){

                        var axx = parseInt( response.data_anio_meses[y].mes )-1; 
                        data_anio_meses[axx] = data_anio_meses[axx]+1;
                    }/**/

                break;
                case 8:
                    ejercicios_completados = parseInt( response.data.length );
                    ejercicios_dosificados = parseInt( response.data_all.length );
                    ejercicios_no_completados = ejercicios_dosificados - ejercicios_completados;

                    //console.log( response.data_anio_meses );
                    
                    for( var y = 0; y < response.data_anio_meses.length; y++ ){

                        var axx = parseInt( response.data_anio_meses[y].mes )-1; 
                        data_anio_meses[axx] = data_anio_meses[axx]+1;
                    }

                break;
            }
            $.each(response.data,function(index, value){
                //porcentaje+=parseFloat(value.promedio);
                
                if( id == 7 ){

                    var tiro =  ( value.tiros != null ) ? $.parseJSON( value.tiros ) : '{}';
                    
                    if( tiro != '{}' ){
                        $.each(tiro.tiros[0].turno,function(ind, valor){

                            var aux = parseFloat( tiempo_plataforma + parseFloat( valor.datas[0].d ) );
                            
                            tiempo_plataforma = parseFloat( tiempo_plataforma) + parseFloat( valor.datas[0].d );

                            canti_movimientos = parseInt( canti_movimientos + parseInt( valor.stats.length ) );

                            for( var x = 0; x < valor.stats.length; x++ ){

                                var au = parseFloat( tiempo_plataforma + parseFloat( valor.stats[x] ) );
                                
                                tiempo_plataforma = parseFloat( tiempo_plataforma + parseFloat( valor.stats[x] ) );
                            }
                        });
                    }
                    else{}
                    /**/
                }
                else if( id == 8 ){ 
                    //cuello_cabeza
                    var tiro =  $.parseJSON( value.tiros );
                    $.each(tiro.tiros[0].turno,function(ind, valor){

                        var aux = parseFloat( tiempo_plataforma + parseFloat( valor.datas[0].d ) );
                        
                        tiempo_plataforma = parseFloat( tiempo_plataforma) + parseFloat( valor.datas[0].d );

                        canti_movimientos = parseInt( canti_movimientos + parseInt( valor.stats.length ) );

                        for( var x = 0; x < valor.stats.length; x++ ){

                            var au = parseFloat( tiempo_plataforma + parseFloat( valor.stats[x] ) );
                            
                            tiempo_plataforma = parseFloat( tiempo_plataforma + parseFloat( valor.stats[x] ) );
                        }
                    });
                }else{}

                switch(id){
                    case 1:
                    break;
                    case 2:
                    break;
                    case 3:
                    break;
                    case 4:
                      cadB+='<tr onclick="trae_detalle_ejercicio('+value.id_ejercicio+','+id+');" style="cursor:pointer;">'+
                      '<td class="text-right">'+value.fecha+'</td><td class="text-right">'+value.cantidad+
                      '</td><td class="text-right">'+value.promedio+'</td><td class="text-right">'+
                      value.nivel_final+'</td><td class="text-right">';
                      if(value.gano==0){cadB+='Perdi&oacute;</td></tr>';}
                      else{cadB+='Gan&oacute;</td></tr>';}
                    break;
                    case 5:
                        cadB+='<tr onclick="trae_detalle_ejercicio('+value.id_ejercicio+','+id+');" style="cursor:pointer;">'+
                        '<td class="text-right">'+value.fecha+'</td><td class="text-right">'+value.cantidad+
                        '</td><td class="text-right">'+value.promedio+'</td><td class="text-right">';
                        if(value.gano==0){cadB+='Perdi&oacute;</td></tr>';}
                        else{cadB+='Gan&oacute;</td></tr>';}
                    break;
                    case 6:
                        cadB+='<tr onclick="trae_detalle_ejercicio('+value.id_ejercicio+','+id+');" style="cursor:pointer;">'+
                        '<td class="text-right">'+value.fecha+'</td><td class="text-right">'+value.cantidad+
                        '</td><td class="text-right">'+value.promedio+'</td><td class="text-right">';
                        if(value.gano==0){cadB+='Perdi&oacute;</td></tr>';}
                        else{cadB+='Gan&oacute;</td></tr>';}
                    break;
                    case 7:
                        cadB+='<tr>'+
                            '<td class="text-right">'+
                                '<a style="cursor:pointer;float:left;margin-left:40px;" onclick="trae_detalle_ejercicio(parseInt('+value.id_ejercicio+'),'+id+');" class="btn btn-primary">Ver</a>'+value.fecha+
                            '</td>'+
                            '<td style="position: relative;">'+
                                '<div class="progress progress-sm inline-block m-bottom-none" style="width:90%;margin-top:6px;background-color: #D9E4E8;border-radius:10px;">'+
                                '<div class="progress-bar '+colors[ Math.floor((Math.random() * 3) ) ]+'" style="width: '+parseFloat(value.promedio)+'%;">'+
                                    '<span class="sr-only">'+value.promedio+'% Completado</span></div>'+
                                '</div>'+
                            '</td>'+
                            '<td class="text-right">'+value.promedio+'%</td>'+
                            '</tr>';
                        $("#div_tiempo_plataforma").text( convertir( parseInt(tiempo_plataforma) ) ).css({'font-weight': 'bold'});
                        $("#div_canti_movimientos").text( canti_movimientos ).css({'font-weight': 'bold'});
                        $("#div_ejercicios_completados").text( ejercicios_completados ).css({'font-weight': 'bold'});
                        /**/
                        
                    break;
                    case 8:
                        cadB+='<tr>'+
                            '<td class="text-right">'+
                                '<a style="cursor:pointer;float:left;margin-left:40px;" onclick="trae_detalle_ejercicio(parseInt('+value.id_ejercicio+'),'+id+');" class="btn btn-primary">Ver</a>'+value.fecha+
                            '</td>'+
                            '<td style="position: relative;">'+
                                '<div class="progress progress-sm inline-block m-bottom-none" style="width:90%;margin-top:6px;background-color: #D9E4E8;border-radius:10px;">'+
                                '<div class="progress-bar '+colors[ Math.floor((Math.random() * 3) ) ]+'" style="width: '+parseFloat(value.promedio)+'%;">'+
                                    '<span class="sr-only">'+value.promedio+'% Completado</span></div>'+
                                '</div>'+
                            '</td>'+
                            '<td class="text-right">'+value.promedio+'%</td>'+
                            '</tr>';
                        $("#div_tiempo_plataforma").text( convertir( parseInt(tiempo_plataforma) ) ).css({'font-weight': 'bold'});
                        $("#div_canti_movimientos").text( canti_movimientos ).css({'font-weight': 'bold'});
                        $("#div_ejercicios_completados").text( ejercicios_completados ).css({'font-weight': 'bold'});
            
                    break;
                }
            });

            if( id == 7 ){

                $("#resultados_puente tbody").empty().html(cadB);
                $('#resultados_puente').dataTable();

                cargarGraficoPie( ejercicios_dosificados, ejercicios_completados, ejercicios_no_completados );
                cargarGraficoLine( data_anio_meses );
                /**/
            }
            else if( id == 8 ){
                $("#resultados_cuello tbody").empty().html(cadB);
                $('#resultados_cuello').dataTable();

                cargarGraficoPie( ejercicios_dosificados, ejercicios_completados, ejercicios_no_completados );
                cargarGraficoLine( data_anio_meses );
            }
            else{

            }
        }
        else{
            //no hay resultados
            noty({
                text: "No se han encontrado resultados en el rango de fecha seleccionado",
                type: 'error',
                layout: 'topCenter',
                timeout: '3000'
            });
        }


        manageLoading(0);
    },"json");
});

function trae_detalle_ejercicio(id_sesion,tipo){
    manageLoading(1);
    $.post(base_url+"perfil/trae_detalle_ejercicio",{id_ejercicio:id_sesion,tipo:tipo},function(response){
        
        var res=$.parseJSON(response.tiros);
        
        var cadH="",cadB="",i=1;
        var cad='';
        var tiempo_plataforma = 0;

        switch(tipo){
            case 1:
            break;
            case 2:
            break;
            case 3:
            break;
            case 4:
                cadH+='<th class="text-right">Nivel</th><th class="text-right">Acierto'+
                '</th><th></th>';
            break;
            case 5:
                cadH+='<th class="text-right">Nivel</th><th class="text-right">Acierto'+
                '</th><th></th>';
            break;

            case 6:
                cadH+='<th class="text-right">Nivel</th><th class="text-right">Acierto'+
                '</th><th></th>';
            break;
            case 7:
                cadH+='<th class="text-right">Tiempo</th><th class="text-right">Angulo logrado'+
                '</th><th>Angulo deseado</th><th>Logrado?</th><th></th>';
            break;
            case 8:
                cadH+='<th class="text-right">Tiempo</th><th class="text-right">Angulo logrado'+
                '</th><th>Angulo deseado</th><th>Logrado?</th><th></th>';
            break;
        }
                
        $.each(res.tiros[0].turno,function(index, value){

            switch(tipo){
                case 1:
                break;
                case 2:
                break;
                case 3:
                break;
                case 4: break;
                case 5:break;
                case 6:break;
                case 7:
                    cadB += '<tr><td class="text-right">'+value.stats[0]+'</td><td class="text-right">'+
                    value.stats[1]+'</td><td class="text-right">'+value.stats[2]+'</td><td class="text-right">';
                    if(value.stats[3]==0){cadB+='No</td>';}
                    else{cadB+='Si</td>';}
                    cadB+='<td class="text-right"><a onclick="ver_movimiento_puente(this)" class="btn btn-primary" data-puntos=\''+
                    JSON.stringify(value.datas)+'\'>Ver</a></td></tr>';

                    //console.log(JSON.stringify(value.datas));
                    $("#tabla_detalle_puente thead tr").html(cadH);
                    $("#tabla_detalle_puente tbody").html(cadB);
                    $('#tabla_detalle_puente').dataTable();
                    $("#div_tabla_detalle").show();
                break;
                case 8:
                    cadB += '<tr><td class="text-right">'+value.stats[0]+'</td><td class="text-right">'+
                    value.stats[1]+'</td><td class="text-right">'+value.stats[2]+'</td><td class="text-right">';
                    if(value.stats[3]==0){cadB+='No</td>';}
                    else{cadB+='Si</td>';}
                    cadB+='<td class="text-right"><a onclick="ver_movimiento(this)" class="btn btn-primary" data-puntos=\''+
                    JSON.stringify(value.datas)+'\'>Ver</a></td></tr>';

                    //console.log(JSON.stringify(value.datas));
                    $("#tabla_detalle_cuello thead tr").html(cadH);
                    $("#tabla_detalle_cuello tbody").html(cadB);
                    $('#tabla_detalle_cuello').dataTable();
                    $("#div_tabla_detalle").show();

                break;
            }
            i++;
        });

        /**/
        manageLoading(0);
    },"json");
    
}
function ver_movimiento(elemento){
    
    noty({
        text: 'Espera unos segundos mientras se carga el simulador',
        type: 'success',
        layout: 'center',
        timeout: '1500'
    });
    $("#puntos").val($(elemento).attr("data-puntos"));

    setTimeout(function(e){
        sendDatosSimuladorCabeza($("#puntos").val());
    }, 1500);
}

function ver_movimiento_puente(elemento){
    
    noty({
        text: 'Espera unos segundos mientras se carga el simulador',
        type: 'success',
        layout: 'center',
        timeout: '1500'
    });

    $("#puntos").val($(elemento).attr("data-puntos"));
    setTimeout(function(e){
        sendDatosSimuladorBrazo($(elemento).attr("data-puntos"));
    }, 1500);

}

$("#dosificacion").on("change","#tipo_guia",function () {
    var padre=$("#tipo_guia").parent().parent();
    if ($("#tipo_guia").val()==="2") {
        padre.after('<div class="form-group" id="guiado_div"><label class="col-sm-3 control-label">Puntos</label>'+
        '<div class="col-sm-9"><input class="form-control" name="guiado" id="guiado" type="text" placeholder='+
        '"Separe con punto (.) en donde apareceran los peces"><img src="'+base_url+'public/images/puntos.jpg"/></div></div>');
    }else{
        $("#guiado_div").remove();
    }
});

function filtrarcliente(){
    var cp = $("#combo-search-paciente").val();
    if(cp==1){
            var f = $("#input-search").val().toLowerCase().trim();
            $("#cont-list-clientes li a").each(function(){
                    var f2 = $(this).text().toLowerCase().trim();
                    if(f2.indexOf(f)===-1){
                            $(this).parents("li").hide();
                    }else{
                            $(this).parents("li").show();
                    }
            });
    }else if(cp==2){
            var f = $("#input-search").val().toLowerCase().trim();
            $("#cont-list-clientes li a").each(function(){
                    var f2 = String($(this).data("dni")).toLowerCase().trim();
                    if(f2.indexOf(f)===-1){
                            $(this).parents("li").hide();
                    }else{
                            $(this).parents("li").show();
                    }
            });
    }else{
            var f = $("#input-search").val().toLowerCase().trim();
            $("#cont-list-clientes li a").each(function(){
                    var f2 = String($(this).data("correo")).toLowerCase().trim();
                    if(f2.indexOf(f)===-1){
                            $(this).parents("li").hide();
                    }else{
                            $(this).parents("li").show();
                    }
            });
    }
}

function modalmapapaciente(id) {
    $.post(base_url+'administrador/selectperfil',{id:id},function(data){
            if(data.status){
                    $("#modalmapapaciente").modal("show");
                    setTimeout(function(){
                        if(data.data.online==1){
                            $("#modalmapapaciente .modal-title").html("Conectado");
                        }else{
                            $("#modalmapapaciente .modal-title").html("Ultima conexión: "+data.data.ultima);
                        }
                            var myLatlng = new google.maps.LatLng(data.data.latitud,data.data.longitud);
                            var mapOptions = {
                                    zoom: 14,
                                center: myLatlng
                            };
                            map = new google.maps.Map(document.getElementById('mapaVisor'), mapOptions);
                            var marker = new google.maps.Marker({
                                    position: myLatlng,
                                    map: map
                            });
                    },2000);
            }else{
                noty({
                    text: data.msg,
                    type: 'error',
                    layout: 'center',
                    timeout: '3000'
                });
            }
    },'json');
}



