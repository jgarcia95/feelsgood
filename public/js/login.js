$(document).ready(function(){
	function manageLoading(type){
	    if(type===1){
	        $("#loading").show(500);
	    }else{
	        $("#loading").hide(500);
	    }
	}
	$("#btn-login").click(function(){
		if($("#email").val()!="" && $("#pass").val()!=""){
			manageLoading(1);
			var SendObj = {
		        "email": $("#email").val(),
		        "pass": $("#pass").val()
		    };
			$.post("login/logear",SendObj,function(response){
				manageLoading(0);
				if(response.status==true){
					switch(response.rol){
						case "1":$(location).attr("href","administrador");break;
						case "2":$(location).attr("href","doctor");break;
						case "3":$(location).attr("href","paciente");break;
						case "4":$(location).attr("href","clinica");break;
					}
				}else{
					$("#messagemodal").html(response.msg);
					$("#modalMessage").modal("show");
				}
			},"json");
		}
	});
	
});
function validar(e) {
  var tecla=(document.all) ? e.keyCode : e.which;
  if (tecla==13){$("#btn-login").trigger("click");}
}