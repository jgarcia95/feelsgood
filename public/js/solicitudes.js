$('#lista-solicitudes-doctores').dataTable();
$('#lista-solicitudes-clinicas').dataTable();
$('#lista-solicitudes-pacientes').dataTable();
$(".openModalSolicitud").click(function(e){
	e.preventDefault();
	$("#id_solicitud").val($(this).data("id"));
	$("#tipo_solicitud").val($(this).data("tipo"));
	$("#perfil_solicitud").val($(this).data("perfil"));
   });
$(".procesar_solicitud").click(function(e){
	manageLoading(1);
	var datos={
		"id_solicitud":$("#id_solicitud").val(),
		"tipo_solicitud":$("#tipo_solicitud").val(),
		"perfil_solicitud":$("#perfil_solicitud").val()
	};
	$.post(base_url+'solicitudes/procesar',datos,function(response){
		$("#close_yesmodal").trigger("click");
		$("#close_nomodal").trigger("click");
		if(response.status){
			noty({
                text: response.msg,
		        type: 'success',
		        layout: 'topCenter',
		        timeout: '3000'
		    });
		    setTimeout(function(){location.reload();},1000);
		}else{
			noty({
                text: response.msg,
		        type: 'error',
		        layout: 'topCenter',
		        timeout: '3000'
		    });
		    manageLoading(1);
        }
    },'json');
});