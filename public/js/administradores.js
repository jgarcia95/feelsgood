if ($('#lista-administradores').length>0) {
	$('#lista-administradores').dataTable();
	$(".deldata").click(function(e){
		e.preventDefault();
		$("#id_administrador_eliminar").val($(this).data("id"));
    });
	$("#eliminar_administrador").click(function(e){
		manageLoading(1);
		$.post(base_url+'administrador/delete',{id:$("#id_administrador_eliminar").val()},function(response){
			manageLoading(0);
			$("#close_admin_del").trigger("click");
			if(response.status){
				noty({
                	text: response.msg,
		            type: 'success',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
		        setTimeout(function(){location.reload();},1000);
			}else{
				noty({
                	text: response.msg,
		            type: 'error',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }
        },'json');
	});
}
if ($('#form-edit-admin').length>0) {
	$('#form-edit-admin').parsley( { listeners: {
		onFormSubmit: function ( isFormValid, event ) {
			return false;
		}
	}});
	$("#form-edit-admin").submit(function(e){
		manageLoading(1);
		e.preventDefault();
        $.post(base_url+'administrador/update',$("#form-edit-admin").serialize(),function(response){
        	manageLoading(0);
            if(response.status){
                noty({
                	text: response.msg,
		            type: 'success',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }else{
                noty({
                	text: response.msg,
		            type: 'error',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }
        },'json');
    });
}
if ($('#form-add-admin').length>0) {
	$('#form-add-admin').parsley( { listeners: {
		onFormSubmit: function ( isFormValid, event ) {
			return false;
		}
	}});
	$("#form-add-admin").submit(function(e){
		manageLoading(1);
        e.preventDefault();
        $.post(base_url+'administrador/add',$("#form-add-admin").serialize(),function(response){
        	manageLoading(0);
            if(response.status){
                noty({
                	text: response.msg,
		            type: 'success',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }else{
                noty({
                	text: response.msg,
		            type: 'error',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }
        },'json');
    });
}