if ($('#lista-clinicas').length>0) {
	$('#lista-clinicas').dataTable({"bSort": false});
	$(".deldata").click(function(e){
		e.preventDefault();
		$("#id_clinica_eliminar").val($(this).data("id"));
    });
	$("#eliminar_clinica").click(function(e){
		manageLoading(1);
		$.post(base_url+'administrador/deleteClinica',{id:$("#id_clinica_eliminar").val()},function(response){
			manageLoading(0);
			$("#close_clinica_del").trigger("click");
			if(response.status){
				noty({
                	text: response.msg,
		            type: 'success',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
		        setTimeout(function(){location.reload();},1000);
			}else{
				noty({
                	text: response.msg,
		            type: 'error',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }
        },'json');
	});
}
if($('.select2').length>0){
	$('.select2').select2();
	$('#paisSolicitud').change(function(e) {
		manageLoading(1);
		$.post(base_url+"login/getProvincias",{id:$("#paisSolicitud").val()},function(response){
			manageLoading(0);
			var cad='<option value="0">-- Seleccionar provincia --</option>';
	        $.each(response,function(index, value){
	            cad+='<option value="'+value.ProvinCod+'">'+value.ProvinNom+'</option>';
	        });
	        $("#provinciaSolicitud").html(cad);
		},"json");
	});
	$('#provinciaSolicitud').change(function(e) {
		manageLoading(1);
		$.post(base_url+"login/getLocalidades",{id:$("#provinciaSolicitud").val()},function(response){
			manageLoading(0);
			var cad='<option value="0">-- Seleccionar localidad --</option>';
	        $.each(response,function(index, value){
	            cad+='<option value="'+value.LocalidCod+'">'+value.LocalidNom+'</option>';
	        });
	        $("#localidadSolicitud").html(cad);
		},"json");
	});
}
if ($('#form-edit-clinica').length>0) {
	$('#form-edit-clinica').parsley( { listeners: {
		onFormSubmit: function ( isFormValid, event ) {
			return false;
		}
	}});
	$("#form-edit-clinica").submit(function(e){
		manageLoading(1);
		e.preventDefault();
        $.post(base_url+'administrador/editClinicaAction',$("#form-edit-clinica").serialize(),function(response){
        	manageLoading(0);
            if(response.status){
                noty({
                	text: response.msg,
		            type: 'success',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }else{
                noty({
                	text: response.msg,
		            type: 'error',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }
        },'json');
    });
}
if ($('#form-add-clinica').length>0) {
	$('#form-add-clinica').parsley( { listeners: {
		onFormSubmit: function ( isFormValid, event ) {
			return false;
		}
	}});
	$("#form-add-clinica").submit(function(e){
		manageLoading(1);
        e.preventDefault();
        $.post(base_url+'administrador/addClinicaAction',$("#form-add-clinica").serialize(),function(response){
        	manageLoading(0);
            if(response.status){
                noty({
                	text: response.msg,
		            type: 'success',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }else{
                noty({
                	text: response.msg,
		            type: 'error',
		            layout: 'topCenter',
		            timeout: '3000'
		        });
            }
        },'json');
    });
}