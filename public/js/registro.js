$(document).ready(function(){
	function manageLoading(type){
		if(type===1){
		    $("#loading").show(500);
		}else{
		    $("#loading").hide(500);
		}
	}
	$('.select2').select2();
	if($("#fileImagen").length){
		Dropzone.autoDiscover = false;
		var myDropzone = new Dropzone("div#fileImagen", { 
			url: base_url+"login/subirFoto",
			dictDefaultMessage: "Arrastra o da click para subir tu foto",
			maxFiles: 1,
			acceptedFiles: 'image/jpeg, image/jpg, image/png',
			addRemoveLinks: true,
			accept: function(file, done) {
			    done();
			},
			init: function() {
				this.on("success", function(file, response) {
					var datos=$.parseJSON(response);
		            $("#renombrado").val(datos.renombrado);
		        });
			    this.on("addedfile", function() {
				    if (this.files[1]!=null){
				    	var archivo=$("#renombrado").val();
				        this.removeFile(this.files[0]);
				        $.ajax({
			                type: 'POST',
			                url: base_url+'login/eliminarFoto',
			                data: "file_name="+archivo,
			                dataType: 'html'
			            });
				    }
			    });
		    }
		});
	}
	if($(".datepicker-input").length){
		$('.datepicker-input').datetimepicker({
			pickTime: false,format:'DD-MM-YYYY'
		});
	}
	$('div.dz-default.dz-message > span').show(); // Show message span
	$('div.dz-default.dz-message').css({'opacity':1, 'background-image': 'none'});
	$('#form-registro-doctor').parsley( { listeners: {
		onFormSubmit: function ( isFormValid, event ) {
			return false;	
		}
	}});
	$('#form-registro-doctor').submit(function(e) {
		e.preventDefault();
		manageLoading(1);
		var numClinicas = $("#clinicas :selected").select2('data');
		
		var clinicas='';
		for (var i = 0; i < numClinicas.length; i++) {
			clinicas += numClinicas[i]["value"]+",";
		};
		clinicas = (clinicas=="")? "" : clinicas.substr(0,clinicas.length-1);
		if ( $(this).parsley().isValid() ) {
			$.post(base_url+"login/addSolicitudDoctor",$('#form-registro-doctor').serialize()+"&clinicas="+clinicas,function(response){
				manageLoading(0);
				if(response.status==true){
					noty({
		            	text: response.msg,
		            	type: 'success',
		            	layout: 'topCenter',
		            	timeout: '3000'
		          	});
					setTimeout(function(){location.href="solicitud-enviada"},2000);
				}else{
					noty({
		            	text: response.msg,
		            	type: 'error',
		            	layout: 'topCenter',
		            	timeout: '3000'
		          	});
				}
			},"json");
		}
	});
	$('#form-registro-paciente').parsley( { listeners: {
		onFormSubmit: function ( isFormValid, event ) {
			console.log("asdad");
			return false;
		}
	}});
	$('#form-registro-paciente').submit(function(e) {
		e.preventDefault();
		manageLoading(1);
		var idFonoParley=$("#fono").attr("data-parsley-id");
		var idEmailParley=$("#userSolicitud").attr("data-parsley-id");
		if($("#userSolicitud").val()==""){
			if($("#fono").val()==""){
				$("#parsley-id-"+idFonoParley).addClass("filled");
				$("#parsley-id-"+idFonoParley).html('<li class="parsley-required">Ingrese e-mail o teléfono.</li>');
				$("#parsley-id-"+idEmailParley).addClass("filled");
				$("#parsley-id-"+idEmailParley).html('<li class="parsley-required">Ingrese e-mail o teléfono.</li>');
				manageLoading(0);
				return false;
			}
		}
		if($("#fono").val()==""){
			if($("#userSolicitud").val()==""){
				$("#parsley-id-"+idFonoParley).addClass("filled");
				$("#parsley-id-"+idFonoParley).html('<li class="parsley-required">Ingrese e-mail o teléfono.</li>');
				$("#parsley-id-"+idEmailParley).addClass("filled");
				$("#parsley-id-"+idEmailParley).html('<li class="parsley-required">Ingrese e-mail o teléfono.</li>');
				manageLoading(0);
				return false;
			}
		}
		if ( $(this).parsley().isValid() ) {
			$.post(base_url+"login/addSolicitudPaciente",$('#form-registro-paciente').serialize(),function(response){
				manageLoading(0);
				if(response.status==true){
					noty({
		            	text: response.msg,
		            	type: 'success',
		            	layout: 'topCenter',
		            	timeout: '3000'
		          	});
		          	setTimeout(function(){location.href="solicitud-enviada"},2000);
				}else{
					noty({
		            	text: response.msg,
		            	type: 'error',
		            	layout: 'topCenter',
		            	timeout: '3000'
		          	});
				}
			},"json");
		}
	});

	$("#agregar_clinica2").click(function (e){
		e.preventDefault();
		if ($('#form-registro-doctor').length>0) {
			$('#clinicas').append($('<option>', {
				value: "nuevo-"+$('#nueva_clinica').val(),
				text: $('#nueva_clinica').val()
			}));
			var act = $('#clinicas').val();
			if(act == null){
				act = "nuevo-"+$('#nueva_clinica').val();
			}else{
				act.push("nuevo-"+$('#nueva_clinica').val());
			}
			$('#clinicas').val(act).trigger("change");
		}
	});

	$("#img-edit").click(function(){
		$("#c-edit-clinica").show();
	});

	$('#form-registro-clinica').parsley( { listeners: {
		onFormSubmit: function ( isFormValid, event ) {
			return false;	
		}
	}});

	$('#form-registro-clinica').submit(function(e) {
		e.preventDefault();
		manageLoading(1);
		if ( $(this).parsley().isValid() ) {
			$.post(base_url+"login/addSolicitudClinica",$('#form-registro-clinica').serialize(),function(response){
				manageLoading(0);
				if(response.status==true){
					noty({
		            	text: response.msg,
		            	type: 'success',
		            	layout: 'topCenter',
		            	timeout: '3000'
		          	});
		          	setTimeout(function(){location.href="solicitud-enviada"},2000);
				}else{
					noty({
		            	text: response.msg,
		            	type: 'error',
		            	layout: 'topCenter',
		            	timeout: '3000'
		          	});
				}
			},"json");
		}
	});

	$('#paisSolicitud').change(function(e) {
		manageLoading(1);
		$.post(base_url+"login/getProvincias",{id:$("#paisSolicitud").val()},function(response){
			var cad='<option value="0">-- Seleccionar provincia --</option>';
            $.each(response,function(index, value){
                cad+='<option value="'+value.ProvinCod+'">'+value.ProvinNom+'</option>';
            });
            $("#provinciaSolicitud").html(cad);
            manageLoading(0);
		},"json");
	});

	$('#provinciaSolicitud').change(function(e) {
		manageLoading(1);
		$.post(base_url+"login/getLocalidades",{id:$("#provinciaSolicitud").val()},function(response){
			var cad='<option value="0">-- Seleccionar localidad --</option>';
            $.each(response,function(index, value){
                cad+='<option value="'+value.LocalidCod+'">'+value.LocalidNom+'</option>';
            });
            $("#localidadSolicitud").html(cad);
            manageLoading(0);
		},"json");
	});

	$("#agregar_clinica2p").click(function (e){
		e.preventDefault();
		$.post(base_url+"login/addClinica",{clinica:$('#nueva_clinicap').val()},function(response){
			if ($('#form-registro-paciente').length>0) {
				$('#clinica').append($('<option>', {
				    value: response.id,
				    text: response.valor
				}));
				$("#clinica").prop("disabled", true);
				$('#clinica').val(response.id).trigger("change");
				$("#nueva_clinica2p").val(response.valor);
				$("#img-edit").show();
				$("#c-add-clinica").hide();
				$("#content-clinica").css("margin-right","30px");
			}
			noty({
		        text: response.msg,
		        type: 'success',
		        layout: 'topCenter',
		        timeout: '3000'
		    });
		},"json");
	});

	$("#agregar_clinica3p").click(function (e){
		e.preventDefault();
		$.post(base_url+"login/editClinica",{clinica:$('#nueva_clinica2p').val(),id:$("#clinica").val()},function(response){
			if ($('#form-registro-paciente').length>0) {
				$('#clinica option[value='+$("#clinica").val()+']').html($('#nueva_clinica2p').val());
				$("#clinica").prop("disabled", true);
				$('#clinica').val($("#clinica").val()).trigger("change");
				$("#img-edit").show();
				$("#c-edit-clinica").hide();
			}
			noty({
		        text: response.msg,
		        type: 'success',
		        layout: 'topCenter',
		        timeout: '3000'
		    });
		},"json");
	});
});