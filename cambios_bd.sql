ALTER TABLE `ejercicio_tamo` ADD `promedio` VARCHAR(10) NULL AFTER `perrito`, ADD `cantidad` INT(3) NULL AFTER `promedio`, ADD `tiros` TEXT NULL AFTER `cantidad`, ADD `s` CHAR(1) NULL AFTER `tiros`;

CREATE TRIGGER `agregar_sex_tamo` BEFORE INSERT ON `ejercicio_tamo` FOR EACH ROW SET NEW.s = (SELECT CASE paciente.sexo WHEN 1 THEN 'm' WHEN 0 THEN 'f' END AS sexo FROM paciente WHERE paciente.id_paciente = NEW.id_paciente);

ALTER TABLE `ejercicio_respiracion` ADD `respiracion` INT(1) NOT NULL DEFAULT '0' AFTER `echado`;